import { username } from 'lite';

/**
 * Get the current loggedin users `username`.
 *
 * @returns {string} The `username` as a `string`.
 */
export default function getUsername() {
  if (process.env.ASP_ENV === 'development') return process.env.DEV_USERNAME;
  return username;
}

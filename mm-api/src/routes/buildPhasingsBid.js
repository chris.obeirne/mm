/* eslint-disable max-len */
/* eslint-disable no-unused-vars */
/* eslint-disable no-param-reassign */
import {
  generateId,
  send,
  get,
  insertInto,
  updateElseInsert,
  update,
  request,
  // username,
  select,
  getColumns,
  execute,
  deleteFrom,
} from 'lite';
const table = 'Web_JLC.dbo.MMF_PhasingsEditSummary';


export default {
  table,
  methods: ['GET'],
  preprocess({
    body, method, query, username, ...leftOvers
  }) {

    const execSql = `exec Web_JLC.dbo.buildPhasing_Data_Bid`;
    execute(execSql);
    const cntSQL = `SELECT [rowCnt]
  FROM [Web_JLC].[dbo].[MMF_Phasings_Data_Bid_RowCnt]`;

        const cnt = execute(cntSQL);
      

        send({
          msg: 'GtG',
          cnt: cnt[0] || {}.rowCnt,
        });
  },

};

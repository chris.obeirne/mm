import analytics from './analytics';
import phasings from './phasings';
import phasingsPatch from './phasingsPatch';
import phasingsPost from './phasingsPost';
import phasingsDelete from './phasingsDelete';
import phasingsSummed from './phasingsSummed';
import jluData from './jluData';
import jluDataPatch from './jluDataPatch';
import jluDataPost from './jluDataPost';
import changeLog from './changeLog';
import changeLogPost from './changeLogPost';
import userData from './userData';
import userDataPatch from './userDataPatch';
import jlus from './jlus';
import dsa from './dsa';
import fy from './fy';
import buildPhasingsBid from './buildPhasingsBid';
import existingEntry from './existingEntry';
import constants from './constants';
import user from './user';
import users from './users';
import settings from './settings';

/**
 * This is where the endpoint structure gets created.
 * For example the export `test` will become the endpoint `/test`.
 */
export default {
  analytics,
  jlus,
  phasings,
  phasingsPatch,
  phasingsPost,
  phasingsDelete,
  phasingsSummed,
  jluData,
  jluDataPatch,
  jluDataPost,
  changeLog,
  changeLogPost,
  userData,
  userDataPatch,
  existingEntry,
  settings,
  dsa,
  fy,
  buildPhasingsBid,
  constants,
  user,
  users,
};

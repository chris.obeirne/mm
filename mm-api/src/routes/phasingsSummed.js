/* eslint-disable max-len */
/* eslint-disable no-unused-vars */
/* eslint-disable no-param-reassign */
import {
  generateId,
  send,
  get,
  insertInto,
  updateElseInsert,
  update,
  request,
  // username,
  select,
  getColumns,
  execute,
  deleteFrom,
} from 'lite';
const table = 'Web_JLC.dbo.MMF_PhasingsSummed';


export default {
  table,
  methods: ['GET'],
  // columns: [{column: 'jluId', hide: true}],
  preprocess({
    body, method, query, username, ...leftOvers
  }) {
    if (method === 'GET') {
      const fySQL = (query.fy === null) ? ` (fy='2223') ` : ` (fy = '${query.fy}') `;
      const jluIdSQL = (query.jluId === null || query.jluId === undefined) ? '' : ` AND (jluId = '${query.jluId}') `;
      const dsaSQL = (query.dsa === null || query.dsa === undefined) ? '' : `AND (dsa = '${query.dsa}') `;
      const SQL = `SELECT 
        sum([bidSummed]) as bidSummed
        ,sum([authSummed]) as authSummed
        FROM [Web_JLC].[dbo].[MMF_PhasingsSummed]
      where ${fySQL} ${dsaSQL} ${jluIdSQL}`
     
   const sumd = execute(SQL);
   if (sumd !== undefined && sumd.length === 1) {
        send({
          bid: sumd[0].bidSummed,
          auth: sumd[0].authSummed,
          SQL,
        });
      } else {
        send({ 
          bid: 0,
          auth: 0,
          SQL,
          });
      }
     }
  },
};

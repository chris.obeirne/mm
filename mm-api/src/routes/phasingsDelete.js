/* eslint-disable max-len */
/* eslint-disable no-unused-vars */
/* eslint-disable no-param-reassign */
import {
  generateId,
  send,
  get,
  insertInto,
  updateElseInsert,
  update,

  select,
  getColumns,
  execute,
  deleteFrom,
} from 'lite';
const table = 'Web_JLC.dbo.MMF_PhasingsEdit';


export default {
  table,
  methods: ['POST'],
  preprocess(request) {
    if (request.method === 'POST') {
      const bid = request.body.toDelete;
      const sqlDelete = `DELETE FROM ${table} WHERE ID = '${bid}'`;
        execute(sqlDelete);
        send({
          sqlDelete
        });
    }

  },

};

/* eslint-disable max-len */
/* eslint-disable no-unused-vars */
/* eslint-disable no-param-reassign */
import {
  generateId,
  send,
  get,
  insertInto,
  updateElseInsert,
  update,
  request,
  // username,
  select,
  getColumns,
  execute,
  deleteFrom,
} from 'lite';

import { getUsername, getUserId } from '../utils/index.js';

const table = 'Web_JLC.dbo.MMF_JLUs';
// const j = 'JLU(SQ)';

export default {
  table,
  methods: ['PATCH'],
  // methods:['GET'],

  preprocess(request) {
    // if (query.getColumns) send('getColumns');
    // if (method === 'GET') {
    //   const pg = query.page;
    //   const ipp = query.itemsPerPage;
    //   const srt = query.sort;
    //   query.filter = [...(query.filter || [])];
    //   query.sort = srt;
    //   query.page = pg;
    //   query.itemsPerPage = ipp;
    //   return {
    //     method, query, username, ...leftOvers,
    //   };
    // }

if (request.method === 'PATCH') {
  const idv = getUserId();
      const b = {
        ...request.body,
        lastModified: new Date(),
        lastModifiedBy: idv,
      };
      request.body = b;
      const recId = request.body.jluId;
      request.filter = request.query.filter;

      // send({
      //   text: 'phasing updated', 
      //   recId,
      //   fil: request.filter,
      //   bdy: request.body,
      // });
      // return { method, query, username, ...leftOvers };
    }
  },

  // postprocess({ data, method, query }) {
  //   if (method === 'GET') {

  //     // send(request);
  //   }
  // },

};

/* eslint-disable max-len */
/* eslint-disable no-unused-vars */
/* eslint-disable no-param-reassign */
import {
  generateId,
  send,
  get,
  insertInto,
  updateElseInsert,
  update,
  request,
  // username,
  select,
  getColumns,
  execute,
  deleteFrom,
} from 'lite';

// import { getUsername, getUserId } from '../utils/index.js';

const table = 'Web_JLC.dbo.MMF_PhasingsEdit';
// const j = 'JLU(SQ)';

export default {
  table,
  methods: ['POST'],
  // methods:['GET'],

  preprocess(request) {
    if (request.method === 'POST') {
      request.body = request.body.map((obj) => ({
        ...obj,
        id: generateId(),
      }));

    }
  },

};


import {
  send,
} from 'lite';
import { 
  BUILD_DATE, 
} from '../constants.js';

const table = 'Web_JLC.dbo.MMF_MiscData';


export default {
  table,
  methods: ['GET'],
  preprocess({
    method, ...leftOvers
  }) {
    if (method === 'GET') {
      const bd = BUILD_DATE;
      send({
        build: bd,
        // test: 'Test'
      });
    }

  },

};

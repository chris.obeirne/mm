/* eslint-disable max-len */
/* eslint-disable no-unused-vars */
/* eslint-disable no-param-reassign */
import {
  generateId,
  send,
  get,
  insertInto,
  updateElseInsert,
  update,
  request,
  // username,
  select,
  getColumns,
  execute,
  deleteFrom,
} from 'lite';

// import { getUsername, getUserId } from '../utils/index.js';

const table = 'Web_JLC.dbo.MMF_PhasingsEdit';
// const j = 'JLU(SQ)';

export default {
  table,
  methods: ['POST'],
  // methods:['GET'],

  preprocess(request) {
    if (request.method === 'POST') {
      request.body = request.body.map((obj) => ({
        ...obj,
        id: generateId(),
      }));

      // const columns = getColumns('Web_JLC', 'LMM_PhasingsEdit2122') // gets all columns and meta from the SQL table
      //   .filter((o) => !['jluOrigBid', 'approvedBudget', 'variation'].includes(o.key)) // removes keys that are comuted columns (cannot post to these)
      //   .map(({ column, key }) => ({ column, key })); // Removes the other chaff that is meta data for each column

      // insertInto('Web_JLC', 'LMM_PhasingsEdit2122', body, { columns: query.columns }); // Inserts the data into the table - still data type issues to resolve

      // send breaks the API here so that implicit lite functionality does not get a chance to run
      // (because we are already run ning the query using insertInto)
      // send({
      //   body, query, leftOvers, columns,
      // });

      // const idv = generateId();
      // const dataArr = [];
      // const d = body;
      // const newI = {
      //   ...d,
      //   id: idv,
      // };

      // dataArr.push(newI);
      // body = dataArr
      //   .map((b) => ({
      //     ...b,
      //   }));
      // insertInto(process.env.DB_NAME, 'LMM_PhasingsEdit2122', dataArr);
      // send({
      //   txt: 'phasing updated',
      //   dataArr,
      // });
      // return { method, query, username, ...leftOvers };
    }
  },

};

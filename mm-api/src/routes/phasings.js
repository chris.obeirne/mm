/* eslint-disable max-len */
/* eslint-disable no-unused-vars */
/* eslint-disable no-param-reassign */
import {
  generateId,
  send,
  get,
  insertInto,
  updateElseInsert,
  update,
  request,
  // username,
  select,
  getColumns,
  execute,
  deleteFrom,
} from 'lite';
const table = 'Web_JLC.dbo.MMF_PhasingsEditSummary';


export default {
  table,
  methods: ['GET'],
  preprocess({
    body, method, query, username, ...leftOvers
  }) {
    if (method === 'GET') {
      const pg = query.page;
      const ipp = query.itemsPerPage;
      const srt = query.sort;
      query.filter = [...(query.filter || [])];
      query.sort = srt;
      query.page = pg;
      query.itemsPerPage = ipp;
    }

  },

};

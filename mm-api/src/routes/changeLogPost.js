/* eslint-disable max-len */
/* eslint-disable no-unused-vars */
/* eslint-disable no-param-reassign */
import {
  generateId,
  send,
  get,
  insertInto,
  updateElseInsert,
  update,
  request,
  // username,
  select,
  getColumns,
  execute,
  deleteFrom,
} from 'lite';

import { getUserId } from '../utils/index.js';

const table = 'Web_JLC.dbo.MMF_PhasingChangeLog';
// const j = 'JLU(SQ)';

export default {
  table,
  methods: ['POST'],
  // methods:['GET'],

  preprocess(request) {
    if (request.method === 'POST') {
      const cDate = new Date().toISOString().replace('T', ' ').slice(0, 19);
      request.body = request.body.map((obj) => ({
        ...obj,
        id: generateId(),
        entryDate: cDate,
        entryBy: getUserId(),
      }));

    }
  },

};

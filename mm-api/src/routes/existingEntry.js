import {
  generateId,
  send,
  get,
  insertInto,
  updateElseInsert,
  update,
  request,
  username,
  select,
  getColumns,
  execute,
  deleteFrom,
} from 'lite';

// import { getUsername, getUserId } from '../utils/index.js';

const table = 'Web_JLC.dbo.MMF_PhasingsEdit';
// const j = 'JLU(SQ)';

export default {
  table,
  methods: ['GET',],
  // methods:['GET'],

preprocess({ body, method, query, username, ...leftOvers }) {
    // if (query.getColumns) send('getColumns');
    if (method === 'GET') {
      const pg = query.page;
      const ipp = query.itemsPerPage;
      const srt = query.sort;
      query.filter = [ ...(query.filter || []), ];
      query.sort = srt;
      query.page = pg;
      query.itemsPerPage = ipp; 
      return { method, query, username, ...leftOvers };
    }
  },
postprocess({data, method, query}) {
    if (method === 'GET') {
      const columns = getColumns(query.database, query.table);
      data.meta.columns = columns;
      // send(request);
    } 
  },

};

// export default {
//   table,
//   methods: ['GET', 'POST', 'PATCH', 'DELETE'],

//   // preprocess hook runs third
//   // preprocess({ body, query, method }) {
//     // const tableLocation = `Web_JLC.dbo.${table}`
//     // if (method === 'PATCH') {
//     //   // send({ body });

//     //   if (body.belowCategories) {
//     //     let sql = '';
//     //     body.belowCategories.forEach((category) => {
//     //       sql = `${sql} UPDATE ${tableLocation}
//     //         SET Name = '${category.name}', Description = '${category.description}', Row_Order = ${category.rowOrder -  1}
//     //         WHERE ID = '${category.id}';`
//     //     })

//     //     execute(sql);
//     //     // send(sql);
//     //     send('update categories below deleted');
//     //   }
//     // }
//   // },

  
  
// }
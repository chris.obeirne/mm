import {
  isSchema,
  isString,
  isOptional,
  isValidNumber,
  isEveryValid,
  isBoolean,
  isLengthLessThan,
  isEvery,
} from 'lite';

/**
 * The endpoint will become `/analytics` based on the file name and export
 * from `index.js`.
 *
 * You will need to set the `original-url-override` request header to
 * `/analytics` to request from this endpoint.
 */
export default {
  table: 'Web_JLC.dbo.Test_Analytics',
  methods: ['POST'],
  validate: isSchema({
    body: isEvery(isSchema({
      /* Track the browser type when the event occurred. */
      userAgent: isString,
      /* The full URL or path (including the query string) on the browser at the
      time when the event occurred. */
      url: isString,
      /* The page title at the time when the event occurred. */
      title: isString,
      /* The event.type. */
      eventType: isString,
      /* The event.path (dom path) where the event occurred. */
      eventPath: isString,
      /* The viewport (size) of the browser window. */
      eventViewportResolution: isEveryValid([isString, isLengthLessThan(1000)]),
      /* The x coordinate where the event occurred. */
      eventX: isOptional(isBoolean),
      /* The y coordinate where the event occurred. */
      eventY: isOptional(isBoolean),
      /* The key code of the event for keyboard based events. */
      eventKeyCode: isOptional(isValidNumber),
      /* The is composing flag of the event for keyboard based events. */
      eventIsComposing: isOptional(isBoolean),
      /* The event value if the event has one. */
      eventValue: isOptional(isValidNumber),
    })),
  }),
  preprocess: (request) => {
    request.body = request.body
      .map((item) => ({
        ...item,
        createdUsername: request.username,
        createdDate: new Date(),
      }));
  },
};

module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [
    'airbnb-base',
  ],
  rules: {
    'no-console': 'error',
    'no-debugger': 'error',
    // Make eslint not require file extensions .js
    'import/extensions': [
      'error',
      'ignorePackages',
      {
        js: 'never',
      },
    ],
  },
  globals: {
    Server: true,
    Application: true,
    Session: true,
    Request: true,
    Response: true,
    ActiveXObject: true,
    VBArray: true,
    Enumerator: true,
  },
  parserOptions: {
    parser: 'babel-eslint',
  },
};

USE [Testing]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Test_Short_ID](
	/* Keeps the record unique and allows multiple short id's. */
	[ID] [uniqueidentifier] NOT NULL,
	/* The short id seed to be incremented. Converted to base 32 for the short id. */
	[Short_ID] [int] NOT NULL,
	PRIMARY KEY (ID)
) ON [PRIMARY]
GO

INSERT [dbo].[Test_Short_ID] ([ID], [Short_ID]) VALUES (N'd0c5a68b-8272-4ecd-989a-ef9d1adb95c2', 100000)
GO




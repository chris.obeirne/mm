USE [Web_JLC]
GO
/****** Object:  Table [dbo].[CFT_Feedback_Shared]    Script Date: 26-Jan-2021 5:28:18 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CFT_Feedback_Shared]') AND type in (N'U'))
DROP TABLE [dbo].[CFT_Feedback_Shared]
GO
/****** Object:  Table [dbo].[CFT_CounterSync]    Script Date: 26-Jan-2021 5:28:18 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CFT_CounterSync]') AND type in (N'U'))
DROP TABLE [dbo].[CFT_CounterSync]
GO
/****** Object:  Table [dbo].[CFT_CounterSync]    Script Date: 26-Jan-2021 5:28:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CFT_CounterSync](
	[fbnum] [int] IDENTITY(1,1) NOT NULL,
	[fid] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CFT_Feedback_Shared]    Script Date: 26-Jan-2021 5:28:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CFT_Feedback_Shared](
	[shid] [uniqueidentifier] NOT NULL,
	[fid] [uniqueidentifier] NOT NULL,
	[sharedUid] [uniqueidentifier] NOT NULL,
	[lastModified] [datetime] NULL,
	[lastModifiedBy] [uniqueidentifier] NULL
) ON [PRIMARY]
GO

use [Web_JLC]

ALTER TABLE CFT_Feedback ADD altUserId uniqueidentifier NULL;
ALTER TABLE CFT_Feedback ADD cdAltOriginator nvarchar(200) NULL;
ALTER TABLE CFT_Feedback ADD cdAltUnit nvarchar(50) NULL;
ALTER TABLE CFT_Feedback ADD cdAltPhone nvarchar(50) NULL;
ALTER TABLE CFT_Feedback ADD cdAltEmail nvarchar(100) NULL;
ALTER TABLE CFT_Feedback ADD fbnum nvarchar(32) NULL;

USE [Web_JLC]
GO

/****** Object:  View [dbo].[LMM_Phasing_DataRaw_2122]    Script Date: 18/09/2021 8:11:40 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[LMM_Phasing_DataRaw_2122]
AS
SELECT     AHQ.[JLU], AHQ.[DSA], AHQ.[AHQ_Budget], Bid.JLU_Phased_Bid, AHQ.[JLUOrigBids], AHQ.[PHASED_AHQ_Bid], AHQ.[INV_MONTH], AHQ.[Division], 
                      AHQ.[FUND_CODE], AHQ.RunDate, AHQ.Extract_Date
FROM         (SELECT     JLU, CASE WHEN DSA = 'CA04 PMV - OSWIF Op Slipper' THEN 'CA04O' WHEN LEFT(DSA, 5) = 'CIO05' THEN 'CIO05' WHEN DSA LIKE '%CA40%' AND 
                                              FUND_CODE = '70465' THEN 'CA40O' WHEN LEFT(DSA, 5) = 'CJC05' THEN 'CJC05' WHEN LEFT(DSA, 5) = 'CAF26' THEN 'CAF26' WHEN LEFT(DSA, 5) 
                                              = 'JHC01' THEN 'JHC1' WHEN LEFT(DSA, 3) = 'UAV' THEN 'CN49' ELSE LEFT(DSA, 4) END AS DSA, JLUOrigBids, AHQ_BUDGET, PHASED_AHQ_Bid, 
                                              INV_MONTH, GETDATE() AS RunDate, GETDATE() AS Extract_Date, CASE WHEN LEFT(DSA, 4) = 'CA42' THEN 'MSD' WHEN LEFT(DSA, 4) IN ('CA31', 'CA32', 
                                              'CA34', 'CA36', 'CA40') THEN 'JSD' WHEN LEFT(DSA, 3) = 'UAV' THEN 'HSD' WHEN LEFT(DSA, 5) = 'CJC05' OR
                                              LEFT(DSA, 5) = 'CIO05' THEN 'JSD' WHEN LEFT(DSA, 5) = 'CAF26' THEN 'ASD' ELSE 'LSD' END AS 'Division', FUND_CODE
                       FROM          (SELECT     [JLU], CAST([DSA] AS NVARCHAR) AS DSA, JLUOrigBids, AHQ_Budget, CAST([2021-07] AS DEC(12, 2)) AS '2021-07', CAST([2021-08] AS DEC(12, 
                                                                      2)) AS '2021-08', CAST([2021-09] AS DEC(12, 2)) AS '2021-09', CAST([2021-10] AS DEC(12, 2)) AS '2021-10', CAST([2021-11] AS DEC(12, 2)) 
                                                                      AS '2021-11', CAST([2021-12] AS DEC(12, 2)) AS '2021-12', CAST([2022-01] AS DEC(12, 2)) AS '2022-01', CAST([2022-02] AS DEC(12, 2)) 
                                                                      AS '2022-02', CAST([2022-03] AS DEC(12, 2)) AS '2022-03', CAST([2022-04] AS DEC(12, 2)) AS '2022-04', CAST([2022-05] AS DEC(12, 2)) 
                                                                      AS '2022-05', CAST([2022-06] AS DEC(12, 2)) AS '2022-06', FUND_CODE
                                               FROM          [Web_JLC].[dbo].[LMM_PhasingsEdit2122]) AS A UNPIVOT (PHASED_AHQ_Bid FOR INV_MONTH IN ([2021-07], [2021-08], [2021-09], 
                                              [2021-10], [2021-11], [2021-12], [2022-01], [2022-02], [2022-03], [2022-04], [2022-05], [2022-06])) AS ahq) AS AHQ INNER JOIN
                          (SELECT     JLU, CASE WHEN DSA = 'CA04 PMV - OSWIF Op Slipper' THEN 'CA04O' WHEN LEFT(DSA, 5) = 'CIO05' THEN 'CIO05' WHEN DSA LIKE '%CA40%' AND 
                                                   FUND_CODE = '70465' THEN 'CA40O' WHEN LEFT(DSA, 5) = 'CJC05' THEN 'CJC05' WHEN LEFT(DSA, 5) = 'CAF26' THEN 'CAF26' WHEN LEFT(DSA, 5) 
                                                   = 'JHC01' THEN 'JHC1' WHEN LEFT(DSA, 3) = 'UAV' THEN 'CN49' ELSE LEFT(DSA, 4) END AS DSA, JLUOrigBids, AHQ_Budget, JLU_Phased_Bid, 
                                                   INV_MONTH, GETDATE() AS RunDate, GETDATE() AS Extract_Date, CASE WHEN LEFT(DSA, 4) = 'CA42' THEN 'MSD' WHEN LEFT(DSA, 4) IN ('CA31', 
                                                   'CA32', 'CA34', 'CA36', 'CA40') THEN 'JSD' WHEN LEFT(DSA, 3) = 'UAV' THEN 'HSD' WHEN LEFT(DSA, 5) = 'CJC05' OR
                                                   LEFT(DSA, 5) = 'CIO05' THEN 'JSD' WHEN LEFT(DSA, 5) = 'CAF26' THEN 'ASD' ELSE 'LSD' END AS 'Division', FUND_CODE
                            FROM          (SELECT     [JLU], CAST([DSA] AS NVARCHAR) AS DSA, JLUOrigBids, AHQ_BUDGET, CAST([2021-07_Bid] AS DEC(12, 2)) AS '2021-07', 
                                                                           CAST([2021-08_Bid] AS DEC(12, 2)) AS '2021-08', CAST([2021-09_Bid] AS DEC(12, 2)) AS '2021-09', CAST([2021-10_Bid] AS DEC(12, 2)) 
                                                                           AS '2021-10', CAST([2021-11_Bid] AS DEC(12, 2)) AS '2021-11', CAST([2021-12_Bid] AS DEC(12, 2)) AS '2021-12', 
                                                                           CAST([2022-01_Bid] AS DEC(12, 2)) AS '2022-01', CAST([2022-02_Bid] AS DEC(12, 2)) AS '2022-02', CAST([2022-03_Bid] AS DEC(12, 2)) 
                                                                           AS '2022-03', CAST([2022-04_Bid] AS DEC(12, 2)) AS '2022-04', CAST([2022-05_Bid] AS DEC(12, 2)) AS '2022-05', 
                                                                           CAST([2022-06_Bid] AS DEC(12, 2)) AS '2022-06', FUND_CODE
                                                    FROM          [Web_JLC].[dbo].[LMM_PhasingsEdit2122]) AS A UNPIVOT (JLU_Phased_Bid FOR INV_MONTH IN ([2021-07], [2021-08], [2021-09], 
                                                   [2021-10], [2021-11], [2021-12], [2022-01], [2022-02], [2022-03], [2022-04], [2022-05], [2022-06])) AS bid) AS Bid ON AHQ.JLU = Bid.JLU AND 
                      AHQ.DSA = Bid.DSA AND AHQ.INV_MONTH = Bid.INV_MONTH
GO



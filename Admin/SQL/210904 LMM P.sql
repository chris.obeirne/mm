USE [Web_JLC]
GO
/****** Object:  View [dbo].[LMM_Phasing_Data_Sub1_1]    Script Date: 09/04/2021 08:25:07 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LMM_Phasing_Data_Sub1_1]'))
DROP VIEW [dbo].[LMM_Phasing_Data_Sub1_1]
GO
/****** Object:  View [dbo].[LMM_Phasing_DataRaw]    Script Date: 09/04/2021 08:25:07 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LMM_Phasing_DataRaw]'))
DROP VIEW [dbo].[LMM_Phasing_DataRaw]
GO
/****** Object:  View [dbo].[LMM_Phasing_DataRaw_1920]    Script Date: 09/04/2021 08:25:07 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LMM_Phasing_DataRaw_1920]'))
DROP VIEW [dbo].[LMM_Phasing_DataRaw_1920]
GO
/****** Object:  View [dbo].[LMM_Phasing_DataRaw_2021]    Script Date: 09/04/2021 08:25:07 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LMM_Phasing_DataRaw_2021]'))
DROP VIEW [dbo].[LMM_Phasing_DataRaw_2021]
GO
/****** Object:  View [dbo].[LMM_Phasing_DataRaw_2122]    Script Date: 09/04/2021 08:25:07 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LMM_Phasing_DataRaw_2122]'))
DROP VIEW [dbo].[LMM_Phasing_DataRaw_2122]
GO
/****** Object:  View [dbo].[vLMM_Phasing_Bid]    Script Date: 09/04/2021 08:25:07 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vLMM_Phasing_Bid]'))
DROP VIEW [dbo].[vLMM_Phasing_Bid]
GO
/****** Object:  View [dbo].[vLMM_Phasing_Bid]    Script Date: 09/04/2021 08:25:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vLMM_Phasing_Bid]'))
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[vLMM_Phasing_Bid]
AS
SELECT     dbo.LMM_FY.FY, dbo.LMM_Phasing_Data_Bid.JLU, dbo.LMM_Phasing_Data_Bid.DSA, dbo.LMM_Phasing_Data_Bid.AHQ_Phased_Bid, 
                      dbo.LMM_Phasing_Data_Bid.JLU_Phased_Bid, dbo.LMM_Phasing_Data_Bid.INV_MONTH, 
                      CAST(dbo.LMM_Phasing_Data_Bid.AHQ_Phased_Bid * dbo.LMM_FY.Phased_Threshold_Upper AS Float) AS Upper_Threshold, 
                      CAST(dbo.LMM_Phasing_Data_Bid.AHQ_Phased_Bid * dbo.LMM_FY.Phased_Threshold_Lower AS Float) AS Lower_Threshold, dbo.LMM_FY.AxisDate, 
                      dbo.LMM_Phasing_Data_Bid.Division, dbo.LMM_Phasing_Data_Bid.FUND_CODE, dbo.LMM_Phasing_Data_Bid.Fund_Desc, dbo.LMM_FY.[Order], 
                      dbo.LMM_Phasing_Data_Bid.AHQ_Budget, dbo.LMM_Phasing_Data_Bid.JLUOrigBids
FROM         dbo.LMM_FY RIGHT OUTER JOIN
                      dbo.LMM_Phasing_Data_Bid ON dbo.LMM_FY.INV_MONTH = dbo.LMM_Phasing_Data_Bid.INV_MONTH
'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DiagramPane1' , N'SCHEMA',N'dbo', N'VIEW',N'vLMM_Phasing_Bid', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "LMM_FY"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 263
               Right = 246
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "LMM_Phasing_Data_Bid"
            Begin Extent = 
               Top = 23
               Left = 630
               Bottom = 280
               Right = 1024
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vLMM_Phasing_Bid'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DiagramPaneCount' , N'SCHEMA',N'dbo', N'VIEW',N'vLMM_Phasing_Bid', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vLMM_Phasing_Bid'
GO
/****** Object:  View [dbo].[LMM_Phasing_DataRaw_2122]    Script Date: 09/04/2021 08:25:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LMM_Phasing_DataRaw_2122]'))
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[LMM_Phasing_DataRaw_2122]
AS
SELECT     AHQ.[JLU], AHQ.[DSA], AHQ.[AHQ_Budget], Bid.JLU_Phased_Bid, AHQ.[JLUOrigBids], AHQ.[PHASED_AHQ_Bid], AHQ.[INV_MONTH], AHQ.[Division], 
                      AHQ.[FUND_CODE], AHQ.RunDate, AHQ.Extract_Date
FROM         (SELECT     JLU, CASE WHEN DSA = ''CA04 PMV - OSWIF Op Slipper'' THEN ''CA04O'' WHEN LEFT(DSA, 5) = ''CIO05'' THEN ''CIO05'' WHEN DSA LIKE ''%CA40%'' AND 
                                              FUND_CODE = ''70465'' THEN ''CA40O'' WHEN LEFT(DSA, 5) = ''CJC05'' THEN ''CJC05'' WHEN LEFT(DSA, 5) = ''CAF26'' THEN ''CAF26'' WHEN LEFT(DSA, 5) 
                                              = ''JHC01'' THEN ''JHC1'' WHEN LEFT(DSA, 3) = ''UAV'' THEN ''CN49'' ELSE LEFT(DSA, 4) END AS DSA, JLUOrigBids, AHQ_BUDGET, PHASED_AHQ_Bid, 
                                              INV_MONTH, GETDATE() AS RunDate, GETDATE() AS Extract_Date, CASE WHEN LEFT(DSA, 4) = ''CA42'' THEN ''MSD'' WHEN LEFT(DSA, 4) IN (''CA31'', ''CA32'', 
                                              ''CA34'', ''CA36'', ''CA40'') THEN ''JSD'' WHEN LEFT(DSA, 3) = ''UAV'' THEN ''HSD'' WHEN LEFT(DSA, 5) = ''CJC05'' OR
                                              LEFT(DSA, 5) = ''CIO05'' THEN ''JSD'' WHEN LEFT(DSA, 5) = ''CAF26'' THEN ''ASD'' ELSE ''LSD'' END AS ''Division'', FUND_CODE
                       FROM          (SELECT     [JLU], CAST([DSA] AS NVARCHAR) AS DSA, JLUOrigBids, AHQ_Budget, CAST([2021-07] AS DEC(12, 2)) AS ''2021-07'', CAST([2021-08] AS DEC(12, 
                                                                      2)) AS ''2021-08'', CAST([2021-09] AS DEC(12, 2)) AS ''2021-09'', CAST([2021-10] AS DEC(12, 2)) AS ''2021-10'', CAST([2021-11] AS DEC(12, 2)) 
                                                                      AS ''2021-11'', CAST([2021-12] AS DEC(12, 2)) AS ''2021-12'', CAST([2022-01] AS DEC(12, 2)) AS ''2022-01'', CAST([2022-02] AS DEC(12, 2)) 
                                                                      AS ''2022-02'', CAST([2022-03] AS DEC(12, 2)) AS ''2022-03'', CAST([2022-04] AS DEC(12, 2)) AS ''2022-04'', CAST([2022-05] AS DEC(12, 2)) 
                                                                      AS ''2022-05'', CAST([2022-06] AS DEC(12, 2)) AS ''2022-06'', FUND_CODE
                                               FROM          [Web_JLC].[dbo].[LMM_PhasingsEdit2122]) AS A UNPIVOT (PHASED_AHQ_Bid FOR INV_MONTH IN ([2021-07], [2021-08], [2021-09], 
                                              [2021-10], [2021-11], [2021-12], [2022-01], [2022-02], [2022-03], [2022-04], [2022-05], [2022-06])) AS ahq) AS AHQ INNER JOIN
                          (SELECT     JLU, CASE WHEN DSA = ''CA04 PMV - OSWIF Op Slipper'' THEN ''CA04O'' WHEN LEFT(DSA, 5) = ''CIO05'' THEN ''CIO05'' WHEN DSA LIKE ''%CA40%'' AND 
                                                   FUND_CODE = ''70465'' THEN ''CA40O'' WHEN LEFT(DSA, 5) = ''CJC05'' THEN ''CJC05'' WHEN LEFT(DSA, 5) = ''CAF26'' THEN ''CAF26'' WHEN LEFT(DSA, 5) 
                                                   = ''JHC01'' THEN ''JHC1'' WHEN LEFT(DSA, 3) = ''UAV'' THEN ''CN49'' ELSE LEFT(DSA, 4) END AS DSA, JLUOrigBids, AHQ_Budget, JLU_Phased_Bid, 
                                                   INV_MONTH, GETDATE() AS RunDate, GETDATE() AS Extract_Date, CASE WHEN LEFT(DSA, 4) = ''CA42'' THEN ''MSD'' WHEN LEFT(DSA, 4) IN (''CA31'', 
                                                   ''CA32'', ''CA34'', ''CA36'', ''CA40'') THEN ''JSD'' WHEN LEFT(DSA, 3) = ''UAV'' THEN ''HSD'' WHEN LEFT(DSA, 5) = ''CJC05'' OR
                                                   LEFT(DSA, 5) = ''CIO05'' THEN ''JSD'' WHEN LEFT(DSA, 5) = ''CAF26'' THEN ''ASD'' ELSE ''LSD'' END AS ''Division'', FUND_CODE
                            FROM          (SELECT     [JLU], CAST([DSA] AS NVARCHAR) AS DSA, JLUOrigBids, AHQ_BUDGET, CAST([2021-07_Bid] AS DEC(12, 2)) AS ''2021-07'', 
                                                                           CAST([2021-08_Bid] AS DEC(12, 2)) AS ''2021-08'', CAST([2021-09_Bid] AS DEC(12, 2)) AS ''2021-09'', CAST([2021-10_Bid] AS DEC(12, 2)) 
                                                                           AS ''2021-10'', CAST([2021-11_Bid] AS DEC(12, 2)) AS ''2021-11'', CAST([2021-12_Bid] AS DEC(12, 2)) AS ''2021-12'', 
                                                                           CAST([2022-01_Bid] AS DEC(12, 2)) AS ''2022-01'', CAST([2022-02_Bid] AS DEC(12, 2)) AS ''2022-02'', CAST([2022-03_Bid] AS DEC(12, 2)) 
                                                                           AS ''2022-03'', CAST([2022-04_Bid] AS DEC(12, 2)) AS ''2022-04'', CAST([2022-05_Bid] AS DEC(12, 2)) AS ''2022-05'', 
                                                                           CAST([2022-06_Bid] AS DEC(12, 2)) AS ''2022-06'', FUND_CODE
                                                    FROM          [Web_JLC].[dbo].[LMM_PhasingsEdit2122]) AS A UNPIVOT (JLU_Phased_Bid FOR INV_MONTH IN ([2021-07], [2021-08], [2021-09], 
                                                   [2021-10], [2021-11], [2021-12], [2022-01], [2022-02], [2022-03], [2022-04], [2022-05], [2022-06])) AS bid) AS Bid ON AHQ.JLU = Bid.JLU AND 
                      AHQ.DSA = Bid.DSA AND AHQ.INV_MONTH = Bid.INV_MONTH
'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DiagramPane1' , N'SCHEMA',N'dbo', N'VIEW',N'LMM_Phasing_DataRaw_2122', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'LMM_Phasing_DataRaw_2122'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DiagramPaneCount' , N'SCHEMA',N'dbo', N'VIEW',N'LMM_Phasing_DataRaw_2122', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'LMM_Phasing_DataRaw_2122'
GO
/****** Object:  View [dbo].[LMM_Phasing_DataRaw_2021]    Script Date: 09/04/2021 08:25:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LMM_Phasing_DataRaw_2021]'))
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[LMM_Phasing_DataRaw_2021]
AS
SELECT     AHQ.[JLU], AHQ.[DSA], AHQ.[AHQ_Budget], Bid.JLU_Phased_Bid, AHQ.[JLUOrigBids], AHQ.[PHASED_AHQ_Bid], AHQ.[INV_MONTH], AHQ.[Division], 
                      AHQ.[FUND_CODE], AHQ.RunDate, AHQ.Extract_Date
FROM         (SELECT     JLU, CASE WHEN DSA = ''CA04 PMV - OSWIF Op Slipper'' THEN ''CA04O'' WHEN LEFT(DSA, 5) = ''CIO05'' THEN ''CIO05'' WHEN DSA LIKE ''%CA40%'' AND 
                                              FUND_CODE = ''70465'' THEN ''CA40O'' WHEN LEFT(DSA, 5) = ''CJC05'' THEN ''CJC05'' WHEN LEFT(DSA, 5) = ''CAF26'' THEN ''CAF26'' WHEN LEFT(DSA, 5) 
                                              = ''JHC01'' THEN ''JHC1'' WHEN LEFT(DSA, 3) = ''UAV'' THEN ''CN49'' ELSE LEFT(DSA, 4) END AS DSA, JLUOrigBids, AHQ_BUDGET, PHASED_AHQ_Bid, 
                                              INV_MONTH, GETDATE() AS RunDate, GETDATE() AS Extract_Date, CASE WHEN LEFT(DSA, 4) = ''CA42'' THEN ''MSD'' WHEN LEFT(DSA, 4) IN (''CA31'', ''CA32'', 
                                              ''CA34'', ''CA36'', ''CA40'') THEN ''JSD'' WHEN LEFT(DSA, 3) = ''UAV'' THEN ''HSD'' WHEN LEFT(DSA, 5) = ''CJC05'' OR
                                              LEFT(DSA, 5) = ''CIO05'' THEN ''JSD'' WHEN LEFT(DSA, 5) = ''CAF26'' THEN ''ASD'' ELSE ''LSD'' END AS ''Division'', FUND_CODE
                       FROM          (SELECT     [JLU], CAST([DSA] AS NVARCHAR) AS DSA, JLUOrigBids, AHQ_Budget, CAST([2020-07] AS DEC(12, 2)) AS ''2020-07'', CAST([2020-08] AS DEC(12, 
                                                                      2)) AS ''2020-08'', CAST([2020-09] AS DEC(12, 2)) AS ''2020-09'', CAST([2020-10] AS DEC(12, 2)) AS ''2020-10'', CAST([2020-11] AS DEC(12, 2)) 
                                                                      AS ''2020-11'', CAST([2020-12] AS DEC(12, 2)) AS ''2020-12'', CAST([2021-01] AS DEC(12, 2)) AS ''2021-01'', CAST([2021-02] AS DEC(12, 2)) 
                                                                      AS ''2021-02'', CAST([2021-03] AS DEC(12, 2)) AS ''2021-03'', CAST([2021-04] AS DEC(12, 2)) AS ''2021-04'', CAST([2021-05] AS DEC(12, 2)) 
                                                                      AS ''2021-05'', CAST([2021-06] AS DEC(12, 2)) AS ''2021-06'', FUND_CODE
                                               FROM          [Web_JLC].[dbo].[LMM_PhasingsEdit2021]) AS A UNPIVOT (PHASED_AHQ_Bid FOR INV_MONTH IN ([2020-07], [2020-08], [2020-09], 
                                              [2020-10], [2020-11], [2020-12], [2021-01], [2021-02], [2021-03], [2021-04], [2021-05], [2021-06])) AS ahq) AS AHQ INNER JOIN
                          (SELECT     JLU, CASE WHEN DSA = ''CA04 PMV - OSWIF Op Slipper'' THEN ''CA04O'' WHEN LEFT(DSA, 5) = ''CIO05'' THEN ''CIO05'' WHEN DSA LIKE ''%CA40%'' AND 
                                                   FUND_CODE = ''70465'' THEN ''CA40O'' WHEN LEFT(DSA, 5) = ''CJC05'' THEN ''CJC05'' WHEN LEFT(DSA, 5) = ''CAF26'' THEN ''CAF26'' WHEN LEFT(DSA, 5) 
                                                   = ''JHC01'' THEN ''JHC1'' WHEN LEFT(DSA, 3) = ''UAV'' THEN ''CN49'' ELSE LEFT(DSA, 4) END AS DSA, JLUOrigBids, AHQ_Budget, JLU_Phased_Bid, 
                                                   INV_MONTH, GETDATE() AS RunDate, GETDATE() AS Extract_Date, CASE WHEN LEFT(DSA, 4) = ''CA42'' THEN ''MSD'' WHEN LEFT(DSA, 4) IN (''CA31'', 
                                                   ''CA32'', ''CA34'', ''CA36'', ''CA40'') THEN ''JSD'' WHEN LEFT(DSA, 3) = ''UAV'' THEN ''HSD'' WHEN LEFT(DSA, 5) = ''CJC05'' OR
                                                   LEFT(DSA, 5) = ''CIO05'' THEN ''JSD'' WHEN LEFT(DSA, 5) = ''CAF26'' THEN ''ASD'' ELSE ''LSD'' END AS ''Division'', FUND_CODE
                            FROM          (SELECT     [JLU], CAST([DSA] AS NVARCHAR) AS DSA, JLUOrigBids, AHQ_BUDGET, CAST([2020-07_Bid] AS DEC(12, 2)) AS ''2020-07'', 
                                                                           CAST([2020-08_Bid] AS DEC(12, 2)) AS ''2020-08'', CAST([2020-09_Bid] AS DEC(12, 2)) AS ''2020-09'', CAST([2020-10_Bid] AS DEC(12, 2)) 
                                                                           AS ''2020-10'', CAST([2020-11_Bid] AS DEC(12, 2)) AS ''2020-11'', CAST([2020-12_Bid] AS DEC(12, 2)) AS ''2020-12'', 
                                                                           CAST([2021-01_Bid] AS DEC(12, 2)) AS ''2021-01'', CAST([2021-02_Bid] AS DEC(12, 2)) AS ''2021-02'', CAST([2021-03_Bid] AS DEC(12, 2)) 
                                                                           AS ''2021-03'', CAST([2021-04_Bid] AS DEC(12, 2)) AS ''2021-04'', CAST([2021-05_Bid] AS DEC(12, 2)) AS ''2021-05'', 
                                                                           CAST([2021-06_Bid] AS DEC(12, 2)) AS ''2021-06'', FUND_CODE
                                                    FROM          [Web_JLC].[dbo].[LMM_PhasingsEdit2021]) AS A UNPIVOT (JLU_Phased_Bid FOR INV_MONTH IN ([2020-07], [2020-08], [2020-09], 
                                                   [2020-10], [2020-11], [2020-12], [2021-01], [2021-02], [2021-03], [2021-04], [2021-05], [2021-06])) AS bid) AS Bid ON AHQ.JLU = Bid.JLU AND 
                      AHQ.DSA = Bid.DSA AND AHQ.INV_MONTH = Bid.INV_MONTH
'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DiagramPane1' , N'SCHEMA',N'dbo', N'VIEW',N'LMM_Phasing_DataRaw_2021', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'LMM_Phasing_DataRaw_2021'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DiagramPaneCount' , N'SCHEMA',N'dbo', N'VIEW',N'LMM_Phasing_DataRaw_2021', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'LMM_Phasing_DataRaw_2021'
GO
/****** Object:  View [dbo].[LMM_Phasing_DataRaw_1920]    Script Date: 09/04/2021 08:25:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LMM_Phasing_DataRaw_1920]'))
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[LMM_Phasing_DataRaw_1920]
AS
SELECT     AHQ.[JLU], AHQ.[DSA], AHQ.[AHQ_Budget], Bid.JLU_Phased_Bid, AHQ.[JLUOrigBids], AHQ.[PHASED_AHQ_Bid], AHQ.[INV_MONTH], AHQ.[Division], 
                      AHQ.[FUND_CODE], AHQ.RunDate, AHQ.Extract_Date
FROM         (SELECT     JLU, CASE WHEN DSA = ''CA04 PMV - OSWIF Op Slipper'' THEN ''CA04O'' WHEN LEFT(DSA, 5) = ''CIO05'' THEN ''CIO05'' WHEN DSA LIKE ''%CA40%'' AND 
                                              FUND_CODE = ''70465'' THEN ''CA40O'' WHEN LEFT(DSA, 5) = ''CJC05'' THEN ''CJC05'' WHEN LEFT(DSA, 5) = ''JHC01'' THEN ''JHC1'' WHEN LEFT(DSA, 3) 
                                              = ''UAV'' THEN ''CN49'' ELSE LEFT(DSA, 4) END AS DSA, JLUOrigBids, AHQ_BUDGET, PHASED_AHQ_Bid, INV_MONTH, GETDATE() AS RunDate, GETDATE() 
                                              AS Extract_Date, CASE WHEN LEFT(DSA, 4) = ''CA42'' THEN ''MSD'' WHEN LEFT(DSA, 4) IN (''CA31'', ''CA32'', ''CA34'', ''CA36'', ''CA40'') 
                                              THEN ''JSD'' WHEN LEFT(DSA, 3) = ''UAV'' THEN ''HSD'' WHEN LEFT(DSA, 5) = ''CJC05'' OR
                                              LEFT(DSA, 5) = ''CIO05'' THEN ''JSD'' ELSE ''LSD'' END AS ''Division'', FUND_CODE
                       FROM          (SELECT     [JLU], CAST([DSA] AS NVARCHAR) AS DSA, JLUOrigBids, AHQ_Budget, CAST([2019-07] AS DEC(12, 2)) AS ''2019-07'', CAST([2019-08] AS DEC(12, 
                                                                      2)) AS ''2019-08'', CAST([2019-09] AS DEC(12, 2)) AS ''2019-09'', CAST([2019-10] AS DEC(12, 2)) AS ''2019-10'', CAST([2019-11] AS DEC(12, 2)) 
                                                                      AS ''2019-11'', CAST([2019-12] AS DEC(12, 2)) AS ''2019-12'', CAST([2020-01] AS DEC(12, 2)) AS ''2020-01'', CAST([2020-02] AS DEC(12, 2)) 
                                                                      AS ''2020-02'', CAST([2020-03] AS DEC(12, 2)) AS ''2020-03'', CAST([2020-04] AS DEC(12, 2)) AS ''2020-04'', CAST([2020-05] AS DEC(12, 2)) 
                                                                      AS ''2020-05'', CAST([2020-06] AS DEC(12, 2)) AS ''2020-06'', FUND_CODE
                                               FROM          [Web_JLC].[dbo].[LMM_PhasingsEdit1920]) AS A UNPIVOT (PHASED_AHQ_Bid FOR INV_MONTH IN ([2019-07], [2019-08], [2019-09], 
                                              [2019-10], [2019-11], [2019-12], [2020-01], [2020-02], [2020-03], [2020-04], [2020-05], [2020-06])) AS ahq) AS AHQ INNER JOIN
                          (SELECT     JLU, CASE WHEN DSA = ''CA04 PMV - OSWIF Op Slipper'' THEN ''CA04O'' WHEN LEFT(DSA, 5) = ''CIO05'' THEN ''CIO05'' WHEN DSA LIKE ''%CA40%'' AND 
                                                   FUND_CODE = ''70465'' THEN ''CA40O'' WHEN LEFT(DSA, 5) = ''CJC05'' THEN ''CJC05'' WHEN LEFT(DSA, 5) = ''JHC01'' THEN ''JHC1'' WHEN LEFT(DSA, 3) 
                                                   = ''UAV'' THEN ''CN49'' ELSE LEFT(DSA, 4) END AS DSA, JLUOrigBids, AHQ_Budget, JLU_Phased_Bid, INV_MONTH, GETDATE() AS RunDate, GETDATE() 
                                                   AS Extract_Date, CASE WHEN LEFT(DSA, 4) = ''CA42'' THEN ''MSD'' WHEN LEFT(DSA, 4) IN (''CA31'', ''CA32'', ''CA34'', ''CA36'', ''CA40'') 
                                                   THEN ''JSD'' WHEN LEFT(DSA, 3) = ''UAV'' THEN ''HSD'' WHEN LEFT(DSA, 5) = ''CJC05'' OR
                                                   LEFT(DSA, 5) = ''CIO05'' THEN ''JSD'' ELSE ''LSD'' END AS ''Division'', FUND_CODE
                            FROM          (SELECT     [JLU], CAST([DSA] AS NVARCHAR) AS DSA, JLUOrigBids, AHQ_BUDGET, CAST([2019-07_Bid] AS DEC(12, 2)) AS ''2019-07'', 
                                                                           CAST([2019-08_Bid] AS DEC(12, 2)) AS ''2019-08'', CAST([2019-09_Bid] AS DEC(12, 2)) AS ''2019-09'', CAST([2019-10_Bid] AS DEC(12, 2)) 
                                                                           AS ''2019-10'', CAST([2019-11_Bid] AS DEC(12, 2)) AS ''2019-11'', CAST([2019-12_Bid] AS DEC(12, 2)) AS ''2019-12'', 
                                                                           CAST([2020-01_Bid] AS DEC(12, 2)) AS ''2020-01'', CAST([2020-02_Bid] AS DEC(12, 2)) AS ''2020-02'', CAST([2020-03_Bid] AS DEC(12, 2)) 
                                                                           AS ''2020-03'', CAST([2020-04_Bid] AS DEC(12, 2)) AS ''2020-04'', CAST([2020-05_Bid] AS DEC(12, 2)) AS ''2020-05'', 
                                                                           CAST([2020-06_Bid] AS DEC(12, 2)) AS ''2020-06'', FUND_CODE
                                                    FROM          [Web_JLC].[dbo].[LMM_PhasingsEdit1920]) AS A UNPIVOT (JLU_Phased_Bid FOR INV_MONTH IN ([2019-07], [2019-08], [2019-09], 
                                                   [2019-10], [2019-11], [2019-12], [2020-01], [2020-02], [2020-03], [2020-04], [2020-05], [2020-06])) AS bid) AS Bid ON AHQ.JLU = Bid.JLU AND 
                      AHQ.DSA = Bid.DSA AND AHQ.INV_MONTH = Bid.INV_MONTH
'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DiagramPane1' , N'SCHEMA',N'dbo', N'VIEW',N'LMM_Phasing_DataRaw_1920', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'LMM_Phasing_DataRaw_1920'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DiagramPaneCount' , N'SCHEMA',N'dbo', N'VIEW',N'LMM_Phasing_DataRaw_1920', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'LMM_Phasing_DataRaw_1920'
GO
/****** Object:  View [dbo].[LMM_Phasing_DataRaw]    Script Date: 09/04/2021 08:25:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LMM_Phasing_DataRaw]'))
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[LMM_Phasing_DataRaw]
AS
SELECT     *
FROM         dbo.LMM_Phasing_DataRaw_1920
UNION
SELECT     *
FROM         dbo.LMM_Phasing_DataRaw_2021
UNION
SELECT     *
FROM         dbo.LMM_Phasing_DataRaw_2122
'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DiagramPane1' , N'SCHEMA',N'dbo', N'VIEW',N'LMM_Phasing_DataRaw', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'LMM_Phasing_DataRaw'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DiagramPaneCount' , N'SCHEMA',N'dbo', N'VIEW',N'LMM_Phasing_DataRaw', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'LMM_Phasing_DataRaw'
GO
/****** Object:  View [dbo].[LMM_Phasing_Data_Sub1_1]    Script Date: 09/04/2021 08:25:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LMM_Phasing_Data_Sub1_1]'))
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[LMM_Phasing_Data_Sub1_1]
AS
SELECT     JLU, LEFT(DSA, 5) AS DSAKey, PHASED_AHQ_Bid, JLU_Phased_Bid, INV_MONTH, RunDate AS ExtractDate, FUND_CODE, Division, JLUOrigBids, AHQ_Budget
FROM         dbo.LMM_Phasing_DataRaw
'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DiagramPane1' , N'SCHEMA',N'dbo', N'VIEW',N'LMM_Phasing_Data_Sub1_1', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "LMM_Phasing_DataRaw"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 118
               Right = 206
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'LMM_Phasing_Data_Sub1_1'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DiagramPaneCount' , N'SCHEMA',N'dbo', N'VIEW',N'LMM_Phasing_Data_Sub1_1', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'LMM_Phasing_Data_Sub1_1'
GO

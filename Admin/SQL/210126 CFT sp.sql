USE [Web_JLC]
GO
/****** Object:  StoredProcedure [dbo].[CFT_fbNumBuilder]    Script Date: 26-Jan-2021 5:31:32 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CFT_fbNumBuilder]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CFT_fbNumBuilder]
GO
/****** Object:  StoredProcedure [dbo].[CFT_fbNumBuilder]    Script Date: 26-Jan-2021 5:31:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Chris O'Beirne 
-- Create date: 24 Jan 21
-- Description:	Generate a formatted feedback Number for the CFT
-- =============================================
CREATE PROCEDURE [dbo].[CFT_fbNumBuilder]
	-- Add the parameters for the stored procedure here
	@fid as uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Y as nvarchar(4)
	declare @Cntr as varchar(4)
	declare @ctr as int
	insert [dbo].CFT_CounterSync (fid) VALUES(@fid)
	select @ctr = fbnum FROM CFT_CounterSync where fid = @fid

	SELECT @Cntr = RIGHT('0000' + CAST(@ctr AS VARCHAR(4)), 4)
	
	SELECT @Y = right(YEAR(getDate()),2)

	UPDATE       [dbo].CFT_feedback
	SET          fbnum = @Y + @Cntr
	WHERE fid = @fid


END
GO

INSERT INTO LMM_PhasingsEdit2122
                         (ID, RomanCode, JLU, DSA, a1, b1, a2, b2, a3, b3, a4, b4, a5, b5, a6, b6, a7, b7, a8, b8, a9, b9, a10, b10, a11, b11, a12, b12)
SELECT        ID, RomanCode, JLU, DSA, [2021-07] AS a1, [2021-07_Bid] AS b1, [2021-08] AS a2, [2021-08_Bid] AS b2, [2021-09] AS a3, [2021-09_Bid] AS b3, [2021-10] AS a4, [2021-10_Bid] AS b4, [2021-11] AS a5, [2021-11_Bid] AS b5, 
                         [2021-12] AS a6, [2021-12_Bid] AS b6, [2022-01] AS a7, [2022-01_Bid] AS b7, [2022-02] AS a8, [2022-02_Bid] AS b8, [2022-03] AS a9, [2022-03_Bid] AS b9, [2022-04] AS a10, [2022-04_Bid] AS b10, [2022-05] AS a11, 
                         [2022-05_Bid] AS b11, [2022-06] AS a12, [2022-06_Bid] AS b12
FROM            LMM_PhasingsEdit2122orig


UPDATE       LMM_PhasingsEdit2122
SET                fy = '2122', lockBids = 0


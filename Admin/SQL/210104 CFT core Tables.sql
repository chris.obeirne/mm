USE [Web_JLC]
GO
/****** Object:  Table [dbo].[CFTUserAccess]    Script Date: 01/04/2021 10:58:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CFTUserAccess]') AND type in (N'U'))
DROP TABLE [dbo].[CFTUserAccess]
GO
/****** Object:  Table [dbo].[Customer_FB_ChangeHistory_prod]    Script Date: 01/04/2021 10:58:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Customer_FB_ChangeHistory_prod]') AND type in (N'U'))
DROP TABLE [dbo].[Customer_FB_ChangeHistory_prod]
GO
/****** Object:  Table [dbo].[Customer_FB_Nature_prod]    Script Date: 01/04/2021 10:58:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Customer_FB_Nature_prod]') AND type in (N'U'))
DROP TABLE [dbo].[Customer_FB_Nature_prod]
GO
/****** Object:  Table [dbo].[Customer_FB_Survey_prod]    Script Date: 01/04/2021 10:58:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Customer_FB_Survey_prod]') AND type in (N'U'))
DROP TABLE [dbo].[Customer_FB_Survey_prod]
GO
/****** Object:  Table [dbo].[Customer_FB_Target_prod]    Script Date: 01/04/2021 10:58:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Customer_FB_Target_prod]') AND type in (N'U'))
DROP TABLE [dbo].[Customer_FB_Target_prod]
GO
/****** Object:  Table [dbo].[Customer_FB_Target_prod]    Script Date: 01/04/2021 10:58:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Customer_FB_Target_prod]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Customer_FB_Target_prod](
	[targetID] [int] NULL,
	[targetName] [nvarchar](max) NULL,
	[targetEmail] [nvarchar](max) NULL,
	[parentTargetID] [int] NULL,
	[districtLink] [nvarchar](4) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
INSERT [dbo].[Customer_FB_Target_prod] ([targetID], [targetName], [targetEmail], [parentTargetID], [districtLink]) VALUES (1, N'SCBHQ', N'JLC-SCBHQ-CustomerFeedback@drn.mil.au', 1, NULL)
INSERT [dbo].[Customer_FB_Target_prod] ([targetID], [targetName], [targetEmail], [parentTargetID], [districtLink]) VALUES (2, N'JLUN', N'JLUN.CLO@Defence.gov.au', 1, N'JLUN')
INSERT [dbo].[Customer_FB_Target_prod] ([targetID], [targetName], [targetEmail], [parentTargetID], [districtLink]) VALUES (3, N'JLUS', N'jlu-scustomerfeedback@drn.mil.au', 1, N'AAAD')
INSERT [dbo].[Customer_FB_Target_prod] ([targetID], [targetName], [targetEmail], [parentTargetID], [districtLink]) VALUES (4, N'JLUE', N'jlu-e.inventorysupport@defence.gov.au', 1, N'AAML')
INSERT [dbo].[Customer_FB_Target_prod] ([targetID], [targetName], [targetEmail], [parentTargetID], [districtLink]) VALUES (5, N'JLUW', N'JLUWCustomer.Liaison@defence.gov.au', 1, N'JLUW')
INSERT [dbo].[Customer_FB_Target_prod] ([targetID], [targetName], [targetEmail], [parentTargetID], [districtLink]) VALUES (6, N'JLUNQ', N'JLUNQ.CustomerSupport@defence.gov.au', 1, N'AA2F')
INSERT [dbo].[Customer_FB_Target_prod] ([targetID], [targetName], [targetEmail], [parentTargetID], [districtLink]) VALUES (7, N'JLUSQ', N'JLUSQCustomerLiaison@drn.mil.au', 1, N'AABN')
INSERT [dbo].[Customer_FB_Target_prod] ([targetID], [targetName], [targetEmail], [parentTargetID], [districtLink]) VALUES (8, N'JLUV', N'jluv.customerfeedback@defence.gov.au', 1, N'AABD')
/****** Object:  Table [dbo].[Customer_FB_Survey_prod]    Script Date: 01/04/2021 10:58:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Customer_FB_Survey_prod]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Customer_FB_Survey_prod](
	[surveyID] [int] NULL,
	[q1response] [int] NULL,
	[q2response] [int] NULL,
	[q3response] [int] NULL,
	[q4response] [int] NULL,
	[q5response] [int] NULL,
	[q6response] [int] NULL,
	[fbID] [int] NULL,
	[unitReply_days] [int] NULL,
	[unitClose_days] [int] NULL,
	[surveyDate] [datetime] NULL
) ON [PRIMARY]
END
GO
INSERT [dbo].[Customer_FB_Survey_prod] ([surveyID], [q1response], [q2response], [q3response], [q4response], [q5response], [q6response], [fbID], [unitReply_days], [unitClose_days], [surveyDate]) VALUES (1, 1, 1, 1, 0, NULL, NULL, 1, 0, 0, CAST(0x0000A95800C94A45 AS DateTime))
INSERT [dbo].[Customer_FB_Survey_prod] ([surveyID], [q1response], [q2response], [q3response], [q4response], [q5response], [q6response], [fbID], [unitReply_days], [unitClose_days], [surveyDate]) VALUES (2, 0, 0, 1, 1, NULL, NULL, 929, 1, 265, CAST(0x0000A99600E0ED6D AS DateTime))
INSERT [dbo].[Customer_FB_Survey_prod] ([surveyID], [q1response], [q2response], [q3response], [q4response], [q5response], [q6response], [fbID], [unitReply_days], [unitClose_days], [surveyDate]) VALUES (3, 1, 1, 1, 0, NULL, NULL, 1947, 2, 2, CAST(0x0000AA8E00B3AA16 AS DateTime))
INSERT [dbo].[Customer_FB_Survey_prod] ([surveyID], [q1response], [q2response], [q3response], [q4response], [q5response], [q6response], [fbID], [unitReply_days], [unitClose_days], [surveyDate]) VALUES (4, 0, 0, 0, 1, NULL, NULL, 1921, 23, 23, CAST(0x0000AA8F00A0D5E5 AS DateTime))
INSERT [dbo].[Customer_FB_Survey_prod] ([surveyID], [q1response], [q2response], [q3response], [q4response], [q5response], [q6response], [fbID], [unitReply_days], [unitClose_days], [surveyDate]) VALUES (5, 1, 1, 0, 1, NULL, NULL, 2013, 1, 53, CAST(0x0000AAE300BAC489 AS DateTime))
INSERT [dbo].[Customer_FB_Survey_prod] ([surveyID], [q1response], [q2response], [q3response], [q4response], [q5response], [q6response], [fbID], [unitReply_days], [unitClose_days], [surveyDate]) VALUES (6, 1, 1, 1, 0, NULL, NULL, 2157, 1, 1, CAST(0x0000AB0300C9EFDC AS DateTime))
INSERT [dbo].[Customer_FB_Survey_prod] ([surveyID], [q1response], [q2response], [q3response], [q4response], [q5response], [q6response], [fbID], [unitReply_days], [unitClose_days], [surveyDate]) VALUES (7, 1, 1, 1, 0, NULL, NULL, 2320, 13, 13, CAST(0x0000ABC700F11087 AS DateTime))
/****** Object:  Table [dbo].[Customer_FB_Nature_prod]    Script Date: 01/04/2021 10:58:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Customer_FB_Nature_prod]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Customer_FB_Nature_prod](
	[NatureID] [nvarchar](4) NULL,
	[NatureParent] [nvarchar](50) NULL,
	[NatureShortDesc] [nvarchar](100) NULL,
	[NatureLongDesc] [nvarchar](500) NULL,
	[NatureStatus] [bit] NULL,
	[NatureParentID] [int] NULL,
	[SCBHQ_Email] [nvarchar](100) NULL,
	[NIDColor] [nvarchar](50) NULL
) ON [PRIMARY]
END
GO
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'CL', N'Clothing', N'Compliments and Comments', NULL, 0, 5, NULL, N'#f20000')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'CL01', N'Clothing', N'Sizing Issue', N'This field is reserved for a longer description of the Nature', 1, 5, N'jlc.scb.dscsd.clothing@defence.gov.au', N'#ffc8bf')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'CL02', N'Clothing', N'Product Issue', N'This field is reserved for a longer description of the Nature', 1, 5, N'jlc.scb.dscsd.clothing@defence.gov.au', N'#592816')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'CL03', N'Clothing', N'Customer Service', N'This field is reserved for a longer description of the Nature', 1, 5, N'jlc.scb.dscsd.clothing@defence.gov.au', N'#f2853d')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'CL04', N'Clothing', N'Timeframes', N'This field is reserved for a longer description of the Nature', 1, 5, N'jlc.scb.dscsd.clothing@defence.gov.au', N'#996b4d')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'CL05', N'Clothing', N'Other', N'This field is reserved for a longer description of the Nature', 1, 5, N'jlc.scb.dscsd.clothing@defence.gov.au', N'#663600')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'CL06', N'Clothing', N'spare', N'This field is reserved for a longer description of the Nature', 0, 5, NULL, N'#ffbf40')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'CL07', N'Clothing', N'spare', N'This field is reserved for a longer description of the Nature', 0, 5, NULL, N'#a67c29')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'CL08', N'Clothing', N'spare', N'This field is reserved for a longer description of the Nature', 0, 5, NULL, N'#403010')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'CL09', N'Clothing', N'spare', N'This field is reserved for a longer description of the Nature', 0, 5, NULL, N'#998c73')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'CL10', N'Clothing', N'spare', N'This field is reserved for a longer description of the Nature', 0, 5, NULL, N'#f2ea79')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'CL11', N'Clothing', N'spare', N'This field is reserved for a longer description of the Nature', 0, 5, NULL, N'#e6e2ac')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'CL12', N'Clothing', N'spare', N'This field is reserved for a longer description of the Nature', 0, 5, NULL, N'#add900')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'GD', N'Global Distribution', N'Compliments and Comments', NULL, 0, 7, NULL, N'#e6acd2')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'GD01', N'Global Distribution', N'Unserviceable Item Received', N'This field is reserved for a longer description of the Nature', 1, 7, N'ADO.Customs@defence.gov.au', N'#990052')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'GD02', N'Global Distribution', N'Incorrect Quantity Received', N'This field is reserved for a longer description of the Nature', 1, 7, N'ADO.Customs@defence.gov.au', N'#990052')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'GD03', N'Global Distribution', N'Incorrect Item Received', N'This field is reserved for a longer description of the Nature', 1, 7, N'ADO.Customs@defence.gov.au', N'#990052')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'GD04', N'Global Distribution', N'No/Expired Technical Inspection', N'This field is reserved for a longer description of the Nature', 1, 7, N'ADO.Customs@defence.gov.au', N'#990052')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'GD05', N'Global Distribution', N'Shelf Life Expired', N'This field is reserved for a longer description of the Nature', 1, 7, N'ADO.Customs@defence.gov.au', N'#990052')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'GD06', N'Global Distribution', N'Delay in Issue', N'This field is reserved for a longer description of the Nature', 1, 7, N'ADO.Customs@defence.gov.au', N'#990052')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'GD07', N'Global Distribution', N'Delay in Pickup', N'This field is reserved for a longer description of the Nature', 1, 7, N'ADO.Customs@defence.gov.au', N'#990052')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'GD08', N'Global Distribution', N'Other', N'This field is reserved for a longer description of the Nature', 1, 7, N'ADO.Customs@defence.gov.au', N'#990052')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'GD09', N'Global Distribution', N'Customer Service', N'This field is reserved for a longer description of the Nature', 1, 7, N'ADO.Customs@defence.gov.au', N'#990052')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'GD10', N'Global Distribution', N'Quality of Service', N'This field is reserved for a longer description of the Nature', 1, 7, N'ADO.Customs@defence.gov.au', N'#990052')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'GD11', N'Global Distribution', N'spare', N'This field is reserved for a longer description of the Nature', 0, 7, NULL, N'#990052')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'GD12', N'Global Distribution', N'spare', N'This field is reserved for a longer description of the Nature', 0, 7, NULL, N'#990052')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'LP', N'Loan Pool', N'Compliments and Comments', NULL, 0, 4, NULL, N'#4c0000')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'LP01', N'Loan Pool', N'Unserviceable Item Issued', N'This field is reserved for a longer description of the Nature', 1, 4, N'jlc.scb.dscsd-warehousing@defence.gov.au', N'#50592d')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'LP02', N'Loan Pool', N'No/Expired Technical Inspection', N'This field is reserved for a longer description of the Nature', 1, 4, N'jlc.scb.dscsd-warehousing@defence.gov.au', N'#639926')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'LP03', N'Loan Pool', N'Delay in Issue', N'This field is reserved for a longer description of the Nature', 1, 4, N'jlc.scb.dscsd-warehousing@defence.gov.au', N'#66ff00')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'LP04', N'Loan Pool', N'Delay in Pickup', N'This field is reserved for a longer description of the Nature', 1, 4, N'jlc.scb.dscsd-warehousing@defence.gov.au', N'#d0ffbf')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'LP05', N'Loan Pool', N'DER Not Met', N'This field is reserved for a longer description of the Nature', 1, 4, N'jlc.scb.dscsd-warehousing@defence.gov.au', N'#6e8c69')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'LP06', N'Loan Pool', N'Unauthorised DER Change', N'This field is reserved for a longer description of the Nature', 1, 4, N'jlc.scb.dscsd-warehousing@defence.gov.au', N'#00d957')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'LP07', N'Loan Pool', N'Incorrect Item Received', N'This field is reserved for a longer description of the Nature', 1, 4, N'jlc.scb.dscsd-warehousing@defence.gov.au', N'#005924')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'LP08', N'Loan Pool', N'Other', N'This field is reserved for a longer description of the Nature', 1, 4, N'jlc.scb.dscsd-warehousing@defence.gov.au', N'#0d3326')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'LP09', N'Loan Pool', N'spare', N'This field is reserved for a longer description of the Nature', 0, 4, NULL, N'#73e6bf')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'LP10', N'Loan Pool', N'spare', N'This field is reserved for a longer description of the Nature', 0, 4, NULL, N'#40807b')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'LP11', N'Loan Pool', N'spare', N'This field is reserved for a longer description of the Nature', 0, 4, NULL, N'#39dae6')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'LP12', N'Loan Pool', N'spare', N'This field is reserved for a longer description of the Nature', 0, 4, NULL, N'#ace2e6')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'MM', N'Materiel Maintenance', N'Compliments and Comments', NULL, 0, 2, NULL, N'#7f4040')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'MM01', N'Materiel Maintenance', N'Unsatisfactory Maintenance', N'This field is reserved for a longer description of the Nature', 1, 2, N'scbservice.delivery-mm@defence.gov.au', N'#00708c')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'MM02', N'Materiel Maintenance', N'DER Not Met', N'This field is reserved for a longer description of the Nature', 1, 2, N'scbservice.delivery-mm@defence.gov.au', N'#80d5ff')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'MM03', N'Materiel Maintenance', N'Unauthorised DER Change', N'This field is reserved for a longer description of the Nature', 1, 2, N'scbservice.delivery-mm@defence.gov.au', N'#2d4a59')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'MM04', N'Materiel Maintenance', N'Unserviceable Item Issued', N'This field is reserved for a longer description of the Nature', 1, 2, N'scbservice.delivery-mm@defence.gov.au', N'#3d9df2')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'MM05', N'Materiel Maintenance', N'No/Expired Technical Inspection', N'This field is reserved for a longer description of the Nature', 1, 2, N'scbservice.delivery-mm@defence.gov.au', N'#235b8c')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'MM06', N'Materiel Maintenance', N'Incorrect Quantity Received', N'This field is reserved for a longer description of the Nature', 1, 2, N'scbservice.delivery-mm@defence.gov.au', N'#738799')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'MM07', N'Materiel Maintenance', N'Incorrect Item Received', N'This field is reserved for a longer description of the Nature', 1, 2, N'scbservice.delivery-mm@defence.gov.au', N'#0044ff')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'MM08', N'Materiel Maintenance', N'Other', N'This field is reserved for a longer description of the Nature', 1, 2, N'scbservice.delivery-mm@defence.gov.au', N'#bfd0ff')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'MM09', N'Materiel Maintenance', N'spare', N'This field is reserved for a longer description of the Nature', 0, 2, NULL, N'#0d1233')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'MM10', N'Materiel Maintenance', N'spare', N'This field is reserved for a longer description of the Nature', 0, 2, NULL, N'#7373e6')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'MM11', N'Materiel Maintenance', N'spare', N'This field is reserved for a longer description of the Nature', 0, 2, NULL, N'#2e1966')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'MM12', N'Materiel Maintenance', N'spare', N'This field is reserved for a longer description of the Nature', 0, 2, NULL, N'#4200a6')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'NV', N'National Vehicle Recovery', N'Compliments and Comments', NULL, 0, 6, NULL, N'#e63995')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'NV01', N'National Vehicle Recovery', N'Meeting Timelines', N'This field is reserved for a longer description of the Nature', 1, 6, N'scbservice.delivery-mm@defence.gov.au', N'#e63995')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'NV02', N'National Vehicle Recovery', N'Responsiveness', N'This field is reserved for a longer description of the Nature', 1, 6, N'scbservice.delivery-mm@defence.gov.au', N'#e63995')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'NV03', N'National Vehicle Recovery', N'Quality of Service', N'This field is reserved for a longer description of the Nature', 1, 6, N'scbservice.delivery-mm@defence.gov.au', N'#e63995')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'NV04', N'National Vehicle Recovery', N'Other', N'This field is reserved for a longer description of the Nature', 1, 6, N'scbservice.delivery-mm@defence.gov.au', N'#e63995')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'NV05', N'National Vehicle Recovery', N'spare', N'This field is reserved for a longer description of the Nature', 0, 6, NULL, N'#e63995')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'NV06', N'National Vehicle Recovery', N'spare', N'This field is reserved for a longer description of the Nature', 0, 6, NULL, N'#e63995')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'NV07', N'National Vehicle Recovery', N'spare', N'This field is reserved for a longer description of the Nature', 0, 6, NULL, N'#e63995')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'NV08', N'National Vehicle Recovery', N'spare', N'This field is reserved for a longer description of the Nature', 0, 6, NULL, N'#e63995')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'NV09', N'National Vehicle Recovery', N'spare', N'This field is reserved for a longer description of the Nature', 0, 6, NULL, N'#e63995')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'NV10', N'National Vehicle Recovery', N'spare', N'This field is reserved for a longer description of the Nature', 0, 6, NULL, N'#e63995')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'NV11', N'National Vehicle Recovery', N'spare', N'This field is reserved for a longer description of the Nature', 0, 6, NULL, N'#e63995')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'NV12', N'National Vehicle Recovery', N'spare', N'This field is reserved for a longer description of the Nature', 0, 6, NULL, N'#e63995')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'OC', N'Operational Contracts', N'Compliments and Comments', NULL, 0, 8, NULL, N'#e6acd2')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'OC01', N'Operational Contracts', N'Unsatisfactory Maintenance', N'This field is reserved for a longer description of the Nature', 1, 8, N'opcontracts.jlc@defence.gov.au', N'#e6acd2')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'OC02', N'Operational Contracts', N'DER Not Met', N'This field is reserved for a longer description of the Nature', 1, 8, N'opcontracts.jlc@defence.gov.au', N'#e6acd2')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'OC03', N'Operational Contracts', N'Unauthorised DER Change', N'This field is reserved for a longer description of the Nature', 1, 8, N'opcontracts.jlc@defence.gov.au', N'#e6acd2')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'OC04', N'Operational Contracts', N'Unserviceable Item Issued', N'This field is reserved for a longer description of the Nature', 1, 8, N'opcontracts.jlc@defence.gov.au', N'#e6acd2')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'OC05', N'Operational Contracts', N'No/Expired Technical Inspection', N'This field is reserved for a longer description of the Nature', 1, 8, N'opcontracts.jlc@defence.gov.au', N'#e6acd2')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'OC06', N'Operational Contracts', N'Incorrect Quantity Received', N'This field is reserved for a longer description of the Nature', 1, 8, N'opcontracts.jlc@defence.gov.au', N'#e6acd2')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'OC07', N'Operational Contracts', N'Incorrect Item Received', N'This field is reserved for a longer description of the Nature', 1, 8, N'opcontracts.jlc@defence.gov.au', N'#e6acd2')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'OC08', N'Operational Contracts', N'Other', N'This field is reserved for a longer description of the Nature', 1, 8, N'opcontracts.jlc@defence.gov.au', N'#e6acd2')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'OC09', N'Operational Contracts', N'spare', N'This field is reserved for a longer description of the Nature', 0, 8, NULL, N'#e6acd2')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'OC10', N'Operational Contracts', N'spare', N'This field is reserved for a longer description of the Nature', 0, 8, NULL, N'#e6acd2')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'OC11', N'Operational Contracts', N'spare', N'This field is reserved for a longer description of the Nature', 0, 8, NULL, N'#e6acd2')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'OC12', N'Operational Contracts', N'spare', N'This field is reserved for a longer description of the Nature', 0, 8, NULL, N'#e6acd2')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'WD', N'Warehouse Distribution', N'Compliments and Comments', NULL, 0, 3, NULL, N'#b23e2d')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'WD01', N'Warehouse Distribution', N'Unserviceable Item Received', N'This field is reserved for a longer description of the Nature', 1, 3, N'jlc-scb.dscsd-warehousing@defence.gov.au', N'#594080')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'WD02', N'Warehouse Distribution', N'Incorrect Quantity Received', N'This field is reserved for a longer description of the Nature', 1, 3, N'jlc-scb.dscsd-warehousing@defence.gov.au', N'#9886b3')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'WD03', N'Warehouse Distribution', N'Incorrect Item Received', N'This field is reserved for a longer description of the Nature', 1, 3, N'jlc-scb.dscsd-warehousing@defence.gov.au', N'#363040')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'WD04', N'Warehouse Distribution', N'No/Expired Technical Inspection', N'This field is reserved for a longer description of the Nature', 1, 3, N'jlc-scb.dscsd-warehousing@defence.gov.au', N'#a200f2')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'WD05', N'Warehouse Distribution', N'Shelf Life Expired', N'This field is reserved for a longer description of the Nature', 1, 3, N'jlc-scb.dscsd-warehousing@defence.gov.au', N'#e680ff')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'WD06', N'Warehouse Distribution', N'Delay in Issue', N'This field is reserved for a longer description of the Nature', 1, 3, N'jlc-scb.dscsd-warehousing@defence.gov.au', N'#85238c')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'WD07', N'Warehouse Distribution', N'Delay in Pickup', N'This field is reserved for a longer description of the Nature', 1, 3, N'jlc-scb.dscsd-warehousing@defence.gov.au', N'#ff00ee')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'WD08', N'Warehouse Distribution', N'Other', N'This field is reserved for a longer description of the Nature', 1, 3, N'jlc-scb.dscsd-warehousing@defence.gov.au', N'#4d1341')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'WD09', N'Warehouse Distribution', N'spare', N'This field is reserved for a longer description of the Nature', 0, 3, NULL, N'#a6538a')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'WD10', N'Warehouse Distribution', N'spare', N'This field is reserved for a longer description of the Nature', 0, 3, NULL, N'#e6acd2')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'WD11', N'Warehouse Distribution', N'spare', N'This field is reserved for a longer description of the Nature', 0, 3, NULL, N'#990052')
INSERT [dbo].[Customer_FB_Nature_prod] ([NatureID], [NatureParent], [NatureShortDesc], [NatureLongDesc], [NatureStatus], [NatureParentID], [SCBHQ_Email], [NIDColor]) VALUES (N'WD12', N'Warehouse Distribution', N'spare', N'This field is reserved for a longer description of the Nature', 0, 3, NULL, N'#e63995')
/****** Object:  Table [dbo].[Customer_FB_ChangeHistory_prod]    Script Date: 01/04/2021 10:58:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Customer_FB_ChangeHistory_prod]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Customer_FB_ChangeHistory_prod](
	[feedbackID] [int] NULL,
	[deleted] [int] NULL,
	[deluser] [nvarchar](100) NULL,
	[deldate] [datetime] NULL,
	[changeuser] [nvarchar](100) NULL,
	[changedate] [datetime] NULL
) ON [PRIMARY]
END
GO
INSERT [dbo].[Customer_FB_ChangeHistory_prod] ([feedbackID], [deleted], [deluser], [deldate], [changeuser], [changedate]) VALUES (793, NULL, NULL, NULL, N'gerry.lukman', CAST(0x0000A95F00B6DE8E AS DateTime))
INSERT [dbo].[Customer_FB_ChangeHistory_prod] ([feedbackID], [deleted], [deluser], [deldate], [changeuser], [changedate]) VALUES (1126, NULL, NULL, NULL, N'gerry.lukman', CAST(0x0000A97400B0B87F AS DateTime))
INSERT [dbo].[Customer_FB_ChangeHistory_prod] ([feedbackID], [deleted], [deluser], [deldate], [changeuser], [changedate]) VALUES (1119, NULL, NULL, NULL, N'gerry.lukman', CAST(0x0000A97600FAA84D AS DateTime))
INSERT [dbo].[Customer_FB_ChangeHistory_prod] ([feedbackID], [deleted], [deluser], [deldate], [changeuser], [changedate]) VALUES (1166, NULL, NULL, NULL, N'shane.kline', CAST(0x0000A9A700E070CD AS DateTime))
INSERT [dbo].[Customer_FB_ChangeHistory_prod] ([feedbackID], [deleted], [deluser], [deldate], [changeuser], [changedate]) VALUES (1248, NULL, NULL, NULL, N'Delia.Anetts', CAST(0x0000AAA300D440FF AS DateTime))
INSERT [dbo].[Customer_FB_ChangeHistory_prod] ([feedbackID], [deleted], [deluser], [deldate], [changeuser], [changedate]) VALUES (1266, NULL, NULL, NULL, N'Delia.Anetts', CAST(0x0000AAA300D50A9C AS DateTime))
INSERT [dbo].[Customer_FB_ChangeHistory_prod] ([feedbackID], [deleted], [deluser], [deldate], [changeuser], [changedate]) VALUES (2273, 0, N'leon.nelson', CAST(0x0000AB8E00CF05F1 AS DateTime), NULL, NULL)
INSERT [dbo].[Customer_FB_ChangeHistory_prod] ([feedbackID], [deleted], [deluser], [deldate], [changeuser], [changedate]) VALUES (2295, NULL, NULL, NULL, N'william.webb3', CAST(0x0000ABB900BE0FC3 AS DateTime))
INSERT [dbo].[Customer_FB_ChangeHistory_prod] ([feedbackID], [deleted], [deluser], [deldate], [changeuser], [changedate]) VALUES (2338, NULL, NULL, NULL, N'leon.nelson', CAST(0x0000ABC700D8A320 AS DateTime))
INSERT [dbo].[Customer_FB_ChangeHistory_prod] ([feedbackID], [deleted], [deluser], [deldate], [changeuser], [changedate]) VALUES (2330, NULL, NULL, NULL, N'andrew.page2', CAST(0x0000ABCF00B79B79 AS DateTime))
INSERT [dbo].[Customer_FB_ChangeHistory_prod] ([feedbackID], [deleted], [deluser], [deldate], [changeuser], [changedate]) VALUES (2331, NULL, NULL, NULL, N'andrew.page2', CAST(0x0000ABCF00B7E981 AS DateTime))
INSERT [dbo].[Customer_FB_ChangeHistory_prod] ([feedbackID], [deleted], [deluser], [deldate], [changeuser], [changedate]) VALUES (2332, NULL, NULL, NULL, N'andrew.page2', CAST(0x0000ABCF00B7FCD5 AS DateTime))
INSERT [dbo].[Customer_FB_ChangeHistory_prod] ([feedbackID], [deleted], [deluser], [deldate], [changeuser], [changedate]) VALUES (2333, NULL, NULL, NULL, N'andrew.page2', CAST(0x0000ABCF00B815AC AS DateTime))
INSERT [dbo].[Customer_FB_ChangeHistory_prod] ([feedbackID], [deleted], [deluser], [deldate], [changeuser], [changedate]) VALUES (2334, NULL, NULL, NULL, N'andrew.page2', CAST(0x0000ABCF00B826CC AS DateTime))
INSERT [dbo].[Customer_FB_ChangeHistory_prod] ([feedbackID], [deleted], [deluser], [deldate], [changeuser], [changedate]) VALUES (2454, 0, N'leon.nelson', CAST(0x0000AC3100ECBDB5 AS DateTime), NULL, NULL)
INSERT [dbo].[Customer_FB_ChangeHistory_prod] ([feedbackID], [deleted], [deluser], [deldate], [changeuser], [changedate]) VALUES (2420, NULL, NULL, NULL, N'christopher.obeirne', CAST(0x0000AC810095191B AS DateTime))
INSERT [dbo].[Customer_FB_ChangeHistory_prod] ([feedbackID], [deleted], [deluser], [deldate], [changeuser], [changedate]) VALUES (2335, NULL, NULL, NULL, N'andrew.page2', CAST(0x0000ABCF00B83DC7 AS DateTime))
/****** Object:  Table [dbo].[CFTUserAccess]    Script Date: 01/04/2021 10:58:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CFTUserAccess]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CFTUserAccess](
	[UID] [uniqueidentifier] NULL,
	[username] [nvarchar](100) NULL,
	[name] [nvarchar](200) NULL,
	[inActive] [bit] NULL,
	[lastModified] [datetime] NULL
) ON [PRIMARY]
END
GO
INSERT [dbo].[CFTUserAccess] ([UID], [username], [name], [inActive], [lastModified]) VALUES (N'21e53d3a-dced-4faa-be93-d7d70b7d7e49', N'christopher.obeirne', N'Chris O''Beirne', 0, CAST(0x0000AC8800000000 AS DateTime))
INSERT [dbo].[CFTUserAccess] ([UID], [username], [name], [inActive], [lastModified]) VALUES (N'314d1638-e772-49f0-86e4-292e181ce8a3', N'natasha.flanigan', NULL, 0, NULL)
INSERT [dbo].[CFTUserAccess] ([UID], [username], [name], [inActive], [lastModified]) VALUES (N'7331d281-5e93-4687-a7ee-479c44223216', N'Natasha.Kostokanellis', NULL, 0, NULL)
INSERT [dbo].[CFTUserAccess] ([UID], [username], [name], [inActive], [lastModified]) VALUES (N'4177e18a-a9af-4547-9ab1-861957c34d7a', N'kim.raymond', NULL, 0, NULL)
INSERT [dbo].[CFTUserAccess] ([UID], [username], [name], [inActive], [lastModified]) VALUES (N'f5c9c6e0-0277-401c-bd93-8fc19416d874', N'melinda.bruce', NULL, 0, NULL)
INSERT [dbo].[CFTUserAccess] ([UID], [username], [name], [inActive], [lastModified]) VALUES (N'ae1e5f34-b726-4648-99ba-99950903d536', N'jason.schmidt', NULL, 0, NULL)
INSERT [dbo].[CFTUserAccess] ([UID], [username], [name], [inActive], [lastModified]) VALUES (N'cb05592e-4f42-4ed0-a73f-7e0a17516b82', N'annette.cazaly', NULL, 0, NULL)

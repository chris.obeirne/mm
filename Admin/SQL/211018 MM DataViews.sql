USE [Web_JLC]
GO
/****** Object:  View [dbo].[LMM_LinkidToJluV]    Script Date: 18/10/2021 1:12:27 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LMM_LinkidToJluV]'))
DROP VIEW [dbo].[LMM_LinkidToJluV]
GO
/****** Object:  View [dbo].[LMM_JLUsV]    Script Date: 18/10/2021 1:12:27 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LMM_JLUsV]'))
DROP VIEW [dbo].[LMM_JLUsV]
GO
/****** Object:  View [dbo].[LMM_JLUIDV]    Script Date: 18/10/2021 1:12:27 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LMM_JLUIDV]'))
DROP VIEW [dbo].[LMM_JLUIDV]
GO
/****** Object:  View [dbo].[LMM_FY_View]    Script Date: 18/10/2021 1:12:27 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LMM_FY_View]'))
DROP VIEW [dbo].[LMM_FY_View]
GO
/****** Object:  View [dbo].[LMM_DSAView]    Script Date: 18/10/2021 1:12:27 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LMM_DSAView]'))
DROP VIEW [dbo].[LMM_DSAView]
GO
/****** Object:  View [dbo].[LMM_Convert2122Data]    Script Date: 18/10/2021 1:12:27 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LMM_Convert2122Data]'))
DROP VIEW [dbo].[LMM_Convert2122Data]
GO
/****** Object:  View [dbo].[LMM_Convert2021Data]    Script Date: 18/10/2021 1:12:27 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LMM_Convert2021Data]'))
DROP VIEW [dbo].[LMM_Convert2021Data]
GO
/****** Object:  View [dbo].[LMM_Convert1920Data]    Script Date: 18/10/2021 1:12:27 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LMM_Convert1920Data]'))
DROP VIEW [dbo].[LMM_Convert1920Data]
GO
/****** Object:  View [dbo].[LMM_Convert1920Data]    Script Date: 18/10/2021 1:12:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[LMM_Convert1920Data]
AS
SELECT        dbo.LMM_PhasingsEdit1920.ID, '1920' AS fy, dbo.LMM_JLUs.jluId, dbo.LMM_PhasingsEdit1920.DSA, dbo.LMM_PhasingsEdit1920.[2019-07_Bid] AS BidJul, dbo.LMM_PhasingsEdit1920.[2019-08_Bid] AS BidAug, 
                         dbo.LMM_PhasingsEdit1920.[2019-09_Bid] AS BidSep, dbo.LMM_PhasingsEdit1920.[2019-10_Bid] AS BidOct, dbo.LMM_PhasingsEdit1920.[2019-11_Bid] AS BidNov, dbo.LMM_PhasingsEdit1920.[2019-12_Bid] AS BidDec, 
                         dbo.LMM_PhasingsEdit1920.[2020-01_Bid] AS BidJan, dbo.LMM_PhasingsEdit1920.[2020-02_Bid] AS BidFeb, dbo.LMM_PhasingsEdit1920.[2020-03_Bid] AS BidMar, dbo.LMM_PhasingsEdit1920.[2020-04_Bid] AS BidApr, 
                         dbo.LMM_PhasingsEdit1920.[2020-05_Bid] AS BidMay, dbo.LMM_PhasingsEdit1920.[2020-06_Bid] AS BidJun, dbo.LMM_PhasingsEdit1920.[2019-07] AS AuthJul, dbo.LMM_PhasingsEdit1920.[2019-08] AS AuthAug, 
                         dbo.LMM_PhasingsEdit1920.[2019-09] AS AuthSep, dbo.LMM_PhasingsEdit1920.[2019-10] AS AuthOct, dbo.LMM_PhasingsEdit1920.[2019-11] AS AuthNov, dbo.LMM_PhasingsEdit1920.[2019-12] AS AuthDec, 
                         dbo.LMM_PhasingsEdit1920.[2020-01] AS AuthJan, dbo.LMM_PhasingsEdit1920.[2020-02] AS AuthFeb, dbo.LMM_PhasingsEdit1920.[2020-03] AS AuthMar, dbo.LMM_PhasingsEdit1920.[2020-04] AS AuthApr, 
                         dbo.LMM_PhasingsEdit1920.[2020-05] AS AuthMay, dbo.LMM_PhasingsEdit1920.[2020-06] AS AuthJun
FROM            dbo.LMM_PhasingsEdit1920 INNER JOIN
                         dbo.LMM_JLUs ON dbo.LMM_PhasingsEdit1920.JLU = dbo.LMM_JLUs.jlu
GO
/****** Object:  View [dbo].[LMM_Convert2021Data]    Script Date: 18/10/2021 1:12:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[LMM_Convert2021Data]
AS
SELECT        dbo.LMM_PhasingsEdit2021.ID, '2021' AS fy, dbo.LMM_JLUs.jluId, dbo.LMM_PhasingsEdit2021.DSA, dbo.LMM_PhasingsEdit2021.[2020-07_Bid] AS BidJul, dbo.LMM_PhasingsEdit2021.[2020-08_Bid] AS BidAug, 
                         dbo.LMM_PhasingsEdit2021.[2020-09_Bid] AS BidSep, dbo.LMM_PhasingsEdit2021.[2020-10_Bid] AS BidOct, dbo.LMM_PhasingsEdit2021.[2020-11_Bid] AS BidNov, dbo.LMM_PhasingsEdit2021.[2020-12_Bid] AS BidDec, 
                         dbo.LMM_PhasingsEdit2021.[2021-01_Bid] AS BidJan, dbo.LMM_PhasingsEdit2021.[2021-02_Bid] AS BidFeb, dbo.LMM_PhasingsEdit2021.[2021-03_Bid] AS BidMar, dbo.LMM_PhasingsEdit2021.[2021-04_Bid] AS BidApr, 
                         dbo.LMM_PhasingsEdit2021.[2021-05_Bid] AS BidMay, dbo.LMM_PhasingsEdit2021.[2021-06_Bid] AS BidJun, dbo.LMM_PhasingsEdit2021.[2020-07] AS AuthJul, dbo.LMM_PhasingsEdit2021.[2020-08] AS AuthAug, 
                         dbo.LMM_PhasingsEdit2021.[2020-09] AS AuthSep, dbo.LMM_PhasingsEdit2021.[2020-10] AS AuthOct, dbo.LMM_PhasingsEdit2021.[2020-11] AS AuthNov, dbo.LMM_PhasingsEdit2021.[2020-12] AS AuthDec, 
                         dbo.LMM_PhasingsEdit2021.[2021-01] AS AuthJan, dbo.LMM_PhasingsEdit2021.[2021-02] AS AuthFeb, dbo.LMM_PhasingsEdit2021.[2021-03] AS AuthMar, dbo.LMM_PhasingsEdit2021.[2021-04] AS AuthApr, 
                         dbo.LMM_PhasingsEdit2021.[2021-05] AS AuthMay, dbo.LMM_PhasingsEdit2021.[2021-06] AS AuthJun
FROM            dbo.LMM_PhasingsEdit2021 INNER JOIN
                         dbo.LMM_JLUs ON dbo.LMM_PhasingsEdit2021.JLU = dbo.LMM_JLUs.jlu
GO
/****** Object:  View [dbo].[LMM_Convert2122Data]    Script Date: 18/10/2021 1:12:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[LMM_Convert2122Data]
AS
SELECT        dbo.LMM_PhasingsEdit2122.ID, '2122' AS fy, dbo.LMM_JLUs.jluId, dbo.LMM_PhasingsEdit2122.DSA, dbo.LMM_PhasingsEdit2122.[2021-07_Bid] AS BidJul, dbo.LMM_PhasingsEdit2122.[2021-08_Bid] AS BidAug, 
                         dbo.LMM_PhasingsEdit2122.[2021-09_Bid] AS BidSep, dbo.LMM_PhasingsEdit2122.[2021-10_Bid] AS BidOct, dbo.LMM_PhasingsEdit2122.[2021-11_Bid] AS BidNov, dbo.LMM_PhasingsEdit2122.[2021-12_Bid] AS BidDec, 
                         dbo.LMM_PhasingsEdit2122.[2022-01_Bid] AS BidJan, dbo.LMM_PhasingsEdit2122.[2022-02_Bid] AS BidFeb, dbo.LMM_PhasingsEdit2122.[2022-03_Bid] AS BidMar, dbo.LMM_PhasingsEdit2122.[2022-04_Bid] AS BidApr, 
                         dbo.LMM_PhasingsEdit2122.[2022-05_Bid] AS BidMay, dbo.LMM_PhasingsEdit2122.[2022-06_Bid] AS BidJun, dbo.LMM_PhasingsEdit2122.[2021-07] AS AuthJul, dbo.LMM_PhasingsEdit2122.[2021-08] AS AuthAug, 
                         dbo.LMM_PhasingsEdit2122.[2021-09] AS AuthSep, dbo.LMM_PhasingsEdit2122.[2021-10] AS AuthOct, dbo.LMM_PhasingsEdit2122.[2021-11] AS AuthNov, dbo.LMM_PhasingsEdit2122.[2021-12] AS AuthDec, 
                         dbo.LMM_PhasingsEdit2122.[2022-01] AS AuthJan, dbo.LMM_PhasingsEdit2122.[2022-02] AS AuthFeb, dbo.LMM_PhasingsEdit2122.[2022-03] AS AuthMar, dbo.LMM_PhasingsEdit2122.[2022-04] AS AuthApr, 
                         dbo.LMM_PhasingsEdit2122.[2022-05] AS AuthMay, dbo.LMM_PhasingsEdit2122.[2022-06] AS AuthJun
FROM            dbo.LMM_PhasingsEdit2122 INNER JOIN
                         dbo.LMM_JLUs ON dbo.LMM_PhasingsEdit2122.JLU = dbo.LMM_JLUs.jlu
GO
/****** Object:  View [dbo].[LMM_DSAView]    Script Date: 18/10/2021 1:12:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[LMM_DSAView]
AS
SELECT        DSA
FROM            dbo.LMM_PhasingsEdit
GROUP BY DSA
GO
/****** Object:  View [dbo].[LMM_FY_View]    Script Date: 18/10/2021 1:12:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[LMM_FY_View]
AS
SELECT        fy
FROM            dbo.LMM_PhasingsEdit
GROUP BY fy
GO
/****** Object:  View [dbo].[LMM_JLUIDV]    Script Date: 18/10/2021 1:12:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[LMM_JLUIDV]
AS
SELECT        dbo.LMM_PhasingsEdit.jluId, dbo.LMM_PhasingsEdit.JLU, dbo.LMM_JLUs.jluId AS newJId
FROM            dbo.LMM_PhasingsEdit INNER JOIN
                         dbo.LMM_JLUs ON dbo.LMM_PhasingsEdit.JLU = dbo.LMM_JLUs.jlu
GO
/****** Object:  View [dbo].[LMM_JLUsV]    Script Date: 18/10/2021 1:12:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[LMM_JLUsV]
AS
SELECT        NEWID() AS jluId, JLU, RomanCode
FROM            dbo.LMM_PhasingsEdit
GROUP BY JLU, RomanCode
GO
/****** Object:  View [dbo].[LMM_LinkidToJluV]    Script Date: 18/10/2021 1:12:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[LMM_LinkidToJluV]
AS
SELECT        dbo.LMM_PhasingsEdit.JLU, dbo.LMM_PhasingsEdit.jluId, dbo.LMM_JLUs.jluId AS julIdNew
FROM            dbo.LMM_PhasingsEdit INNER JOIN
                         dbo.LMM_JLUs ON dbo.LMM_PhasingsEdit.JLU = dbo.LMM_JLUs.jlu
GO

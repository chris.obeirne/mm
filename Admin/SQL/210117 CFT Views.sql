USE [Web_JLC]
GO
/****** Object:  View [dbo].[CFT_Users_PermissionSummary]    Script Date: 17-Jan-2021 2:47:20 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_Users_PermissionSummary]'))
DROP VIEW [dbo].[CFT_Users_PermissionSummary]
GO
/****** Object:  View [dbo].[CFT_UserDetsForScy]    Script Date: 17-Jan-2021 2:47:20 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_UserDetsForScy]'))
DROP VIEW [dbo].[CFT_UserDetsForScy]
GO
/****** Object:  View [dbo].[CFT_TargetRoleCnt]    Script Date: 17-Jan-2021 2:47:20 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_TargetRoleCnt]'))
DROP VIEW [dbo].[CFT_TargetRoleCnt]
GO
/****** Object:  View [dbo].[CFT_FBListClosed]    Script Date: 17-Jan-2021 2:47:20 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_FBListClosed]'))
DROP VIEW [dbo].[CFT_FBListClosed]
GO
/****** Object:  View [dbo].[CFT_Users]    Script Date: 17-Jan-2021 2:47:20 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_Users]'))
DROP VIEW [dbo].[CFT_Users]
GO
/****** Object:  View [dbo].[CFT_Users]    Script Date: 17-Jan-2021 2:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_Users]
AS
SELECT WebTools_DM.dbo.APPS_Persons.ID, ISNULL(WebTools_DM.dbo.APPS_Persons.Name, '') AS Name, WebTools_DM.dbo.APPS_Persons.Username, ISNULL(WebTools_DM.dbo.APPS_Persons.Email, '') AS Email, 
                  WebTools_DM.dbo.APPS_Persons.Phone, ISNULL(dbo.CFT_Users_Settings.Settings, N'{"darkMode": true, "defaultTableDensity":1,"defaultRowsPerPage":"3", "emailNotifications": true,"navDrawer": true}') AS Settings, 
                  ISNULL(dbo.CFT_Users_Settings.CFT, 0) AS CFT, ISNULL(dbo.CFT_Users_Settings.AltEmail, N'') AS AltEmail, ISNULL(dbo.CFT_Users_Settings.AltPhone, N'') AS AltPhone, ISNULL(dbo.CFT_Users_Settings.UserNotes, N'') AS UserNotes, 
                  CASE WHEN AltPhone = '' OR
                  AltPhone IS NULL THEN isnull(Phone, '') ELSE AltPhone END AS PrefPhone, CASE WHEN AltEmail = '' OR
                  AltEmail IS NULL THEN isnull(Email, '') ELSE AltEmail END AS PrefEmail
FROM     dbo.CFT_Users_Settings RIGHT OUTER JOIN
                  WebTools_DM.dbo.APPS_Persons ON dbo.CFT_Users_Settings.ID = WebTools_DM.dbo.APPS_Persons.ID
GO
/****** Object:  View [dbo].[CFT_FBListClosed]    Script Date: 17-Jan-2021 2:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_FBListClosed]
AS
SELECT dbo.CFT_Feedback.fid, dbo.CFT_Feedback.tid, dbo.CFT_Targets.target, dbo.CFT_Feedback.feedbackType, dbo.CFT_Feedback.feedbackText, dbo.CFT_Feedback.feedbackDateTime, dbo.CFT_NatureGroups.parentGroup, 
                  dbo.CFT_Feedback.gid, dbo.CFT_Feedback.nid, dbo.CFT_Natures.natAbbrev, dbo.CFT_Natures.nature, dbo.CFT_Users.Name AS cdOriginator, dbo.CFT_Feedback.creationUserID, dbo.CFT_Feedback.cdUnit, dbo.CFT_Feedback.cdPhone, 
                  dbo.CFT_Feedback.cdEmail, dbo.CFT_Feedback.fdRodum, dbo.CFT_Feedback.fdEquipNo, dbo.CFT_Feedback.fdEquipAN, dbo.CFT_Feedback.fdMaintReqNo, dbo.CFT_Feedback.fdWorkOrderNo, dbo.CFT_Feedback.fdReqNo, 
                  ISNULL(dbo.CFT_fileCnt.fileCnt, 0) AS fileCnt, ISNULL(dbo.CFT_imageCnt.imageCnt, 0) AS imageCnt, dbo.CFT_Feedback.lastModified, CFT_Users_1.Name AS lastModifiedBy, CFT_Users_2.Name AS closedBy, 
                  dbo.CFT_Feedback.dateClosed, CASE WHEN dateClosed IS NULL THEN 'Open' ELSE 'Closed' END AS status, ISNULL(dbo.CFT_Targets.target, N'') + N', ' + ISNULL(dbo.CFT_Feedback.feedbackType, N'') 
                  + N', ' + ISNULL(dbo.CFT_Natures.nature, N'') + N', ' + ISNULL(dbo.CFT_Feedback.cdOriginator, N'') + N', ' + ISNULL(dbo.CFT_NatureGroups.parentGroup, N'') AS searchField
FROM     dbo.CFT_Feedback LEFT OUTER JOIN
                  dbo.CFT_imageCnt ON dbo.CFT_Feedback.fid = dbo.CFT_imageCnt.fid LEFT OUTER JOIN
                  dbo.CFT_fileCnt ON dbo.CFT_Feedback.fid = dbo.CFT_fileCnt.fid LEFT OUTER JOIN
                  dbo.CFT_Natures ON dbo.CFT_Feedback.nid = dbo.CFT_Natures.nid LEFT OUTER JOIN
                  dbo.CFT_Users AS CFT_Users_2 ON dbo.CFT_Feedback.closingUserID = CFT_Users_2.ID LEFT OUTER JOIN
                  dbo.CFT_Users AS CFT_Users_1 ON dbo.CFT_Feedback.lastModifiedBy = CFT_Users_1.ID LEFT OUTER JOIN
                  dbo.CFT_NatureGroups ON dbo.CFT_Feedback.gid = dbo.CFT_NatureGroups.gid LEFT OUTER JOIN
                  dbo.CFT_Targets ON dbo.CFT_Feedback.tid = dbo.CFT_Targets.tid LEFT OUTER JOIN
                  dbo.CFT_Users ON dbo.CFT_Feedback.creationUserID = dbo.CFT_Users.ID
WHERE  (NOT (dbo.CFT_Feedback.dateClosed IS NULL))
GO
/****** Object:  View [dbo].[CFT_TargetRoleCnt]    Script Date: 17-Jan-2021 2:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_TargetRoleCnt]
AS
SELECT dbo.CFT_Users_Groups_Roles_XRef.User_ID, COUNT(dbo.CFT_Users_Groups_Roles_XRef.Role_ID) AS roleCnt
FROM     dbo.CFT_Users_Groups_Roles_XRef INNER JOIN
                  dbo.CFT_TargetsForRoles ON dbo.CFT_Users_Groups_Roles_XRef.Role_ID = dbo.CFT_TargetsForRoles.roleID
GROUP BY dbo.CFT_Users_Groups_Roles_XRef.User_ID
GO
/****** Object:  View [dbo].[CFT_UserDetsForScy]    Script Date: 17-Jan-2021 2:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_UserDetsForScy]
AS
SELECT WebTools_DM.dbo.APPS_Persons.ID, WebTools_DM.dbo.APPS_Persons.Name, ISNULL(dbo.CFT_Users_Settings.CFT, 0) AS CFT
FROM     dbo.CFT_Users_Settings RIGHT OUTER JOIN
                  WebTools_DM.dbo.APPS_Persons ON dbo.CFT_Users_Settings.ID = WebTools_DM.dbo.APPS_Persons.ID
GO
/****** Object:  View [dbo].[CFT_Users_PermissionSummary]    Script Date: 17-Jan-2021 2:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_Users_PermissionSummary]
AS
SELECT dbo.CFT_UserDetsForScy.ID, ISNULL(dbo.CFT_UserDetsForScy.Name, 'No Name') AS Name, ISNULL(dbo.CFT_UserDetsForScy.CFT, 0) AS CFT, 
                  CAST(MAX(CASE WHEN Role_ID = '0EDB1401-F41E-4A16-B710-7FF9CEF77074' THEN 1 ELSE 0 END) AS bit) AS Reporting, CAST(MAX(CASE WHEN Role_ID = '01AA0953-58DD-4BCB-8ECD-13FFA5B77D69' THEN 1 ELSE 0 END) AS bit) 
                  AS IsAdmin, ISNULL(dbo.CFT_TargetRoleCnt.roleCnt, 0) AS roleCnt
FROM     dbo.CFT_TargetRoleCnt RIGHT OUTER JOIN
                  dbo.CFT_Users_Groups_Roles_XRef ON dbo.CFT_TargetRoleCnt.User_ID = dbo.CFT_Users_Groups_Roles_XRef.User_ID RIGHT OUTER JOIN
                  dbo.CFT_UserDetsForScy ON dbo.CFT_Users_Groups_Roles_XRef.User_ID = dbo.CFT_UserDetsForScy.ID
GROUP BY dbo.CFT_UserDetsForScy.ID, ISNULL(dbo.CFT_UserDetsForScy.Name, 'No Name'), ISNULL(dbo.CFT_TargetRoleCnt.roleCnt, 0), ISNULL(dbo.CFT_UserDetsForScy.CFT, 0)
GO

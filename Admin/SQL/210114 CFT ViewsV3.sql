USE [Web_JLC]
GO
/****** Object:  View [dbo].[CFT_Users_PermissionSummary]    Script Date: 14-Jan-2021 4:51:01 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_Users_PermissionSummary]'))
DROP VIEW [dbo].[CFT_Users_PermissionSummary]
GO
/****** Object:  View [dbo].[CFT_Users_PermissionSummary]    Script Date: 14-Jan-2021 4:51:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_Users_PermissionSummary]
AS
SELECT dbo.CFT_UserDetsForScy.ID, ISNULL(dbo.CFT_UserDetsForScy.Name, 'No Name') AS Name, dbo.CFT_UserDetsForScy.SCB, CAST(MAX(CASE WHEN Role_ID = '0EDB1401-F41E-4A16-B710-7FF9CEF77074' THEN 1 ELSE 0 END) AS bit) 
                  AS Reporting, CAST(MAX(CASE WHEN Role_ID = '01AA0953-58DD-4BCB-8ECD-13FFA5B77D69' THEN 1 ELSE 0 END) AS bit) AS IsAdmin, ISNULL(dbo.CFT_TargetRoleCnt.roleCnt, 0) AS roleCnt
FROM     dbo.CFT_TargetRoleCnt RIGHT OUTER JOIN
                  dbo.CFT_Users_Groups_Roles_XRef ON dbo.CFT_TargetRoleCnt.User_ID = dbo.CFT_Users_Groups_Roles_XRef.User_ID RIGHT OUTER JOIN
                  dbo.CFT_UserDetsForScy ON dbo.CFT_Users_Groups_Roles_XRef.User_ID = dbo.CFT_UserDetsForScy.ID
GROUP BY dbo.CFT_UserDetsForScy.ID, dbo.CFT_UserDetsForScy.SCB, ISNULL(dbo.CFT_UserDetsForScy.Name, 'No Name'), ISNULL(dbo.CFT_TargetRoleCnt.roleCnt, 0)
GO

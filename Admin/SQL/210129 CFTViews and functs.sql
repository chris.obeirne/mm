USE [Web_JLC]
GO
IF  EXISTS (SELECT * FROM sys.fn_listextendedproperty(N'MS_DiagramPaneCount' , N'SCHEMA',N'dbo', N'VIEW',N'CFT_fbotDets', NULL,NULL))
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPaneCount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'CFT_fbotDets'
GO
IF  EXISTS (SELECT * FROM sys.fn_listextendedproperty(N'MS_DiagramPane1' , N'SCHEMA',N'dbo', N'VIEW',N'CFT_fbotDets', NULL,NULL))
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPane1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'CFT_fbotDets'
GO
IF  EXISTS (SELECT * FROM sys.fn_listextendedproperty(N'MS_DiagramPaneCount' , N'SCHEMA',N'dbo', N'VIEW',N'CFT_fbotAsMs', NULL,NULL))
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPaneCount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'CFT_fbotAsMs'
GO
IF  EXISTS (SELECT * FROM sys.fn_listextendedproperty(N'MS_DiagramPane1' , N'SCHEMA',N'dbo', N'VIEW',N'CFT_fbotAsMs', NULL,NULL))
EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPane1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'CFT_fbotAsMs'
GO
/****** Object:  View [dbo].[CFT_fbotDets]    Script Date: 2021/01/29 2:20:00 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_fbotDets]'))
DROP VIEW [dbo].[CFT_fbotDets]
GO
/****** Object:  View [dbo].[CFT_fbotAsMs]    Script Date: 2021/01/29 2:20:00 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_fbotAsMs]'))
DROP VIEW [dbo].[CFT_fbotAsMs]
GO
/****** Object:  UserDefinedFunction [dbo].[UTCtoShort]    Script Date: 2021/01/29 2:20:00 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UTCtoShort]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[UTCtoShort]
GO
/****** Object:  UserDefinedFunction [dbo].[UTCtoLong]    Script Date: 2021/01/29 2:20:00 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UTCtoLong]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[UTCtoLong]
GO
/****** Object:  UserDefinedFunction [dbo].[toMSCfromDT]    Script Date: 2021/01/29 2:20:00 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[toMSCfromDT]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[toMSCfromDT]
GO
/****** Object:  UserDefinedFunction [dbo].[toDbTimeMSC]    Script Date: 2021/01/29 2:20:00 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[toDbTimeMSC]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[toDbTimeMSC]
GO
/****** Object:  UserDefinedFunction [dbo].[toDbTimeMSC]    Script Date: 2021/01/29 2:20:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[toDbTimeMSC] (@unixTimeMSC BIGINT) RETURNS DATETIME
BEGIN
    RETURN DATEADD(MILLISECOND, @unixTimeMSC % 1000, DATEADD(SECOND, @unixTimeMSC / 1000, '19700101'))
END
GO
/****** Object:  UserDefinedFunction [dbo].[toMSCfromDT]    Script Date: 2021/01/29 2:20:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[toMSCfromDT] (@dt datetime) RETURNS BIGINT
BEGIN
    RETURN CAST(DATEDIFF(ss, '19700101', CAST(LEFT (@dt, 11) AS datetime)) AS bigint) * 1000 + CAST(DATEPART(ms, CAST(LEFT (@dt, 11) AS datetime)) AS bigint)
END
GO
/****** Object:  UserDefinedFunction [dbo].[UTCtoLong]    Script Date: 2021/01/29 2:20:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Chris O'Beirne
-- Create date: 29 Jan 21
-- Description:	Convert UTC to dd mmm yyyy hh:mm am/pm
-- =============================================
CREATE FUNCTION [dbo].[UTCtoLong]
(
	-- Add the parameters for the function here
	@dte as datetime
)
RETURNS nvarchar(40)
AS
BEGIN
	--declare @dteLong as nvarchar(40)
	-- Add the T-SQL statements to compute the return value here

	-- Return the result of the function
	RETURN (SELECT CONVERT(nvarchar(30), DATEADD(mi, DATEDIFF(mi, GETUTCDATE(), GETDATE()), @dte), 106) + ' ' + RIGHT(CONVERT(nvarchar(30), DATEADD(mi, DATEDIFF(mi, GETUTCDATE(), GETDATE()), @dte), 100), 7));

END
GO
/****** Object:  UserDefinedFunction [dbo].[UTCtoShort]    Script Date: 2021/01/29 2:20:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Chris O'Beirne
-- Create date: 29 Jan 21
-- Description:	Convert UTC to dd mmm yyyy only
-- =============================================
CREATE FUNCTION [dbo].[UTCtoShort]
(
	-- Add the parameters for the function here
	@dte as datetime
)
RETURNS nvarchar(20)
AS
BEGIN
	--declare @dteLong as nvarchar(40)
	-- Add the T-SQL statements to compute the return value here

	-- Return the result of the function
	RETURN (SELECT CONVERT(nvarchar(20), DATEADD(mi, DATEDIFF(mi, GETUTCDATE(), GETDATE()), @dte), 106));

END
GO
/****** Object:  View [dbo].[CFT_fbotAsMs]    Script Date: 2021/01/29 2:20:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_fbotAsMs]
AS
SELECT dbo.toMSCfromDT(dbo.CFT_Feedback.feedbackDateTime) AS dtms, dbo.CFT_Targets.target, COUNT(dbo.CFT_Feedback.fid) AS fbCnt
FROM     dbo.CFT_Feedback INNER JOIN
                  dbo.CFT_Targets ON dbo.CFT_Feedback.tid = dbo.CFT_Targets.tid
GROUP BY dbo.CFT_Targets.target, dbo.toMSCfromDT(dbo.CFT_Feedback.feedbackDateTime)
GO
/****** Object:  View [dbo].[CFT_fbotDets]    Script Date: 2021/01/29 2:20:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_fbotDets]
AS
SELECT TOP (100) PERCENT dbo.toMSCfromDT(dbo.UTCtoLong(feedbackDateTime)) AS dtms, fid, fbnum, target, feedbackType, feedbackText, feedbackDateTime, parentGroup, nature, cdOriginator, cdUnit, cdPhone, cdEmail, status, 
                  dateClosed
FROM     dbo.CFT_FBListAll
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[30] 2[15] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "CFT_Feedback"
            Begin Extent = 
               Top = 7
               Left = 48
               Bottom = 437
               Right = 455
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "CFT_Targets"
            Begin Extent = 
               Top = 25
               Left = 769
               Bottom = 188
               Right = 980
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1728
         Width = 1200
         Width = 2052
         Width = 1368
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 12048
         Alias = 900
         Table = 1176
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1356
         SortOrder = 1416
         GroupBy = 1350
         Filter = 1356
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'CFT_fbotAsMs'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'CFT_fbotAsMs'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[25] 4[34] 2[13] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -120
         Left = 0
      End
      Begin Tables = 
         Begin Table = "CFT_FBListAll"
            Begin Extent = 
               Top = 162
               Left = 132
               Bottom = 459
               Right = 353
            End
            DisplayFlags = 280
            TopColumn = 1
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 38
         Width = 284
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 2052
         Width = 1872
         Width = 1848
         Width = 1428
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 9144
         Alias = 900
         Table = 1176
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1356
         SortOrder = 1416
         GroupBy = 1350
         Filter = 7752
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'CFT_fbotDets'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'CFT_fbotDets'
GO

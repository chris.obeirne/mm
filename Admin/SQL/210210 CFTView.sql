USE [Web_JLC]
GO
/****** Object:  View [dbo].[CFT_EmailDetails]    Script Date: 2021/02/10 9:04:20 AM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_EmailDetails]'))
DROP VIEW [dbo].[CFT_EmailDetails]
GO
/****** Object:  View [dbo].[CFT_EmailTargetEmail]    Script Date: 2021/02/10 9:04:20 AM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_EmailTargetEmail]'))
DROP VIEW [dbo].[CFT_EmailTargetEmail]
GO
/****** Object:  View [dbo].[CFT_EmailSharedRecipient]    Script Date: 2021/02/10 9:04:20 AM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_EmailSharedRecipient]'))
DROP VIEW [dbo].[CFT_EmailSharedRecipient]
GO
/****** Object:  View [dbo].[CFT_Users]    Script Date: 2021/02/10 9:04:20 AM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_Users]'))
DROP VIEW [dbo].[CFT_Users]
GO
/****** Object:  View [dbo].[CFT_Users]    Script Date: 2021/02/10 9:04:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_Users]
AS
SELECT WebTools_DM.dbo.APPS_Persons.ID, ISNULL(WebTools_DM.dbo.APPS_Persons.Name, '') AS Name, WebTools_DM.dbo.APPS_Persons.Username, ISNULL(WebTools_DM.dbo.APPS_Persons.Email, '') AS Email, 
                  WebTools_DM.dbo.APPS_Persons.Phone, ISNULL(dbo.CFT_Users_Settings.Settings, 
                  N'{"darkMode": true, "defaultTableDensity":1,"defaultRowsPerPage":"3", "emailNotifications": true, "emailNotifyPOC": true,"emailNotifyTarget": true,"emailNotifyShared": true,"navDrawer": true}') AS Settings, 
                  ISNULL(dbo.CFT_Users_Settings.CFT, 0) AS CFT, ISNULL(dbo.CFT_Users_Settings.AltEmail, N'') AS AltEmail, ISNULL(dbo.CFT_Users_Settings.AltPhone, N'') AS AltPhone, ISNULL(dbo.CFT_Users_Settings.UserNotes, N'') AS UserNotes, 
                  CASE WHEN AltPhone = '' OR
                  AltPhone IS NULL THEN isnull(Phone, '') ELSE AltPhone END AS PrefPhone, CASE WHEN AltEmail = '' OR
                  AltEmail IS NULL THEN isnull(Email, '') ELSE AltEmail END AS PrefEmail
FROM     dbo.CFT_Users_Settings RIGHT OUTER JOIN
                  WebTools_DM.dbo.APPS_Persons ON dbo.CFT_Users_Settings.ID = WebTools_DM.dbo.APPS_Persons.ID
GO
/****** Object:  View [dbo].[CFT_EmailSharedRecipient]    Script Date: 2021/02/10 9:04:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_EmailSharedRecipient]
AS
SELECT dbo.CFT_Feedback.fid, dbo.CFT_Feedback.fbnum, dbo.CFT_Users.ID AS sharedUid, dbo.CFT_Users.Name AS sharedName, dbo.CFT_Users.PrefEmail AS sharedEmail
FROM     dbo.CFT_Feedback INNER JOIN
                  dbo.CFT_Feedback_Shared ON dbo.CFT_Feedback.fid = dbo.CFT_Feedback_Shared.fid LEFT OUTER JOIN
                  dbo.CFT_Users ON dbo.CFT_Feedback_Shared.sharedUid = dbo.CFT_Users.ID
WHERE  (CASE WHEN Settings LIKE '%"emailNotifyShared":true%' OR
                  Settings LIKE '%"emailNotifyShared": true%' THEN 1 ELSE 0 END = 1)
GO
/****** Object:  View [dbo].[CFT_EmailTargetEmail]    Script Date: 2021/02/10 9:04:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_EmailTargetEmail]
AS
SELECT dbo.CFT_Feedback.fid, dbo.CFT_Targets.target AS targetName, dbo.CFT_Targets.targetEmail, 1 AS notifyOnTarget, dbo.CFT_Feedback.fbnum, dbo.CFT_Feedback.feedbackType, dbo.CFT_Feedback.feedbackText, 
                  dbo.UTCtoShort(dbo.CFT_Feedback.feedbackDateTime) AS fbDT, ISNULL(dbo.CFT_Targets.target, N'') AS target, ISNULL(dbo.CFT_NatureGroups.parentGroup, N'') AS parentGroup, ISNULL(dbo.CFT_Natures.nature, N'') AS nature, 
                  dbo.CFT_Feedback.creationUserID AS uid, dbo.CFT_Feedback.cdOriginator AS recipientName, ISNULL(dbo.CFT_Feedback.cdAltPhone, N'') AS recipientPhone, ISNULL(dbo.CFT_Feedback.cdEmail, dbo.CFT_Users.PrefEmail) 
                  AS recipientEmail, dbo.CFT_Feedback.cdUnit AS recipientUnit, CASE WHEN dbo.CFT_Users.Settings LIKE '%"emailNotifications":true%' OR
                  dbo.CFT_Users.Settings LIKE '%"emailNotifications": true%' THEN 1 ELSE 0 END AS notifyOn, dbo.CFT_Feedback.altUserId, CASE WHEN CFT_Users_3.Settings LIKE '%"emailNotifyPOC":true%' OR
                  CFT_Users_3.Settings LIKE '%"emailNotifyPOC": true%' THEN 1 ELSE 0 END AS pocNotifyOn, ISNULL(dbo.CFT_Feedback.cdAltOriginator, N'') AS altName, ISNULL(dbo.CFT_Feedback.cdAltUnit, N'') AS altUnit, 
                  ISNULL(dbo.CFT_Feedback.cdAltPhone, N'') AS altPhone, ISNULL(dbo.CFT_Feedback.cdAltEmail, N'') AS altEmail, dbo.UTCtoLong(dbo.CFT_Feedback.lastModified) AS lastModified, CFT_Users_1.Name AS lastModifiedBy, 
                  ISNULL(dbo.UTCtoLong(dbo.CFT_Feedback.dateClosed), N'') AS dateClosed, CFT_Users_2.Name AS closedBy, CASE WHEN dateClosed IS NULL THEN 'Open' ELSE 'Closed' END AS status, ISNULL(dbo.CFT_Feedback.fdRodum, N'') 
                  AS fdRodum, ISNULL(dbo.CFT_Feedback.fdEquipNo, N'') AS fdEquipNo, ISNULL(dbo.CFT_Feedback.fdEquipAN, N'') AS fdEquipAN, ISNULL(dbo.CFT_Feedback.fdMaintReqNo, N'') AS fdMaintReqNo, 
                  ISNULL(dbo.CFT_Feedback.fdWorkOrderNo, N'') AS fdWorkOrderNo, ISNULL(dbo.CFT_Feedback.fdReqNo, N'') AS fdReqNo
FROM     dbo.CFT_Feedback LEFT OUTER JOIN
                  dbo.CFT_Users AS CFT_Users_3 ON dbo.CFT_Feedback.altUserId = CFT_Users_3.ID LEFT OUTER JOIN
                  dbo.CFT_Users AS CFT_Users_2 ON dbo.CFT_Feedback.closingUserID = CFT_Users_2.ID LEFT OUTER JOIN
                  dbo.CFT_Users AS CFT_Users_1 ON dbo.CFT_Feedback.lastModifiedBy = CFT_Users_1.ID LEFT OUTER JOIN
                  dbo.CFT_Users ON dbo.CFT_Feedback.creationUserID = dbo.CFT_Users.ID LEFT OUTER JOIN
                  dbo.CFT_Natures ON dbo.CFT_Feedback.nid = dbo.CFT_Natures.nid LEFT OUTER JOIN
                  dbo.CFT_NatureGroups ON dbo.CFT_Feedback.gid = dbo.CFT_NatureGroups.gid LEFT OUTER JOIN
                  dbo.CFT_Targets ON dbo.CFT_Feedback.tid = dbo.CFT_Targets.tid
GO
/****** Object:  View [dbo].[CFT_EmailDetails]    Script Date: 2021/02/10 9:04:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_EmailDetails]
AS
SELECT dbo.CFT_Feedback.fid, dbo.CFT_Feedback.fbnum, dbo.CFT_Feedback.feedbackType, dbo.CFT_Feedback.feedbackText, dbo.UTCtoShort(dbo.CFT_Feedback.feedbackDateTime) AS fbDT, ISNULL(dbo.CFT_Targets.target, N'') AS target, 
                  ISNULL(dbo.CFT_NatureGroups.parentGroup, N'') AS parentGroup, ISNULL(dbo.CFT_Natures.nature, N'') AS nature, dbo.CFT_Feedback.creationUserID AS uid, dbo.CFT_Feedback.cdOriginator AS recipientName, 
                  ISNULL(dbo.CFT_Feedback.cdAltPhone, N'') AS recipientPhone, ISNULL(dbo.CFT_Feedback.cdEmail, dbo.CFT_Users.PrefEmail) AS recipientEmail, dbo.CFT_Feedback.cdUnit AS recipientUnit, 
                  CASE WHEN dbo.CFT_Users.Settings LIKE '%"emailNotifications":true%' OR
                  dbo.CFT_Users.Settings LIKE '%"emailNotifications": true%' THEN 1 ELSE 0 END AS notifyOn, dbo.CFT_Feedback.altUserId, CASE WHEN CFT_Users_3.Settings LIKE '%"emailNotifyPOC":true%' OR
                  CFT_Users_3.Settings LIKE '%"emailNotifyPOC": true%' THEN 1 ELSE 0 END AS pocNotifyOn, ISNULL(dbo.CFT_Feedback.cdAltOriginator, N'') AS altName, ISNULL(dbo.CFT_Feedback.cdAltUnit, N'') AS altUnit, 
                  ISNULL(dbo.CFT_Feedback.cdAltPhone, N'') AS altPhone, ISNULL(dbo.CFT_Feedback.cdAltEmail, N'') AS altEmail, dbo.UTCtoLong(dbo.CFT_Feedback.lastModified) AS lastModified, CFT_Users_1.Name AS lastModifiedBy, 
                  ISNULL(dbo.UTCtoLong(dbo.CFT_Feedback.dateClosed), N'') AS dateClosed, CFT_Users_2.Name AS closedBy, CASE WHEN dateClosed IS NULL THEN 'Open' ELSE 'Closed' END AS status, ISNULL(dbo.CFT_Feedback.fdRodum, N'') 
                  AS fdRodum, ISNULL(dbo.CFT_Feedback.fdEquipNo, N'') AS fdEquipNo, ISNULL(dbo.CFT_Feedback.fdEquipAN, N'') AS fdEquipAN, ISNULL(dbo.CFT_Feedback.fdMaintReqNo, N'') AS fdMaintReqNo, 
                  ISNULL(dbo.CFT_Feedback.fdWorkOrderNo, N'') AS fdWorkOrderNo, ISNULL(dbo.CFT_Feedback.fdReqNo, N'') AS fdReqNo
FROM     dbo.CFT_Feedback LEFT OUTER JOIN
                  dbo.CFT_Users AS CFT_Users_3 ON dbo.CFT_Feedback.altUserId = CFT_Users_3.ID LEFT OUTER JOIN
                  dbo.CFT_Users AS CFT_Users_2 ON dbo.CFT_Feedback.closingUserID = CFT_Users_2.ID LEFT OUTER JOIN
                  dbo.CFT_Users AS CFT_Users_1 ON dbo.CFT_Feedback.lastModifiedBy = CFT_Users_1.ID LEFT OUTER JOIN
                  dbo.CFT_Users ON dbo.CFT_Feedback.creationUserID = dbo.CFT_Users.ID LEFT OUTER JOIN
                  dbo.CFT_Natures ON dbo.CFT_Feedback.nid = dbo.CFT_Natures.nid LEFT OUTER JOIN
                  dbo.CFT_NatureGroups ON dbo.CFT_Feedback.gid = dbo.CFT_NatureGroups.gid LEFT OUTER JOIN
                  dbo.CFT_Targets ON dbo.CFT_Feedback.tid = dbo.CFT_Targets.tid
GO

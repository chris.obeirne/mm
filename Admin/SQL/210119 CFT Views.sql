USE [Web_JLC]
GO
/****** Object:  View [dbo].[CFT_RptMatrix]    Script Date: 19-Jan-2021 1:25:49 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_RptMatrix]'))
DROP VIEW [dbo].[CFT_RptMatrix]
GO
/****** Object:  View [dbo].[CFT_FBListAll]    Script Date: 19-Jan-2021 1:25:49 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_FBListAll]'))
DROP VIEW [dbo].[CFT_FBListAll]
GO
/****** Object:  View [dbo].[CFT_FBListAll]    Script Date: 19-Jan-2021 1:25:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_FBListAll]
AS
SELECT dbo.CFT_Feedback.fid, dbo.CFT_Feedback.tid, dbo.CFT_Targets.target, dbo.CFT_Feedback.feedbackType, dbo.CFT_Feedback.feedbackText, dbo.CFT_Feedback.feedbackDateTime, dbo.CFT_NatureGroups.parentGroup, 
                  dbo.CFT_Feedback.gid, dbo.CFT_Feedback.nid, dbo.CFT_Natures.natAbbrev, dbo.CFT_Natures.nature, dbo.CFT_Users.Name AS cdOriginator, dbo.CFT_Feedback.creationUserID, dbo.CFT_Feedback.cdUnit, dbo.CFT_Feedback.cdPhone, 
                  dbo.CFT_Feedback.cdEmail, dbo.CFT_Feedback.fdRodum, dbo.CFT_Feedback.fdEquipNo, dbo.CFT_Feedback.fdEquipAN, dbo.CFT_Feedback.fdMaintReqNo, dbo.CFT_Feedback.fdWorkOrderNo, dbo.CFT_Feedback.fdReqNo, 
                  ISNULL(dbo.CFT_fileCnt.fileCnt, 0) AS fileCnt, ISNULL(dbo.CFT_imageCnt.imageCnt, 0) AS imageCnt, dbo.CFT_Feedback.lastModified, CFT_Users_1.Name AS lastModifiedBy, CFT_Users_2.Name AS closedBy, 
                  dbo.CFT_Feedback.dateClosed, CASE WHEN dateClosed IS NULL THEN 'Open' ELSE 'Closed' END AS status, YEAR(dbo.CFT_Feedback.feedbackDateTime) AS filterYear
FROM     dbo.CFT_Feedback LEFT OUTER JOIN
                  dbo.CFT_imageCnt ON dbo.CFT_Feedback.fid = dbo.CFT_imageCnt.fid LEFT OUTER JOIN
                  dbo.CFT_fileCnt ON dbo.CFT_Feedback.fid = dbo.CFT_fileCnt.fid LEFT OUTER JOIN
                  dbo.CFT_Natures ON dbo.CFT_Feedback.nid = dbo.CFT_Natures.nid LEFT OUTER JOIN
                  dbo.CFT_Users AS CFT_Users_2 ON dbo.CFT_Feedback.closingUserID = CFT_Users_2.ID LEFT OUTER JOIN
                  dbo.CFT_Users AS CFT_Users_1 ON dbo.CFT_Feedback.lastModifiedBy = CFT_Users_1.ID LEFT OUTER JOIN
                  dbo.CFT_NatureGroups ON dbo.CFT_Feedback.gid = dbo.CFT_NatureGroups.gid LEFT OUTER JOIN
                  dbo.CFT_Targets ON dbo.CFT_Feedback.tid = dbo.CFT_Targets.tid LEFT OUTER JOIN
                  dbo.CFT_Users ON dbo.CFT_Feedback.creationUserID = dbo.CFT_Users.ID
GO
/****** Object:  View [dbo].[CFT_RptMatrix]    Script Date: 19-Jan-2021 1:25:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_RptMatrix]
AS
SELECT COUNT(dbo.CFT_Feedback.fid) AS fbCnt, dbo.CFT_Feedback.feedbackType, YEAR(dbo.CFT_Feedback.feedbackDateTime) AS fbYear, dbo.CFT_Feedback.tid, dbo.CFT_Targets.target, dbo.CFT_Feedback.gid, 
                  dbo.CFT_NatureGroups.parentGroup, CASE WHEN dateClosed IS NULL THEN 'Open' ELSE 'Closed' END AS status
FROM     dbo.CFT_Feedback LEFT OUTER JOIN
                  dbo.CFT_Targets ON dbo.CFT_Feedback.tid = dbo.CFT_Targets.tid LEFT OUTER JOIN
                  dbo.CFT_NatureGroups ON dbo.CFT_Feedback.gid = dbo.CFT_NatureGroups.gid
GROUP BY YEAR(dbo.CFT_Feedback.feedbackDateTime), dbo.CFT_Feedback.tid, dbo.CFT_Feedback.gid, CASE WHEN dateClosed IS NULL THEN 'Open' ELSE 'Closed' END, dbo.CFT_Targets.target, dbo.CFT_NatureGroups.parentGroup, 
                  dbo.CFT_Feedback.feedbackType
GO

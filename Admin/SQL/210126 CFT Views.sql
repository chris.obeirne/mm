USE [Web_JLC]
GO
/****** Object:  View [dbo].[CFT_selectedFB]    Script Date: 26-Jan-2021 5:30:37 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_selectedFB]'))
DROP VIEW [dbo].[CFT_selectedFB]
GO
/****** Object:  View [dbo].[CFT_feedbackidInsertToSync]    Script Date: 26-Jan-2021 5:30:37 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_feedbackidInsertToSync]'))
DROP VIEW [dbo].[CFT_feedbackidInsertToSync]
GO
/****** Object:  View [dbo].[CFT_feedback_FBnumUpdate]    Script Date: 26-Jan-2021 5:30:37 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_feedback_FBnumUpdate]'))
DROP VIEW [dbo].[CFT_feedback_FBnumUpdate]
GO
/****** Object:  View [dbo].[CFT_FBShared]    Script Date: 26-Jan-2021 5:30:37 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_FBShared]'))
DROP VIEW [dbo].[CFT_FBShared]
GO
/****** Object:  View [dbo].[CFT_FBListClosed]    Script Date: 26-Jan-2021 5:30:37 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_FBListClosed]'))
DROP VIEW [dbo].[CFT_FBListClosed]
GO
/****** Object:  View [dbo].[CFT_FBListAll]    Script Date: 26-Jan-2021 5:30:37 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_FBListAll]'))
DROP VIEW [dbo].[CFT_FBListAll]
GO
/****** Object:  View [dbo].[CFT_FBList]    Script Date: 26-Jan-2021 5:30:37 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_FBList]'))
DROP VIEW [dbo].[CFT_FBList]
GO
/****** Object:  View [dbo].[CFT_FBList]    Script Date: 26-Jan-2021 5:30:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_FBList]
AS
SELECT dbo.CFT_Feedback.fid, dbo.CFT_Feedback.fbnum, dbo.CFT_Feedback.tid, dbo.CFT_Targets.target, dbo.CFT_Feedback.feedbackType, dbo.CFT_Feedback.feedbackText, dbo.CFT_Feedback.feedbackDateTime, 
                  dbo.CFT_NatureGroups.parentGroup, dbo.CFT_Feedback.gid, dbo.CFT_Feedback.nid, dbo.CFT_Natures.natAbbrev, dbo.CFT_Natures.nature, dbo.CFT_Users.Name AS cdOriginator, dbo.CFT_Feedback.creationUserID, 
                  dbo.CFT_Feedback.cdUnit, dbo.CFT_Feedback.cdPhone, dbo.CFT_Feedback.cdEmail, dbo.CFT_Feedback.fdRodum, dbo.CFT_Feedback.fdEquipNo, dbo.CFT_Feedback.fdEquipAN, dbo.CFT_Feedback.fdMaintReqNo, 
                  dbo.CFT_Feedback.fdWorkOrderNo, dbo.CFT_Feedback.fdReqNo, ISNULL(dbo.CFT_fileCnt.fileCnt, 0) AS fileCnt, ISNULL(dbo.CFT_imageCnt.imageCnt, 0) AS imageCnt, dbo.CFT_Feedback.lastModified, 
                  CFT_Users_1.Name AS lastModifiedBy, CFT_Users_2.Name AS closedBy, dbo.CFT_Feedback.dateClosed, CASE WHEN dateClosed IS NULL THEN 'Open' ELSE 'Closed' END AS status, dbo.CFT_Feedback.altUserId, 
                  dbo.CFT_Feedback.cdAltOriginator, dbo.CFT_Feedback.cdAltUnit, dbo.CFT_Feedback.cdAltPhone, dbo.CFT_Feedback.cdAltEmail
FROM     dbo.CFT_Feedback LEFT OUTER JOIN
                  dbo.CFT_imageCnt ON dbo.CFT_Feedback.fid = dbo.CFT_imageCnt.fid LEFT OUTER JOIN
                  dbo.CFT_fileCnt ON dbo.CFT_Feedback.fid = dbo.CFT_fileCnt.fid LEFT OUTER JOIN
                  dbo.CFT_Natures ON dbo.CFT_Feedback.nid = dbo.CFT_Natures.nid LEFT OUTER JOIN
                  dbo.CFT_Users AS CFT_Users_2 ON dbo.CFT_Feedback.closingUserID = CFT_Users_2.ID LEFT OUTER JOIN
                  dbo.CFT_Users AS CFT_Users_1 ON dbo.CFT_Feedback.lastModifiedBy = CFT_Users_1.ID LEFT OUTER JOIN
                  dbo.CFT_NatureGroups ON dbo.CFT_Feedback.gid = dbo.CFT_NatureGroups.gid LEFT OUTER JOIN
                  dbo.CFT_Targets ON dbo.CFT_Feedback.tid = dbo.CFT_Targets.tid LEFT OUTER JOIN
                  dbo.CFT_Users ON dbo.CFT_Feedback.creationUserID = dbo.CFT_Users.ID
GO
/****** Object:  View [dbo].[CFT_FBListAll]    Script Date: 26-Jan-2021 5:30:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_FBListAll]
AS
SELECT dbo.CFT_Feedback.fid, dbo.CFT_Feedback.fbnum, dbo.CFT_Feedback.tid, dbo.CFT_Targets.target, dbo.CFT_Feedback.feedbackType, dbo.CFT_Feedback.feedbackText, dbo.CFT_Feedback.feedbackDateTime, 
                  dbo.CFT_NatureGroups.parentGroup, dbo.CFT_Feedback.gid, dbo.CFT_Feedback.nid, dbo.CFT_Natures.natAbbrev, dbo.CFT_Natures.nature, dbo.CFT_Users.Name AS cdOriginator, dbo.CFT_Feedback.creationUserID, 
                  dbo.CFT_Feedback.cdUnit, dbo.CFT_Feedback.cdPhone, dbo.CFT_Feedback.cdEmail, dbo.CFT_Feedback.fdRodum, dbo.CFT_Feedback.fdEquipNo, dbo.CFT_Feedback.fdEquipAN, dbo.CFT_Feedback.fdMaintReqNo, 
                  dbo.CFT_Feedback.fdWorkOrderNo, dbo.CFT_Feedback.fdReqNo, ISNULL(dbo.CFT_fileCnt.fileCnt, 0) AS fileCnt, ISNULL(dbo.CFT_imageCnt.imageCnt, 0) AS imageCnt, dbo.CFT_Feedback.lastModified, 
                  CFT_Users_1.Name AS lastModifiedBy, CFT_Users_2.Name AS closedBy, dbo.CFT_Feedback.dateClosed, CASE WHEN dateClosed IS NULL THEN 'Open' ELSE 'Closed' END AS status, YEAR(dbo.CFT_Feedback.feedbackDateTime) 
                  AS filterYear, dbo.CFT_Feedback.altUserId, dbo.CFT_Feedback.cdAltOriginator, dbo.CFT_Feedback.cdAltUnit, dbo.CFT_Feedback.cdAltPhone, dbo.CFT_Feedback.cdAltEmail
FROM     dbo.CFT_Feedback LEFT OUTER JOIN
                  dbo.CFT_imageCnt ON dbo.CFT_Feedback.fid = dbo.CFT_imageCnt.fid LEFT OUTER JOIN
                  dbo.CFT_fileCnt ON dbo.CFT_Feedback.fid = dbo.CFT_fileCnt.fid LEFT OUTER JOIN
                  dbo.CFT_Natures ON dbo.CFT_Feedback.nid = dbo.CFT_Natures.nid LEFT OUTER JOIN
                  dbo.CFT_Users AS CFT_Users_2 ON dbo.CFT_Feedback.closingUserID = CFT_Users_2.ID LEFT OUTER JOIN
                  dbo.CFT_Users AS CFT_Users_1 ON dbo.CFT_Feedback.lastModifiedBy = CFT_Users_1.ID LEFT OUTER JOIN
                  dbo.CFT_NatureGroups ON dbo.CFT_Feedback.gid = dbo.CFT_NatureGroups.gid LEFT OUTER JOIN
                  dbo.CFT_Targets ON dbo.CFT_Feedback.tid = dbo.CFT_Targets.tid LEFT OUTER JOIN
                  dbo.CFT_Users ON dbo.CFT_Feedback.creationUserID = dbo.CFT_Users.ID
GO
/****** Object:  View [dbo].[CFT_FBListClosed]    Script Date: 26-Jan-2021 5:30:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_FBListClosed]
AS
SELECT dbo.CFT_Feedback.fid, dbo.CFT_Feedback.fbnum, dbo.CFT_Feedback.tid, dbo.CFT_Targets.target, dbo.CFT_Feedback.feedbackType, dbo.CFT_Feedback.feedbackText, dbo.CFT_Feedback.feedbackDateTime, 
                  dbo.CFT_NatureGroups.parentGroup, dbo.CFT_Feedback.gid, dbo.CFT_Feedback.nid, dbo.CFT_Natures.natAbbrev, dbo.CFT_Natures.nature, dbo.CFT_Users.Name AS cdOriginator, dbo.CFT_Feedback.creationUserID, 
                  dbo.CFT_Feedback.cdUnit, dbo.CFT_Feedback.cdPhone, dbo.CFT_Feedback.cdEmail, dbo.CFT_Feedback.fdRodum, dbo.CFT_Feedback.fdEquipNo, dbo.CFT_Feedback.fdEquipAN, dbo.CFT_Feedback.fdMaintReqNo, 
                  dbo.CFT_Feedback.fdWorkOrderNo, dbo.CFT_Feedback.fdReqNo, ISNULL(dbo.CFT_fileCnt.fileCnt, 0) AS fileCnt, ISNULL(dbo.CFT_imageCnt.imageCnt, 0) AS imageCnt, dbo.CFT_Feedback.lastModified, 
                  CFT_Users_1.Name AS lastModifiedBy, CFT_Users_2.Name AS closedBy, dbo.CFT_Feedback.dateClosed, CASE WHEN dateClosed IS NULL THEN 'Open' ELSE 'Closed' END AS status, ISNULL(dbo.CFT_Targets.target, N'') 
                  + N', ' + ISNULL(dbo.CFT_Feedback.feedbackType, N'') + N', ' + ISNULL(dbo.CFT_Natures.nature, N'') + N', ' + ISNULL(dbo.CFT_Feedback.cdOriginator, N'') + N', ' + ISNULL(dbo.CFT_NatureGroups.parentGroup, N'') AS searchField, 
                  dbo.CFT_Feedback.altUserId, dbo.CFT_Feedback.cdAltOriginator, dbo.CFT_Feedback.cdAltUnit, dbo.CFT_Feedback.cdAltPhone, dbo.CFT_Feedback.cdAltEmail
FROM     dbo.CFT_Feedback LEFT OUTER JOIN
                  dbo.CFT_imageCnt ON dbo.CFT_Feedback.fid = dbo.CFT_imageCnt.fid LEFT OUTER JOIN
                  dbo.CFT_fileCnt ON dbo.CFT_Feedback.fid = dbo.CFT_fileCnt.fid LEFT OUTER JOIN
                  dbo.CFT_Natures ON dbo.CFT_Feedback.nid = dbo.CFT_Natures.nid LEFT OUTER JOIN
                  dbo.CFT_Users AS CFT_Users_2 ON dbo.CFT_Feedback.closingUserID = CFT_Users_2.ID LEFT OUTER JOIN
                  dbo.CFT_Users AS CFT_Users_1 ON dbo.CFT_Feedback.lastModifiedBy = CFT_Users_1.ID LEFT OUTER JOIN
                  dbo.CFT_NatureGroups ON dbo.CFT_Feedback.gid = dbo.CFT_NatureGroups.gid LEFT OUTER JOIN
                  dbo.CFT_Targets ON dbo.CFT_Feedback.tid = dbo.CFT_Targets.tid LEFT OUTER JOIN
                  dbo.CFT_Users ON dbo.CFT_Feedback.creationUserID = dbo.CFT_Users.ID
WHERE  (NOT (dbo.CFT_Feedback.dateClosed IS NULL))
GO
/****** Object:  View [dbo].[CFT_FBShared]    Script Date: 26-Jan-2021 5:30:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_FBShared]
AS
SELECT dbo.CFT_Feedback_Shared.shid, dbo.CFT_Feedback_Shared.fid, dbo.CFT_Feedback_Shared.sharedUid, dbo.CFT_Users.Name AS sharedUser, dbo.CFT_Feedback_Shared.lastModified, CFT_Users_1.Name AS lastModifiedBy
FROM     dbo.CFT_Feedback_Shared LEFT OUTER JOIN
                  dbo.CFT_Users AS CFT_Users_1 ON dbo.CFT_Feedback_Shared.lastModifiedBy = CFT_Users_1.ID LEFT OUTER JOIN
                  dbo.CFT_Users ON dbo.CFT_Feedback_Shared.sharedUid = dbo.CFT_Users.ID
GO
/****** Object:  View [dbo].[CFT_feedback_FBnumUpdate]    Script Date: 26-Jan-2021 5:30:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_feedback_FBnumUpdate]
AS
SELECT fbnum, CAST(RIGHT(YEAR(feedbackDateTime), 2) AS varchar(2)) + RIGHT('0000' + CAST(feedbackID AS VARCHAR(4)), 4) AS newNum
FROM     dbo.CFT_Feedback
WHERE  (NOT (feedbackID IS NULL))
GO
/****** Object:  View [dbo].[CFT_feedbackidInsertToSync]    Script Date: 26-Jan-2021 5:30:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_feedbackidInsertToSync]
AS
SELECT fid, feedbackID
FROM     dbo.CFT_Feedback
WHERE  (NOT (feedbackID IS NULL))
GO
/****** Object:  View [dbo].[CFT_selectedFB]    Script Date: 26-Jan-2021 5:30:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_selectedFB]
AS
SELECT dbo.CFT_Feedback.fid, dbo.CFT_Feedback.fbnum, dbo.CFT_Feedback.tid, dbo.CFT_Targets.target, dbo.CFT_Feedback.feedbackType, dbo.CFT_Feedback.feedbackText, dbo.CFT_Feedback.feedbackDateTime, 
                  dbo.CFT_NatureGroups.parentGroup, dbo.CFT_Feedback.gid, dbo.CFT_Feedback.nid, dbo.CFT_Natures.natAbbrev, dbo.CFT_Natures.nature, dbo.CFT_Users.Name AS cdOriginator, dbo.CFT_Feedback.creationUserID, 
                  dbo.CFT_Feedback.cdUnit, dbo.CFT_Feedback.cdPhone, dbo.CFT_Feedback.cdEmail, dbo.CFT_Feedback.fdRodum, dbo.CFT_Feedback.fdEquipNo, dbo.CFT_Feedback.fdEquipAN, dbo.CFT_Feedback.fdMaintReqNo, 
                  dbo.CFT_Feedback.fdWorkOrderNo, dbo.CFT_Feedback.fdReqNo, dbo.CFT_Feedback.lastModified, CFT_Users_1.Name AS lastModifiedBy, CFT_Users_2.Name AS closedBy, dbo.CFT_Feedback.dateClosed, 
                  CASE WHEN dateClosed IS NULL THEN 'Open' ELSE 'Closed' END AS status, CASE WHEN lockTime IS NULL THEN 0 ELSE 1 END AS locked, CFT_Users_3.Name AS lockedByName, dbo.CFT_FB_Lock.uid AS lockedBy, 
                  dbo.CFT_FB_Lock.lockTime, dbo.CFT_Feedback.altUserId, dbo.CFT_Feedback.cdAltOriginator, dbo.CFT_Feedback.cdAltUnit, dbo.CFT_Feedback.cdAltPhone, dbo.CFT_Feedback.cdAltEmail
FROM     dbo.CFT_Feedback LEFT OUTER JOIN
                  dbo.CFT_FB_Lock ON dbo.CFT_Feedback.fid = dbo.CFT_FB_Lock.fid LEFT OUTER JOIN
                  dbo.CFT_Users AS CFT_Users_3 ON dbo.CFT_FB_Lock.uid = CFT_Users_3.ID LEFT OUTER JOIN
                  dbo.CFT_Natures ON dbo.CFT_Feedback.nid = dbo.CFT_Natures.nid LEFT OUTER JOIN
                  dbo.CFT_Users AS CFT_Users_2 ON dbo.CFT_Feedback.closingUserID = CFT_Users_2.ID LEFT OUTER JOIN
                  dbo.CFT_Users AS CFT_Users_1 ON dbo.CFT_Feedback.lastModifiedBy = CFT_Users_1.ID LEFT OUTER JOIN
                  dbo.CFT_NatureGroups ON dbo.CFT_Feedback.gid = dbo.CFT_NatureGroups.gid LEFT OUTER JOIN
                  dbo.CFT_Targets ON dbo.CFT_Feedback.tid = dbo.CFT_Targets.tid LEFT OUTER JOIN
                  dbo.CFT_Users ON dbo.CFT_Feedback.creationUserID = dbo.CFT_Users.ID
GO

USE [Web_JLC]
GO
/****** Object:  View [dbo].[MM_PhasingsSummed]    Script Date: 18/09/2021 12:30:22 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[MM_PhasingsSummed]'))
DROP VIEW [dbo].[MM_PhasingsSummed]
GO
/****** Object:  View [dbo].[MM_PhaseEdit]    Script Date: 18/09/2021 12:30:22 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[MM_PhaseEdit]'))
DROP VIEW [dbo].[MM_PhaseEdit]
GO
/****** Object:  View [dbo].[MM_DevPhasingBids]    Script Date: 18/09/2021 12:30:22 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[MM_DevPhasingBids]'))
DROP VIEW [dbo].[MM_DevPhasingBids]
GO
/****** Object:  View [dbo].[LMM_Phasings_Data_Bid_RowCnt]    Script Date: 18/09/2021 12:30:22 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LMM_Phasings_Data_Bid_RowCnt]'))
DROP VIEW [dbo].[LMM_Phasings_Data_Bid_RowCnt]
GO
/****** Object:  View [dbo].[LMM_LinkidToJluV]    Script Date: 18/09/2021 12:30:22 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LMM_LinkidToJluV]'))
DROP VIEW [dbo].[LMM_LinkidToJluV]
GO
/****** Object:  View [dbo].[LMM_JLUsV]    Script Date: 18/09/2021 12:30:22 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LMM_JLUsV]'))
DROP VIEW [dbo].[LMM_JLUsV]
GO
/****** Object:  View [dbo].[LMM_JLUIDV]    Script Date: 18/09/2021 12:30:22 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LMM_JLUIDV]'))
DROP VIEW [dbo].[LMM_JLUIDV]
GO
/****** Object:  View [dbo].[LMM_FY_View]    Script Date: 18/09/2021 12:30:22 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LMM_FY_View]'))
DROP VIEW [dbo].[LMM_FY_View]
GO
/****** Object:  View [dbo].[LMM_DSAView]    Script Date: 18/09/2021 12:30:22 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LMM_DSAView]'))
DROP VIEW [dbo].[LMM_DSAView]
GO
/****** Object:  View [dbo].[LMM_Phasing_Data_Bid_View]    Script Date: 18/09/2021 12:30:22 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LMM_Phasing_Data_Bid_View]'))
DROP VIEW [dbo].[LMM_Phasing_Data_Bid_View]
GO
/****** Object:  View [dbo].[LMM_Phasing_Data_Bid_01]    Script Date: 18/09/2021 12:30:22 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LMM_Phasing_Data_Bid_01]'))
DROP VIEW [dbo].[LMM_Phasing_Data_Bid_01]
GO
/****** Object:  View [dbo].[LMM_Phasing_DataRaw_2223]    Script Date: 18/09/2021 12:30:22 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LMM_Phasing_DataRaw_2223]'))
DROP VIEW [dbo].[LMM_Phasing_DataRaw_2223]
GO
/****** Object:  View [dbo].[LMM_Phasing_DataRaw_2122]    Script Date: 18/09/2021 12:30:22 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LMM_Phasing_DataRaw_2122]'))
DROP VIEW [dbo].[LMM_Phasing_DataRaw_2122]
GO
/****** Object:  View [dbo].[LMM_Phasing_DataRaw_2021]    Script Date: 18/09/2021 12:30:22 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LMM_Phasing_DataRaw_2021]'))
DROP VIEW [dbo].[LMM_Phasing_DataRaw_2021]
GO
/****** Object:  View [dbo].[LMM_Phasing_DataRaw_1920]    Script Date: 18/09/2021 12:30:22 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LMM_Phasing_DataRaw_1920]'))
DROP VIEW [dbo].[LMM_Phasing_DataRaw_1920]
GO
/****** Object:  View [dbo].[LMM_PhasingChangeLogView]    Script Date: 18/09/2021 12:30:22 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LMM_PhasingChangeLogView]'))
DROP VIEW [dbo].[LMM_PhasingChangeLogView]
GO
/****** Object:  View [dbo].[MM_JLUDataView]    Script Date: 18/09/2021 12:30:22 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[MM_JLUDataView]'))
DROP VIEW [dbo].[MM_JLUDataView]
GO
/****** Object:  View [dbo].[MM_SecurityPermissionSummary]    Script Date: 18/09/2021 12:30:22 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[MM_SecurityPermissionSummary]'))
DROP VIEW [dbo].[MM_SecurityPermissionSummary]
GO
/****** Object:  View [dbo].[MM_UserDetsForScy]    Script Date: 18/09/2021 12:30:22 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[MM_UserDetsForScy]'))
DROP VIEW [dbo].[MM_UserDetsForScy]
GO
/****** Object:  View [dbo].[LMM_PhasingsEditSummary]    Script Date: 18/09/2021 12:30:22 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LMM_PhasingsEditSummary]'))
DROP VIEW [dbo].[LMM_PhasingsEditSummary]
GO
/****** Object:  View [dbo].[MM_Users]    Script Date: 18/09/2021 12:30:22 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[MM_Users]'))
DROP VIEW [dbo].[MM_Users]
GO
/****** Object:  View [dbo].[MM_Users]    Script Date: 18/09/2021 12:30:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[MM_Users]
AS
SELECT        WebTools_DM.dbo.APPS_Persons.ID, ISNULL(WebTools_DM.dbo.APPS_Persons.Name, '') AS Name, WebTools_DM.dbo.APPS_Persons.Username, ISNULL(WebTools_DM.dbo.APPS_Persons.Email, '') AS Email, 
                         WebTools_DM.dbo.APPS_Persons.Phone, ISNULL(dbo.MM_Users_Settings.darkMode, 0) AS darkMode, ISNULL(dbo.MM_Users_Settings.defaultTableDensity, 0) AS defaultTableDensity, 
                         ISNULL(dbo.MM_Users_Settings.isAdmin, 0) AS isAdmin, ISNULL(dbo.MM_Users_Settings.defaultRowsPerPage, 5) AS defaultRowsPerPage, ISNULL(dbo.MM_Users_Settings.MM, 0) AS MM, 
                         ISNULL(dbo.MM_Users_Settings.AltEmail, N'') AS AltEmail, ISNULL(dbo.MM_Users_Settings.AltPhone, N'') AS AltPhone, ISNULL(dbo.MM_Users_Settings.UserNotes, N'') AS UserNotes, CASE WHEN AltPhone = '' OR
                         AltPhone IS NULL THEN isnull(Phone, '') ELSE AltPhone END AS PrefPhone, CASE WHEN AltEmail = '' OR
                         AltEmail IS NULL THEN isnull(Email, '') ELSE AltEmail END AS PrefEmail, ISNULL(dbo.MM_Users_Settings.visitCount, 0) AS visitCount, ISNULL(dbo.MM_Users_Settings.searchable, 
                         WebTools_DM.dbo.APPS_Persons.Name + N' ' + WebTools_DM.dbo.APPS_Persons.Username + N' ' + WebTools_DM.dbo.APPS_Persons.Phone) AS searchable
FROM            dbo.MM_Users_Settings RIGHT OUTER JOIN
                         WebTools_DM.dbo.APPS_Persons ON dbo.MM_Users_Settings.ID = WebTools_DM.dbo.APPS_Persons.ID
GO
/****** Object:  View [dbo].[LMM_PhasingsEditSummary]    Script Date: 18/09/2021 12:30:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[LMM_PhasingsEditSummary]
AS
SELECT        dbo.LMM_PhasingsEdit.ID, dbo.LMM_PhasingsEdit.jluId, dbo.LMM_JLUs.jlu, dbo.LMM_JLUs.wbs, dbo.LMM_JLUs.romanCode, dbo.LMM_PhasingsEdit.DSA, CAST(ISNULL(dbo.LMM_PhasingsEdit.BidJul, 0) AS int) 
                         + CAST(ISNULL(dbo.LMM_PhasingsEdit.BidAug, 0) AS int) + CAST(ISNULL(dbo.LMM_PhasingsEdit.BidSep, 0) AS int) + CAST(ISNULL(dbo.LMM_PhasingsEdit.BidOct, 0) AS decimal(12, 2)) 
                         + CAST(ISNULL(dbo.LMM_PhasingsEdit.BidNov, 0) AS int) + CAST(ISNULL(dbo.LMM_PhasingsEdit.BidDec, 0) AS int) + CAST(ISNULL(dbo.LMM_PhasingsEdit.BidJan, 0) AS int) + CAST(ISNULL(dbo.LMM_PhasingsEdit.BidFeb, 0) 
                         AS int) + CAST(ISNULL(dbo.LMM_PhasingsEdit.BidMar, 0) AS int) + CAST(ISNULL(dbo.LMM_PhasingsEdit.BidApr, 0) AS int) + CAST(ISNULL(dbo.LMM_PhasingsEdit.BidMay, 0) AS int) 
                         + CAST(ISNULL(dbo.LMM_PhasingsEdit.BidJun, 0) AS int) AS jluOrigBid, CAST(ISNULL(dbo.LMM_PhasingsEdit.AuthJul, 0) AS int) + CAST(ISNULL(dbo.LMM_PhasingsEdit.AuthAug, 0) AS int) 
                         + CAST(ISNULL(dbo.LMM_PhasingsEdit.AuthSep, 0) AS int) + CAST(ISNULL(dbo.LMM_PhasingsEdit.AuthOct, 0) AS decimal(12, 2)) + CAST(ISNULL(dbo.LMM_PhasingsEdit.AuthNov, 0) AS int) 
                         + CAST(ISNULL(dbo.LMM_PhasingsEdit.AuthDec, 0) AS int) + CAST(ISNULL(dbo.LMM_PhasingsEdit.AuthJan, 0) AS int) + CAST(ISNULL(dbo.LMM_PhasingsEdit.AuthFeb, 0) AS int) + CAST(ISNULL(dbo.LMM_PhasingsEdit.AuthMar, 
                         0) AS int) + CAST(ISNULL(dbo.LMM_PhasingsEdit.AuthApr, 0) AS int) + CAST(ISNULL(dbo.LMM_PhasingsEdit.AuthMay, 0) AS int) + CAST(ISNULL(dbo.LMM_PhasingsEdit.AuthJun, 0) AS int) AS approvedBudget, 
                         dbo.LMM_PhasingsEdit.fy, dbo.LMM_PhasingsEdit.lockBids, dbo.LMM_PhasingsEdit.BidJul, dbo.LMM_PhasingsEdit.BidAug, dbo.LMM_PhasingsEdit.BidSep, dbo.LMM_PhasingsEdit.BidOct, dbo.LMM_PhasingsEdit.BidNov, 
                         dbo.LMM_PhasingsEdit.BidDec, dbo.LMM_PhasingsEdit.BidJan, dbo.LMM_PhasingsEdit.BidFeb, dbo.LMM_PhasingsEdit.BidMar, dbo.LMM_PhasingsEdit.BidApr, dbo.LMM_PhasingsEdit.BidMay, 
                         dbo.LMM_PhasingsEdit.BidJun, dbo.LMM_PhasingsEdit.AuthJul, dbo.LMM_PhasingsEdit.AuthAug, dbo.LMM_PhasingsEdit.AuthSep, dbo.LMM_PhasingsEdit.AuthOct, dbo.LMM_PhasingsEdit.AuthNov, 
                         dbo.LMM_PhasingsEdit.AuthDec, dbo.LMM_PhasingsEdit.AuthJan, dbo.LMM_PhasingsEdit.AuthFeb, dbo.LMM_PhasingsEdit.AuthMar, dbo.LMM_PhasingsEdit.AuthApr, dbo.LMM_PhasingsEdit.AuthMay, 
                         dbo.LMM_PhasingsEdit.AuthJun, dbo.LMM_PhasingsEdit.lastModified, ISNULL(dbo.MM_Users.Name, '') AS lastModifiedBy
FROM            dbo.LMM_PhasingsEdit INNER JOIN
                         dbo.LMM_JLUs ON dbo.LMM_PhasingsEdit.jluId = dbo.LMM_JLUs.jluId LEFT OUTER JOIN
                         dbo.MM_Users ON dbo.LMM_PhasingsEdit.lastModifiedBy = dbo.MM_Users.ID
GO
/****** Object:  View [dbo].[MM_UserDetsForScy]    Script Date: 18/09/2021 12:30:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[MM_UserDetsForScy]
AS
SELECT        WebTools_DM.dbo.APPS_Persons.ID, WebTools_DM.dbo.APPS_Persons.Name, ISNULL(dbo.MM_Users_Settings.MM, 0) AS MM
FROM            dbo.MM_Users_Settings RIGHT OUTER JOIN
                         WebTools_DM.dbo.APPS_Persons ON dbo.MM_Users_Settings.ID = WebTools_DM.dbo.APPS_Persons.ID
GO
/****** Object:  View [dbo].[MM_SecurityPermissionSummary]    Script Date: 18/09/2021 12:30:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[MM_SecurityPermissionSummary]
AS
SELECT        dbo.MM_UserDetsForScy.ID, ISNULL(dbo.MM_UserDetsForScy.Name, 'No Name') AS name, ISNULL(dbo.MM_UserDetsForScy.MM, 0) AS mm, 
                         CAST(MAX(CASE WHEN Role_ID = 'FAED53EF-D54A-47FB-99DC-06E5C8AE58BC' THEN 1 ELSE 0 END) AS bit) AS bidManager, CAST(MAX(CASE WHEN Role_ID = '1C2AC56C-DBE7-4727-A173-CD8EFEFCE9E2' THEN 1 ELSE 0 END)
                          AS bit) AS IsAdmin
FROM            dbo.MM_Users_Groups_Roles_XRef RIGHT OUTER JOIN
                         dbo.MM_UserDetsForScy ON dbo.MM_Users_Groups_Roles_XRef.User_ID = dbo.MM_UserDetsForScy.ID
GROUP BY dbo.MM_UserDetsForScy.ID, ISNULL(dbo.MM_UserDetsForScy.Name, 'No Name'), ISNULL(dbo.MM_UserDetsForScy.MM, 0)
GO
/****** Object:  View [dbo].[MM_JLUDataView]    Script Date: 18/09/2021 12:30:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[MM_JLUDataView]
AS
SELECT        dbo.LMM_JLUs.jluId, dbo.LMM_JLUs.jlu, dbo.LMM_JLUs.wbs, dbo.LMM_JLUs.romanCode, dbo.LMM_JLUs.lastModified, ISNULL(dbo.MM_Users.Name, '') AS lastModifiedBy
FROM            dbo.LMM_JLUs LEFT OUTER JOIN
                         dbo.MM_Users ON dbo.LMM_JLUs.lastModifiedBy = dbo.MM_Users.ID
GO
/****** Object:  View [dbo].[LMM_PhasingChangeLogView]    Script Date: 18/09/2021 12:30:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[LMM_PhasingChangeLogView]
AS
SELECT        dbo.LMM_PhasingChangeLog.id, dbo.LMM_PhasingChangeLog.entryDate, dbo.LMM_PhasingChangeLog.entryBy, ISNULL(dbo.MM_Users.Name, '') AS changeBy, dbo.LMM_PhasingChangeLog.change, 
                         dbo.LMM_PhasingChangeLog.fy, dbo.LMM_PhasingChangeLog.dsa, dbo.LMM_PhasingChangeLog.jluId, dbo.LMM_JLUs.jlu
FROM            dbo.LMM_PhasingChangeLog INNER JOIN
                         dbo.LMM_JLUs ON dbo.LMM_PhasingChangeLog.jluId = dbo.LMM_JLUs.jluId LEFT OUTER JOIN
                         dbo.MM_Users ON dbo.LMM_PhasingChangeLog.entryBy = dbo.MM_Users.ID
GO
/****** Object:  View [dbo].[LMM_Phasing_DataRaw_1920]    Script Date: 18/09/2021 12:30:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[LMM_Phasing_DataRaw_1920]
AS
SELECT        AHQ.fy, AHQ.[jluId], AHQ.[jlu], AHQ.[wbs], AHQ.[romanCode], AHQ.[DSA], Bid.JLUBid, AHQ.[AuthBid], AHQ.[INV_MONTH], AHQ.[Division], AHQ.RunDate, AHQ.Extract_Date
FROM            (SELECT        fy, jluId, jlu, wbs, romanCode, CASE WHEN DSA = 'CA04 PMV - OSWIF Op Slipper' THEN 'CA04O' WHEN LEFT(DSA, 5) = 'CIO05' THEN 'CIO05' WHEN LEFT(DSA, 5) = 'CJC05' THEN 'CJC05' WHEN LEFT(DSA, 5) 
                                                    = 'CAF26' THEN 'CAF26' WHEN LEFT(DSA, 5) = 'JHC01' THEN 'JHC1' WHEN LEFT(DSA, 3) = 'UAV' THEN 'CN49' ELSE LEFT(DSA, 4) END AS DSA, AuthBid, INV_MONTH, GETDATE() AS RunDate, GETDATE() 
                                                    AS Extract_Date, CASE WHEN LEFT(DSA, 4) = 'CA42' THEN 'MSD' WHEN LEFT(DSA, 4) IN ('CA31', 'CA32', 'CA34', 'CA36', 'CA40') THEN 'JSD' WHEN LEFT(DSA, 3) = 'UAV' THEN 'HSD' WHEN LEFT(DSA, 5) = 'CJC05' OR
                                                    LEFT(DSA, 5) = 'CIO05' THEN 'JSD' WHEN LEFT(DSA, 5) = 'CAF26' THEN 'ASD' ELSE 'LSD' END AS 'Division'
                          FROM            (SELECT        dbo.LMM_PhasingsEdit.fy, dbo.LMM_PhasingsEdit.jluId, dbo.LMM_JLUs.jlu, dbo.LMM_JLUs.wbs, dbo.LMM_JLUs.romanCode, CAST(dbo.LMM_PhasingsEdit.DSA AS NVARCHAR) AS DSA, 
                                                                              dbo.LMM_PhasingsEdit.AuthJul AS '2019-07', dbo.LMM_PhasingsEdit.AuthAug AS '2019-08', dbo.LMM_PhasingsEdit.AuthSep AS '2019-09', dbo.LMM_PhasingsEdit.AuthOct AS '2019-10', 
                                                                              dbo.LMM_PhasingsEdit.AuthNov AS '2019-11', dbo.LMM_PhasingsEdit.AuthDec AS '2019-12', dbo.LMM_PhasingsEdit.AuthJan AS '2020-01', dbo.LMM_PhasingsEdit.AuthFeb AS '2020-02', 
                                                                              dbo.LMM_PhasingsEdit.AuthMar AS '2020-03', dbo.LMM_PhasingsEdit.AuthApr AS '2020-04', dbo.LMM_PhasingsEdit.AuthMay AS '2020-05', dbo.LMM_PhasingsEdit.AuthJun AS '2020-06'
                                                    FROM            dbo.LMM_PhasingsEdit INNER JOIN
                                                                              dbo.LMM_JLUs ON dbo.LMM_PhasingsEdit.jluId = dbo.LMM_JLUs.jluId
                                                    WHERE        (dbo.LMM_PhasingsEdit.fy = '1920')) AS A UNPIVOT (AuthBid FOR INV_MONTH IN ([2019-07], [2019-08], [2019-09], [2019-10], [2019-11], [2019-12], [2020-01], [2020-02], [2020-03], [2020-04], [2020-05], 
                                                    [2020-06])) AS ahq) AS AHQ INNER JOIN
                             (SELECT        fy, jluId, jlu, wbs, romanCode, CASE WHEN DSA = 'CA04 PMV - OSWIF Op Slipper' THEN 'CA04O' WHEN LEFT(DSA, 5) = 'CIO05' THEN 'CIO05' WHEN LEFT(DSA, 5) = 'CJC05' THEN 'CJC05' WHEN LEFT(DSA, 5) 
                                                         = 'CAF26' THEN 'CAF26' WHEN LEFT(DSA, 5) = 'JHC01' THEN 'JHC1' WHEN LEFT(DSA, 3) = 'UAV' THEN 'CN49' ELSE LEFT(DSA, 4) END AS DSA, JLUBid, INV_MONTH, GETDATE() AS RunDate, GETDATE() 
                                                         AS Extract_Date, CASE WHEN LEFT(DSA, 4) = 'CA42' THEN 'MSD' WHEN LEFT(DSA, 4) IN ('CA31', 'CA32', 'CA34', 'CA36', 'CA40') THEN 'JSD' WHEN LEFT(DSA, 3) = 'UAV' THEN 'HSD' WHEN LEFT(DSA, 5) 
                                                         = 'CJC05' OR
                                                         LEFT(DSA, 5) = 'CIO05' THEN 'JSD' WHEN LEFT(DSA, 5) = 'CAF26' THEN 'ASD' ELSE 'LSD' END AS 'Division'
                               FROM            (SELECT        fy, dbo.LMM_PhasingsEdit.jluId, dbo.LMM_JLUs.jlu, dbo.LMM_JLUs.wbs, dbo.LMM_JLUs.romanCode, CAST([DSA] AS NVARCHAR) AS DSA, BidJul AS '2019-07', [BidAug] AS '2019-08', 
                                                                                   [BidSep] AS '2019-09', [BidOct] AS '2019-10', [BidNov] AS '2019-11', [BidDec] AS '2019-12', [BidJan] AS '2020-01', [BidFeb] AS '2020-02', [BidMar] AS '2020-03', [BidApr] AS '2020-04', 
                                                                                   [BidMay] AS '2020-05', [BidJun] AS '2020-06'
                                                         FROM            [Web_JLC].[dbo].[LMM_PhasingsEdit] INNER JOIN
                                                                                   dbo.LMM_JLUs ON dbo.LMM_PhasingsEdit.jluId = dbo.LMM_JLUs.jluId
                                                         WHERE        (fy = '1920')) AS A UNPIVOT (JLUBid FOR INV_MONTH IN ([2019-07], [2019-08], [2019-09], [2019-10], [2019-11], [2019-12], [2020-01], [2020-02], [2020-03], [2020-04], [2020-05], [2020-06])) AS bid) 
                         AS Bid ON AHQ.JLUid = Bid.JLUid AND AHQ.DSA = Bid.DSA AND AHQ.INV_MONTH = Bid.INV_MONTH
GO
/****** Object:  View [dbo].[LMM_Phasing_DataRaw_2021]    Script Date: 18/09/2021 12:30:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[LMM_Phasing_DataRaw_2021]
AS
SELECT        AHQ.fy, AHQ.[jluId], AHQ.[jlu], AHQ.[wbs], AHQ.[romanCode], AHQ.[DSA], Bid.JLUBid, AHQ.[AuthBid], AHQ.[INV_MONTH], AHQ.[Division], AHQ.RunDate, AHQ.Extract_Date
FROM            (SELECT        fy, jluId, jlu, wbs, romanCode, CASE WHEN DSA = 'CA04 PMV - OSWIF Op Slipper' THEN 'CA04O' WHEN LEFT(DSA, 5) = 'CIO05' THEN 'CIO05' WHEN LEFT(DSA, 5) = 'CJC05' THEN 'CJC05' WHEN LEFT(DSA, 5) 
                                                    = 'CAF26' THEN 'CAF26' WHEN LEFT(DSA, 5) = 'JHC01' THEN 'JHC1' WHEN LEFT(DSA, 3) = 'UAV' THEN 'CN49' ELSE LEFT(DSA, 4) END AS DSA, AuthBid, INV_MONTH, GETDATE() AS RunDate, GETDATE() 
                                                    AS Extract_Date, CASE WHEN LEFT(DSA, 4) = 'CA42' THEN 'MSD' WHEN LEFT(DSA, 4) IN ('CA31', 'CA32', 'CA34', 'CA36', 'CA40') THEN 'JSD' WHEN LEFT(DSA, 3) = 'UAV' THEN 'HSD' WHEN LEFT(DSA, 5) = 'CJC05' OR
                                                    LEFT(DSA, 5) = 'CIO05' THEN 'JSD' WHEN LEFT(DSA, 5) = 'CAF26' THEN 'ASD' ELSE 'LSD' END AS 'Division'
                          FROM            (SELECT        dbo.LMM_PhasingsEdit.fy, dbo.LMM_PhasingsEdit.jluId, dbo.LMM_JLUs.jlu, dbo.LMM_JLUs.wbs, dbo.LMM_JLUs.romanCode, CAST(dbo.LMM_PhasingsEdit.DSA AS NVARCHAR) AS DSA, 
                                                                              dbo.LMM_PhasingsEdit.AuthJul AS '2020-07', dbo.LMM_PhasingsEdit.AuthAug AS '2020-08', dbo.LMM_PhasingsEdit.AuthSep AS '2020-09', dbo.LMM_PhasingsEdit.AuthOct AS '2020-10', 
                                                                              dbo.LMM_PhasingsEdit.AuthNov AS '2020-11', dbo.LMM_PhasingsEdit.AuthDec AS '2020-12', dbo.LMM_PhasingsEdit.AuthJan AS '2021-01', dbo.LMM_PhasingsEdit.AuthFeb AS '2021-02', 
                                                                              dbo.LMM_PhasingsEdit.AuthMar AS '2021-03', dbo.LMM_PhasingsEdit.AuthApr AS '2021-04', dbo.LMM_PhasingsEdit.AuthMay AS '2021-05', dbo.LMM_PhasingsEdit.AuthJun AS '2021-06'
                                                    FROM            dbo.LMM_PhasingsEdit INNER JOIN
                                                                              dbo.LMM_JLUs ON dbo.LMM_PhasingsEdit.jluId = dbo.LMM_JLUs.jluId
                                                    WHERE        (dbo.LMM_PhasingsEdit.fy = '2021')) AS A UNPIVOT (AuthBid FOR INV_MONTH IN ([2020-07], [2020-08], [2020-09], [2020-10], [2020-11], [2020-12], [2021-01], [2021-02], [2021-03], [2021-04], [2021-05], 
                                                    [2021-06])) AS ahq) AS AHQ INNER JOIN
                             (SELECT        fy, jluId, jlu, wbs, romanCode, CASE WHEN DSA = 'CA04 PMV - OSWIF Op Slipper' THEN 'CA04O' WHEN LEFT(DSA, 5) = 'CIO05' THEN 'CIO05' WHEN LEFT(DSA, 5) = 'CJC05' THEN 'CJC05' WHEN LEFT(DSA, 5) 
                                                         = 'CAF26' THEN 'CAF26' WHEN LEFT(DSA, 5) = 'JHC01' THEN 'JHC1' WHEN LEFT(DSA, 3) = 'UAV' THEN 'CN49' ELSE LEFT(DSA, 4) END AS DSA, JLUBid, INV_MONTH, GETDATE() AS RunDate, GETDATE() 
                                                         AS Extract_Date, CASE WHEN LEFT(DSA, 4) = 'CA42' THEN 'MSD' WHEN LEFT(DSA, 4) IN ('CA31', 'CA32', 'CA34', 'CA36', 'CA40') THEN 'JSD' WHEN LEFT(DSA, 3) = 'UAV' THEN 'HSD' WHEN LEFT(DSA, 5) 
                                                         = 'CJC05' OR
                                                         LEFT(DSA, 5) = 'CIO05' THEN 'JSD' WHEN LEFT(DSA, 5) = 'CAF26' THEN 'ASD' ELSE 'LSD' END AS 'Division'
                               FROM            (SELECT        fy, dbo.LMM_PhasingsEdit.jluId, dbo.LMM_JLUs.jlu, dbo.LMM_JLUs.wbs, dbo.LMM_JLUs.romanCode, CAST([DSA] AS NVARCHAR) AS DSA, BidJul AS '2020-07', [BidAug] AS '2020-08', 
                                                                                   [BidSep] AS '2020-09', [BidOct] AS '2020-10', [BidNov] AS '2020-11', [BidDec] AS '2020-12', [BidJan] AS '2021-01', [BidFeb] AS '2021-02', [BidMar] AS '2021-03', [BidApr] AS '2021-04', 
                                                                                   [BidMay] AS '2021-05', [BidJun] AS '2021-06'
                                                         FROM            [Web_JLC].[dbo].[LMM_PhasingsEdit] INNER JOIN
                                                                                   dbo.LMM_JLUs ON dbo.LMM_PhasingsEdit.jluId = dbo.LMM_JLUs.jluId
                                                         WHERE        (fy = '2021')) AS A UNPIVOT (JLUBid FOR INV_MONTH IN ([2020-07], [2020-08], [2020-09], [2020-10], [2020-11], [2020-12], [2021-01], [2021-02], [2021-03], [2021-04], [2021-05], [2021-06])) AS bid) 
                         AS Bid ON AHQ.JLUid = Bid.JLUid AND AHQ.DSA = Bid.DSA AND AHQ.INV_MONTH = Bid.INV_MONTH
GO
/****** Object:  View [dbo].[LMM_Phasing_DataRaw_2122]    Script Date: 18/09/2021 12:30:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[LMM_Phasing_DataRaw_2122]
AS
SELECT        AHQ.fy, AHQ.[jluId], AHQ.[jlu], AHQ.[wbs], AHQ.[romanCode], AHQ.[DSA], Bid.JLUBid, AHQ.[AuthBid], AHQ.[INV_MONTH], AHQ.[Division], AHQ.RunDate, AHQ.Extract_Date
FROM            (SELECT        fy, jluId, jlu, wbs, romanCode, CASE WHEN DSA = 'CA04 PMV - OSWIF Op Slipper' THEN 'CA04O' WHEN LEFT(DSA, 5) = 'CIO05' THEN 'CIO05' WHEN LEFT(DSA, 5) = 'CJC05' THEN 'CJC05' WHEN LEFT(DSA, 5) 
                                                    = 'CAF26' THEN 'CAF26' WHEN LEFT(DSA, 5) = 'JHC01' THEN 'JHC1' WHEN LEFT(DSA, 3) = 'UAV' THEN 'CN49' ELSE LEFT(DSA, 4) END AS DSA, AuthBid, INV_MONTH, GETDATE() AS RunDate, GETDATE() 
                                                    AS Extract_Date, CASE WHEN LEFT(DSA, 4) = 'CA42' THEN 'MSD' WHEN LEFT(DSA, 4) IN ('CA31', 'CA32', 'CA34', 'CA36', 'CA40') THEN 'JSD' WHEN LEFT(DSA, 3) = 'UAV' THEN 'HSD' WHEN LEFT(DSA, 5) = 'CJC05' OR
                                                    LEFT(DSA, 5) = 'CIO05' THEN 'JSD' WHEN LEFT(DSA, 5) = 'CAF26' THEN 'ASD' ELSE 'LSD' END AS 'Division'
                          FROM            (SELECT        dbo.LMM_PhasingsEdit.fy, dbo.LMM_PhasingsEdit.jluId, dbo.LMM_JLUs.jlu, dbo.LMM_JLUs.wbs, dbo.LMM_JLUs.romanCode, CAST(dbo.LMM_PhasingsEdit.DSA AS NVARCHAR) AS DSA, 
                         dbo.LMM_PhasingsEdit.AuthJul AS '2021-07', dbo.LMM_PhasingsEdit.AuthAug AS '2021-08', dbo.LMM_PhasingsEdit.AuthSep AS '2021-09', dbo.LMM_PhasingsEdit.AuthOct AS '2021-10', 
                         dbo.LMM_PhasingsEdit.AuthNov AS '2021-11', dbo.LMM_PhasingsEdit.AuthDec AS '2021-12', dbo.LMM_PhasingsEdit.AuthJan AS '2022-01', dbo.LMM_PhasingsEdit.AuthFeb AS '2022-02', 
                         dbo.LMM_PhasingsEdit.AuthMar AS '2022-03', dbo.LMM_PhasingsEdit.AuthApr AS '2022-04', dbo.LMM_PhasingsEdit.AuthMay AS '2022-05', dbo.LMM_PhasingsEdit.AuthJun AS '2022-06'
FROM            dbo.LMM_PhasingsEdit INNER JOIN
                         dbo.LMM_JLUs ON dbo.LMM_PhasingsEdit.jluId = dbo.LMM_JLUs.jluId
WHERE        (dbo.LMM_PhasingsEdit.fy = '2122')) AS A UNPIVOT (AuthBid FOR INV_MONTH IN ([2021-07], [2021-08], [2021-09], [2021-10], [2021-11], [2021-12], [2022-01], [2022-02], [2022-03], [2022-04], [2022-05], [2022-06])) AS ahq) 
                         AS AHQ INNER JOIN
                             (SELECT        fy, jluId, jlu, wbs, romanCode, CASE WHEN DSA = 'CA04 PMV - OSWIF Op Slipper' THEN 'CA04O' WHEN LEFT(DSA, 5) = 'CIO05' THEN 'CIO05' WHEN LEFT(DSA, 5) = 'CJC05' THEN 'CJC05' WHEN LEFT(DSA, 5) 
                                                         = 'CAF26' THEN 'CAF26' WHEN LEFT(DSA, 5) = 'JHC01' THEN 'JHC1' WHEN LEFT(DSA, 3) = 'UAV' THEN 'CN49' ELSE LEFT(DSA, 4) END AS DSA, JLUBid, INV_MONTH, GETDATE() AS RunDate, GETDATE() 
                                                         AS Extract_Date, CASE WHEN LEFT(DSA, 4) = 'CA42' THEN 'MSD' WHEN LEFT(DSA, 4) IN ('CA31', 'CA32', 'CA34', 'CA36', 'CA40') THEN 'JSD' WHEN LEFT(DSA, 3) = 'UAV' THEN 'HSD' WHEN LEFT(DSA, 5) 
                                                         = 'CJC05' OR
                                                         LEFT(DSA, 5) = 'CIO05' THEN 'JSD' WHEN LEFT(DSA, 5) = 'CAF26' THEN 'ASD' ELSE 'LSD' END AS 'Division'
                               FROM            (SELECT        fy,  dbo.LMM_PhasingsEdit.jluId,  dbo.LMM_JLUs.jlu, dbo.LMM_JLUs.wbs, dbo.LMM_JLUs.romanCode, CAST([DSA] AS NVARCHAR) AS DSA, BidJul AS '2021-07', [BidAug] AS '2021-08', [BidSep] AS '2021-09', [BidOct] AS '2021-10', [BidNov] AS '2021-11', [BidDec] AS '2021-12', 
                                                                                   [BidJan] AS '2022-01', [BidFeb] AS '2022-02', [BidMar] AS '2022-03', [BidApr] AS '2022-04', [BidMay] AS '2022-05', [BidJun] AS '2022-06'
                                                         FROM            [Web_JLC].[dbo].[LMM_PhasingsEdit]  INNER JOIN
                         dbo.LMM_JLUs ON dbo.LMM_PhasingsEdit.jluId = dbo.LMM_JLUs.jluId
                                                         WHERE        (fy = '2122')) AS A UNPIVOT (JLUBid FOR INV_MONTH IN ([2021-07], [2021-08], [2021-09], [2021-10], [2021-11], [2021-12], [2022-01], [2022-02], [2022-03], [2022-04], [2022-05], [2022-06])) AS bid) 
                         AS Bid ON AHQ.JLUid = Bid.JLUid AND AHQ.DSA = Bid.DSA AND AHQ.INV_MONTH = Bid.INV_MONTH
GO
/****** Object:  View [dbo].[LMM_Phasing_DataRaw_2223]    Script Date: 18/09/2021 12:30:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[LMM_Phasing_DataRaw_2223]
AS
SELECT        AHQ.fy, AHQ.[jluId], AHQ.[jlu], AHQ.[wbs], AHQ.[romanCode], AHQ.[DSA], Bid.JLUBid, AHQ.[AuthBid], AHQ.[INV_MONTH], AHQ.[Division], AHQ.RunDate, AHQ.Extract_Date
FROM            (SELECT        fy, jluId, jlu, wbs, romanCode, CASE WHEN DSA = 'CA04 PMV - OSWIF Op Slipper' THEN 'CA04O' WHEN LEFT(DSA, 5) = 'CIO05' THEN 'CIO05' WHEN LEFT(DSA, 5) = 'CJC05' THEN 'CJC05' WHEN LEFT(DSA, 5) 
                                                    = 'CAF26' THEN 'CAF26' WHEN LEFT(DSA, 5) = 'JHC01' THEN 'JHC1' WHEN LEFT(DSA, 3) = 'UAV' THEN 'CN49' ELSE LEFT(DSA, 4) END AS DSA, AuthBid, INV_MONTH, GETDATE() AS RunDate, GETDATE() 
                                                    AS Extract_Date, CASE WHEN LEFT(DSA, 4) = 'CA42' THEN 'MSD' WHEN LEFT(DSA, 4) IN ('CA31', 'CA32', 'CA34', 'CA36', 'CA40') THEN 'JSD' WHEN LEFT(DSA, 3) = 'UAV' THEN 'HSD' WHEN LEFT(DSA, 5) = 'CJC05' OR
                                                    LEFT(DSA, 5) = 'CIO05' THEN 'JSD' WHEN LEFT(DSA, 5) = 'CAF26' THEN 'ASD' ELSE 'LSD' END AS 'Division'
                          FROM            (SELECT        dbo.LMM_PhasingsEdit.fy, dbo.LMM_PhasingsEdit.jluId, dbo.LMM_JLUs.jlu, dbo.LMM_JLUs.wbs, dbo.LMM_JLUs.romanCode, CAST(dbo.LMM_PhasingsEdit.DSA AS NVARCHAR) AS DSA, 
                                                                              dbo.LMM_PhasingsEdit.AuthJul AS '2022-07', dbo.LMM_PhasingsEdit.AuthAug AS '2022-08', dbo.LMM_PhasingsEdit.AuthSep AS '2022-09', dbo.LMM_PhasingsEdit.AuthOct AS '2022-10', 
                                                                              dbo.LMM_PhasingsEdit.AuthNov AS '2022-11', dbo.LMM_PhasingsEdit.AuthDec AS '2022-12', dbo.LMM_PhasingsEdit.AuthJan AS '2023-01', dbo.LMM_PhasingsEdit.AuthFeb AS '2023-02', 
                                                                              dbo.LMM_PhasingsEdit.AuthMar AS '2023-03', dbo.LMM_PhasingsEdit.AuthApr AS '2023-04', dbo.LMM_PhasingsEdit.AuthMay AS '2023-05', dbo.LMM_PhasingsEdit.AuthJun AS '2023-06'
                                                    FROM            dbo.LMM_PhasingsEdit INNER JOIN
                                                                              dbo.LMM_JLUs ON dbo.LMM_PhasingsEdit.jluId = dbo.LMM_JLUs.jluId
                                                    WHERE        (dbo.LMM_PhasingsEdit.fy = '2223')) AS A UNPIVOT (AuthBid FOR INV_MONTH IN ([2022-07], [2022-08], [2022-09], [2022-10], [2022-11], [2022-12], [2023-01], [2023-02], [2023-03], [2023-04], [2023-05], 
                                                    [2023-06])) AS ahq) AS AHQ INNER JOIN
                             (SELECT        fy, jluId, jlu, wbs, romanCode, CASE WHEN DSA = 'CA04 PMV - OSWIF Op Slipper' THEN 'CA04O' WHEN LEFT(DSA, 5) = 'CIO05' THEN 'CIO05' WHEN LEFT(DSA, 5) = 'CJC05' THEN 'CJC05' WHEN LEFT(DSA, 5) 
                                                         = 'CAF26' THEN 'CAF26' WHEN LEFT(DSA, 5) = 'JHC01' THEN 'JHC1' WHEN LEFT(DSA, 3) = 'UAV' THEN 'CN49' ELSE LEFT(DSA, 4) END AS DSA, JLUBid, INV_MONTH, GETDATE() AS RunDate, GETDATE() 
                                                         AS Extract_Date, CASE WHEN LEFT(DSA, 4) = 'CA42' THEN 'MSD' WHEN LEFT(DSA, 4) IN ('CA31', 'CA32', 'CA34', 'CA36', 'CA40') THEN 'JSD' WHEN LEFT(DSA, 3) = 'UAV' THEN 'HSD' WHEN LEFT(DSA, 5) 
                                                         = 'CJC05' OR
                                                         LEFT(DSA, 5) = 'CIO05' THEN 'JSD' WHEN LEFT(DSA, 5) = 'CAF26' THEN 'ASD' ELSE 'LSD' END AS 'Division'
                               FROM            (SELECT        fy, dbo.LMM_PhasingsEdit.jluId, dbo.LMM_JLUs.jlu, dbo.LMM_JLUs.wbs, dbo.LMM_JLUs.romanCode, CAST([DSA] AS NVARCHAR) AS DSA, BidJul AS '2022-07', [BidAug] AS '2022-08', 
                                                                                   [BidSep] AS '2022-09', [BidOct] AS '2022-10', [BidNov] AS '2022-11', [BidDec] AS '2022-12', [BidJan] AS '2023-01', [BidFeb] AS '2023-02', [BidMar] AS '2023-03', [BidApr] AS '2023-04', 
                                                                                   [BidMay] AS '2023-05', [BidJun] AS '2023-06'
                                                         FROM            [Web_JLC].[dbo].[LMM_PhasingsEdit] INNER JOIN
                                                                                   dbo.LMM_JLUs ON dbo.LMM_PhasingsEdit.jluId = dbo.LMM_JLUs.jluId
                                                         WHERE        (fy = '2223')) AS A UNPIVOT (JLUBid FOR INV_MONTH IN ([2022-07], [2022-08], [2022-09], [2022-10], [2022-11], [2022-12], [2023-01], [2023-02], [2023-03], [2023-04], [2023-05], [2023-06])) AS bid) 
                         AS Bid ON AHQ.JLUid = Bid.JLUid AND AHQ.DSA = Bid.DSA AND AHQ.INV_MONTH = Bid.INV_MONTH
GO
/****** Object:  View [dbo].[LMM_Phasing_Data_Bid_01]    Script Date: 18/09/2021 12:30:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[LMM_Phasing_Data_Bid_01]
AS

SELECT [fy]
      ,[jluId]
      ,[jlu]
      ,[wbs]
      ,[romanCode]
      ,[DSA]
      ,[JLUBid]
      ,[AuthBid]
      ,[INV_MONTH]
      ,[Division]
      ,[RunDate]
      ,[Extract_Date]
  FROM [Web_JLC].[dbo].[LMM_Phasing_DataRaw_2122]
  UNION
  SELECT [fy]
      ,[jluId]
      ,[jlu]
      ,[wbs]
      ,[romanCode]
      ,[DSA]
      ,[JLUBid]
      ,[AuthBid]
      ,[INV_MONTH]
      ,[Division]
      ,[RunDate]
      ,[Extract_Date]
  FROM [Web_JLC].[dbo].[LMM_Phasing_DataRaw_1920]
    UNION
  SELECT [fy]
      ,[jluId]
      ,[jlu]
      ,[wbs]
      ,[romanCode]
      ,[DSA]
      ,[JLUBid]
      ,[AuthBid]
      ,[INV_MONTH]
      ,[Division]
      ,[RunDate]
      ,[Extract_Date]
  FROM [Web_JLC].[dbo].[LMM_Phasing_DataRaw_2021]
      UNION
  SELECT [fy]
      ,[jluId]
      ,[jlu]
      ,[wbs]
      ,[romanCode]
      ,[DSA]
      ,[JLUBid]
      ,[AuthBid]
      ,[INV_MONTH]
      ,[Division]
      ,[RunDate]
      ,[Extract_Date]
  FROM [Web_JLC].[dbo].[LMM_Phasing_DataRaw_2223]
GO
/****** Object:  View [dbo].[LMM_Phasing_Data_Bid_View]    Script Date: 18/09/2021 12:30:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[LMM_Phasing_Data_Bid_View]
AS
SELECT        NEWID() AS bId, dbo.LMM_Phasing_Data_Bid_01.fy, dbo.LMM_Phasing_Data_Bid_01.jluId, dbo.LMM_Phasing_Data_Bid_01.jlu, dbo.LMM_Phasing_Data_Bid_01.wbs, dbo.LMM_Phasing_Data_Bid_01.romanCode, 
                         dbo.LMM_Phasing_Data_Bid_01.DSA AS dsaKey, dbo.LMM_ListOfDSA.DSA, dbo.LMM_Phasing_Data_Bid_01.JLUBid, dbo.LMM_Phasing_Data_Bid_01.AuthBid, dbo.LMM_Phasing_Data_Bid_01.INV_MONTH, 
                         dbo.LMM_Phasing_Data_Bid_01.Division, dbo.LMM_Phasing_Data_Bid_01.RunDate, dbo.LMM_Phasing_Data_Bid_01.Extract_Date
FROM            dbo.LMM_Phasing_Data_Bid_01 INNER JOIN
                         dbo.LMM_ListOfDSA ON dbo.LMM_Phasing_Data_Bid_01.DSA = dbo.LMM_ListOfDSA.DSAKey
GO
/****** Object:  View [dbo].[LMM_DSAView]    Script Date: 18/09/2021 12:30:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[LMM_DSAView]
AS
SELECT        DSA
FROM            dbo.LMM_PhasingsEdit
GROUP BY DSA
GO
/****** Object:  View [dbo].[LMM_FY_View]    Script Date: 18/09/2021 12:30:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[LMM_FY_View]
AS
SELECT        fy
FROM            dbo.LMM_PhasingsEdit
GROUP BY fy
GO
/****** Object:  View [dbo].[LMM_JLUIDV]    Script Date: 18/09/2021 12:30:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[LMM_JLUIDV]
AS
SELECT        dbo.LMM_PhasingsEdit.jluId, dbo.LMM_PhasingsEdit.JLU, dbo.LMM_JLUs.jluId AS newJId
FROM            dbo.LMM_PhasingsEdit INNER JOIN
                         dbo.LMM_JLUs ON dbo.LMM_PhasingsEdit.JLU = dbo.LMM_JLUs.jlu
GO
/****** Object:  View [dbo].[LMM_JLUsV]    Script Date: 18/09/2021 12:30:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[LMM_JLUsV]
AS
SELECT        NEWID() AS jluId, JLU, RomanCode
FROM            dbo.LMM_PhasingsEdit
GROUP BY JLU, RomanCode
GO
/****** Object:  View [dbo].[LMM_LinkidToJluV]    Script Date: 18/09/2021 12:30:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[LMM_LinkidToJluV]
AS
SELECT        dbo.LMM_PhasingsEdit.JLU, dbo.LMM_PhasingsEdit.jluId, dbo.LMM_JLUs.jluId AS julIdNew
FROM            dbo.LMM_PhasingsEdit INNER JOIN
                         dbo.LMM_JLUs ON dbo.LMM_PhasingsEdit.JLU = dbo.LMM_JLUs.jlu
GO
/****** Object:  View [dbo].[LMM_Phasings_Data_Bid_RowCnt]    Script Date: 18/09/2021 12:30:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[LMM_Phasings_Data_Bid_RowCnt]
AS
SELECT        COUNT(bId) AS rowCnt
FROM            dbo.LMM_Phasing_Data_Bid
GO
/****** Object:  View [dbo].[MM_DevPhasingBids]    Script Date: 18/09/2021 12:30:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[MM_DevPhasingBids]
AS
SELECT     AHQ.[JLU], AHQ.[DSA], AHQ.FY, AHQ.[INV_MONTH], AHQ.[Division], 
                      AHQ.WBS, AHQ.RunDate, AHQ.Extract_Date
FROM         (SELECT     JLU, CASE WHEN DSA = 'CA04 PMV - OSWIF Op Slipper' THEN 'CA04O' WHEN LEFT(DSA, 5) = 'CIO05' THEN 'CIO05'  WHEN LEFT(DSA, 5) = 'CJC05' THEN 'CJC05' WHEN LEFT(DSA, 5) = 'CAF26' THEN 'CAF26' WHEN LEFT(DSA, 5) 
                                              = 'JHC01' THEN 'JHC1' WHEN LEFT(DSA, 3) = 'UAV' THEN 'CN49' ELSE LEFT(DSA, 4) END AS DSA, FY, 
                                              INV_MONTH, GETDATE() AS RunDate, GETDATE() AS Extract_Date, CASE WHEN LEFT(DSA, 4) = 'CA42' THEN 'MSD' WHEN LEFT(DSA, 4) IN ('CA31', 'CA32', 
                                              'CA34', 'CA36', 'CA40') THEN 'JSD' WHEN LEFT(DSA, 3) = 'UAV' THEN 'HSD' WHEN LEFT(DSA, 5) = 'CJC05' OR
                                              LEFT(DSA, 5) = 'CIO05' THEN 'JSD' WHEN LEFT(DSA, 5) = 'CAF26' THEN 'ASD' ELSE 'LSD' END AS 'Division', WBS
                       FROM          (SELECT        JLU, DSA, fy, AuthJul AS [2021-07], AuthAug AS [2021-08], AuthSep AS [2021-09], AuthOct AS [2021-10], AuthNov AS [2021-11], AuthDec AS [2021-12], AuthJan AS [2022-01], AuthFeb AS [2022-02], AuthMar AS [2022-03], 
                         AuthApr AS [2022-04], AuthMay AS [2022-05], AuthJun AS [2022-06], WBS
FROM            dbo.LMM_PhasingsEdit) AS A UNPIVOT (PHASED_AHQ_Bid FOR INV_MONTH IN ([2021-07], [2021-08], [2021-09], 
                                              [2021-10], [2021-11], [2021-12], [2022-01], [2022-02], [2022-03], [2022-04], [2022-05], [2022-06])) AS ahq) AS AHQ INNER JOIN
                          (SELECT     JLU, CASE WHEN DSA = 'CA04 PMV - OSWIF Op Slipper' THEN 'CA04O' WHEN LEFT(DSA, 5) = 'CIO05' THEN 'CIO05'  WHEN LEFT(DSA, 5) = 'CJC05' THEN 'CJC05' WHEN LEFT(DSA, 5) = 'CAF26' THEN 'CAF26' WHEN LEFT(DSA, 5) 
                                                   = 'JHC01' THEN 'JHC1' WHEN LEFT(DSA, 3) = 'UAV' THEN 'CN49' ELSE LEFT(DSA, 4) END AS DSA, FY,
                                                   INV_MONTH, GETDATE() AS RunDate, GETDATE() AS Extract_Date, CASE WHEN LEFT(DSA, 4) = 'CA42' THEN 'MSD' WHEN LEFT(DSA, 4) IN ('CA31', 
                                                   'CA32', 'CA34', 'CA36', 'CA40') THEN 'JSD' WHEN LEFT(DSA, 3) = 'UAV' THEN 'HSD' WHEN LEFT(DSA, 5) = 'CJC05' OR
                                                   LEFT(DSA, 5) = 'CIO05' THEN 'JSD' WHEN LEFT(DSA, 5) = 'CAF26' THEN 'ASD' ELSE 'LSD' END AS 'Division', WBS
                            FROM          (SELECT        JLU, DSA, fy, BidJul AS [2021-07], BidAug AS [2021-08], BidSep AS [2021-09], BidOct AS [2021-10], BidNov AS [2021-11], BidDec AS [2021-12], BidJan AS [2022-01], BidFeb AS [2022-02], BidMar AS [2022-03], BidApr AS [2022-04], 
                         BidMay AS [2022-05], BidJun AS [2022-06], WBS
FROM            dbo.LMM_PhasingsEdit) AS A UNPIVOT (JLU_Phased_Bid FOR INV_MONTH IN ([2021-07], [2021-08], [2021-09], 
                                                   [2021-10], [2021-11], [2021-12], [2022-01], [2022-02], [2022-03], [2022-04], [2022-05], [2022-06])) AS bid) AS Bid ON AHQ.JLU = Bid.JLU AND 
                      AHQ.DSA = Bid.DSA AND AHQ.INV_MONTH = Bid.INV_MONTH
GO
/****** Object:  View [dbo].[MM_PhaseEdit]    Script Date: 18/09/2021 12:30:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[MM_PhaseEdit]
AS
SELECT        ID, RomanCode, JLU, DSA, [2021-07], [2021-07_Bid], [2021-08], [2021-08_Bid], [2021-09], [2021-09_Bid], [2021-10], [2021-10_Bid], [2021-11], [2021-11_Bid], [2021-12], [2021-12_Bid], [2022-01], [2022-01_Bid], [2022-02], 
                         [2022-02_Bid], [2022-03], [2022-03_Bid], [2022-04], [2022-04_Bid], [2022-05], [2022-05_Bid], [2022-06], [2022-06_Bid], FUND_CODE, AHQ_Budget, JLUOrigBids, Total_Phased
FROM            dbo.LMM_PhasingsEdit2122
GO
/****** Object:  View [dbo].[MM_PhasingsSummed]    Script Date: 18/09/2021 12:30:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[MM_PhasingsSummed]
AS
SELECT        jluId, DSA, fy, ISNULL(BidJul, 0) + ISNULL(BidAug, 0) + ISNULL(BidSep, 0) + ISNULL(BidOct, 0) + ISNULL(BidNov, 0) + ISNULL(BidDec, 0) + ISNULL(BidJan, 0) + ISNULL(BidFeb, 0) + ISNULL(BidMar, 0) + ISNULL(BidApr, 0) 
                         + ISNULL(BidMay, 0) + ISNULL(BidJun, 0) AS bidSummed, ISNULL(AuthJul, 0) + ISNULL(AuthAug, 0) + ISNULL(AuthSep, 0) + ISNULL(AuthOct, 0) + ISNULL(AuthNov, 0) + ISNULL(AuthDec, 0) + ISNULL(AuthJan, 0) + ISNULL(AuthFeb, 
                         0) + ISNULL(AuthMar, 0) + ISNULL(AuthApr, 0) + ISNULL(AuthMay, 0) + ISNULL(AuthJun, 0) AS authSummed
FROM            dbo.LMM_PhasingsEdit
GO

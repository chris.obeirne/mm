USE [Web_JLC]
GO
/****** Object:  Table [dbo].[LMM_PhasingChangeLog]    Script Date: 18/09/2021 1:30:45 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LMM_PhasingChangeLog]') AND type in (N'U'))
DROP TABLE [dbo].[LMM_PhasingChangeLog]
GO
/****** Object:  Table [dbo].[LMM_PhasingChangeLog]    Script Date: 18/09/2021 1:30:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LMM_PhasingChangeLog](
	[id] [uniqueidentifier] NOT NULL,
	[entryDate] [datetime] NULL,
	[entryBy] [uniqueidentifier] NULL,
	[change] [nvarchar](max) NULL,
	[jluId] [uniqueidentifier] NULL,
	[dsa] [nvarchar](255) NULL,
	[fy] [nvarchar](4) NULL,
 CONSTRAINT [PK_PLL_PhasingChangeLog] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

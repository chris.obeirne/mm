use Web_JLC

--insert 2122 data into PhasingsEdit

INSERT INTO LMM_PhasingsEdit
                         (ID, fy, jluId, DSA, BidJul, BidAug, BidSep, BidOct, BidNov, BidDec, BidJan, BidFeb, BidMar, BidApr, BidMay, BidJun, AuthJul, AuthAug, AuthSep, AuthOct, AuthNov, AuthDec, AuthJan, AuthFeb, AuthMar, AuthApr, AuthMay, AuthJun)
SELECT        ID, fy, jluId, DSA, BidJul, BidAug, BidSep, BidOct, BidNov, BidDec, BidJan, BidFeb, BidMar, BidApr, BidMay, BidJun, AuthJul, AuthAug, AuthSep, AuthOct, AuthNov, AuthDec, AuthJan, AuthFeb, AuthMar, AuthApr, AuthMay, 
                         AuthJun
FROM            LMM_Convert2122Data


-- insert 2021 data into Phasing Edit
INSERT INTO LMM_PhasingsEdit
                         (ID, fy, jluId, DSA, BidJul, BidAug, BidSep, BidOct, BidNov, BidDec, BidJan, BidFeb, BidMar, BidApr, BidMay, BidJun, AuthJul, AuthAug, AuthSep, AuthOct, AuthNov, AuthDec, AuthJan, AuthFeb, AuthMar, AuthApr, AuthMay, AuthJun)
SELECT        ID, fy, jluId, DSA, BidJul, BidAug, BidSep, BidOct, BidNov, BidDec, BidJan, BidFeb, BidMar, BidApr, BidMay, BidJun, AuthJul, AuthAug, AuthSep, AuthOct, AuthNov, AuthDec, AuthJan, AuthFeb, AuthMar, AuthApr, AuthMay, 
                         AuthJun
FROM            LMM_Convert2021Data

-- insert 1920 data into Phasing Edit
INSERT INTO LMM_PhasingsEdit
                         (ID, fy, jluId, DSA, BidJul, BidAug, BidSep, BidOct, BidNov, BidDec, BidJan, BidFeb, BidMar, BidApr, BidMay, BidJun, AuthJul, AuthAug, AuthSep, AuthOct, AuthNov, AuthDec, AuthJan, AuthFeb, AuthMar, AuthApr, AuthMay, AuthJun)
SELECT        ID, fy, jluId, DSA, BidJul, BidAug, BidSep, BidOct, BidNov, BidDec, BidJan, BidFeb, BidMar, BidApr, BidMay, BidJun, AuthJul, AuthAug, AuthSep, AuthOct, AuthNov, AuthDec, AuthJan, AuthFeb, AuthMar, AuthApr, AuthMay, 
                         AuthJun
FROM            LMM_Convert1920Data

Set lockbids to true
UPDATE       LMM_PhasingsEdit
SET                lockBids = 1
WHERE        (lockBids IS NULL)
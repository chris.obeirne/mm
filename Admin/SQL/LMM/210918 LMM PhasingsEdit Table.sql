USE [Web_JLC]
GO

/****** Object:  Table [dbo].[LMM_PhasingsEdit]    Script Date: 18/09/2021 8:53:25 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[LMM_PhasingsEdit](
	[ID] [uniqueidentifier] NOT NULL,
	[jluId] [uniqueidentifier] NULL,
	[DSA] [nvarchar](255) NULL,
	[fy] [nvarchar](4) NULL,
	[lockBids] [bit] NULL,
	[BidJul] [int] NULL,
	[BidAug] [int] NULL,
	[BidSep] [int] NULL,
	[BidOct] [int] NULL,
	[BidNov] [int] NULL,
	[BidDec] [int] NULL,
	[BidJan] [int] NULL,
	[BidFeb] [int] NULL,
	[BidMar] [int] NULL,
	[BidApr] [int] NULL,
	[BidMay] [int] NULL,
	[BidJun] [int] NULL,
	[AuthJul] [int] NULL,
	[AuthAug] [int] NULL,
	[AuthSep] [int] NULL,
	[AuthOct] [int] NULL,
	[AuthNov] [int] NULL,
	[AuthDec] [int] NULL,
	[AuthJan] [int] NULL,
	[AuthFeb] [int] NULL,
	[AuthMar] [int] NULL,
	[AuthApr] [int] NULL,
	[AuthMay] [int] NULL,
	[AuthJun] [int] NULL,
	[WBS] [nvarchar](50) NULL,
	[lastModified] [datetime] NULL,
	[lastModifiedBy] [uniqueidentifier] NULL
) ON [PRIMARY]
GO



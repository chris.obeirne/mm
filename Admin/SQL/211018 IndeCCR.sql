USE [Web_JLC]
GO

SET ANSI_PADDING ON
GO

/****** Object:  Index [newIndex]    Script Date: 18/10/2021 1:27:34 PM ******/
CREATE NONCLUSTERED INDEX [newIndex] ON [dbo].[LMM_CostCommReport_TABLE2122]
(
	[RomanCode] ASC,
	[jlu] ASC,
	[DSA] ASC,
	[Division] ASC
)
INCLUDE([Budget],[IHouse_Cost],[Trade_Cost],[YTD_Cost],[Balance_Budget],[Percentage_Exp],[InHouse_Open_Commit],[Trade_Open_Commit],[InHouse_Stale_Commit],[Trade_Stale_Commit],[Total_Open_Commit],[Per_Commit_Exp],[Funds_Remaining_lessActual,OpenCommit,Stale],[Trade_WOPlanning],[InHouse_WOPlanning],[Trade_OpenStale_Planning],[InHouse_OpenStale_Planning],[FundsRemaining_lessWOPlanning]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


USE [Web_JLC]
GO
/****** Object:  View [dbo].[CFT_fbotDets]    Script Date: 2021/02/08 3:11:34 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_fbotDets]'))
DROP VIEW [dbo].[CFT_fbotDets]
GO
/****** Object:  View [dbo].[CFT_fbotAsMs]    Script Date: 2021/02/08 3:11:34 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_fbotAsMs]'))
DROP VIEW [dbo].[CFT_fbotAsMs]
GO
/****** Object:  View [dbo].[CFT_FBList_ForEmailTest]    Script Date: 2021/02/08 3:11:34 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_FBList_ForEmailTest]'))
DROP VIEW [dbo].[CFT_FBList_ForEmailTest]
GO
/****** Object:  View [dbo].[CFT_EmailTargetEmail]    Script Date: 2021/02/08 3:11:34 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_EmailTargetEmail]'))
DROP VIEW [dbo].[CFT_EmailTargetEmail]
GO
/****** Object:  View [dbo].[CFT_EmailFunct]    Script Date: 2021/02/08 3:11:34 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_EmailFunct]'))
DROP VIEW [dbo].[CFT_EmailFunct]
GO
/****** Object:  View [dbo].[CFT_EmailMessagesMasterList]    Script Date: 2021/02/08 3:11:34 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_EmailMessagesMasterList]'))
DROP VIEW [dbo].[CFT_EmailMessagesMasterList]
GO
/****** Object:  View [dbo].[CFT_MiscDefaults]    Script Date: 2021/02/08 3:11:34 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_MiscDefaults]'))
DROP VIEW [dbo].[CFT_MiscDefaults]
GO
/****** Object:  View [dbo].[CFT_EmailDetails]    Script Date: 2021/02/08 3:11:34 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_EmailDetails]'))
DROP VIEW [dbo].[CFT_EmailDetails]
GO
/****** Object:  View [dbo].[CFT_EmailSharedRecipient]    Script Date: 2021/02/08 3:11:34 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_EmailSharedRecipient]'))
DROP VIEW [dbo].[CFT_EmailSharedRecipient]
GO
/****** Object:  View [dbo].[CFT_EmailAltPOC]    Script Date: 2021/02/08 3:11:34 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_EmailAltPOC]'))
DROP VIEW [dbo].[CFT_EmailAltPOC]
GO
/****** Object:  View [dbo].[CFT_fbImageListWithBase64]    Script Date: 2021/02/08 3:11:34 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_fbImageListWithBase64]'))
DROP VIEW [dbo].[CFT_fbImageListWithBase64]
GO
/****** Object:  View [dbo].[CFT_CommentsList]    Script Date: 2021/02/08 3:11:34 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_CommentsList]'))
DROP VIEW [dbo].[CFT_CommentsList]
GO
/****** Object:  View [dbo].[CFT_Users]    Script Date: 2021/02/08 3:11:34 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_Users]'))
DROP VIEW [dbo].[CFT_Users]
GO
/****** Object:  View [dbo].[CFT_Users]    Script Date: 2021/02/08 3:11:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_Users]
AS
SELECT WebTools_DM.dbo.APPS_Persons.ID, ISNULL(WebTools_DM.dbo.APPS_Persons.Name, '') AS Name, WebTools_DM.dbo.APPS_Persons.Username, ISNULL(WebTools_DM.dbo.APPS_Persons.Email, '') AS Email, 
                  WebTools_DM.dbo.APPS_Persons.Phone, ISNULL(dbo.CFT_Users_Settings.Settings, 
                  N'{"darkMode": true, "defaultTableDensity":1,"defaultRowsPerPage":"3", "emailNotifications": true, "emailNotifyPOC": true,"emailNotifyTarget": true,"emailNotifyShared": true,"navDrawer": true}') AS Settings, 
                  ISNULL(dbo.CFT_Users_Settings.CFT, 0) AS CFT, ISNULL(dbo.CFT_Users_Settings.AltEmail, N'') AS AltEmail, ISNULL(dbo.CFT_Users_Settings.AltPhone, N'') AS AltPhone, ISNULL(dbo.CFT_Users_Settings.UserNotes, N'') AS UserNotes, 
                  CASE WHEN AltPhone = '' OR
                  AltPhone IS NULL THEN isnull(Phone, '') ELSE AltPhone END AS PrefPhone, CASE WHEN AltEmail = '' OR
                  AltEmail IS NULL THEN isnull(Email, '') ELSE AltEmail END AS PrefEmail
FROM     dbo.CFT_Users_Settings RIGHT OUTER JOIN
                  WebTools_DM.dbo.APPS_Persons ON dbo.CFT_Users_Settings.ID = WebTools_DM.dbo.APPS_Persons.ID
GO
/****** Object:  View [dbo].[CFT_CommentsList]    Script Date: 2021/02/08 3:11:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_CommentsList]
AS
SELECT dbo.CFT_Comments.cid, dbo.CFT_Comments.fid, dbo.CFT_Comments.commentText, dbo.CFT_Comments.uid, dbo.CFT_Users.Name AS commentBy, dbo.CFT_Comments.commentDateTime, 
                  dbo.UTCtoShort(dbo.CFT_Comments.commentDateTime) AS cmtDT, dbo.CFT_Comments.tid, dbo.CFT_Targets.target, dbo.CFT_Users.PrefPhone, dbo.CFT_Users.PrefEmail, dbo.CFT_Comments.actionRec
FROM     dbo.CFT_Comments LEFT OUTER JOIN
                  dbo.CFT_Targets ON dbo.CFT_Comments.tid = dbo.CFT_Targets.tid LEFT OUTER JOIN
                  dbo.CFT_Users ON dbo.CFT_Comments.uid = dbo.CFT_Users.ID
GO
/****** Object:  View [dbo].[CFT_fbImageListWithBase64]    Script Date: 2021/02/08 3:11:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_fbImageListWithBase64]
AS
SELECT dbo.CFT_fbImages.iid, dbo.CFT_fbImages.fid, dbo.CFT_fbImages.fName, dbo.CFT_fbImages.base64, dbo.CFT_Users.Name AS uploadedBy, dbo.CFT_fbImages.uploadedDate, dbo.CFT_fbImages.fSize, dbo.CFT_fbImages.mimeType, 
                  CASE WHEN mimeType LIKE '%pdf%' THEN 'PDF' WHEN mimeType LIKE '%jpeg%' OR
                  mimetype LIKE '%png%' THEN 'IMAGE' WHEN mimetype LIKE '%doc%' THEN 'DOC' WHEN mimetype LIKE '%XLS%' OR
                  mimetype LIKE '%CSV%' THEN 'Excel' ELSE 'Unknown' END AS fileType
FROM     dbo.CFT_fbImages LEFT OUTER JOIN
                  dbo.CFT_Users ON dbo.CFT_fbImages.uploadedBy = dbo.CFT_Users.ID
GO
/****** Object:  View [dbo].[CFT_EmailAltPOC]    Script Date: 2021/02/08 3:11:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_EmailAltPOC]
AS
SELECT dbo.CFT_Feedback.fid, dbo.CFT_Feedback.fbnum, ISNULL(dbo.CFT_Feedback.cdAltOriginator, N'') AS recipientName, dbo.CFT_Feedback.altUserId AS uid, ISNULL(dbo.CFT_Feedback.cdAltEmail, N'') AS recipientEmail, 
                  CASE WHEN Settings LIKE '%"emailNotifications": true%' THEN 1 ELSE 0 END AS notifyOn
FROM     dbo.CFT_Feedback LEFT OUTER JOIN
                  dbo.CFT_Users ON dbo.CFT_Feedback.altUserId = dbo.CFT_Users.ID
GO
/****** Object:  View [dbo].[CFT_EmailSharedRecipient]    Script Date: 2021/02/08 3:11:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_EmailSharedRecipient]
AS
SELECT dbo.CFT_Feedback.fid, dbo.CFT_Feedback.fbnum, dbo.CFT_Users.ID AS uid, dbo.CFT_Users.Name AS recipientName, dbo.CFT_Users.PrefEmail AS recipientEmail, 
                  CASE WHEN Settings LIKE '%"emailNotifications": true%' THEN 1 ELSE 0 END AS notifyOn
FROM     dbo.CFT_Feedback INNER JOIN
                  dbo.CFT_Feedback_Shared ON dbo.CFT_Feedback.fid = dbo.CFT_Feedback_Shared.fid LEFT OUTER JOIN
                  dbo.CFT_Users ON dbo.CFT_Feedback_Shared.sharedUid = dbo.CFT_Users.ID
GO
/****** Object:  View [dbo].[CFT_EmailDetails]    Script Date: 2021/02/08 3:11:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_EmailDetails]
AS
SELECT dbo.CFT_Feedback.fid, dbo.CFT_Feedback.fbnum, dbo.CFT_Feedback.feedbackType, dbo.CFT_Feedback.feedbackText, dbo.UTCtoShort(dbo.CFT_Feedback.feedbackDateTime) AS fbDT, ISNULL(dbo.CFT_Targets.target, N'') AS target, 
                  ISNULL(dbo.CFT_NatureGroups.parentGroup, N'') AS parentGroup, ISNULL(dbo.CFT_Natures.nature, N'') AS nature, dbo.CFT_Feedback.creationUserID AS uid, dbo.CFT_Feedback.cdOriginator AS recipientName, 
                  ISNULL(dbo.CFT_Feedback.cdAltPhone, N'') AS recipientPhone, ISNULL(dbo.CFT_Feedback.cdEmail, dbo.CFT_Users.PrefEmail) AS recipientEmail, dbo.CFT_Feedback.cdUnit AS recipientUnit, 
                  CASE WHEN dbo.CFT_Users.Settings LIKE '%"emailNotifications":true%' OR
                  dbo.CFT_Users.Settings LIKE '%"emailNotifications": true%' THEN 1 ELSE 0 END AS notifyOn, dbo.CFT_Feedback.altUserId, CASE WHEN CFT_Users_3.Settings LIKE '%"emailNotifyPOC":true%' OR
                  CFT_Users_3.Settings LIKE '%"emailNotifyPOC": true%' THEN 1 ELSE 0 END AS pocNotifyOn, ISNULL(dbo.CFT_Feedback.cdAltOriginator, N'') AS altName, ISNULL(dbo.CFT_Feedback.cdAltUnit, N'') AS altUnit, 
                  ISNULL(dbo.CFT_Feedback.cdAltPhone, N'') AS altPhone, ISNULL(dbo.CFT_Feedback.cdAltEmail, N'') AS altEmail, dbo.UTCtoLong(dbo.CFT_Feedback.lastModified) AS lastModified, CFT_Users_1.Name AS lastModifiedBy, 
                  ISNULL(dbo.UTCtoLong(dbo.CFT_Feedback.dateClosed), N'') AS dateClosed, CFT_Users_2.Name AS closedBy, CASE WHEN dateClosed IS NULL THEN 'Open' ELSE 'Closed' END AS status, ISNULL(dbo.CFT_Feedback.fdRodum, N'') 
                  AS fdRodum, ISNULL(dbo.CFT_Feedback.fdEquipNo, N'') AS fdEquipNo, ISNULL(dbo.CFT_Feedback.fdEquipAN, N'') AS fdEquipAN, ISNULL(dbo.CFT_Feedback.fdMaintReqNo, N'') AS fdMaintReqNo, 
                  ISNULL(dbo.CFT_Feedback.fdWorkOrderNo, N'') AS fdWorkOrderNo, ISNULL(dbo.CFT_Feedback.fdReqNo, N'') AS fdReqNo
FROM     dbo.CFT_Feedback LEFT OUTER JOIN
                  dbo.CFT_Users AS CFT_Users_3 ON dbo.CFT_Feedback.altUserId = CFT_Users_3.ID LEFT OUTER JOIN
                  dbo.CFT_Users AS CFT_Users_2 ON dbo.CFT_Feedback.closingUserID = CFT_Users_2.ID LEFT OUTER JOIN
                  dbo.CFT_Users AS CFT_Users_1 ON dbo.CFT_Feedback.lastModifiedBy = CFT_Users_1.ID LEFT OUTER JOIN
                  dbo.CFT_Users ON dbo.CFT_Feedback.creationUserID = dbo.CFT_Users.ID LEFT OUTER JOIN
                  dbo.CFT_Natures ON dbo.CFT_Feedback.nid = dbo.CFT_Natures.nid LEFT OUTER JOIN
                  dbo.CFT_NatureGroups ON dbo.CFT_Feedback.gid = dbo.CFT_NatureGroups.gid LEFT OUTER JOIN
                  dbo.CFT_Targets ON dbo.CFT_Feedback.tid = dbo.CFT_Targets.tid
GO
/****** Object:  View [dbo].[CFT_MiscDefaults]    Script Date: 2021/02/08 3:11:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_MiscDefaults]
AS
SELECT dbo.CFT_MiscData.id, dbo.CFT_MiscData.keyName, dbo.CFT_MiscData.keyValue, dbo.CFT_MiscData.description, ISNULL(dbo.CFT_Users.Name, '') AS lastModifiedBy, dbo.CFT_MiscData.lastModified
FROM     dbo.CFT_MiscData LEFT OUTER JOIN
                  dbo.CFT_Users ON dbo.CFT_MiscData.lastModifiedBy = dbo.CFT_Users.ID
GO
/****** Object:  View [dbo].[CFT_EmailMessagesMasterList]    Script Date: 2021/02/08 3:11:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_EmailMessagesMasterList]
AS
SELECT dbo.CFT_EmailMessages.emid, dbo.CFT_EmailMessages.action, dbo.CFT_EmailMessages.subj, dbo.CFT_EmailMessages.leadin1, dbo.CFT_EmailMessages.leadin2, dbo.CFT_EmailMessages.description, dbo.CFT_EmailMessages.inactive, 
                  ISNULL(dbo.CFT_Users.Name, '') AS lastModifiedBy, dbo.CFT_EmailMessages.lastModified, dbo.CFT_EmailMessages.type
FROM     dbo.CFT_EmailMessages LEFT OUTER JOIN
                  dbo.CFT_Users ON dbo.CFT_EmailMessages.lastModifiedBy = dbo.CFT_Users.ID
GO
/****** Object:  View [dbo].[CFT_EmailFunct]    Script Date: 2021/02/08 3:11:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_EmailFunct]
AS
SELECT dbo.CFT_MiscData.keyName, dbo.CFT_MiscData.keyValue, dbo.CFT_MiscData.description, dbo.CFT_MiscData.lastModified, ISNULL(dbo.CFT_Users.Name, '') AS lastModifiedBy
FROM     dbo.CFT_MiscData LEFT OUTER JOIN
                  dbo.CFT_Users ON dbo.CFT_MiscData.lastModifiedBy = dbo.CFT_Users.ID
WHERE  (dbo.CFT_MiscData.id = 'F691F51E-6845-4D38-BC75-5A87F209FE4B')
GO
/****** Object:  View [dbo].[CFT_EmailTargetEmail]    Script Date: 2021/02/08 3:11:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_EmailTargetEmail]
AS
SELECT dbo.CFT_Feedback.fid, dbo.CFT_Feedback.fbnum, dbo.CFT_Feedback.tid AS uid, dbo.CFT_Targets.target AS recipientName, dbo.CFT_Targets.targetEmail AS recipientEmail, 1 AS onNotify
FROM     dbo.CFT_Feedback INNER JOIN
                  dbo.CFT_Targets ON dbo.CFT_Feedback.tid = dbo.CFT_Targets.tid
GO
/****** Object:  View [dbo].[CFT_FBList_ForEmailTest]    Script Date: 2021/02/08 3:11:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_FBList_ForEmailTest]
AS
SELECT fid, fbnum + N' Created By ' + ISNULL(cdOriginator, N'') AS fbDetail, fbnum
FROM     dbo.CFT_Feedback
GO
/****** Object:  View [dbo].[CFT_fbotAsMs]    Script Date: 2021/02/08 3:11:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_fbotAsMs]
AS
SELECT dbo.toMSCfromDT(dbo.UTCtoShort(dbo.CFT_Feedback.feedbackDateTime)) AS dtms, dbo.CFT_Targets.target, COUNT(dbo.CFT_Feedback.fid) AS fbCnt
FROM     dbo.CFT_Feedback INNER JOIN
                  dbo.CFT_Targets ON dbo.CFT_Feedback.tid = dbo.CFT_Targets.tid
GROUP BY dbo.CFT_Targets.target, dbo.toMSCfromDT(dbo.UTCtoShort(dbo.CFT_Feedback.feedbackDateTime))
GO
/****** Object:  View [dbo].[CFT_fbotDets]    Script Date: 2021/02/08 3:11:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_fbotDets]
AS
SELECT TOP (100) PERCENT dbo.toMSCfromDT(dbo.UTCtoShort(feedbackDateTime)) AS dtms, feedbackDateTime, fid, fbnum, target, feedbackType, feedbackText, parentGroup, nature, cdOriginator, cdUnit, cdPhone, cdEmail, status, 
                  dateClosed
FROM     dbo.CFT_FBListAll
GO

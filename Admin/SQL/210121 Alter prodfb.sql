use [Web_JLC]

EXEC sp_rename 'CUSTOMDATA_Feedback_prod.creationUserID', 'creationUserIDOld', 'COLUMN';
EXEC sp_rename 'CUSTOMDATA_Feedback_prod.closingUserID', 'closingUserIDOld', 'COLUMN';

ALTER TABLE CUSTOMDATA_Feedback_prod ADD fid uniqueidentifier;
ALTER TABLE CUSTOMDATA_Feedback_prod ADD tid uniqueidentifier;
ALTER TABLE CUSTOMDATA_Feedback_prod ADD gid uniqueidentifier;
ALTER TABLE CUSTOMDATA_Feedback_prod ADD nid uniqueidentifier;
ALTER TABLE CUSTOMDATA_Feedback_prod ADD lastModifiedBy uniqueidentifier;
ALTER TABLE CUSTOMDATA_Feedback_prod ADD lastModified datetime;
ALTER TABLE CUSTOMDATA_Feedback_prod ADD creationUserID uniqueidentifier;
ALTER TABLE CUSTOMDATA_Feedback_prod ADD closingUserID uniqueidentifier;


--create FID
UPDATE CUSTOMDATA_Feedback_prod
SET          fid = NEWID()
WHERE  (fid IS NULL)

--update tid
CREATE VIEW [dbo].[cft_targetUpdate]
AS
SELECT dbo.CUSTOMDATA_Feedback_prod.targetID, dbo.CUSTOMDATA_Feedback_prod.tid AS tidBlank, dbo.CFT_Targets.tid
FROM     dbo.CUSTOMDATA_Feedback_prod LEFT OUTER JOIN
                  dbo.CFT_Targets ON dbo.CUSTOMDATA_Feedback_prod.targetID = dbo.CFT_Targets.tidint

UPDATE cft_targetUpdate
SET          tidBlank = tid

--update gid
CREATE VIEW [dbo].[cft_gidUpdate]
AS
SELECT dbo.CUSTOMDATA_Feedback_prod.natureParentID, dbo.CUSTOMDATA_Feedback_prod.gid, dbo.CFT_NatureGroups.gid AS newgid
FROM     dbo.CUSTOMDATA_Feedback_prod LEFT OUTER JOIN
                  dbo.CFT_NatureGroups ON dbo.CUSTOMDATA_Feedback_prod.natureParentID = dbo.CFT_NatureGroups.gidint
UPDATE cft_gidUpdate
SET           gid = newgid 
--update nid
CREATE VIEW [dbo].[cft_natureUpdate]
AS
SELECT dbo.CUSTOMDATA_Feedback_prod.natureID, dbo.CUSTOMDATA_Feedback_prod.nid, dbo.CFT_Natures.nid AS nidNew
FROM     dbo.CUSTOMDATA_Feedback_prod INNER JOIN
                  dbo.CFT_Natures ON dbo.CUSTOMDATA_Feedback_prod.natureID = dbo.CFT_Natures.natAbbrev
UPDATE cft_natureUpdate
SET        nid = nidNew 

-- update lastMod
CREATE VIEW [dbo].[cft_lastModDateUpdate]
AS
SELECT dateClosed, lastModified, lastModifiedBy, closingUserID
FROM     dbo.CUSTOMDATA_Feedback_prod
WHERE  (NOT (dateClosed IS NULL))

UPDATE cft_lastModDateUpdate
SET           lastModified = dateClosed, lastModifiedBy = closingUserID

--get creationUserID 
CREATE VIEW [dbo].[cft_creationUserIDUpdate]
AS
SELECT dbo.CUSTOMDATA_Feedback_prod.creationUserID, dbo.cft_tempCreationUserIDt.creationUserIDNew
FROM     dbo.CUSTOMDATA_Feedback_prod LEFT OUTER JOIN
                  dbo.cft_tempCreationUserIDt ON dbo.CUSTOMDATA_Feedback_prod.creationUserIDOld = dbo.cft_tempCreationUserIDt.creationUserID

UPDATE cft_creationUserIDUpdate
SET          creationUserID = creationUserIDNew 


-- get closingUserID
CREATE VIEW [dbo].[cft_closingUserIdUpdate]
AS
SELECT dbo.CUSTOMDATA_Feedback_prod.closingUserID, dbo.cft_tempClosingUserIDt.closingUserIDNew
FROM     dbo.CUSTOMDATA_Feedback_prod LEFT OUTER JOIN
                  dbo.cft_tempClosingUserIDt ON dbo.CUSTOMDATA_Feedback_prod.closingUserIDOld = dbo.cft_tempClosingUserIDt.closingUserID

UPDATE cft_closingUserIdUpdate
SET          closingUserID = closingUserIDNew 

-- where can't find user anymore use Kim '82975180-2F21-4C8A-A36D-DE0DA379D2F3'
CREATE VIEW [dbo].[cft_closingUserIDAltUpdate]
AS
SELECT closingUserID
FROM     dbo.CUSTOMDATA_Feedback_prod
WHERE  (NOT (closingUserIDOld IS NULL)) AND (closingUserID IS NULL)

UPDATE cft_closingUserIDAltUpdate
SET          closingUserID = '82975180-2F21-4C8A-A36D-DE0DA379D2F3'

--tip all data into feedback
INSERT INTO CFT_Feedback
                  (fid, feedbackID, tid, targetID, feedbackType, feedbackText, creationUserID, feedbackDateTime, natureParentID, gid, natureID, nid, cdOriginator, cdUnit, cdPhone, cdEmail, fdRodum, fdEquipNo, fdEquipAN, fdMaintReqNo, fdWorkOrderNo, 
                  fdReqNo, lastModifiedBy, lastModified, dateClosed, closingUserID)
SELECT fid, feedbackID, tid, targetID, feedbackType, FeedbackText, creationUserID, feedbackDateTime, natureParentID, gid, natureID, nid, cdOriginator, cdUnit, cdPhone, cdEmail, fdRodum, fdEquipNo, fdEquipAN, fdMaintReqNo, fdWorkOrderNo, 
                  fdReqNo, lastModifiedBy, lastModified, dateClosed, closingUserID
FROM     CUSTOMDATA_Feedback_prod

-- now for comments
--EXEC sp_rename 'Customer_FB_Comments_prod.closingUserID', 'closingUserIDOld', 'COLUMN';

ALTER TABLE Customer_FB_Comments_prod ADD cid uniqueidentifier;
ALTER TABLE Customer_FB_Comments_prod ADD fid uniqueidentifier;
ALTER TABLE Customer_FB_Comments_prod ADD uid uniqueidentifier;
ALTER TABLE Customer_FB_Comments_prod ADD tid uniqueidentifier;
-- create blank CID
UPDATE Customer_FB_Comments_prod
SET          cid = NEWID()
-- link fid
CREATE VIEW [dbo].[cft_comments_UpdateFID]
AS
SELECT dbo.Customer_FB_Comments_prod.fid, dbo.CUSTOMDATA_Feedback_prod.fid AS newFid
FROM     dbo.Customer_FB_Comments_prod INNER JOIN
                  dbo.CUSTOMDATA_Feedback_prod ON dbo.Customer_FB_Comments_prod.feedback_ID = dbo.CUSTOMDATA_Feedback_prod.feedbackID

UPDATE cft_comments_UpdateFID
SET          fid =newFid

--link uid
CREATE VIEW [dbo].[cft_commentUIDFromcreation]
AS
SELECT dbo.Customer_FB_Comments_prod.uid, dbo.cft_tempCreationUserIDt.creationUserIDNew
FROM     dbo.Customer_FB_Comments_prod LEFT OUTER JOIN
                  dbo.cft_tempCreationUserIDt ON dbo.Customer_FB_Comments_prod.UserID = dbo.cft_tempCreationUserIDt.creationUserID

UPDATE cft_commentUIDFromcreation
SET          uid = creationUserIDNew 


CREATE VIEW [dbo].[cft_commentUIDFromclosing]
AS
SELECT dbo.Customer_FB_Comments_prod.uid, dbo.cft_tempClosingUserIDt.closingUserIDNew
FROM     dbo.Customer_FB_Comments_prod LEFT OUTER JOIN
                  dbo.cft_tempClosingUserIDt ON dbo.Customer_FB_Comments_prod.UserID = dbo.cft_tempClosingUserIDt.closingUserID
WHERE  (NOT (dbo.Customer_FB_Comments_prod.UserID IS NULL)) AND (NOT (dbo.cft_tempClosingUserIDt.closingUserID IS NULL))

UPDATE cft_commentUIDFromclosing
SET          uid = closingUserIDNew 
-- link tid
CREATE VIEW [dbo].[cft_commentsUpdateTid]
AS
SELECT dbo.Customer_FB_Comments_prod.tid, dbo.CFT_Targets.tid AS newTid
FROM     dbo.Customer_FB_Comments_prod LEFT OUTER JOIN
                  dbo.CFT_Targets ON dbo.Customer_FB_Comments_prod.commentOwner = dbo.CFT_Targets.tidint

UPDATE cft_commentsUpdateTid
SET          tid = newTid 

-- tip comments into new table

INSERT INTO CFT_Comments
                  (cid, commentID, feedback_ID, fid, commentText, UserID, uid, commentDateTime, commentOwner, tid)
SELECT cid, commentID, feedback_ID, fid, commentText, UserID, uid, commentDateTime, commentOwner, tid
FROM     Customer_FB_Comments_prod
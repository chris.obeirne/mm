USE [Web_JLC]
GO
/****** Object:  Table [dbo].[LMM_JLUs]    Script Date: 26/08/2021 9:32:56 AM ******/
DROP TABLE IF EXISTS [dbo].[LMM_JLUs]
GO
/****** Object:  Table [dbo].[LMM_JLUs]    Script Date: 26/08/2021 9:32:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LMM_JLUs](
	[jluId] [uniqueidentifier] NOT NULL,
	[jlu] [nvarchar](20) NOT NULL,
	[wbs] [nvarchar](50) NULL,
	[romanCode] [nvarchar](30) NULL,
	[lastModified] [datetime] NULL,
	[lastModifiedBy] [uniqueidentifier] NULL,
 CONSTRAINT [PK_LMM_JLUs] PRIMARY KEY CLUSTERED 
(
	[jluId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[LMM_JLUs] ([jluId], [jlu], [wbs], [romanCode], [lastModified], [lastModifiedBy]) VALUES (N'74216446-a2fb-4bf2-97b1-069205f2db3c', N'JLU(SQ)', NULL, N'ZRFM006', NULL, NULL)
INSERT [dbo].[LMM_JLUs] ([jluId], [jlu], [wbs], [romanCode], [lastModified], [lastModifiedBy]) VALUES (N'ee22158f-af62-4573-8759-12d25db70440', N'JLU(NQ)', NULL, N'ZRFM005', NULL, NULL)
INSERT [dbo].[LMM_JLUs] ([jluId], [jlu], [wbs], [romanCode], [lastModified], [lastModifiedBy]) VALUES (N'5a844b23-505d-4cfc-8bd9-493c85d87e7a', N'JLU(E)', NULL, N'ZRFM007', NULL, NULL)
INSERT [dbo].[LMM_JLUs] ([jluId], [jlu], [wbs], [romanCode], [lastModified], [lastModifiedBy]) VALUES (N'ff5d8fa3-143d-4431-9514-6b0e8fcc3255', N'JLU(V)P', NULL, N'ZRFM001.2', NULL, NULL)
INSERT [dbo].[LMM_JLUs] ([jluId], [jlu], [wbs], [romanCode], [lastModified], [lastModifiedBy]) VALUES (N'45d29ee3-5acc-42da-9c14-7bb65db34866', N'JLU(V)B', NULL, N'ZRFM001', NULL, NULL)
INSERT [dbo].[LMM_JLUs] ([jluId], [jlu], [wbs], [romanCode], [lastModified], [lastModifiedBy]) VALUES (N'b611f8e7-49af-4f60-bf89-8cc8171b2d32', N'JLU(V)H', NULL, N'ZRFM001.1', NULL, NULL)
INSERT [dbo].[LMM_JLUs] ([jluId], [jlu], [wbs], [romanCode], [lastModified], [lastModifiedBy]) VALUES (N'bf303465-3eae-4a9a-9f94-a686f5b709bc', N'JLU(N)', NULL, N'ZRFM003', NULL, NULL)
INSERT [dbo].[LMM_JLUs] ([jluId], [jlu], [wbs], [romanCode], [lastModified], [lastModifiedBy]) VALUES (N'c4de319f-07be-42e6-9bb9-c820480c05a3', N'JLU(W)', NULL, N'ZRFM004', NULL, NULL)
INSERT [dbo].[LMM_JLUs] ([jluId], [jlu], [wbs], [romanCode], [lastModified], [lastModifiedBy]) VALUES (N'747f997c-db86-45c1-a2ca-ea0986b04082', N'SCBHQ', NULL, N'ZRFM000', NULL, NULL)
INSERT [dbo].[LMM_JLUs] ([jluId], [jlu], [wbs], [romanCode], [lastModified], [lastModifiedBy]) VALUES (N'7e42041e-85ea-4cb1-acba-fdfd1c29d4e4', N'JLU(S)', NULL, N'ZRFM002', NULL, NULL)
GO

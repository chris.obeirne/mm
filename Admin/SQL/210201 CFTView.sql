USE [Web_JLC]
GO
/****** Object:  View [dbo].[CFT_fbotDets]    Script Date: 2021/02/01 4:10:17 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_fbotDets]'))
DROP VIEW [dbo].[CFT_fbotDets]
GO
/****** Object:  View [dbo].[CFT_fbotAsMs]    Script Date: 2021/02/01 4:10:17 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_fbotAsMs]'))
DROP VIEW [dbo].[CFT_fbotAsMs]
GO
/****** Object:  View [dbo].[CFT_fbImageListWithBase64]    Script Date: 2021/02/01 4:10:17 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_fbImageListWithBase64]'))
DROP VIEW [dbo].[CFT_fbImageListWithBase64]
GO
/****** Object:  View [dbo].[CFT_fbImageListWithBase64]    Script Date: 2021/02/01 4:10:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_fbImageListWithBase64]
AS
SELECT dbo.CFT_fbImages.iid, dbo.CFT_fbImages.fid, dbo.CFT_fbImages.fName, dbo.CFT_fbImages.base64, dbo.CFT_Users.Name AS uploadedBy, dbo.CFT_fbImages.uploadedDate, dbo.CFT_fbImages.fSize, dbo.CFT_fbImages.mimeType, 
                  CASE WHEN mimeType LIKE '%pdf%' THEN 'PDF' WHEN mimeType LIKE '%jpeg%' OR
                  mimetype LIKE '%png%' THEN 'IMAGE' WHEN mimetype LIKE '%doc%' THEN 'DOC' WHEN mimetype LIKE '%XLS%' OR
                  mimetype LIKE '%CSV%' THEN 'Excel' ELSE 'Unknown' END AS fileType
FROM     dbo.CFT_fbImages LEFT OUTER JOIN
                  dbo.CFT_Users ON dbo.CFT_fbImages.uploadedBy = dbo.CFT_Users.ID
GO
/****** Object:  View [dbo].[CFT_fbotAsMs]    Script Date: 2021/02/01 4:10:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_fbotAsMs]
AS
SELECT dbo.toMSCfromDT(dbo.UTCtoShort(dbo.CFT_Feedback.feedbackDateTime)) AS dtms, dbo.CFT_Targets.target, COUNT(dbo.CFT_Feedback.fid) AS fbCnt
FROM     dbo.CFT_Feedback INNER JOIN
                  dbo.CFT_Targets ON dbo.CFT_Feedback.tid = dbo.CFT_Targets.tid
GROUP BY dbo.CFT_Targets.target, dbo.toMSCfromDT(dbo.UTCtoShort(dbo.CFT_Feedback.feedbackDateTime))
GO
/****** Object:  View [dbo].[CFT_fbotDets]    Script Date: 2021/02/01 4:10:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_fbotDets]
AS
SELECT TOP (100) PERCENT dbo.toMSCfromDT(dbo.UTCtoShort(feedbackDateTime)) AS dtms, feedbackDateTime, fid, fbnum, target, feedbackType, feedbackText, parentGroup, nature, cdOriginator, cdUnit, cdPhone, cdEmail, status, 
                  dateClosed
FROM     dbo.CFT_FBListAll
GO

USE [Web_JLC]
GO
/****** Object:  Table [dbo].[LMM_ListOfDSA]    Script Date: 08/28/2021 10:05:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LMM_ListOfDSA]') AND type in (N'U'))
DROP TABLE [dbo].[LMM_ListOfDSA]
GO
/****** Object:  Table [dbo].[LMM_ListOfDSA]    Script Date: 08/28/2021 10:05:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LMM_ListOfDSA]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[LMM_ListOfDSA](
	[DSAKey] [nvarchar](255) NULL,
	[DSA_Desc] [nvarchar](255) NULL,
	[DSA] [nvarchar](511) NULL,
	[Division] [nvarchar](255) NULL,
	[DSAComment] [varchar](max) NULL,
	[ExtractDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[LMM_ListOfDSA] ([DSAKey], [DSA_Desc], [DSA], [Division], [DSAComment], [ExtractDate]) VALUES (N'CA29N', N'NINOX Disposal', N'CA29N - NINOX Disposal', N'LSD', N'Added 5 Aug 2021 at direction of Lori Turle', CAST(0x0000AD920023C485 AS DateTime))
INSERT [dbo].[LMM_ListOfDSA] ([DSAKey], [DSA_Desc], [DSA], [Division], [DSAComment], [ExtractDate]) VALUES (N'CA51', N'ISREW', N'CA51 ISREW', N'JSD', NULL, CAST(0x0000AD920023C485 AS DateTime))
INSERT [dbo].[LMM_ListOfDSA] ([DSAKey], [DSA_Desc], [DSA], [Division], [DSAComment], [ExtractDate]) VALUES (N'CN49', N'MCPSPO- Utility Craft', N'CN49 MCPSPO- Utility Craft', N'MSD', NULL, CAST(0x0000AD920023C485 AS DateTime))
INSERT [dbo].[LMM_ListOfDSA] ([DSAKey], [DSA_Desc], [DSA], [Division], [DSAComment], [ExtractDate]) VALUES (N'CIO05', N'TMIAS', N'CIO05 TMIAS (IASPO)', N'ASD', NULL, CAST(0x0000AD920023C485 AS DateTime))
INSERT [dbo].[LMM_ListOfDSA] ([DSAKey], [DSA_Desc], [DSA], [Division], [DSAComment], [ExtractDate]) VALUES (N'CA66', N'GPS and SATCOM', N'CA66 GPS and SATCOM', N'JSD', NULL, CAST(0x0000AD920023C485 AS DateTime))
INSERT [dbo].[LMM_ListOfDSA] ([DSAKey], [DSA_Desc], [DSA], [Division], [DSAComment], [ExtractDate]) VALUES (N'CJC05', N'Comm Sec', N'CJC05 Comm Sec', N'JSD', NULL, CAST(0x0000AD920023C485 AS DateTime))
INSERT [dbo].[LMM_ListOfDSA] ([DSAKey], [DSA_Desc], [DSA], [Division], [DSAComment], [ExtractDate]) VALUES (N'CA16', N'LV Project', N'CA16 LV Project', N'LSD', NULL, CAST(0x0000AD920023C485 AS DateTime))
INSERT [dbo].[LMM_ListOfDSA] ([DSAKey], [DSA_Desc], [DSA], [Division], [DSAComment], [ExtractDate]) VALUES (N'CA40O', N'Operations - Comd and Intel', N'CA40O - Comd and Intel', N'JSD', NULL, CAST(0x0000AD920023C485 AS DateTime))
INSERT [dbo].[LMM_ListOfDSA] ([DSAKey], [DSA_Desc], [DSA], [Division], [DSAComment], [ExtractDate]) VALUES (N'CA57', N'Army Gen Diving and Hyperbaric Fleet ', N'CA57 Army Gen Diving and Hyperbaric Fleet ', N'LSD', NULL, CAST(0x0000AD920023C485 AS DateTime))
INSERT [dbo].[LMM_ListOfDSA] ([DSAKey], [DSA_Desc], [DSA], [Division], [DSAComment], [ExtractDate]) VALUES (N'CAF24', N'Ground Support Equipment', N'CAF24 Ground Support Equipment', N'ASD', N'Rolled into CAF26', CAST(0x0000AD920023C485 AS DateTime))
INSERT [dbo].[LMM_ListOfDSA] ([DSAKey], [DSA_Desc], [DSA], [Division], [DSAComment], [ExtractDate]) VALUES (N'CA15', N'Minigun', N'CA15 Minigun', N'LSD', N'Added in at direction of Lori T 10 Oct 2020', CAST(0x0000AD920023C485 AS DateTime))
INSERT [dbo].[LMM_ListOfDSA] ([DSAKey], [DSA_Desc], [DSA], [Division], [DSAComment], [ExtractDate]) VALUES (N'CAF26', N'Aviation Commons', N'CAF26 Aviation Commons', N'ASD', N'Change to ACMU', CAST(0x0000AD920023C485 AS DateTime))
INSERT [dbo].[LMM_ListOfDSA] ([DSAKey], [DSA_Desc], [DSA], [Division], [DSAComment], [ExtractDate]) VALUES (N'CN09', N'PBSPO Trailers', N'CN09 PBSPO Trailers', N'MSD', N'Added from Lori Turle 19 Apr', CAST(0x0000AD920023C485 AS DateTime))
INSERT [dbo].[LMM_ListOfDSA] ([DSAKey], [DSA_Desc], [DSA], [Division], [DSAComment], [ExtractDate]) VALUES (N'CA01', N'Abrams Tank', N'CA01 Abrams Tank', N'LSD', NULL, CAST(0x0000AD920023C485 AS DateTime))
INSERT [dbo].[LMM_ListOfDSA] ([DSAKey], [DSA_Desc], [DSA], [Division], [DSAComment], [ExtractDate]) VALUES (N'CA02', N'ASLAV', N'CA02 ASLAV', N'LSD', NULL, CAST(0x0000AD920023C485 AS DateTime))
INSERT [dbo].[LMM_ListOfDSA] ([DSAKey], [DSA_Desc], [DSA], [Division], [DSAComment], [ExtractDate]) VALUES (N'CA03', N'M113', N'CA03 M113', N'LSD', NULL, CAST(0x0000AD920023C485 AS DateTime))
INSERT [dbo].[LMM_ListOfDSA] ([DSAKey], [DSA_Desc], [DSA], [Division], [DSAComment], [ExtractDate]) VALUES (N'CA04', N'PMV', N'CA04 PMV', N'LSD', NULL, CAST(0x0000AD920023C485 AS DateTime))
INSERT [dbo].[LMM_ListOfDSA] ([DSAKey], [DSA_Desc], [DSA], [Division], [DSAComment], [ExtractDate]) VALUES (N'CA05', N'RADAR', N'CA05 RADAR', N'LSD', NULL, CAST(0x0000AD920023C485 AS DateTime))
INSERT [dbo].[LMM_ListOfDSA] ([DSAKey], [DSA_Desc], [DSA], [Division], [DSAComment], [ExtractDate]) VALUES (N'CA08', N'Small Arms', N'CA08 Small Arms', N'LSD', NULL, CAST(0x0000AD920023C485 AS DateTime))
INSERT [dbo].[LMM_ListOfDSA] ([DSAKey], [DSA_Desc], [DSA], [Division], [DSAComment], [ExtractDate]) VALUES (N'CA09', N'IFSW', N'CA09 IFSW', N'LSD', NULL, CAST(0x0000AD920023C485 AS DateTime))
INSERT [dbo].[LMM_ListOfDSA] ([DSAKey], [DSA_Desc], [DSA], [Division], [DSAComment], [ExtractDate]) VALUES (N'CA10', N'MG and  DFSW', N'CA10 MG and  DFSW', N'LSD', NULL, CAST(0x0000AD920023C485 AS DateTime))
INSERT [dbo].[LMM_ListOfDSA] ([DSAKey], [DSA_Desc], [DSA], [Division], [DSAComment], [ExtractDate]) VALUES (N'CA17', N'Light LW A Capability', N'CA17 Light LW A Capability', N'LSD', NULL, CAST(0x0000AD920023C485 AS DateTime))
INSERT [dbo].[LMM_ListOfDSA] ([DSAKey], [DSA_Desc], [DSA], [Division], [DSAComment], [ExtractDate]) VALUES (N'CA19', N'Commercial Vehicles', N'CA19 Commercial Vehicles', N'LSD', NULL, CAST(0x0000AD920023C485 AS DateTime))
INSERT [dbo].[LMM_ListOfDSA] ([DSAKey], [DSA_Desc], [DSA], [Division], [DSAComment], [ExtractDate]) VALUES (N'CA20', N'Veh and Maint Support Equip', N'CA20 Veh and Maint Support Equip', N'LSD', NULL, CAST(0x0000AD920023C485 AS DateTime))
INSERT [dbo].[LMM_ListOfDSA] ([DSAKey], [DSA_Desc], [DSA], [Division], [DSAComment], [ExtractDate]) VALUES (N'CA22', N'Electro - Mech Equip', N'CA22 Electro - Mech Equip', N'LSD', NULL, CAST(0x0000AD920023C485 AS DateTime))
INSERT [dbo].[LMM_ListOfDSA] ([DSAKey], [DSA_Desc], [DSA], [Division], [DSAComment], [ExtractDate]) VALUES (N'CA23', N'BLD', N'CA23 BLD', N'LSD', NULL, CAST(0x0000AD920023C485 AS DateTime))
INSERT [dbo].[LMM_ListOfDSA] ([DSAKey], [DSA_Desc], [DSA], [Division], [DSAComment], [ExtractDate]) VALUES (N'CA24', N'Eng Vehicles', N'CA24 Eng Vehicles', N'LSD', NULL, CAST(0x0000AD920023C485 AS DateTime))
INSERT [dbo].[LMM_ListOfDSA] ([DSAKey], [DSA_Desc], [DSA], [Division], [DSAComment], [ExtractDate]) VALUES (N'CA25', N'Eng Equip', N'CA25 Eng Equip', N'LSD', NULL, CAST(0x0000AD920023C485 AS DateTime))
INSERT [dbo].[LMM_ListOfDSA] ([DSAKey], [DSA_Desc], [DSA], [Division], [DSAComment], [ExtractDate]) VALUES (N'CA26', N'CBRNE', N'CA26 CBRNE', N'LSD', NULL, CAST(0x0000AD920023C485 AS DateTime))
INSERT [dbo].[LMM_ListOfDSA] ([DSAKey], [DSA_Desc], [DSA], [Division], [DSAComment], [ExtractDate]) VALUES (N'CA27', N'Personnel Field and Aerial Delivery Equip', N'CA27 Personnel Field and Aerial Delivery Equip', N'LSD', NULL, CAST(0x0000AD920023C485 AS DateTime))
INSERT [dbo].[LMM_ListOfDSA] ([DSAKey], [DSA_Desc], [DSA], [Division], [DSAComment], [ExtractDate]) VALUES (N'CA29', N'Surveillance Systems', N'CA29 Surveillance Systems', N'LSD', NULL, CAST(0x0000AD920023C485 AS DateTime))
INSERT [dbo].[LMM_ListOfDSA] ([DSAKey], [DSA_Desc], [DSA], [Division], [DSAComment], [ExtractDate]) VALUES (N'CA30', N'Simulation Equip', N'CA30 Simulation Equip', N'LSD', NULL, CAST(0x0000AD920023C485 AS DateTime))
INSERT [dbo].[LMM_ListOfDSA] ([DSAKey], [DSA_Desc], [DSA], [Division], [DSAComment], [ExtractDate]) VALUES (N'CA31', N'Battlespace Comms', N'CA31 Battlespace Comms', N'JSD', NULL, CAST(0x0000AD920023C485 AS DateTime))
INSERT [dbo].[LMM_ListOfDSA] ([DSAKey], [DSA_Desc], [DSA], [Division], [DSAComment], [ExtractDate]) VALUES (N'CA32', N'Satellite Comms', N'CA32 Satellite Comms', N'JSD', NULL, CAST(0x0000AD920023C485 AS DateTime))
INSERT [dbo].[LMM_ListOfDSA] ([DSAKey], [DSA_Desc], [DSA], [Division], [DSAComment], [ExtractDate]) VALUES (N'CA34', N'GPS Equip', N'CA34 GPS Equip', N'JSD', NULL, CAST(0x0000AD920023C485 AS DateTime))
INSERT [dbo].[LMM_ListOfDSA] ([DSAKey], [DSA_Desc], [DSA], [Division], [DSAComment], [ExtractDate]) VALUES (N'CA36', N'Tactical E Warfare', N'CA36 Tactical E Warfare', N'JSD', NULL, CAST(0x0000AD920023C485 AS DateTime))
INSERT [dbo].[LMM_ListOfDSA] ([DSAKey], [DSA_Desc], [DSA], [Division], [DSAComment], [ExtractDate]) VALUES (N'CA39', N'Combat Footwear', N'CA39 Combat Footwear', N'LSD', NULL, CAST(0x0000AD920023C485 AS DateTime))
INSERT [dbo].[LMM_ListOfDSA] ([DSAKey], [DSA_Desc], [DSA], [Division], [DSAComment], [ExtractDate]) VALUES (N'CA40', N'Comd and Intel', N'CA40 Comd and Intel', N'JSD', NULL, CAST(0x0000AD920023C485 AS DateTime))
INSERT [dbo].[LMM_ListOfDSA] ([DSAKey], [DSA_Desc], [DSA], [Division], [DSAComment], [ExtractDate]) VALUES (N'CA42', N'Army Marine', N'CA42 Army Marine', N'MSD', NULL, CAST(0x0000AD920023C485 AS DateTime))
INSERT [dbo].[LMM_ListOfDSA] ([DSAKey], [DSA_Desc], [DSA], [Division], [DSAComment], [ExtractDate]) VALUES (N'CA45', N'B Veh', N'CA45 B Veh', N'LSD', NULL, CAST(0x0000AD920023C485 AS DateTime))
INSERT [dbo].[LMM_ListOfDSA] ([DSAKey], [DSA_Desc], [DSA], [Division], [DSAComment], [ExtractDate]) VALUES (N'CA46', N'Fire Veh', N'CA46 Fire Veh', N'LSD', NULL, CAST(0x0000AD920023C485 AS DateTime))
INSERT [dbo].[LMM_ListOfDSA] ([DSAKey], [DSA_Desc], [DSA], [Division], [DSAComment], [ExtractDate]) VALUES (N'CA47', N'General Stores', N'CA47 General Stores', N'LSD', NULL, CAST(0x0000AD920023C485 AS DateTime))
INSERT [dbo].[LMM_ListOfDSA] ([DSAKey], [DSA_Desc], [DSA], [Division], [DSAComment], [ExtractDate]) VALUES (N'CA50', N'Combat Ration Packs', N'CA50 Combat Ration Packs', N'LSD', NULL, CAST(0x0000AD920023C485 AS DateTime))
INSERT [dbo].[LMM_ListOfDSA] ([DSAKey], [DSA_Desc], [DSA], [Division], [DSAComment], [ExtractDate]) VALUES (N'CA52', N'Combat Protective Equipment', N'CA52 Combat Protective Equipment', N'LSD', NULL, CAST(0x0000AD920023C485 AS DateTime))
INSERT [dbo].[LMM_ListOfDSA] ([DSAKey], [DSA_Desc], [DSA], [Division], [DSAComment], [ExtractDate]) VALUES (N'CA54', N'Special ops Vehicle Fleet', N'CA54 Special ops Vehicle Fleet', N'LSD', NULL, CAST(0x0000AD920023C485 AS DateTime))
INSERT [dbo].[LMM_ListOfDSA] ([DSAKey], [DSA_Desc], [DSA], [Division], [DSAComment], [ExtractDate]) VALUES (N'CA59', N'Munitions SPO Army', N'CA59 Munitions SPO Army', N'LSD', NULL, CAST(0x0000AD920023C485 AS DateTime))
INSERT [dbo].[LMM_ListOfDSA] ([DSAKey], [DSA_Desc], [DSA], [Division], [DSAComment], [ExtractDate]) VALUES (N'CA61', N'ADF Diving Equipment', N'CA61 ADF Diving Equipment', N'MSD', NULL, CAST(0x0000AD920023C485 AS DateTime))
INSERT [dbo].[LMM_ListOfDSA] ([DSAKey], [DSA_Desc], [DSA], [Division], [DSAComment], [ExtractDate]) VALUES (N'CAF32', N'Munitions SPO RAAF', N'CAF32 Munitions SPO RAAF', N'LSD', NULL, CAST(0x0000AD920023C485 AS DateTime))
INSERT [dbo].[LMM_ListOfDSA] ([DSAKey], [DSA_Desc], [DSA], [Division], [DSAComment], [ExtractDate]) VALUES (N'CN37', N'Munitions SPO Navy', N'CN37 Munitions SPO Navy', N'LSD', NULL, CAST(0x0000AD920023C485 AS DateTime))
INSERT [dbo].[LMM_ListOfDSA] ([DSAKey], [DSA_Desc], [DSA], [Division], [DSAComment], [ExtractDate]) VALUES (N'JHC1', N'Medical and Dental', N'JHC1 Medical and Dental', N'LSD', NULL, CAST(0x0000AD920023C485 AS DateTime))
INSERT [dbo].[LMM_ListOfDSA] ([DSAKey], [DSA_Desc], [DSA], [Division], [DSAComment], [ExtractDate]) VALUES (N'CN61', N'Navy RHIBS', N'CN61 Navy RHIBS', N'MSD', N'Added from Adam Patterson', CAST(0x0000AD920023C485 AS DateTime))

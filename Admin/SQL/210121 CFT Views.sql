USE [Web_JLC]
GO
/****** Object:  View [dbo].[CFT_ytd_Target_Type]    Script Date: 21-Jan-2021 9:26:31 AM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_ytd_Target_Type]'))
DROP VIEW [dbo].[CFT_ytd_Target_Type]
GO
/****** Object:  View [dbo].[CFT_ytd_Target_Compl_Group]    Script Date: 21-Jan-2021 9:26:31 AM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_ytd_Target_Compl_Group]'))
DROP VIEW [dbo].[CFT_ytd_Target_Compl_Group]
GO
/****** Object:  View [dbo].[CFT_ytd_Natures_Type]    Script Date: 21-Jan-2021 9:26:31 AM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_ytd_Natures_Type]'))
DROP VIEW [dbo].[CFT_ytd_Natures_Type]
GO
/****** Object:  View [dbo].[CFT_ytd_Natures_Compl_SpecNat]    Script Date: 21-Jan-2021 9:26:31 AM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_ytd_Natures_Compl_SpecNat]'))
DROP VIEW [dbo].[CFT_ytd_Natures_Compl_SpecNat]
GO
/****** Object:  View [dbo].[CFT_ytd_Gen_Sub]    Script Date: 21-Jan-2021 9:26:31 AM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_ytd_Gen_Sub]'))
DROP VIEW [dbo].[CFT_ytd_Gen_Sub]
GO
/****** Object:  View [dbo].[CFT_ytd_Gen_Open]    Script Date: 21-Jan-2021 9:26:31 AM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_ytd_Gen_Open]'))
DROP VIEW [dbo].[CFT_ytd_Gen_Open]
GO
/****** Object:  View [dbo].[CFT_ytd_Gen_Closed]    Script Date: 21-Jan-2021 9:26:31 AM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_ytd_Gen_Closed]'))
DROP VIEW [dbo].[CFT_ytd_Gen_Closed]
GO
/****** Object:  View [dbo].[CFT_RptMatrix]    Script Date: 21-Jan-2021 9:26:31 AM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_RptMatrix]'))
DROP VIEW [dbo].[CFT_RptMatrix]
GO
/****** Object:  View [dbo].[CFT_FBListAll]    Script Date: 21-Jan-2021 9:26:31 AM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_FBListAll]'))
DROP VIEW [dbo].[CFT_FBListAll]
GO
/****** Object:  View [dbo].[CFT_FBListAll]    Script Date: 21-Jan-2021 9:26:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_FBListAll]
AS
SELECT dbo.CFT_Feedback.fid, dbo.CFT_Feedback.tid, dbo.CFT_Targets.target, dbo.CFT_Feedback.feedbackType, dbo.CFT_Feedback.feedbackText, dbo.CFT_Feedback.feedbackDateTime, dbo.CFT_NatureGroups.parentGroup, 
                  dbo.CFT_Feedback.gid, dbo.CFT_Feedback.nid, dbo.CFT_Natures.natAbbrev, dbo.CFT_Natures.nature, dbo.CFT_Users.Name AS cdOriginator, dbo.CFT_Feedback.creationUserID, dbo.CFT_Feedback.cdUnit, dbo.CFT_Feedback.cdPhone, 
                  dbo.CFT_Feedback.cdEmail, dbo.CFT_Feedback.fdRodum, dbo.CFT_Feedback.fdEquipNo, dbo.CFT_Feedback.fdEquipAN, dbo.CFT_Feedback.fdMaintReqNo, dbo.CFT_Feedback.fdWorkOrderNo, dbo.CFT_Feedback.fdReqNo, 
                  ISNULL(dbo.CFT_fileCnt.fileCnt, 0) AS fileCnt, ISNULL(dbo.CFT_imageCnt.imageCnt, 0) AS imageCnt, dbo.CFT_Feedback.lastModified, CFT_Users_1.Name AS lastModifiedBy, CFT_Users_2.Name AS closedBy, 
                  dbo.CFT_Feedback.dateClosed, CASE WHEN dateClosed IS NULL THEN 'Open' ELSE 'Closed' END AS status, YEAR(dbo.CFT_Feedback.feedbackDateTime) AS filterYear
FROM     dbo.CFT_Feedback LEFT OUTER JOIN
                  dbo.CFT_imageCnt ON dbo.CFT_Feedback.fid = dbo.CFT_imageCnt.fid LEFT OUTER JOIN
                  dbo.CFT_fileCnt ON dbo.CFT_Feedback.fid = dbo.CFT_fileCnt.fid LEFT OUTER JOIN
                  dbo.CFT_Natures ON dbo.CFT_Feedback.nid = dbo.CFT_Natures.nid LEFT OUTER JOIN
                  dbo.CFT_Users AS CFT_Users_2 ON dbo.CFT_Feedback.closingUserID = CFT_Users_2.ID LEFT OUTER JOIN
                  dbo.CFT_Users AS CFT_Users_1 ON dbo.CFT_Feedback.lastModifiedBy = CFT_Users_1.ID LEFT OUTER JOIN
                  dbo.CFT_NatureGroups ON dbo.CFT_Feedback.gid = dbo.CFT_NatureGroups.gid LEFT OUTER JOIN
                  dbo.CFT_Targets ON dbo.CFT_Feedback.tid = dbo.CFT_Targets.tid LEFT OUTER JOIN
                  dbo.CFT_Users ON dbo.CFT_Feedback.creationUserID = dbo.CFT_Users.ID
GO
/****** Object:  View [dbo].[CFT_RptMatrix]    Script Date: 21-Jan-2021 9:26:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_RptMatrix]
AS
SELECT COUNT(dbo.CFT_Feedback.fid) AS fbCnt, dbo.CFT_Feedback.feedbackType, YEAR(dbo.CFT_Feedback.feedbackDateTime) AS fbYear, dbo.CFT_Feedback.tid, dbo.CFT_Targets.target, dbo.CFT_Feedback.gid, 
                  dbo.CFT_NatureGroups.parentGroup, CASE WHEN dateClosed IS NULL THEN 'Open' ELSE 'Closed' END AS status
FROM     dbo.CFT_Feedback LEFT OUTER JOIN
                  dbo.CFT_Targets ON dbo.CFT_Feedback.tid = dbo.CFT_Targets.tid LEFT OUTER JOIN
                  dbo.CFT_NatureGroups ON dbo.CFT_Feedback.gid = dbo.CFT_NatureGroups.gid
GROUP BY YEAR(dbo.CFT_Feedback.feedbackDateTime), dbo.CFT_Feedback.tid, dbo.CFT_Feedback.gid, CASE WHEN dateClosed IS NULL THEN 'Open' ELSE 'Closed' END, dbo.CFT_Targets.target, dbo.CFT_NatureGroups.parentGroup, 
                  dbo.CFT_Feedback.feedbackType
GO
/****** Object:  View [dbo].[CFT_ytd_Gen_Closed]    Script Date: 21-Jan-2021 9:26:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_ytd_Gen_Closed]
AS
SELECT COUNT(fid) AS fbCnt, YEAR(dateClosed) AS fbYear, AVG(DATEDIFF(dd, feedbackDateTime, dateClosed)) AS aveDays
FROM     dbo.CFT_Feedback
WHERE  (NOT (dateClosed IS NULL))
GROUP BY YEAR(dateClosed)
GO
/****** Object:  View [dbo].[CFT_ytd_Gen_Open]    Script Date: 21-Jan-2021 9:26:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_ytd_Gen_Open]
AS
SELECT COUNT(fid) AS fbCnt, MAX(DATEDIFF(dd, feedbackDateTime, GETDATE())) AS oldest, YEAR(feedbackDateTime) AS fbYear
FROM     dbo.CFT_Feedback
WHERE  (dateClosed IS NULL)
GROUP BY YEAR(feedbackDateTime)
GO
/****** Object:  View [dbo].[CFT_ytd_Gen_Sub]    Script Date: 21-Jan-2021 9:26:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_ytd_Gen_Sub]
AS
SELECT YEAR(feedbackDateTime) AS fbYear, feedbackType, COUNT(fid) AS fbCnt
FROM     dbo.CFT_Feedback
GROUP BY YEAR(feedbackDateTime), feedbackType
GO
/****** Object:  View [dbo].[CFT_ytd_Natures_Compl_SpecNat]    Script Date: 21-Jan-2021 9:26:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_ytd_Natures_Compl_SpecNat]
AS
SELECT YEAR(dbo.CFT_Feedback.feedbackDateTime) AS fbYear, dbo.CFT_NatureGroups.parentGroup, ISNULL(dbo.CFT_Natures.nature, N'Unspecified') AS nature, COUNT(dbo.CFT_Feedback.fid) AS fbCnt
FROM     dbo.CFT_Feedback INNER JOIN
                  dbo.CFT_NatureGroups ON dbo.CFT_Feedback.gid = dbo.CFT_NatureGroups.gid LEFT OUTER JOIN
                  dbo.CFT_Natures ON dbo.CFT_Feedback.nid = dbo.CFT_Natures.nid
WHERE  (dbo.CFT_Feedback.feedbackType = N'Complaint')
GROUP BY YEAR(dbo.CFT_Feedback.feedbackDateTime), dbo.CFT_NatureGroups.parentGroup, ISNULL(dbo.CFT_Natures.nature, N'Unspecified')
GO
/****** Object:  View [dbo].[CFT_ytd_Natures_Type]    Script Date: 21-Jan-2021 9:26:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_ytd_Natures_Type]
AS
SELECT YEAR(dbo.CFT_Feedback.feedbackDateTime) AS fbYear, dbo.CFT_Feedback.feedbackType, dbo.CFT_NatureGroups.parentGroup, COUNT(dbo.CFT_Feedback.fid) AS fbCnt
FROM     dbo.CFT_Feedback INNER JOIN
                  dbo.CFT_NatureGroups ON dbo.CFT_Feedback.gid = dbo.CFT_NatureGroups.gid
GROUP BY YEAR(dbo.CFT_Feedback.feedbackDateTime), dbo.CFT_NatureGroups.parentGroup, dbo.CFT_Feedback.feedbackType
GO
/****** Object:  View [dbo].[CFT_ytd_Target_Compl_Group]    Script Date: 21-Jan-2021 9:26:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_ytd_Target_Compl_Group]
AS
SELECT YEAR(dbo.CFT_Feedback.feedbackDateTime) AS fbYear, dbo.CFT_Targets.target, COUNT(dbo.CFT_Feedback.fid) AS fbCnt, dbo.CFT_NatureGroups.parentGroup
FROM     dbo.CFT_Feedback INNER JOIN
                  dbo.CFT_Targets ON dbo.CFT_Feedback.tid = dbo.CFT_Targets.tid INNER JOIN
                  dbo.CFT_NatureGroups ON dbo.CFT_Feedback.gid = dbo.CFT_NatureGroups.gid
WHERE  (dbo.CFT_Feedback.feedbackType = N'Complaint')
GROUP BY YEAR(dbo.CFT_Feedback.feedbackDateTime), dbo.CFT_Targets.target, dbo.CFT_NatureGroups.parentGroup
GO
/****** Object:  View [dbo].[CFT_ytd_Target_Type]    Script Date: 21-Jan-2021 9:26:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_ytd_Target_Type]
AS
SELECT YEAR(dbo.CFT_Feedback.feedbackDateTime) AS fbYear, dbo.CFT_Feedback.feedbackType, dbo.CFT_Targets.target, COUNT(dbo.CFT_Feedback.fid) AS fbCnt
FROM     dbo.CFT_Feedback INNER JOIN
                  dbo.CFT_Targets ON dbo.CFT_Feedback.tid = dbo.CFT_Targets.tid
GROUP BY YEAR(dbo.CFT_Feedback.feedbackDateTime), dbo.CFT_Feedback.feedbackType, dbo.CFT_Targets.target
GO

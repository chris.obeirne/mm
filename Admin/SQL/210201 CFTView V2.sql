USE [Web_JLC]
GO
/****** Object:  View [dbo].[CFT_TimeCorrection]    Script Date: 2021/02/01 4:09:17 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_TimeCorrection]'))
DROP VIEW [dbo].[CFT_TimeCorrection]
GO
/****** Object:  UserDefinedFunction [dbo].[toMSCfromDT]    Script Date: 2021/02/01 4:09:17 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[toMSCfromDT]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[toMSCfromDT]
GO
/****** Object:  UserDefinedFunction [dbo].[toMSCfromDT]    Script Date: 2021/02/01 4:09:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[toMSCfromDT] (@dt datetime) RETURNS BIGINT
BEGIN
    --RETURN CAST(DATEDIFF(ss, '19700101', CAST(LEFT (@dt, 11) AS datetime)) AS bigint) * 1000 + CAST(DATEPART(ms, CAST(LEFT (@dt, 11) AS datetime)) AS bigint)
	RETURN CAST(DATEDIFF(ss, '19700101', @dt) AS bigint) * 1000 + CAST(DATEPART(ms, @dt) AS bigint)
END
GO
/****** Object:  View [dbo].[CFT_TimeCorrection]    Script Date: 2021/02/01 4:09:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_TimeCorrection]
AS
SELECT feedbackDateTime, DATEADD(hh, DATEDIFF(hh, GETDATE(), GETUTCDATE()), feedbackDateTime) AS utcDiff
FROM     dbo.CFT_Feedback
GO

USE [Web_JLC]
GO
/****** Object:  View [dbo].[CFT_User_Roles]    Script Date: 14-Jan-2021 8:04:03 AM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_User_Roles]'))
DROP VIEW [dbo].[CFT_User_Roles]
GO
/****** Object:  View [dbo].[CFT_Role]    Script Date: 14-Jan-2021 8:04:03 AM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_Role]'))
DROP VIEW [dbo].[CFT_Role]
GO
/****** Object:  View [dbo].[CFT_fbImageArray]    Script Date: 14-Jan-2021 8:04:03 AM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_fbImageArray]'))
DROP VIEW [dbo].[CFT_fbImageArray]
GO
/****** Object:  View [dbo].[CFT_FBList]    Script Date: 14-Jan-2021 8:04:03 AM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_FBList]'))
DROP VIEW [dbo].[CFT_FBList]
GO
/****** Object:  View [dbo].[CFT_imageCnt]    Script Date: 14-Jan-2021 8:04:03 AM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_imageCnt]'))
DROP VIEW [dbo].[CFT_imageCnt]
GO
/****** Object:  View [dbo].[CFT_fileCnt]    Script Date: 14-Jan-2021 8:04:03 AM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_fileCnt]'))
DROP VIEW [dbo].[CFT_fileCnt]
GO
/****** Object:  View [dbo].[CFT_FileList]    Script Date: 14-Jan-2021 8:04:03 AM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_FileList]'))
DROP VIEW [dbo].[CFT_FileList]
GO
/****** Object:  View [dbo].[CFT_fbImageList]    Script Date: 14-Jan-2021 8:04:03 AM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_fbImageList]'))
DROP VIEW [dbo].[CFT_fbImageList]
GO
/****** Object:  View [dbo].[CFT_TargetsList]    Script Date: 14-Jan-2021 8:04:03 AM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_TargetsList]'))
DROP VIEW [dbo].[CFT_TargetsList]
GO
/****** Object:  View [dbo].[CFT_NatureGroupsList]    Script Date: 14-Jan-2021 8:04:03 AM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_NatureGroupsList]'))
DROP VIEW [dbo].[CFT_NatureGroupsList]
GO
/****** Object:  View [dbo].[CFT_NaturesList]    Script Date: 14-Jan-2021 8:04:03 AM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_NaturesList]'))
DROP VIEW [dbo].[CFT_NaturesList]
GO
/****** Object:  View [dbo].[CFT_Users_PermissionSummary]    Script Date: 14-Jan-2021 8:04:03 AM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_Users_PermissionSummary]'))
DROP VIEW [dbo].[CFT_Users_PermissionSummary]
GO
/****** Object:  View [dbo].[CFT_UserDetsForScy]    Script Date: 14-Jan-2021 8:04:03 AM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_UserDetsForScy]'))
DROP VIEW [dbo].[CFT_UserDetsForScy]
GO
/****** Object:  View [dbo].[CFT_selectedFB]    Script Date: 14-Jan-2021 8:04:03 AM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_selectedFB]'))
DROP VIEW [dbo].[CFT_selectedFB]
GO
/****** Object:  View [dbo].[CFT_CommentsList]    Script Date: 14-Jan-2021 8:04:03 AM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_CommentsList]'))
DROP VIEW [dbo].[CFT_CommentsList]
GO
/****** Object:  View [dbo].[CFT_Users]    Script Date: 14-Jan-2021 8:04:03 AM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_Users]'))
DROP VIEW [dbo].[CFT_Users]
GO
/****** Object:  View [dbo].[CFT_Users]    Script Date: 14-Jan-2021 8:04:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_Users]
AS
SELECT WebTools_DM.dbo.APPS_Persons.ID, ISNULL(WebTools_DM.dbo.APPS_Persons.Name, '') AS Name, WebTools_DM.dbo.APPS_Persons.Username, ISNULL(WebTools_DM.dbo.APPS_Persons.Email, '') AS Email, 
                  WebTools_DM.dbo.APPS_Persons.Phone, ISNULL(dbo.CFT_Users_Settings.Settings, N'{"darkMode": true, "defaultTableDensity":1,"defaultRowsPerPage":"3", "emailNotifications": true,"navDrawer": true}') AS Settings, 
                  ISNULL(dbo.CFT_Users_Settings.SCB, 0) AS SCB, ISNULL(dbo.CFT_Users_Settings.AltEmail, N'') AS AltEmail, ISNULL(dbo.CFT_Users_Settings.AltPhone, N'') AS AltPhone, ISNULL(dbo.CFT_Users_Settings.UserNotes, N'') AS UserNotes, 
                  CASE WHEN AltPhone = '' OR
                  AltPhone IS NULL THEN isnull(Phone, '') ELSE AltPhone END AS PrefPhone, CASE WHEN AltEmail = '' OR
                  AltEmail IS NULL THEN isnull(Email, '') ELSE AltEmail END AS PrefEmail
FROM     dbo.CFT_Users_Settings RIGHT OUTER JOIN
                  WebTools_DM.dbo.APPS_Persons ON dbo.CFT_Users_Settings.ID = WebTools_DM.dbo.APPS_Persons.ID
GO
/****** Object:  View [dbo].[CFT_CommentsList]    Script Date: 14-Jan-2021 8:04:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_CommentsList]
AS
SELECT dbo.CFT_Comments.cid, dbo.CFT_Comments.fid, dbo.CFT_Comments.commentText, dbo.CFT_Comments.uid, dbo.CFT_Users.Name AS commentBy, dbo.CFT_Comments.commentDateTime, dbo.CFT_Comments.tid, dbo.CFT_Targets.target, 
                  dbo.CFT_Users.PrefPhone AS prefPhone, dbo.CFT_Users.PrefEmail AS prefEmail, dbo.CFT_Comments.actionRec
FROM     dbo.CFT_Comments LEFT OUTER JOIN
                  dbo.CFT_Targets ON dbo.CFT_Comments.tid = dbo.CFT_Targets.tid LEFT OUTER JOIN
                  dbo.CFT_Users ON dbo.CFT_Comments.uid = dbo.CFT_Users.ID
GO
/****** Object:  View [dbo].[CFT_selectedFB]    Script Date: 14-Jan-2021 8:04:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_selectedFB]
AS
SELECT dbo.CFT_Feedback.fid, dbo.CFT_Feedback.tid, dbo.CFT_Targets.target, dbo.CFT_Feedback.feedbackType, dbo.CFT_Feedback.feedbackText, dbo.CFT_Feedback.feedbackDateTime, dbo.CFT_NatureGroups.parentGroup, 
                  dbo.CFT_Feedback.gid, dbo.CFT_Feedback.nid, dbo.CFT_Natures.natAbbrev, dbo.CFT_Natures.nature, dbo.CFT_Users.Name AS cdOriginator, dbo.CFT_Feedback.creationUserID, dbo.CFT_Feedback.cdUnit, dbo.CFT_Feedback.cdPhone, 
                  dbo.CFT_Feedback.cdEmail, dbo.CFT_Feedback.fdRodum, dbo.CFT_Feedback.fdEquipNo, dbo.CFT_Feedback.fdEquipAN, dbo.CFT_Feedback.fdMaintReqNo, dbo.CFT_Feedback.fdWorkOrderNo, dbo.CFT_Feedback.fdReqNo, 
                  dbo.CFT_Feedback.lastModified, CFT_Users_1.Name AS lastModifiedBy, CFT_Users_2.Name AS closedBy, dbo.CFT_Feedback.dateClosed, CASE WHEN dateClosed IS NULL THEN 'Open' ELSE 'Closed' END AS status, 
                  CASE WHEN lockTime IS NULL THEN 0 ELSE 1 END AS locked, CFT_Users_3.Name AS lockedByName, dbo.CFT_FB_Lock.uid AS lockedBy, dbo.CFT_FB_Lock.lockTime
FROM     dbo.CFT_Feedback LEFT OUTER JOIN
                  dbo.CFT_FB_Lock ON dbo.CFT_Feedback.fid = dbo.CFT_FB_Lock.fid LEFT OUTER JOIN
                  dbo.CFT_Users AS CFT_Users_3 ON dbo.CFT_FB_Lock.uid = CFT_Users_3.ID LEFT OUTER JOIN
                  dbo.CFT_Natures ON dbo.CFT_Feedback.nid = dbo.CFT_Natures.nid LEFT OUTER JOIN
                  dbo.CFT_Users AS CFT_Users_2 ON dbo.CFT_Feedback.closingUserID = CFT_Users_2.ID LEFT OUTER JOIN
                  dbo.CFT_Users AS CFT_Users_1 ON dbo.CFT_Feedback.lastModifiedBy = CFT_Users_1.ID LEFT OUTER JOIN
                  dbo.CFT_NatureGroups ON dbo.CFT_Feedback.gid = dbo.CFT_NatureGroups.gid LEFT OUTER JOIN
                  dbo.CFT_Targets ON dbo.CFT_Feedback.tid = dbo.CFT_Targets.tid LEFT OUTER JOIN
                  dbo.CFT_Users ON dbo.CFT_Feedback.creationUserID = dbo.CFT_Users.ID
GO
/****** Object:  View [dbo].[CFT_UserDetsForScy]    Script Date: 14-Jan-2021 8:04:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_UserDetsForScy]
AS
SELECT WebTools_DM.dbo.APPS_Persons.ID, WebTools_DM.dbo.APPS_Persons.Name, ISNULL(dbo.CFT_Users_Settings.SCB, 0) AS SCB
FROM     dbo.CFT_Users_Settings RIGHT OUTER JOIN
                  WebTools_DM.dbo.APPS_Persons ON dbo.CFT_Users_Settings.ID = WebTools_DM.dbo.APPS_Persons.ID
GO
/****** Object:  View [dbo].[CFT_Users_PermissionSummary]    Script Date: 14-Jan-2021 8:04:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_Users_PermissionSummary]
AS
SELECT dbo.CFT_UserDetsForScy.ID, ISNULL(dbo.CFT_UserDetsForScy.Name, 'No Name') AS Name, dbo.CFT_UserDetsForScy.SCB, CAST(MAX(CASE WHEN Role_ID = '0EDB1401-F41E-4A16-B710-7FF9CEF77074' THEN 1 ELSE 0 END) AS bit) 
                  AS Reporting, CAST(MAX(CASE WHEN Role_ID = '01AA0953-58DD-4BCB-8ECD-13FFA5B77D69' THEN 1 ELSE 0 END) AS bit) AS IsAdmin
FROM     dbo.CFT_UserDetsForScy LEFT OUTER JOIN
                  dbo.CFT_Users_Groups_Roles_XRef ON dbo.CFT_UserDetsForScy.ID = dbo.CFT_Users_Groups_Roles_XRef.User_ID
GROUP BY dbo.CFT_UserDetsForScy.ID, dbo.CFT_UserDetsForScy.SCB, ISNULL(dbo.CFT_UserDetsForScy.Name, 'No Name')
GO
/****** Object:  View [dbo].[CFT_NaturesList]    Script Date: 14-Jan-2021 8:04:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_NaturesList]
AS
SELECT dbo.CFT_Natures.nid, dbo.CFT_Natures.natAbbrev, dbo.CFT_Natures.nature, dbo.CFT_NatureGroups.parentGroup, dbo.CFT_Natures.gid, dbo.CFT_Natures.natureLongDesc, dbo.CFT_Natures.status, 
                  ISNULL(dbo.CFT_Natures.scbhqEmail, dbo.CFT_NatureGroups.scbhqEmail) AS scbhqEmail, dbo.CFT_Natures.nidColour, dbo.CFT_Natures.lastModified, dbo.CFT_Users.Name AS lastModifiedBy
FROM     dbo.CFT_Natures LEFT OUTER JOIN
                  dbo.CFT_NatureGroups ON dbo.CFT_Natures.gid = dbo.CFT_NatureGroups.gid LEFT OUTER JOIN
                  dbo.CFT_Users ON dbo.CFT_Natures.lastModifiedBy = dbo.CFT_Users.ID
GO
/****** Object:  View [dbo].[CFT_NatureGroupsList]    Script Date: 14-Jan-2021 8:04:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_NatureGroupsList]
AS
SELECT dbo.CFT_NatureGroups.gid, dbo.CFT_NatureGroups.parentGroup, dbo.CFT_NatureGroups.description, dbo.CFT_NatureGroups.scbhqEmail, dbo.CFT_NatureGroups.status, dbo.CFT_NatureGroups.lastModified, 
                  dbo.CFT_Users.Name AS lastModifiedBy
FROM     dbo.CFT_NatureGroups LEFT OUTER JOIN
                  dbo.CFT_Users ON dbo.CFT_NatureGroups.lastModifiedBy = dbo.CFT_Users.ID
GO
/****** Object:  View [dbo].[CFT_TargetsList]    Script Date: 14-Jan-2021 8:04:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_TargetsList]
AS
SELECT dbo.CFT_Targets.tid, dbo.CFT_Targets.target, dbo.CFT_Targets.targetEmail, dbo.CFT_Targets.parentTargetId, dbo.CFT_Targets.status, dbo.CFT_Targets.districtLink, dbo.CFT_Targets.lastModified, 
                  dbo.CFT_Users.Name AS lastModifiedBy
FROM     dbo.CFT_Targets LEFT OUTER JOIN
                  dbo.CFT_Users ON dbo.CFT_Targets.lastModifiedBy = dbo.CFT_Users.ID
GO
/****** Object:  View [dbo].[CFT_fbImageList]    Script Date: 14-Jan-2021 8:04:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_fbImageList]
AS
SELECT dbo.CFT_fbImages.iid, dbo.CFT_fbImages.fid, dbo.CFT_fbImages.fName, dbo.CFT_Users.Name AS uploadedBy, dbo.CFT_fbImages.uploadedDate, dbo.CFT_fbImages.fSize, dbo.CFT_fbImages.mimeType, 
                  CASE WHEN mimeType LIKE '%pdf%' THEN 'PDF' WHEN mimeType LIKE '%jpeg%' OR
                  mimetype LIKE '%png%' THEN 'IMAGE' WHEN mimetype LIKE '%doc%' THEN 'DOC' WHEN mimetype LIKE '%XLS%' OR
                  mimetype LIKE '%CSV%' THEN 'Excel' ELSE 'Unknown' END AS fileType
FROM     dbo.CFT_fbImages LEFT OUTER JOIN
                  dbo.CFT_Users ON dbo.CFT_fbImages.uploadedBy = dbo.CFT_Users.ID
GO
/****** Object:  View [dbo].[CFT_FileList]    Script Date: 14-Jan-2021 8:04:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_FileList]
AS
SELECT dbo.CFT_fbFiles.fid, dbo.CFT_fbFiles.ufid, dbo.CFT_fbFiles.fName, ISNULL(dbo.CFT_Users.Name, 'Unknown') AS uploadedBy, dbo.CFT_fbFiles.uploadedDate, dbo.CFT_fbFiles.fSize, 
                  CASE WHEN mimeType LIKE '%pdf%' THEN 'PDF' WHEN mimeType LIKE '%jpeg%' OR
                  mimetype LIKE '%png%' THEN 'IMAGE' WHEN mimetype LIKE '%doc%' THEN 'DOC' WHEN mimetype LIKE '%XLS%' OR
                  mimetype LIKE '%CSV%' THEN 'Excel' ELSE 'Unknown' END AS FileType, dbo.CFT_fbFiles.mimeType
FROM     dbo.CFT_fbFiles LEFT OUTER JOIN
                  dbo.CFT_Users ON dbo.CFT_fbFiles.uploadedBy = dbo.CFT_Users.ID
WHERE  (dbo.CFT_fbFiles.fName <> '')
GO
/****** Object:  View [dbo].[CFT_fileCnt]    Script Date: 14-Jan-2021 8:04:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_fileCnt]
AS
SELECT fid, COUNT(ufid) AS fileCnt
FROM     dbo.CFT_fbFiles
GROUP BY fid
GO
/****** Object:  View [dbo].[CFT_imageCnt]    Script Date: 14-Jan-2021 8:04:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_imageCnt]
AS
SELECT fid, COUNT(iid) AS imageCnt
FROM     dbo.CFT_fbImages
GROUP BY fid
GO
/****** Object:  View [dbo].[CFT_FBList]    Script Date: 14-Jan-2021 8:04:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_FBList]
AS
SELECT dbo.CFT_Feedback.fid, dbo.CFT_Feedback.tid, dbo.CFT_Targets.target, dbo.CFT_Feedback.feedbackType, dbo.CFT_Feedback.feedbackText, dbo.CFT_Feedback.feedbackDateTime, dbo.CFT_NatureGroups.parentGroup, 
                  dbo.CFT_Feedback.gid, dbo.CFT_Feedback.nid, dbo.CFT_Natures.natAbbrev, dbo.CFT_Natures.nature, dbo.CFT_Users.Name AS cdOriginator, dbo.CFT_Feedback.creationUserID, dbo.CFT_Feedback.cdUnit, dbo.CFT_Feedback.cdPhone, 
                  dbo.CFT_Feedback.cdEmail, dbo.CFT_Feedback.fdRodum, dbo.CFT_Feedback.fdEquipNo, dbo.CFT_Feedback.fdEquipAN, dbo.CFT_Feedback.fdMaintReqNo, dbo.CFT_Feedback.fdWorkOrderNo, dbo.CFT_Feedback.fdReqNo, 
                  ISNULL(dbo.CFT_fileCnt.fileCnt, 0) AS fileCnt, ISNULL(dbo.CFT_imageCnt.imageCnt, 0) AS imageCnt, dbo.CFT_Feedback.lastModified, CFT_Users_1.Name AS lastModifiedBy, CFT_Users_2.Name AS closedBy, 
                  dbo.CFT_Feedback.dateClosed, CASE WHEN dateClosed IS NULL THEN 'Open' ELSE 'Closed' END AS status
FROM     dbo.CFT_Feedback LEFT OUTER JOIN
                  dbo.CFT_imageCnt ON dbo.CFT_Feedback.fid = dbo.CFT_imageCnt.fid LEFT OUTER JOIN
                  dbo.CFT_fileCnt ON dbo.CFT_Feedback.fid = dbo.CFT_fileCnt.fid LEFT OUTER JOIN
                  dbo.CFT_Natures ON dbo.CFT_Feedback.nid = dbo.CFT_Natures.nid LEFT OUTER JOIN
                  dbo.CFT_Users AS CFT_Users_2 ON dbo.CFT_Feedback.closingUserID = CFT_Users_2.ID LEFT OUTER JOIN
                  dbo.CFT_Users AS CFT_Users_1 ON dbo.CFT_Feedback.lastModifiedBy = CFT_Users_1.ID LEFT OUTER JOIN
                  dbo.CFT_NatureGroups ON dbo.CFT_Feedback.gid = dbo.CFT_NatureGroups.gid LEFT OUTER JOIN
                  dbo.CFT_Targets ON dbo.CFT_Feedback.tid = dbo.CFT_Targets.tid LEFT OUTER JOIN
                  dbo.CFT_Users ON dbo.CFT_Feedback.creationUserID = dbo.CFT_Users.ID
GO
/****** Object:  View [dbo].[CFT_fbImageArray]    Script Date: 14-Jan-2021 8:04:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_fbImageArray]
AS
SELECT iid, fid, fName, base64
FROM     dbo.CFT_fbImages
GO
/****** Object:  View [dbo].[CFT_Role]    Script Date: 14-Jan-2021 8:04:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_Role]
AS
SELECT TOP (100) PERCENT ID AS roleID, Name AS RoleName
FROM     dbo.CFT_Roles
WHERE  (ID <> '7419347f-fa57-4d29-bb3b-0a8b6b378f7f')
ORDER BY ListOrder
GO
/****** Object:  View [dbo].[CFT_User_Roles]    Script Date: 14-Jan-2021 8:04:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_User_Roles]
AS
SELECT TOP (100) PERCENT dbo.CFT_Users_Groups_Roles_XRef.User_ID, dbo.CFT_Users_Groups_Roles_XRef.Role_ID, dbo.CFT_Roles.Name AS Role_Name, dbo.CFT_Roles.ListOrder
FROM     dbo.CFT_Roles INNER JOIN
                  dbo.CFT_Users_Groups_Roles_XRef ON dbo.CFT_Roles.ID = dbo.CFT_Users_Groups_Roles_XRef.Role_ID
WHERE  (dbo.CFT_Roles.InActive = 0) OR
                  (dbo.CFT_Roles.InActive IS NULL)
GROUP BY dbo.CFT_Users_Groups_Roles_XRef.User_ID, dbo.CFT_Users_Groups_Roles_XRef.Role_ID, dbo.CFT_Roles.Name, dbo.CFT_Roles.ListOrder
GO

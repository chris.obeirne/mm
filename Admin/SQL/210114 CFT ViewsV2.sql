USE [Web_JLC]
GO
/****** Object:  View [dbo].[CFT_selectedFB]    Script Date: 14-Jan-2021 4:31:34 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_selectedFB]'))
DROP VIEW [dbo].[CFT_selectedFB]
GO
/****** Object:  View [dbo].[CFT_User_Roles]    Script Date: 14-Jan-2021 4:31:34 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_User_Roles]'))
DROP VIEW [dbo].[CFT_User_Roles]
GO
/****** Object:  View [dbo].[CFT_TargetRoleCnt]    Script Date: 14-Jan-2021 4:31:34 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_TargetRoleCnt]'))
DROP VIEW [dbo].[CFT_TargetRoleCnt]
GO
/****** Object:  View [dbo].[CFT_Role]    Script Date: 14-Jan-2021 4:31:34 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_Role]'))
DROP VIEW [dbo].[CFT_Role]
GO
/****** Object:  View [dbo].[CFT_TargetsForRoles]    Script Date: 14-Jan-2021 4:31:34 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_TargetsForRoles]'))
DROP VIEW [dbo].[CFT_TargetsForRoles]
GO
/****** Object:  View [dbo].[CFT_RolesforRole]    Script Date: 14-Jan-2021 4:31:34 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CFT_RolesforRole]'))
DROP VIEW [dbo].[CFT_RolesforRole]
GO
/****** Object:  View [dbo].[CFT_RolesforRole]    Script Date: 14-Jan-2021 4:31:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_RolesforRole]
AS
SELECT TOP (100) PERCENT ID AS roleID, Name AS RoleName, ListOrder, functionalRole
FROM     dbo.CFT_Roles
WHERE  (ID <> '7419347f-fa57-4d29-bb3b-0a8b6b378f7f') AND (InActive = 0 OR
                  InActive IS NULL)
GO
/****** Object:  View [dbo].[CFT_TargetsForRoles]    Script Date: 14-Jan-2021 4:31:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_TargetsForRoles]
AS
SELECT tid AS roleID, target AS RoleName, tidint AS listOrder, 0 AS functionalRole
FROM     dbo.CFT_Targets
WHERE  (status = 1)
GO
/****** Object:  View [dbo].[CFT_Role]    Script Date: 14-Jan-2021 4:31:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_Role]
AS
SELECT *
FROM     dbo.CFT_RolesforRole 
UNION
SELECT *
FROM     dbo.CFT_TargetsForRoles
GO
/****** Object:  View [dbo].[CFT_TargetRoleCnt]    Script Date: 14-Jan-2021 4:31:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_TargetRoleCnt]
AS
SELECT dbo.CFT_Users_Groups_Roles_XRef.User_ID, COUNT(dbo.CFT_Users_Groups_Roles_XRef.Role_ID) AS roleCnt
FROM     dbo.CFT_Users_Groups_Roles_XRef INNER JOIN
                  dbo.CFT_TargetsForRoles ON dbo.CFT_Users_Groups_Roles_XRef.Role_ID = dbo.CFT_TargetsForRoles.roleID
GROUP BY dbo.CFT_Users_Groups_Roles_XRef.User_ID
GO
/****** Object:  View [dbo].[CFT_User_Roles]    Script Date: 14-Jan-2021 4:31:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_User_Roles]
AS
SELECT TOP (100) PERCENT dbo.CFT_Users_Groups_Roles_XRef.User_ID, dbo.CFT_Role.roleID, dbo.CFT_Role.RoleName AS roleName, dbo.CFT_Role.ListOrder AS listOrder, dbo.CFT_Role.functionalRole
FROM     dbo.CFT_Users_Groups_Roles_XRef INNER JOIN
                  dbo.CFT_Role ON dbo.CFT_Users_Groups_Roles_XRef.Role_ID = dbo.CFT_Role.roleID
GROUP BY dbo.CFT_Users_Groups_Roles_XRef.User_ID, dbo.CFT_Role.roleID, dbo.CFT_Role.RoleName, dbo.CFT_Role.ListOrder, dbo.CFT_Role.functionalRole
GO
/****** Object:  View [dbo].[CFT_selectedFB]    Script Date: 14-Jan-2021 4:31:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CFT_selectedFB]
AS
SELECT dbo.CFT_Feedback.fid, dbo.CFT_Feedback.tid, dbo.CFT_Targets.target, dbo.CFT_Feedback.feedbackType, dbo.CFT_Feedback.feedbackText, dbo.CFT_Feedback.feedbackDateTime, dbo.CFT_NatureGroups.parentGroup, 
                  dbo.CFT_Feedback.gid, dbo.CFT_Feedback.nid, dbo.CFT_Natures.natAbbrev, dbo.CFT_Natures.nature, dbo.CFT_Users.Name AS cdOriginator, dbo.CFT_Feedback.creationUserID, dbo.CFT_Feedback.cdUnit, dbo.CFT_Feedback.cdPhone, 
                  dbo.CFT_Feedback.cdEmail, dbo.CFT_Feedback.fdRodum, dbo.CFT_Feedback.fdEquipNo, dbo.CFT_Feedback.fdEquipAN, dbo.CFT_Feedback.fdMaintReqNo, dbo.CFT_Feedback.fdWorkOrderNo, dbo.CFT_Feedback.fdReqNo, 
                  dbo.CFT_Feedback.lastModified, CFT_Users_1.Name AS lastModifiedBy, CFT_Users_2.Name AS closedBy, dbo.CFT_Feedback.dateClosed, CASE WHEN dateClosed IS NULL THEN 'Open' ELSE 'Closed' END AS status, 
                  CASE WHEN lockTime IS NULL THEN 0 ELSE 1 END AS locked, CFT_Users_3.Name AS lockedByName, dbo.CFT_FB_Lock.uid AS lockedBy, dbo.CFT_FB_Lock.lockTime
FROM     dbo.CFT_Feedback LEFT OUTER JOIN
                  dbo.CFT_FB_Lock ON dbo.CFT_Feedback.fid = dbo.CFT_FB_Lock.fid LEFT OUTER JOIN
                  dbo.CFT_Users AS CFT_Users_3 ON dbo.CFT_FB_Lock.uid = CFT_Users_3.ID LEFT OUTER JOIN
                  dbo.CFT_Natures ON dbo.CFT_Feedback.nid = dbo.CFT_Natures.nid LEFT OUTER JOIN
                  dbo.CFT_Users AS CFT_Users_2 ON dbo.CFT_Feedback.closingUserID = CFT_Users_2.ID LEFT OUTER JOIN
                  dbo.CFT_Users AS CFT_Users_1 ON dbo.CFT_Feedback.lastModifiedBy = CFT_Users_1.ID LEFT OUTER JOIN
                  dbo.CFT_NatureGroups ON dbo.CFT_Feedback.gid = dbo.CFT_NatureGroups.gid LEFT OUTER JOIN
                  dbo.CFT_Targets ON dbo.CFT_Feedback.tid = dbo.CFT_Targets.tid LEFT OUTER JOIN
                  dbo.CFT_Users ON dbo.CFT_Feedback.creationUserID = dbo.CFT_Users.ID
GO

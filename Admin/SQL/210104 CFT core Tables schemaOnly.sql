USE [Web_JLC]
GO
/****** Object:  Table [dbo].[CUSTOMDATA_Feedback_prod]    Script Date: 01/04/2021 11:00:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CUSTOMDATA_Feedback_prod]') AND type in (N'U'))
DROP TABLE [dbo].[CUSTOMDATA_Feedback_prod]
GO
/****** Object:  Table [dbo].[Customer_FB_ChangeHistory_prod]    Script Date: 01/04/2021 11:00:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Customer_FB_ChangeHistory_prod]') AND type in (N'U'))
DROP TABLE [dbo].[Customer_FB_ChangeHistory_prod]
GO
/****** Object:  Table [dbo].[Customer_FB_Comments_prod]    Script Date: 01/04/2021 11:00:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Customer_FB_Comments_prod]') AND type in (N'U'))
DROP TABLE [dbo].[Customer_FB_Comments_prod]
GO
/****** Object:  Table [dbo].[Customer_FB_Survey_prod]    Script Date: 01/04/2021 11:00:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Customer_FB_Survey_prod]') AND type in (N'U'))
DROP TABLE [dbo].[Customer_FB_Survey_prod]
GO
/****** Object:  Table [dbo].[Customer_FB_Survey_prod]    Script Date: 01/04/2021 11:00:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Customer_FB_Survey_prod]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Customer_FB_Survey_prod](
	[surveyID] [int] NULL,
	[q1response] [int] NULL,
	[q2response] [int] NULL,
	[q3response] [int] NULL,
	[q4response] [int] NULL,
	[q5response] [int] NULL,
	[q6response] [int] NULL,
	[fbID] [int] NULL,
	[unitReply_days] [int] NULL,
	[unitClose_days] [int] NULL,
	[surveyDate] [datetime] NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Customer_FB_Comments_prod]    Script Date: 01/04/2021 11:00:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Customer_FB_Comments_prod]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Customer_FB_Comments_prod](
	[commentID] [int] NULL,
	[feedback_ID] [int] NULL,
	[commentText] [nvarchar](1800) NULL,
	[UserID] [nvarchar](100) NULL,
	[commentDateTime] [datetime] NULL,
	[commentOwner] [int] NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Customer_FB_ChangeHistory_prod]    Script Date: 01/04/2021 11:00:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Customer_FB_ChangeHistory_prod]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Customer_FB_ChangeHistory_prod](
	[feedbackID] [int] NULL,
	[deleted] [int] NULL,
	[deluser] [nvarchar](100) NULL,
	[deldate] [datetime] NULL,
	[changeuser] [nvarchar](100) NULL,
	[changedate] [datetime] NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[CUSTOMDATA_Feedback_prod]    Script Date: 01/04/2021 11:00:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CUSTOMDATA_Feedback_prod]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CUSTOMDATA_Feedback_prod](
	[feedbackID] [int] NULL,
	[targetID] [int] NULL,
	[feedbackType] [nvarchar](50) NULL,
	[FeedbackText] [nvarchar](4000) NULL,
	[creationUserID] [nvarchar](100) NULL,
	[feedbackDateTime] [datetime] NULL,
	[natureParentID] [int] NULL,
	[natureID] [nvarchar](4) NULL,
	[dateClosed] [datetime] NULL,
	[closingUserID] [nvarchar](100) NULL,
	[cdOriginator] [nvarchar](100) NULL,
	[cdUnit] [nvarchar](50) NULL,
	[cdPhone] [nvarchar](50) NULL,
	[cdEmail] [nvarchar](100) NULL,
	[fdRodum] [nvarchar](50) NULL,
	[fdEquipNo] [nvarchar](50) NULL,
	[fdEquipAN] [nvarchar](50) NULL,
	[fdMaintReqNo] [nvarchar](50) NULL,
	[fdWorkOrderNo] [nvarchar](50) NULL,
	[fdReqNo] [nvarchar](50) NULL
) ON [PRIMARY]
END
GO

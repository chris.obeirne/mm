<!-- #include virtual = "/includes/WebTools_Core.asp" -->
<html>
<head>
  <meta name="Shane Kline" content="CFTv2">
<!-- Bootstrap -->  
    <link href="<%= Application("TP-BOOTSTRAP-CSS") %>" rel="stylesheet">
    <link href="<%= Application("TP-BOOTSTRAP-TOGGLE-CSS") %>" rel="stylesheet">
<!-- Jquery -->
	<script src='<%= Application("TP-JQUERY") %>'></script>
	<script src='<%= Application("TP-JQUERYUI") %>'></script>
<!-- Bootstrap -->  	
	<script src='<%= Application("TP-BOOTSTRAP") %>'></script>
	<script src='<%= Application("TP-BOOTSTRAP-TOGGLE") %>'></script>
<!-- Font Awesome -->
	    <link href="<%= Application("TP-FONTAWESOME") %>" rel="stylesheet">
<!-- Modernizr -->
    <script src='<%= Application("TP-MODERNIZR") %>'></script>

</head>

<%

Dim YrMth
YrMth = request.querystring("mthSelect")

If YrMth = "undefined" Then
yearmonthsel = "undefined"
period = "YTD"
head = "(Year To Date "&Year(Now)&")"
spstat = "active"

Dim fbyrtot(3)
fbyrtot(0)="Comment"
fbyrtot(1)="Complaint"
fbyrtot(2)="Compliment"

For Each fbyrtotloop In fbyrtot


SQL1Query = "SELECT     TOP (100) PERCENT COUNT(feedbackID) AS Expr1, feedbackType FROM WEB_JLC.dbo.CUSTOMDATA_Feedback_prod "&_
"WHERE (YEAR(feedbackDateTime) = YEAR(GETDATE())) GROUP BY feedbackType HAVING (feedbackType = N'"&fbyrtotloop&"') ORDER BY feedbackType"
Set SQL1Data = SQL_WebTools_PRODDB.Execute(SQL1Query)
	If SQL1Data.BOF Then
	subtota = "0,"
	Else
 Do while not SQL1Data.EOF	
 subtotfb = SQL1Data("Expr1")
 SQL1Data.MoveNext
 subtot = subtot&subtotfb&","
 Loop
 End IF
 subtotpre = subtotpre&subtot&subtota
 subtota = ""
 subtot = ""
Next

'subtotalfinal = subtotalfinal&subtotpre
'response.write(subtotpre)
strsplit = Split(subtotpre,",")
totcomment = strsplit(0)
totcomplaint = strsplit(1)
totcompliment = strsplit(2)
totfb = Int(totcomment)+Int(totcomplaint)+Int(totcompliment)
'response.write("thecomment is_"&totcomment&"<br>thecomplaint is_"&totcomplaint&"<br>thecompliment is_"&totcompliment&"<br>thetotalfb is_"&totfb)


SQL2aQuery = "SELECT     COUNT(feedbackID) AS Expr1a, MAX(CASE WHEN dateClosed IS NULL "&_
"THEN DATEDIFF(dd, feedbackDateTime, GETDATE()) END) AS Expr4 FROM WEB_JLC.dbo.CUSTOMDATA_Feedback_prod WHERE (CASE WHEN dateclosed IS NULL THEN 1 ELSE 0 END = 1)"
Set SQL2aData = SQL_WebTools_PRODDB.Execute(SQL2aQuery)
 Do while not SQL2aData.EOF	
 totopen = SQL2aData("Expr1a")
 longopen = SQL2aData("Expr4")
 SQL2aData.MoveNext
 Loop
 
'If subtotpre = "0,0,0,0," THEN
'totopen = "0"
'longopen = "0"
'End IF
 
SQL2bQuery = "SELECT     TOP (100) PERCENT COUNT(feedbackID) AS Expr1b, AVG(DATEDIFF(dd, feedbackDateTime, dateClosed)) AS Expr3 FROM WEB_JLC.dbo.CUSTOMDATA_Feedback_prod WHERE (YEAR(dateClosed) = YEAR(GETDATE()))"
 Set SQL2bData = SQL_WebTools_PRODDB.Execute(SQL2bQuery)
 Do while not SQL2bData.EOF	
 totclosed = SQL2bData("Expr1b")
 avgtclosed = SQL2bData("Expr3")
 SQL2bData.MoveNext
 Loop
 
 
SQL3Query = "SELECT COUNT(CASE WHEN q3response = 1 THEN 1 ELSE NULL END) AS q3Yes, COUNT(CASE WHEN q3response = 0 THEN 1 ELSE NULL END) AS q3No FROM WEB_JLC.dbo.Customer_FB_Survey_prod WHERE (YEAR(surveyDate) = YEAR(GETDATE()))"
Set SQL3Data = SQL_WebTools_PRODDB.Execute(SQL3Query)
 Do while not SQL3Data.EOF	
 q3Yes = SQL3Data("q3Yes")
 q3No = SQL3Data("q3No")
 SQL3Data.MoveNext
 IF totsurveys > "0" Then
 totsurveys = Int(q3Yes)+Int(q3No)
 totsurvsat = Int(q3Yes)/Int(totsurveys)*100
 Else
 totsurveys = "0"
 totsurvsat = "na"
 End IF
 Loop

SQLouter1Query = "SELECT     TOP (100) PERCENT feedbackType FROM WEB_JLC.dbo.CUSTOMDATA_Feedback_prod GROUP BY feedbackType ORDER BY feedbackType"
Set SQLouter1Data = SQL_WebTools_PRODDB.Execute(SQLouter1Query) 
Do while not SQLouter1Data.EOF	
outerfb = SQLouter1Data("feedbackType")

	SQLouter2Query = "SELECT     TOP (100) PERCENT natureParentID FROM WEB_JLC.dbo.CUSTOMDATA_Feedback_prod GROUP BY natureParentID ORDER BY natureParentID"
	Set SQLouter2Data = SQL_WebTools_PRODDB.Execute(SQLouter2Query) 
	Do while not SQLouter2Data.EOF
	outer2np = SQLouter2Data("natureParentID")

		SQLInnerQuery = "SELECT TOP (100) PERCENT feedbackType, natureParentID, COUNT(feedbackID) AS Expr2 FROM WEB_JLC.dbo.CUSTOMDATA_Feedback_prod "&_
		"WHERE (YEAR(feedbackDateTime) = "&Year(Now)&") GROUP BY natureParentID, feedbackType HAVING (feedbackType = N'"&outerfb&"') AND (natureParentID = "&outer2np&")"
		Set SQLInnerData = SQL_WebTools_PRODDB.Execute(SQLInnerQuery) 
		If SQLInnerData.BOF Then
		countnils2 = "0,"
		Else
		Do while not SQLInnerData.EOF
		countfb2 = SQLInnerData("Expr2")	
		SQLInnerData.MoveNext
		countz2 = countz2&countfb2&","
		Loop
		End If
		countall2 = countall2&countz2&countnils2
		countz2 = ""
		countnils2 = ""
	
	SQLouter2Data.MoveNext
	Loop
	countouter2 = countouter2&countall2&",,|"

SQLouter1Data.MoveNext
countall2= ""
Loop
countallfinal = countallfinal&countouter2

fbtypesplit = Split(countallfinal,"|")
commentarray    = fbtypesplit(0)
complaintarray  = fbtypesplit(1)
complimentarray = fbtypesplit(2)


cmtnature = Split(commentarray,",")

cmtmatmaint = cmtnature(0)
If cmtmatmaint = "" THEN
cmtmatmaint = 0
End If

cmtwhdist = cmtnature(1)
If cmtwhdist = "" THEN
cmtwhdist = 0
End If

cmtclothing = cmtnature(3)
If cmtclothing = "" THEN
cmtclothing = 0
End If


cmtloanpool = cmtnature(2)
If cmtloanpool = "" THEN
cmtloanpool = 0
End If

cmtnvr = cmtnature(4)
If cmtnvr = "" THEN
cmtnvr = 0
End If

cmtglobdist = cmtnature(5)
If cmtglobdist = "" THEN
cmtglobdist = 0
End If

cmtopercon = cmtnature(6)
If cmtopercon = "" THEN
cmtopercon = 0
End If
commentnp = Int(cmtmatmaint)+Int(cmtwhdist)+Int(cmtclothing)+Int(cmtloanpool)+Int(cmtnvr)+Int(cmtglobdist)+Int(cmtopercon)

cmpnature = Split(complimentarray,",")
cmpmatmaint = cmpnature(0)
cmpwhdist = cmpnature(1)
cmpclothing = cmpnature(3)
cmploanpool = cmpnature(2)
cmpnvr = cmpnature(4)
If cmpnvr = "" THEN
cmpnvr = 0
End If
cmpglobdist = cmpnature(5)
If cmpglobdist = "" THEN
cmpglobdist = 0
End If
cmpopercon = cmpnature(6)
If cmpopercon = "" THEN
cmpopercon = 0
End If
complimentnp = Int(cmpmatmaint)+Int(cmpwhdist)+Int(cmpclothing)+Int(cmploanpool)+Int(cmpnvr)+Int(cmpglobdist)+Int(cmpopercon)

cmlnature = Split(complaintarray,",")
cmlmatmaint = cmlnature(0)
cmlwhdist = cmlnature(1)
cmlclothing = cmlnature(3)
cmlloanpool = cmlnature(2)
cmlnvr = cmlnature(4)
If cmlnvr = "" THEN
cmlnvr = 0
End If
cmlglobdist = cmlnature(5)
If cmlglobdist = "" THEN
cmlglobdist = 0
End If
cmlopercon = cmlnature(6)
If cmlopercon = "" THEN
cmlopercon = 0
End If
complaintnp = Int(cmlmatmaint)+Int(cmlwhdist)+Int(cmlclothing)+Int(cmlloanpool)+Int(cmlnvr)+Int(cmlglobdist)+Int(cmlopercon)


Dim fbyr(3)
fbyr(0)="Comment"
fbyr(1)="Complaint"
fbyr(2)="Compliment"

For Each fbyrunit In fbyr


	SQLouter4Query = "SELECT  TOP (100) PERCENT WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.targetID, WEB_JLC.dbo.Customer_FB_Target_prod.targetName FROM WEB_JLC.dbo.CUSTOMDATA_Feedback_prod LEFT OUTER JOIN "&_
"WEB_JLC.dbo.Customer_FB_Target_prod ON WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.targetID = WEB_JLC.dbo.Customer_FB_Target_prod.targetID GROUP BY WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.targetID, WEB_JLC.dbo.Customer_FB_Target_prod.targetName "&_
"ORDER BY WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.targetID"
	Set SQLouter4Data = SQL_WebTools_PRODDB.Execute(SQLouter4Query)
	If SQLouter4Data.BOF Then
	countnils5 = "nothing,"
	Else
	Do while not SQLouter4Data.EOF
	outer4unit = SQLouter4Data("targetID")
	targetName = SQLouter4Data("targetName")	

		SQLInner2Query = "SELECT TOP (100) PERCENT WEB_JLC.dbo.Customer_FB_Target_prod.targetName, COUNT(WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackID) AS Expr6 FROM WEB_JLC.dbo.CUSTOMDATA_Feedback_prod LEFT OUTER JOIN "&_
"WEB_JLC.dbo.Customer_FB_Target_prod ON WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.targetID = WEB_JLC.dbo.Customer_FB_Target_prod.targetID "&_
"WHERE     (WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackType = N'"&fbyrunit&"') AND (YEAR(WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackDateTime) = "&Year(Now)&") AND (WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.targetID = "&outer4unit&") "&_
"GROUP BY WEB_JLC.dbo.Customer_FB_Target_prod.targetName"
		Set SQLInner2Data = SQL_WebTools_PRODDB.Execute(SQLInner2Query) 
		If SQLInner2Data.BOF Then
		countnils3 = "("&targetName&" - 0),"
		Else
		Do while not SQLInner2Data.EOF
		targetname = SQLInner2Data("targetname")	
		countfb2 = SQLInner2Data("Expr6")
		SQLInner2Data.MoveNext
		countz4 = countz4&"("&targetname&" - "&countfb2&"),"
		Loop
		End If
		countall5 = countall5&countz4&countnils3
		countz4 = ""
		countnils3 = ""
		
	SQLouter4Data.MoveNext
	Loop
	End IF
countall6 = countall6&countall5&countnils5&"|"
countnils5 = ""
countall5 = ""
Next	

countalltu = countalltu&countall6
'response.write(countalltu)

strsplittu = Split(countalltu,"|")
commenttu = strsplittu(0)
complimenttu = strsplittu(2)
complainttu = strsplittu(1)

cmtnatureu = Split(commenttu,",")
scbhqcmt = cmtnatureu(0)
jluncmt = cmtnatureu(1)
jluscmt = cmtnatureu(2)
jluecmt = cmtnatureu(3)
jluwcmt = cmtnatureu(4)
jlunqcmt = cmtnatureu(5)
jlusqcmt = cmtnatureu(6)
jluvcmt = cmtnatureu(7)

cmpnatureu = Split(complimenttu,",")
scbhqcmp = cmpnatureu(0)
jluncmp = cmpnatureu(1)
jluscmp = cmpnatureu(2)
jluecmp = cmpnatureu(3)
jluwcmp = cmpnatureu(4)
jlunqcmp = cmpnatureu(5)
jlusqcmp = cmpnatureu(6)
jluvcmp = cmpnatureu(7)

cmlnatureu = Split(complainttu,",")
scbhqcml = cmlnatureu(0)
jluncml = cmlnatureu(1)
jluscml = cmlnatureu(2)
jluecml = cmlnatureu(3)
jluwcml = cmlnatureu(4)
jlunqcml = cmlnatureu(5)
jlusqcml = cmlnatureu(6)
jluvcml = cmlnatureu(7)

Dim fbnp(7)
fbnp(0)="2" 'materiel maint
fbnp(1)="3" 'warehouse dist
fbnp(2)="4" 'loan pool
fbnp(3)="5" 'clothing
fbnp(4)="6" 'nat veh rec
fbnp(5)="7" 'glob dist
fbnp(6)="8" 'oper contr

For Each fbni In fbnp

	SQLouter3Query = "SELECT TOP (100) PERCENT NatureParentID, NatureID FROM WEB_JLC.dbo.Customer_FB_Nature_prod WHERE (NatureParentID = '"&fbni&"') AND (LEN(NatureID) = 4)"
	Set SQLouter3Data = SQL_WebTools_PRODDB.Execute(SQLouter3Query)
	If SQLouter3Data.BOF Then
	countnils4 = "<p hidden>()</p>,<p hidden>()</p>,<p hidden>()</p>,<p hidden>()</p>,<p hidden>()</p>,<p hidden>()</p>,<p hidden>()</p>,<p hidden>()</p>,<p hidden>()</p>,<p hidden>()</p>,<p hidden>()</p>,<p hidden>()</p>,"
	Else
	Do while not SQLouter3Data.EOF
	outer3ni = SQLouter3Data("NatureID")

		SQLInner1Query = "SELECT TOP (100) PERCENT WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.natureID, WEB_JLC.dbo.Customer_FB_Nature_prod.NatureShortDesc, "&_
		"COUNT(WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackID) AS Expr4 FROM WEB_JLC.dbo.CUSTOMDATA_Feedback_prod LEFT OUTER JOIN "&_
		"WEB_JLC.dbo.Customer_FB_Nature_prod ON WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.natureID = WEB_JLC.dbo.Customer_FB_Nature_prod.NatureID "&_
		"WHERE (YEAR(WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackDateTime) = "&Year(Now)&") AND "&_
		"(WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackType = N'Complaint') AND (WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.natureID = '"&outer3ni&"') "&_
		"GROUP BY WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.natureID, WEB_JLC.dbo.Customer_FB_Nature_prod.NatureShortDesc "&_
		"ORDER BY WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.natureID"
		Set SQLInner1Data = SQL_WebTools_PRODDB.Execute(SQLInner1Query) 
		If SQLInner1Data.BOF Then
		countnils3 = "<p hidden>()</p>,"
		Else
		Do while not SQLInner1Data.EOF
		natshdesc = SQLInner1Data("NatureShortDesc")	
		countfb3 = SQLInner1Data("Expr4")
		SQLInner1Data.MoveNext
		countz3 = countz3&"("&natshdesc&" - "&countfb3&"),"
		Loop
		End If
		countall3 = countall3&countz3&countnils3
		countz3 = ""
		countnils3 = ""
		
	SQLouter3Data.MoveNext
	Loop
	End IF
countall4 = countall4&countall3&countnils4&"|"
countnils4 = ""
countall3 = ""
Next	

countouter3 = countouter3&countall4

strsplitni = Split(countall4,"|")
mm = strsplitni(0)
wd = strsplitni(1)
lp = strsplitni(2)
cl = strsplitni(3)
nv = strsplitni(4)
gd = strsplitni(5)
oc = strsplitni(6)

mmsplit = Split(mm,",")
mm1 = mmsplit(0)
mm2 = mmsplit(1)
mm3 = mmsplit(2)
mm4 = mmsplit(3)
mm5 = mmsplit(4)
mm6 = mmsplit(5)
mm7 = mmsplit(6)
mm8 = mmsplit(7)
mm9 = mmsplit(8)
mm10 = mmsplit(9)
mm11 = mmsplit(10)
mm12 = mmsplit(11)

wdsplit = Split(wd,",")
wd1 = wdsplit(0)
wd2 = wdsplit(1)
wd3 = wdsplit(2)
wd4 = wdsplit(3)
wd5 = wdsplit(4)
wd6 = wdsplit(5)
wd7 = wdsplit(6)
wd8 = wdsplit(7)
wd9 = wdsplit(8)
wd10 = wdsplit(9)
wd11 = wdsplit(10)
wd12 = wdsplit(11)

lpsplit = Split(lp,",")
lp1 = lpsplit(0)
lp2 = lpsplit(1)
lp3 = lpsplit(2)
lp4 = lpsplit(3)
lp5 = lpsplit(4)
lp6 = lpsplit(5)
lp7 = lpsplit(6)
lp8 = lpsplit(7)
lp9 = lpsplit(8)
lp10 = lpsplit(9)
lp11 = lpsplit(10)
lp12 = lpsplit(11)

clsplit = Split(cl,",")
cl1 = clsplit(0)
cl2 = clsplit(1)
cl3 = clsplit(2)
cl4 = clsplit(3)
cl5 = clsplit(4)
cl6 = clsplit(5)
cl7 = clsplit(6)
cl8 = clsplit(7)
cl9 = clsplit(8)
cl10 = clsplit(9)
cl11 = clsplit(10)
cl12 = clsplit(11)

nvsplit = Split(nv,",")
nv1 = nvsplit(0)
nv2 = nvsplit(1)
nv3 = nvsplit(2)
nv4 = nvsplit(3)
nv5 = nvsplit(4)
nv6 = nvsplit(5)
nv7 = nvsplit(6)
nv8 = nvsplit(7)
nv9 = nvsplit(8)
nv10 = nvsplit(9)
nv11 = nvsplit(10)
nv12 = nvsplit(11)

gdsplit = Split(gd,",")
gd1 = gdsplit(0)
gd2 = gdsplit(1)
gd3 = gdsplit(2)
gd4 = gdsplit(3)
gd5 = gdsplit(4)
gd6 = gdsplit(5)
gd7 = gdsplit(6)
gd8 = gdsplit(7)
gd9 = gdsplit(8)
gd10 = gdsplit(9)
gd11 = gdsplit(10)
gd12 = gdsplit(11)

ocsplit = Split(oc,",")
oc1 = ocsplit(0)
oc2 = ocsplit(1)
oc3 = ocsplit(2)
oc4 = ocsplit(3)
oc5 = ocsplit(4)
oc6 = ocsplit(5)
oc7 = ocsplit(6)
oc8 = ocsplit(7)
oc9 = ocsplit(8)
oc10 = ocsplit(9)
oc11 = ocsplit(10)
oc12 = ocsplit(11)




'---------------------
'1	SCBHQ
'2	JLUN
'3	JLUS
'4	JLUE
'5	JLUW
'6	JLUNQ
'7	JLUSQ
'8	JLUV

Dim fbyrunitnp(7)
fbyrunitnp(0)="1"
fbyrunitnp(1)="2"
fbyrunitnp(2)="3"
fbyrunitnp(3)="4"
fbyrunitnp(4)="5"
fbyrunitnp(5)="6"
fbyrunitnp(6)="7"
fbyrunitnp(7)="8"

For Each fbyrnnpunit In fbyrunitnp


	SQLouter5Query = "SELECT TOP (100) PERCENT NatureParentID, NatureParent FROM WEB_JLC.dbo.Customer_FB_Nature_prod GROUP BY NatureParent, NatureParentID ORDER BY NatureParentID"
	Set SQLouter5Data = SQL_WebTools_PRODDB.Execute(SQLouter5Query)
	If SQLouter5Data.BOF Then
	countnils7 = ",,,,,,,,,,"
	countnils7numbers = "0,0,0,0,0,0,0"
	Else
	Do while not SQLouter5Data.EOF
	outer5npid = SQLouter5Data("NatureParentID")
	outer5np = SQLouter5Data("NatureParent")	

		SQLInner3Query = "SELECT TOP (100) PERCENT WEB_JLC.dbo.Customer_FB_Target_prod.targetName, WEB_JLC.dbo.Customer_FB_Nature_prod.NatureParent, COUNT(DISTINCT WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackID) AS Expr7 "&_
"FROM WEB_JLC.dbo.CUSTOMDATA_Feedback_prod LEFT OUTER JOIN WEB_JLC.dbo.Customer_FB_Nature_prod ON WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.natureParentID = WEB_JLC.dbo.Customer_FB_Nature_prod.NatureParentID LEFT OUTER JOIN "&_
"WEB_JLC.dbo.Customer_FB_Target_prod ON WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.targetID = WEB_JLC.dbo.Customer_FB_Target_prod.targetID WHERE     (WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackType = N'Complaint') AND "&_
"(YEAR(WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackDateTime) = "&Year(Now)&") AND (WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.natureParentID = "&outer5npid&") AND "&_
"(WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.targetID = '"&fbyrnnpunit&"') GROUP BY WEB_JLC.dbo.Customer_FB_Target_prod.targetName, WEB_JLC.dbo.Customer_FB_Nature_prod.NatureParent"
		Set SQLInner3Data = SQL_WebTools_PRODDB.Execute(SQLInner3Query) 
		If SQLInner3Data.BOF Then
		countnils6 = "("&outer5np&" - 0),"
		countnils6numbers = "0,"
		Else
		Do while not SQLInner3Data.EOF
		NatureParent = SQLInner3Data("NatureParent")	
		countfb3 = SQLInner3Data("Expr7")
		SQLInner3Data.MoveNext
		countz5 = countz5&"("&NatureParent&" - "&countfb3&"),"
		countz5numbers = countz5numbers&countfb3&","
		Loop
		End If
		countall7 = countall7&countz5&countnils6
		countz5 = ""
		countnils6 = ""
		
		countall7numbers = countall7numbers&countz5numbers&countnils6numbers
		countz5numbers = ""
		countnils6numbers = ""
		
	SQLouter5Data.MoveNext
	Loop
	End IF
countall8 = countall8&countall7&countnils7&",,|"
countall7 = ""
countnils7 = ""

countall8numbers = countall8numbers&countall7numbers&countnils7numbers&"0,0,0|"
countnils7numbers = ""
countall7numbers = ""
Next	

countallunitni = countallunitni&countall8
countallunitninumbers = countallunitninumbers&countall8numbers
'response.write(countallunitni)

strsplitunitni = Split(countallunitni,"|")
SCBHQunit = strsplitunitni(0)
JLUNunit = strsplitunitni(1)
JLUSunit = strsplitunitni(2)
JLUEunit = strsplitunitni(3)
JLUWunit = strsplitunitni(4)
JLUNQunit = strsplitunitni(5)
JLUSQunit = strsplitunitni(6)
JLUVunit = strsplitunitni(7)

strsplitunitninumbers = Split(countallunitninumbers,"|")
SCBHQunitnumbers = strsplitunitninumbers(0)
JLUNunitnumbers = strsplitunitninumbers(1)
JLUSunitnumbers = strsplitunitninumbers(2)
JLUEunitnumbers = strsplitunitninumbers(3)
JLUWunitnumbers = strsplitunitninumbers(4)
JLUNQunitnumbers = strsplitunitninumbers(5)
JLUSQunitnumbers = strsplitunitninumbers(6)
JLUVunitnumbers = strsplitunitninumbers(7)

SCBHQunitni = Split(SCBHQunit,",")
scbhqmm = SCBHQunitni(0)
scbhqwd = SCBHQunitni(1)
scbhqlp = SCBHQunitni(2)
scbhqcl = SCBHQunitni(3)
scbhqnv = SCBHQunitni(4)
scbhqgd = SCBHQunitni(5)
scbhqop = SCBHQunitni(6)
'response.write(SCBHQunitnumbers)
SCBHQunitninumbers = Split(SCBHQunitnumbers,",")
scbhqall = int(SCBHQunitninumbers(0)) + int(SCBHQunitninumbers(1)) + int(SCBHQunitninumbers(2)) + int(SCBHQunitninumbers(3)) + int(SCBHQunitninumbers(4)) + int(SCBHQunitninumbers(5)) + int(SCBHQunitninumbers(6)) 

JLUSunitni = Split(JLUSunit,",")
JLUSmm = JLUSunitni(0)
JLUSwd = JLUSunitni(1)
JLUSlp = JLUSunitni(2)
JLUScl = JLUSunitni(3)
JLUSnv = JLUSunitni(4)
JLUSgd = JLUSunitni(5)
JLUSop = JLUSunitni(6)
jlusunitninumbers = Split(jlusunitnumbers,",")
jlusall = int(jlusunitninumbers(0)) + int(jlusunitninumbers(1)) + int(jlusunitninumbers(2)) + int(jlusunitninumbers(3)) + int(jlusunitninumbers(4)) + int(jlusunitninumbers(5)) + int(jlusunitninumbers(6)) 

JLUVunitni = Split(JLUVunit,",")
JLUVmm = JLUVunitni(0)
JLUVwd = JLUVunitni(1)
JLUVlp = JLUVunitni(2)
JLUVcl = JLUVunitni(3)
JLUVnv = JLUVunitni(4)
JLUVgd = JLUVunitni(5)
JLUVop = JLUVunitni(6)
jluvunitninumbers = Split(jluvunitnumbers,",")
jluvall = int(jluvunitninumbers(0)) + int(jluvunitninumbers(1)) + int(jluvunitninumbers(2)) + int(jluvunitninumbers(3)) + int(jluvunitninumbers(4)) + int(jluvunitninumbers(5)) + int(jluvunitninumbers(6)) 

JLUEunitni = Split(JLUEunit,",")
JLUEmm = JLUEunitni(0)
JLUEwd = JLUEunitni(1)
JLUElp = JLUEunitni(2)
JLUEcl = JLUEunitni(3)
JLUEnv = JLUEunitni(4)
JLUEgd = JLUEunitni(5)
JLUEop = JLUEunitni(6)
jlueunitninumbers = Split(jlueunitnumbers,",")
jlueall = int(jlueunitninumbers(0)) + int(jlueunitninumbers(1)) + int(jlueunitninumbers(2)) + int(jlueunitninumbers(3)) + int(jlueunitninumbers(4)) + int(jlueunitninumbers(5)) + int(jlueunitninumbers(6)) 

JLUWunitni = Split(JLUWunit,",")
JLUWmm = JLUWunitni(0)
JLUWwd = JLUWunitni(1)
JLUWlp = JLUWunitni(2)
JLUWcl = JLUWunitni(3)
JLUWnv = JLUWunitni(4)
JLUWgd = JLUWunitni(5)
JLUWop = JLUWunitni(6)
jluwunitninumbers = Split(jluwunitnumbers,",")
jluwall = int(jluwunitninumbers(0)) + int(jluwunitninumbers(1)) + int(jluwunitninumbers(2)) + int(jluwunitninumbers(3)) + int(jluwunitninumbers(4)) + int(jluwunitninumbers(5)) + int(jluwunitninumbers(6))  

JLUSQunitni = Split(JLUSQunit,",")
JLUSQmm = JLUSQunitni(0)
JLUSQwd = JLUSQunitni(1)
JLUSQlp = JLUSQunitni(2)
JLUSQcl = JLUSQunitni(3)
JLUSQnv = JLUSQunitni(4)
JLUSQgd = JLUSQunitni(5)
JLUSQop = JLUSQunitni(6)
jlusqunitninumbers = Split(jlusqunitnumbers,",")
jlusqall = int(jlusqunitninumbers(0)) + int(jlusqunitninumbers(1)) + int(jlusqunitninumbers(2)) + int(jlusqunitninumbers(3)) + int(jlusqunitninumbers(4)) + int(jlusqunitninumbers(5)) + int(jlusqunitninumbers(6))  

JLUNQunitni = Split(JLUNQunit,",")
JLUNQmm = JLUNQunitni(0)
JLUNQwd = JLUNQunitni(1)
JLUNQlp = JLUNQunitni(2)
JLUNQcl = JLUNQunitni(3)
JLUNQnv = JLUNQunitni(4)
JLUNQgd = JLUNQunitni(5)
JLUNQop = JLUNQunitni(6)
jlunqunitninumbers = Split(jlunqunitnumbers,",")
jlunqall = int(jlunqunitninumbers(0)) + int(jlunqunitninumbers(1)) + int(jlunqunitninumbers(2)) + int(jlunqunitninumbers(3)) + int(jlunqunitninumbers(4)) + int(jlunqunitninumbers(5)) + int(jlunqunitninumbers(6))  

JLUNunitni = Split(JLUNunit,",")
JLUNmm = JLUNunitni(0)
JLUNwd = JLUNunitni(1)
JLUNlp = JLUNunitni(2)
JLUNcl = JLUNunitni(3)
JLUNnv = JLUNunitni(4)
JLUNgd = JLUNunitni(5)
JLUNop = JLUNunitni(6)
jlununitninumbers = Split(jlununitnumbers,",")
jlunall = int(jlununitninumbers(0)) + int(jlununitninumbers(1)) + int(jlununitninumbers(2)) + int(jlununitninumbers(3)) + int(jlununitninumbers(4)) + int(jlununitninumbers(5)) + int(jlununitninumbers(6))  



'--------------------

Else

'--------------------------------------------------------------
'this is the month select part
period = ""
spstat = "hidden"

strsplitYrMth = Split(YrMth,",")
mth = strsplitYrMth(0)
yr = strsplitYrMth(1)

head = "(For the month of "&MonthName(mth)&" "&yr&")"

Dim fbms1(3)
fbms1(0)="Comment"
fbms1(1)="Complaint"
fbms1(2)="Compliment"

For Each fbtype1 In fbms1

SQL1Query = "SELECT TOP (100) PERCENT COUNT(feedbackID) AS Expr1, feedbackType FROM WEB_JLC.dbo.CUSTOMDATA_Feedback_prod "&_
"WHERE     (YEAR(feedbackDateTime) = "&yr&") AND (MONTH(feedbackDateTime) = "&mth&") GROUP BY feedbackType HAVING (feedbackType = N'"&fbtype1&"') ORDER BY feedbackType"
Set SQL1Data = SQL_WebTools_PRODDB.Execute(SQL1Query)
If SQL1Data.BOF Then
countfbtz = "0,"
Else
 Do while not SQL1Data.EOF	
 subtotfb = SQL1Data("Expr1")
 SQL1Data.MoveNext
 subtot = subtot&subtotfb&","
 Loop
End If
 subtot = subtot&countfbtz
 countfbtz = ""

Next
totalstring = left(subtot,len(subtot)-1)
strsplit = Split(totalstring,",")
totcomment = strsplit(0)
totcomplaint = strsplit(1)
totcompliment = strsplit(2)

totfb = Int(totcomment)+Int(totcomplaint)+Int(totcompliment)

SQL2bQuery = "SELECT TOP (100) PERCENT COUNT(feedbackID) AS Expr1b, AVG(DATEDIFF(dd, feedbackDateTime, dateClosed)) AS Expr3 FROM web_jlc.dbo.CUSTOMDATA_Feedback_prod "&_
"WHERE     (YEAR(dateClosed) = "&yr&") AND (MONTH(dateClosed) = "&mth&")"
 Set SQL2bData = SQL_WebTools_PRODDB.Execute(SQL2bQuery)
 Do while not SQL2bData.EOF	
 totclosed = SQL2bData("Expr1b")
 avgtclosed = SQL2bData("Expr3")
 SQL2bData.MoveNext
 Loop
 
SQL3Query = "SELECT COUNT(CASE WHEN q3response = 1 THEN 1 ELSE NULL END) AS q3Yes, COUNT(CASE WHEN q3response = 0 THEN 1 ELSE NULL END) AS q3No "&_
"FROM WEB_JLC.dbo.Customer_FB_Survey_prod WHERE (YEAR(surveyDate) = "&yr&") AND (MONTH(surveyDate) = "&mth&")"
Set SQL3Data = SQL_WebTools_PRODDB.Execute(SQL3Query)
 Do while not SQL3Data.EOF	
 q3Yes = SQL3Data("q3Yes")
 q3No = SQL3Data("q3No")
 SQL3Data.MoveNext
 totsurveys = Int(q3Yes)+Int(q3No)
 If totsurveys = 0 THEN
 totsurvsat = "na"
 Else
 totsurvsat = Int(q3Yes)/Int(totsurveys)*100
 End If
 Loop

SQLouter1Query = "SELECT     TOP (100) PERCENT feedbackType FROM WEB_JLC.dbo.CUSTOMDATA_Feedback_prod GROUP BY feedbackType ORDER BY feedbackType"
Set SQLouter1Data = SQL_WebTools_PRODDB.Execute(SQLouter1Query) 
Do while not SQLouter1Data.EOF	
outerfb = SQLouter1Data("feedbackType")

	SQLouter2Query = "SELECT     TOP (100) PERCENT natureParentID FROM WEB_JLC.dbo.CUSTOMDATA_Feedback_prod GROUP BY natureParentID ORDER BY natureParentID"
	Set SQLouter2Data = SQL_WebTools_PRODDB.Execute(SQLouter2Query) 
	Do while not SQLouter2Data.EOF
	outer2np = SQLouter2Data("natureParentID")

		SQLInnerQuery = "SELECT TOP (100) PERCENT feedbackType, natureParentID, COUNT(feedbackID) AS Expr2 FROM WEB_JLC.dbo.CUSTOMDATA_Feedback_prod "&_
		"WHERE (YEAR(feedbackDateTime) = "&yr&") AND (MONTH(feedbackDateTime) = "&mth&") GROUP BY natureParentID, feedbackType HAVING (feedbackType = N'"&outerfb&"') AND (natureParentID = "&outer2np&")"
		Set SQLInnerData = SQL_WebTools_PRODDB.Execute(SQLInnerQuery) 
		If SQLInnerData.BOF Then
		countnils2 = "0,"
		Else
		Do while not SQLInnerData.EOF
		countfb2 = SQLInnerData("Expr2")	
		SQLInnerData.MoveNext
		countz2 = countz2&countfb2&","
		Loop
		End If
		countall2 = countall2&countz2&countnils2
		countz2 = ""
		countnils2 = ""
	
	SQLouter2Data.MoveNext
	Loop
	countouter2 = countouter2&countall2&",,|"

SQLouter1Data.MoveNext
countall2= ""
Loop
countallfinal = countallfinal&countouter2

fbtypesplit = Split(countallfinal,"|")
commentarray    = fbtypesplit(0)
complaintarray  = fbtypesplit(1)
complimentarray = fbtypesplit(2)


cmtnature = Split(commentarray,",")
cmtmatmaint = cmtnature(0)
cmtwhdist = cmtnature(1)
cmtclothing = cmtnature(3)
cmtloanpool = cmtnature(2)

cmtnvr = cmtnature(4)
If cmtnvr = "" THEN
cmtnvr = 0
End If

cmtglobdist = cmtnature(5)
If cmtglobdist = "" THEN
cmtglobdist = 0
End If

cmtopercon = cmtnature(6)
If cmtopercon = "" THEN
cmtopercon = 0
End If
commentnp = Int(cmtmatmaint)+Int(cmtwhdist)+Int(cmtclothing)+Int(cmtloanpool)+Int(cmtnvr)+Int(cmtglobdist)+Int(cmtopercon)

cmpnature = Split(complimentarray,",")
cmpmatmaint = cmpnature(0)
cmpwhdist = cmpnature(1)
cmpclothing = cmpnature(3)
cmploanpool = cmpnature(2)
cmpnvr = cmpnature(4)
If cmpnvr = "" THEN
cmpnvr = 0
End If
cmpglobdist = cmpnature(5)
If cmpglobdist = "" THEN
cmpglobdist = 0
End If
cmpopercon = cmpnature(6)
If cmpopercon = "" THEN
cmpopercon = 0
End If
complimentnp = Int(cmpmatmaint)+Int(cmpwhdist)+Int(cmpclothing)+Int(cmploanpool)+Int(cmpnvr)+Int(cmpglobdist)+Int(cmpopercon)

cmlnature = Split(complaintarray,",")
cmlmatmaint = cmlnature(0)
cmlwhdist = cmlnature(1)
cmlclothing = cmlnature(3)
cmlloanpool = cmlnature(2)
cmlnvr = cmlnature(4)
If cmlnvr = "" THEN
cmlnvr = 0
End If
cmlglobdist = cmlnature(5)
If cmlglobdist = "" THEN
cmlglobdist = 0
End If
cmlopercon = cmlnature(6)
If cmlopercon = "" THEN
cmlopercon = 0
End If
complaintnp = Int(cmlmatmaint)+Int(cmlwhdist)+Int(cmlclothing)+Int(cmlloanpool)+Int(cmlnvr)+Int(cmlglobdist)+Int(cmlopercon)




Dim fbnpmth(7)
fbnpmth(0)="2" 'materiel maint
fbnpmth(1)="3" 'warehouse dist
fbnpmth(2)="4" 'loan pool
fbnpmth(3)="5" 'clothing
fbnpmth(4)="6" 'nat veh rec
fbnpmth(5)="7" 'glob dist
fbnpmth(6)="8" 'oper contr

For Each fbnimth In fbnpmth

	SQLouter3Query = "SELECT TOP (100) PERCENT NatureParentID, NatureID FROM WEB_JLC.dbo.Customer_FB_Nature_prod WHERE (NatureParentID = '"&fbnimth&"') AND (LEN(NatureID) = 4)"
	Set SQLouter3Data = SQL_WebTools_PRODDB.Execute(SQLouter3Query)
	If SQLouter3Data.BOF Then
	countnils4 = ",,,,,,,,,,,"
	Else
	Do while not SQLouter3Data.EOF
	outer3ni = SQLouter3Data("NatureID")

		SQLInner1Query = "SELECT TOP (100) PERCENT WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.natureID, WEB_JLC.dbo.Customer_FB_Nature_prod.NatureShortDesc, "&_
		"COUNT(WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackID) AS Expr4 FROM WEB_JLC.dbo.CUSTOMDATA_Feedback_prod LEFT OUTER JOIN "&_
		"WEB_JLC.dbo.Customer_FB_Nature_prod ON WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.natureID = WEB_JLC.dbo.Customer_FB_Nature_prod.NatureID "&_
		"WHERE (YEAR(WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackDateTime) = "&yr&") AND (MONTH(WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackDateTime) = "&mth&") AND "&_
		"(WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackType = N'Complaint') AND (WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.natureID = '"&outer3ni&"') "&_
		"GROUP BY WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.natureID, WEB_JLC.dbo.Customer_FB_Nature_prod.NatureShortDesc "&_
		"ORDER BY WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.natureID"
		Set SQLInner1Data = SQL_WebTools_PRODDB.Execute(SQLInner1Query) 
		If SQLInner1Data.BOF Then
		countnils3 = ","
		Else
		Do while not SQLInner1Data.EOF
		natshdesc = SQLInner1Data("NatureShortDesc")	
		countfb3 = SQLInner1Data("Expr4")
		SQLInner1Data.MoveNext
		countz3 = countz3&"("&natshdesc&" - "&countfb3&"),"
		Loop
		End If
		countall3 = countall3&countz3&countnils3
		countz3 = ""
		countnils3 = ""
		
	SQLouter3Data.MoveNext
	Loop
	End IF
countall4 = countall4&countall3&countnils4&"|"
countnils4 = ""
countall3 = ""
Next	

countouter3 = countouter3&countall4

strsplitni = Split(countall4,"|")
mm = strsplitni(0)
wd = strsplitni(1)
lp = strsplitni(2)
cl = strsplitni(3)
nv = strsplitni(4)
gd = strsplitni(5)
oc = strsplitni(6)

mmsplit = Split(mm,",")
mm1 = mmsplit(0)
mm2 = mmsplit(1)
mm3 = mmsplit(2)
mm4 = mmsplit(3)
mm5 = mmsplit(4)
mm6 = mmsplit(5)
mm7 = mmsplit(6)
mm8 = mmsplit(7)
mm9 = mmsplit(8)
mm10 = mmsplit(9)
mm11 = mmsplit(10)
mm12 = mmsplit(11)

wdsplit = Split(wd,",")
wd1 = wdsplit(0)
wd2 = wdsplit(1)
wd3 = wdsplit(2)
wd4 = wdsplit(3)
wd5 = wdsplit(4)
wd6 = wdsplit(5)
wd7 = wdsplit(6)
wd8 = wdsplit(7)
wd9 = wdsplit(8)
wd10 = wdsplit(9)
wd11 = wdsplit(10)
wd12 = wdsplit(11)

lpsplit = Split(lp,",")
lp1 = lpsplit(0)
lp2 = lpsplit(1)
lp3 = lpsplit(2)
lp4 = lpsplit(3)
lp5 = lpsplit(4)
lp6 = lpsplit(5)
lp7 = lpsplit(6)
lp8 = lpsplit(7)
lp9 = lpsplit(8)
lp10 = lpsplit(9)
lp11 = lpsplit(10)
lp12 = lpsplit(11)

clsplit = Split(cl,",")
cl1 = clsplit(0)
cl2 = clsplit(1)
cl3 = clsplit(2)
cl4 = clsplit(3)
cl5 = clsplit(4)
cl6 = clsplit(5)
cl7 = clsplit(6)
cl8 = clsplit(7)
cl9 = clsplit(8)
cl10 = clsplit(9)
cl11 = clsplit(10)
cl12 = clsplit(11)

nvsplit = Split(nv,",")
nv1 = nvsplit(0)
nv2 = nvsplit(1)
nv3 = nvsplit(2)
nv4 = nvsplit(3)
nv5 = nvsplit(4)
nv6 = nvsplit(5)
nv7 = nvsplit(6)
nv8 = nvsplit(7)
nv9 = nvsplit(8)
nv10 = nvsplit(9)
nv11 = nvsplit(10)
nv12 = nvsplit(11)

gdsplit = Split(gd,",")
gd1 = gdsplit(0)
gd2 = gdsplit(1)
gd3 = gdsplit(2)
gd4 = gdsplit(3)
gd5 = gdsplit(4)
gd6 = gdsplit(5)
gd7 = gdsplit(6)
gd8 = gdsplit(7)
gd9 = gdsplit(8)
gd10 = gdsplit(9)
gd11 = gdsplit(10)
gd12 = gdsplit(11)

ocsplit = Split(oc,",")
oc1 = ocsplit(0)
oc2 = ocsplit(1)
oc3 = ocsplit(2)
oc4 = ocsplit(3)
oc5 = ocsplit(4)
oc6 = ocsplit(5)
oc7 = ocsplit(6)
oc8 = ocsplit(7)
oc9 = ocsplit(8)
oc10 = ocsplit(9)
oc11 = ocsplit(10)
oc12 = ocsplit(11)






Dim fbmth(3)
fbmth(0)="Comment"
fbmth(1)="Complaint"
fbmth(2)="Compliment"

For Each fbmthunit In fbmth


	SQLouter4Query = "SELECT  TOP (100) PERCENT WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.targetID, WEB_JLC.dbo.Customer_FB_Target_prod.targetName FROM WEB_JLC.dbo.CUSTOMDATA_Feedback_prod LEFT OUTER JOIN "&_
"WEB_JLC.dbo.Customer_FB_Target_prod ON WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.targetID = WEB_JLC.dbo.Customer_FB_Target_prod.targetID GROUP BY WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.targetID, WEB_JLC.dbo.Customer_FB_Target_prod.targetName "&_
"ORDER BY WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.targetID"
	Set SQLouter4Data = SQL_WebTools_PRODDB.Execute(SQLouter4Query)
	If SQLouter4Data.BOF Then
	countnils5 = ",,,,,,,"
	Else
	Do while not SQLouter4Data.EOF
	outer4unit = SQLouter4Data("targetID")
	targetName = SQLouter4Data("targetName")	

		SQLInner2Query = "SELECT TOP (100) PERCENT WEB_JLC.dbo.Customer_FB_Target_prod.targetName, COUNT(WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackID) AS Expr6 FROM WEB_JLC.dbo.CUSTOMDATA_Feedback_prod LEFT OUTER JOIN "&_
"WEB_JLC.dbo.Customer_FB_Target_prod ON WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.targetID = WEB_JLC.dbo.Customer_FB_Target_prod.targetID "&_
"WHERE     (WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackType = N'"&fbmthunit&"') AND (YEAR(WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackDateTime) = "&yr&") AND "&_ 
"(MONTH(WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackDateTime) = "&mth&") AND (WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.targetID = "&outer4unit&") "&_
"GROUP BY WEB_JLC.dbo.Customer_FB_Target_prod.targetName"
		Set SQLInner2Data = SQL_WebTools_PRODDB.Execute(SQLInner2Query) 
		If SQLInner2Data.BOF Then
		countnils3 = "("&targetName&" - 0),"
		Else
		Do while not SQLInner2Data.EOF
		targetname = SQLInner2Data("targetname")	
		countfb2 = SQLInner2Data("Expr6")
		SQLInner2Data.MoveNext
		countz4 = countz4&"("&targetname&" - "&countfb2&"),"
		Loop
		End If
		countall5 = countall5&countz4&countnils3
		countz4 = ""
		countnils3 = ""
		
	SQLouter4Data.MoveNext
	Loop
	End IF
countall6 = countall6&countall5&countnils5&"|"
countnils5 = ""
countall5 = ""
Next	

countalltu = countalltu&countall6
'response.write(countalltu)

strsplittu = Split(countalltu,"|")
commenttu = strsplittu(0)
complimenttu = strsplittu(2)
complainttu = strsplittu(1)

cmtnatureu = Split(commenttu,",")
scbhqcmt = cmtnatureu(0)
jluncmt = cmtnatureu(1)
jluscmt = cmtnatureu(2)
jluecmt = cmtnatureu(3)
jluwcmt = cmtnatureu(4)
jlunqcmt = cmtnatureu(5)
jlusqcmt = cmtnatureu(6)
jluvcmt = cmtnatureu(7)

cmpnatureu = Split(complimenttu,",")
scbhqcmp = cmpnatureu(0)
jluncmp = cmpnatureu(1)
jluscmp = cmpnatureu(2)
jluecmp = cmpnatureu(3)
jluwcmp = cmpnatureu(4)
jlunqcmp = cmpnatureu(5)
jlusqcmp = cmpnatureu(6)
jluvcmp = cmpnatureu(7)

cmlnatureu = Split(complainttu,",")
scbhqcml = cmlnatureu(0)
jluncml = cmlnatureu(1)
jluscml = cmlnatureu(2)
jluecml = cmlnatureu(3)
jluwcml = cmlnatureu(4)
jlunqcml = cmlnatureu(5)
jlusqcml = cmlnatureu(6)
jluvcml = cmlnatureu(7)




'---------------------
'1	SCBHQ
'2	JLUN
'3	JLUS
'4	JLUE
'5	JLUW
'6	JLUNQ
'7	JLUSQ
'8	JLUV

Dim fbmthunitnp(7)
fbmthunitnp(0)="1"
fbmthunitnp(1)="2"
fbmthunitnp(2)="3"
fbmthunitnp(3)="4"
fbmthunitnp(4)="5"
fbmthunitnp(5)="6"
fbmthunitnp(6)="7"
fbmthunitnp(7)="8"

For Each fbmthnnpunit In fbmthunitnp


	SQLouter5Query = "SELECT TOP (100) PERCENT NatureParentID, NatureParent FROM WEB_JLC.dbo.Customer_FB_Nature_prod GROUP BY NatureParent, NatureParentID ORDER BY NatureParentID"
	Set SQLouter5Data = SQL_WebTools_PRODDB.Execute(SQLouter5Query)
	If SQLouter5Data.BOF Then
	countnils7 = ",,,,,,,,,,"
	countnils7numbers = "0,0,0,0,0,0,0"
	Else
	Do while not SQLouter5Data.EOF
	outer5npid = SQLouter5Data("NatureParentID")
	outer5np = SQLouter5Data("NatureParent")	

		SQLInner3Query = "SELECT TOP (100) PERCENT WEB_JLC.dbo.Customer_FB_Target_prod.targetName, WEB_JLC.dbo.Customer_FB_Nature_prod.NatureParent, COUNT(DISTINCT WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackID) AS Expr7 "&_
"FROM WEB_JLC.dbo.CUSTOMDATA_Feedback_prod LEFT OUTER JOIN WEB_JLC.dbo.Customer_FB_Nature_prod ON WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.natureParentID = WEB_JLC.dbo.Customer_FB_Nature_prod.NatureParentID LEFT OUTER JOIN "&_
"WEB_JLC.dbo.Customer_FB_Target_prod ON WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.targetID = WEB_JLC.dbo.Customer_FB_Target_prod.targetID WHERE     (WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackType = N'Complaint') AND "&_
"(YEAR(WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackDateTime) = "&yr&") AND (MONTH(WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackDateTime) = "&mth&") AND (WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.natureParentID = "&outer5npid&") AND "&_
"(WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.targetID = '"&fbmthnnpunit&"') GROUP BY WEB_JLC.dbo.Customer_FB_Target_prod.targetName, WEB_JLC.dbo.Customer_FB_Nature_prod.NatureParent"
		Set SQLInner3Data = SQL_WebTools_PRODDB.Execute(SQLInner3Query) 
		If SQLInner3Data.BOF Then
		countnils6 = "("&outer5np&" - 0),"
		countnils6numbers = "0,"
		Else
		Do while not SQLInner3Data.EOF
		NatureParent = SQLInner3Data("NatureParent")	
		countfb3 = SQLInner3Data("Expr7")
		SQLInner3Data.MoveNext
		countz5 = countz5&"("&NatureParent&" - "&countfb3&"),"
		countz5numbers = countz5numbers&countfb3&","
		Loop
		End If
		countall7 = countall7&countz5&countnils6
		countz5 = ""
		countnils6 = ""
		
		countall7numbers = countall7numbers&countz5numbers&countnils6numbers
		countz5numbers = ""
		countnils6numbers = ""
		
	SQLouter5Data.MoveNext
	Loop
	End IF
countall8 = countall8&countall7&countnils7&",,|"
countall7 = ""
countnils7 = ""

countall8numbers = countall8numbers&countall7numbers&countnils7numbers&"0,0,0|"
countnils7numbers = ""
countall7numbers = ""
Next	

countallunitni = countallunitni&countall8
countallunitninumbers = countallunitninumbers&countall8numbers
'response.write(countallunitni)

strsplitunitni = Split(countallunitni,"|")
SCBHQunit = strsplitunitni(0)
JLUNunit = strsplitunitni(1)
JLUSunit = strsplitunitni(2)
JLUEunit = strsplitunitni(3)
JLUWunit = strsplitunitni(4)
JLUNQunit = strsplitunitni(5)
JLUSQunit = strsplitunitni(6)
JLUVunit = strsplitunitni(7)

strsplitunitninumbers = Split(countallunitninumbers,"|")
SCBHQunitnumbers = strsplitunitninumbers(0)
JLUNunitnumbers = strsplitunitninumbers(1)
JLUSunitnumbers = strsplitunitninumbers(2)
JLUEunitnumbers = strsplitunitninumbers(3)
JLUWunitnumbers = strsplitunitninumbers(4)
JLUNQunitnumbers = strsplitunitninumbers(5)
JLUSQunitnumbers = strsplitunitninumbers(6)
JLUVunitnumbers = strsplitunitninumbers(7)

SCBHQunitni = Split(SCBHQunit,",")
scbhqmm = SCBHQunitni(0)
scbhqwd = SCBHQunitni(1)
scbhqlp = SCBHQunitni(2)
scbhqcl = SCBHQunitni(3)
scbhqnv = SCBHQunitni(4)
scbhqgd = SCBHQunitni(5)
scbhqop = SCBHQunitni(6)
'response.write(SCBHQunitnumbers)
SCBHQunitninumbers = Split(SCBHQunitnumbers,",")
scbhqall = int(SCBHQunitninumbers(0)) + int(SCBHQunitninumbers(1)) + int(SCBHQunitninumbers(2)) + int(SCBHQunitninumbers(3)) + int(SCBHQunitninumbers(4)) + int(SCBHQunitninumbers(5)) + int(SCBHQunitninumbers(6)) 

JLUSunitni = Split(JLUSunit,",")
JLUSmm = JLUSunitni(0)
JLUSwd = JLUSunitni(1)
JLUSlp = JLUSunitni(2)
JLUScl = JLUSunitni(3)
JLUSnv = JLUSunitni(4)
JLUSgd = JLUSunitni(5)
JLUSop = JLUSunitni(6)
jlusunitninumbers = Split(jlusunitnumbers,",")
jlusall = int(jlusunitninumbers(0)) + int(jlusunitninumbers(1)) + int(jlusunitninumbers(2)) + int(jlusunitninumbers(3)) + int(jlusunitninumbers(4)) + int(jlusunitninumbers(5)) + int(jlusunitninumbers(6)) 

JLUVunitni = Split(JLUVunit,",")
JLUVmm = JLUVunitni(0)
JLUVwd = JLUVunitni(1)
JLUVlp = JLUVunitni(2)
JLUVcl = JLUVunitni(3)
JLUVnv = JLUVunitni(4)
JLUVgd = JLUVunitni(5)
JLUVop = JLUVunitni(6)
jluvunitninumbers = Split(jluvunitnumbers,",")
jluvall = int(jluvunitninumbers(0)) + int(jluvunitninumbers(1)) + int(jluvunitninumbers(2)) + int(jluvunitninumbers(3)) + int(jluvunitninumbers(4)) + int(jluvunitninumbers(5)) + int(jluvunitninumbers(6)) 

JLUEunitni = Split(JLUEunit,",")
JLUEmm = JLUEunitni(0)
JLUEwd = JLUEunitni(1)
JLUElp = JLUEunitni(2)
JLUEcl = JLUEunitni(3)
JLUEnv = JLUEunitni(4)
JLUEgd = JLUEunitni(5)
JLUEop = JLUEunitni(6)
jlueunitninumbers = Split(jlueunitnumbers,",")
jlueall = int(jlueunitninumbers(0)) + int(jlueunitninumbers(1)) + int(jlueunitninumbers(2)) + int(jlueunitninumbers(3)) + int(jlueunitninumbers(4)) + int(jlueunitninumbers(5)) + int(jlueunitninumbers(6)) 

JLUWunitni = Split(JLUWunit,",")
JLUWmm = JLUWunitni(0)
JLUWwd = JLUWunitni(1)
JLUWlp = JLUWunitni(2)
JLUWcl = JLUWunitni(3)
JLUWnv = JLUWunitni(4)
JLUWgd = JLUWunitni(5)
JLUWop = JLUWunitni(6)
jluwunitninumbers = Split(jluwunitnumbers,",")
jluwall = int(jluwunitninumbers(0)) + int(jluwunitninumbers(1)) + int(jluwunitninumbers(2)) + int(jluwunitninumbers(3)) + int(jluwunitninumbers(4)) + int(jluwunitninumbers(5)) + int(jluwunitninumbers(6))  

JLUSQunitni = Split(JLUSQunit,",")
JLUSQmm = JLUSQunitni(0)
JLUSQwd = JLUSQunitni(1)
JLUSQlp = JLUSQunitni(2)
JLUSQcl = JLUSQunitni(3)
JLUSQnv = JLUSQunitni(4)
JLUSQgd = JLUSQunitni(5)
JLUSQop = JLUSQunitni(6)
jlusqunitninumbers = Split(jlusqunitnumbers,",")
jlusqall = int(jlusqunitninumbers(0)) + int(jlusqunitninumbers(1)) + int(jlusqunitninumbers(2)) + int(jlusqunitninumbers(3)) + int(jlusqunitninumbers(4)) + int(jlusqunitninumbers(5)) + int(jlusqunitninumbers(6))  

JLUNQunitni = Split(JLUNQunit,",")
JLUNQmm = JLUNQunitni(0)
JLUNQwd = JLUNQunitni(1)
JLUNQlp = JLUNQunitni(2)
JLUNQcl = JLUNQunitni(3)
JLUNQnv = JLUNQunitni(4)
JLUNQgd = JLUNQunitni(5)
JLUNQop = JLUNQunitni(6)
jlunqunitninumbers = Split(jlunqunitnumbers,",")
jlunqall = int(jlunqunitninumbers(0)) + int(jlunqunitninumbers(1)) + int(jlunqunitninumbers(2)) + int(jlunqunitninumbers(3)) + int(jlunqunitninumbers(4)) + int(jlunqunitninumbers(5)) + int(jlunqunitninumbers(6))  

JLUNunitni = Split(JLUNunit,",")
JLUNmm = JLUNunitni(0)
JLUNwd = JLUNunitni(1)
JLUNlp = JLUNunitni(2)
JLUNcl = JLUNunitni(3)
JLUNnv = JLUNunitni(4)
JLUNgd = JLUNunitni(5)
JLUNop = JLUNunitni(6)
jlununitninumbers = Split(jlununitnumbers,",")
jlunall = int(jlununitninumbers(0)) + int(jlununitninumbers(1)) + int(jlununitninumbers(2)) + int(jlununitninumbers(3)) + int(jlununitninumbers(4)) + int(jlununitninumbers(5)) + int(jlununitninumbers(6))  



'--------------------
yearmonthsel = mth&yr
yearmonthsel = replace(yearmonthsel,",","")
End IF
%>

<style type="text/css">

#heading {
	font-family: Times;
	font-size:24pt;
	text-align:center;
	vertical-align: text-bottom;
	color:blue;
	padding-top: 5px;
	padding-bottom: 0px;
}

#accordion {
	font-size:14pt;
	font-family: Times;
}

#collapse3 {
	font-size:12pt;
	font-family: Times;
}

<!--red 80% #ff9999 yellow 80% #ffff00 green 80% #99ff99   background-color: -->
</style>
</head>
<body>
<script>
function execSumPrint(yrmth) {
	  //var monthSelect = <%=scbhqcml%>;
	  //alert("hello, the monthselect is_" + yrmth);
	  parent.closeOverlay();
	  parent.SetupOverlay(parent.document.body.clientWidth-700,parent.document.body.clientHeight-100);
      parent.window.frames["ifrmFrontOverlay"].location ="<%= Application("CONTENTLOAD") %>?PageURL=<%= Application("USERDEVELOPED") %>JLC/DSCNO/Customer_FB_Exec_Summary_Print.asp|mthSelect=" + yrmth
};

</script>
<div class="container">

<h1 id="heading">CFT Executive Summary <%=response.write(head)%></h1>

<div class="panel-group" id="accordion">
<div class="panel panel-default">
<div class="panel-heading">
<h1 class="panel-title">
<a href="#collapse1" data-toggle="collapse" data-parent="#accordion" >General Stats</a></div>
</h1></div>
<div id="collapse1" class="panel-collapse collapse in"> 
<div class="panel-body">
<font style="font-size:14pt;"><%=period%> Feedback submitted (<%=totfb%>): </font><font style="color:grey;">(Complaints - <%=totcomplaint%>) &nbsp; (Comments - <%=totcomment%>) &nbsp; (Compliments - <%=totcompliment%>)</font><br>
<font style="font-size:14pt;" id="secondpoint" <%=spstat%>>Feedback Open (<%=totopen%>): </font><font style="color:grey;" id="secondpoint" <%=spstat%>>(Longest open - <%=longopen%> Days)</font><br>
<font style="font-size:14pt;"><%=period%> Feedback Closed (<%=totclosed%>): </font><font style="color:grey;">(Average Closure Time - <%=avgtclosed%> Days)</font><br>
<font style="font-size:14pt;"><%=period%> Customer Surveys completed (<%=totsurveys%>): </font><font style="color:grey;">(Overall Customer Survey Satisfaction - <%=totsurvsat%>%)</font><br><br>
</div></div>

<div class="panel panel-default">
<div class="panel-heading">
<h1 class="panel-title">
<a href="#collapse2" data-toggle="collapse" data-parent="#accordion" ><%=period%> Submitted By Nature +</a></div>
</h1></div>
<div id="collapse2" class="panel-collapse collapse"> 
<div class="panel-body">
<font style="font-size:14pt;">Complaints (<%=complaintnp%>): </font><font style="color:grey;">(Materiel Maint - <%=cmlmatmaint%>) &nbsp; (Warehouse Dist - <%=cmlwhdist%>) &nbsp;  (Loan Pool - <%=cmlloanpool%>) &nbsp;  (Clothing - <%=cmlclothing%>) <br>  (Nat Vehicle Recovery  - <%=cmlnvr%>) &nbsp;  (Global Distribution  - <%=cmlglobdist%>) &nbsp;  (Operational Contracts - <%=cmlopercon%>)</font><br>
<font style="font-size:14pt;">Comments (<%=commentnp%>): </font><font style="color:grey;">(Materiel Maint - <%=cmtmatmaint%>) &nbsp; (Warehouse Dist - <%=cmtwhdist%>) &nbsp;  (Loan Pool - <%=cmtloanpool%>) &nbsp;  (Clothing - <%=cmtclothing%>) <br>  (Nat Vehicle Recovery - <%=cmtnvr%>) &nbsp;  (Global Distribution  - <%=cmtglobdist%>) &nbsp;  (Operational Contracts - <%=cmtopercon%>)</font><br>
<font style="font-size:14pt;">Compliments (<%=complimentnp%>): </font><font style="color:grey;">(Materiel Maint - <%=cmpmatmaint%>) &nbsp; (Warehouse Dist - <%=cmpwhdist%>) &nbsp;  (Loan Pool - <%=cmploanpool%>) &nbsp;  (Clothing - <%=cmpclothing%>) <br>  (Nat Vehicle Recovery - <%=cmpnvr%>) &nbsp;  (Global Distribution  - <%=cmpglobdist%>) &nbsp;  (Operational Contracts - <%=cmpopercon%>)</font><br>
</div></div>

<div class="panel panel-default">
<div class="panel-heading">
<h1 class="panel-title">
<a href="#collapse3" data-toggle="collapse" data-parent="#accordion" ><%=period%> Submitted Complaints By Nature Specific +</a></div>
</h1></div>
<div id="collapse3" class="panel-collapse collapse"> 
<div class="panel-body">
<font style="font-size:14pt;">Materiel Maint - (<%=cmlmatmaint%>):</font> <font style="color:grey;"><%=mm1%> &nbsp; <%=mm2%> &nbsp; <%=mm3%> &nbsp; <%=mm4%> &nbsp; <%=mm5%> &nbsp; <%=mm6%> &nbsp; <%=mm7%> &nbsp; <%=mm8%> &nbsp; <%=mm9%> &nbsp; <%=mm10%> &nbsp; <%=mm11%> &nbsp; <%=mm12%></font><br>
<font style="font-size:14pt;">Warehouse Dist - (<%=cmlwhdist%>):</font> <font style="color:grey;"><%=wd1%> &nbsp; <%=wd2%> &nbsp; <%=wd3%> &nbsp; <%=wd4%> &nbsp; <%=wd5%> &nbsp; <%=wd6%> &nbsp; <%=wd7%> &nbsp; <%=wd8%> &nbsp; <%=wd9%> &nbsp; <%=wd10%> &nbsp; <%=wd11%> &nbsp; <%=wd12%></font><br>
<font style="font-size:14pt;">Loan Pool - (<%=cmlloanpool%>):</font> <font style="color:grey;"><%=lp1%> &nbsp; <%=lp2%> &nbsp; <%=lp3%> &nbsp; <%=lp4%> &nbsp; <%=lp5%> &nbsp; <%=lp6%> &nbsp; <%=lp7%> &nbsp; <%=lp8%> &nbsp; <%=lp9%> &nbsp; <%=lp10%> &nbsp; <%=lp11%> &nbsp; <%=lp12%></font><br>
<font style="font-size:14pt;">Clothing - (<%=cmlclothing%>):</font> <font style="color:grey;"><%=cl1%> &nbsp; <%=cl2%> &nbsp; <%=cl3%> &nbsp; <%=cl4%> &nbsp; <%=cl5%> &nbsp; <%=cl6%> &nbsp; <%=cl7%> &nbsp; <%=cl8%> &nbsp; <%=cl9%> &nbsp; <%=cl10%> &nbsp; <%=cl11%> &nbsp; <%=cl12%></font><br>
<font style="font-size:14pt;">National Vehicle Recovery - (<%=cmlnvr%>):</font> <font style="color:grey;"><%=nv1%> &nbsp; <%=nv2%> &nbsp; <%=nv3%> &nbsp; <%=nv4%> &nbsp; <%=nv5%> &nbsp; <%=nv6%> &nbsp; <%=nv7%> &nbsp; <%=nv8%> &nbsp; <%=nv9%> &nbsp; <%=nv10%> &nbsp; <%=nv11%> &nbsp; <%=nv12%></font><br>
<font style="font-size:14pt;">Global Distribution - (<%=cmlglobdist%>):</font> <font style="color:grey;"><%=gd1%> &nbsp; <%=gd2%> &nbsp; <%=gd3%> &nbsp; <%=gd4%> &nbsp; <%=gd5%> &nbsp; <%=gd6%> &nbsp; <%=gd7%> &nbsp; <%=gd8%> &nbsp; <%=gd9%> &nbsp; <%=gd10%> &nbsp; <%=gd11%> &nbsp; <%=gd12%></font><br>
<font style="font-size:14pt;">Operational Contracts - (<%=cmlopercon%>):</font> <font style="color:grey;"><%=oc1%> &nbsp; <%=oc2%> &nbsp; <%=oc3%> &nbsp; <%=oc4%> &nbsp; <%=oc5%> &nbsp; <%=oc6%> &nbsp; <%=oc7%> &nbsp; <%=oc8%> &nbsp; <%=oc9%> &nbsp; <%=oc10%> &nbsp; <%=oc11%> &nbsp; <%=oc12%></font><br><br>
</div></div>



<div class="panel panel-default">
<div class="panel-heading">
<h1 class="panel-title">
<a href="#collapse4" data-toggle="collapse" data-parent="#accordion" ><%=period%> Submitted By Target Unit +</a></div>
</h1></div>
<div id="collapse4" class="panel-collapse collapse"> 
<div class="panel-body">
<font style="font-size:14pt;">Complaints (<%=complaintnp%>): </font><font style="color:grey;"> <%=scbhqcml%> &nbsp; <%=jluvcml%> &nbsp; <%=jluecml%> &nbsp; <%=jlusqcml%> &nbsp; <%=jlunqcml%> &nbsp; <%=jluncml%> &nbsp; <%=jluscml%> &nbsp; <%=jluwcml%></font><br>
<font style="font-size:14pt;">Comments (<%=commentnp%>): </font><font style="color:grey;"> <%=scbhqcmt%> &nbsp; <%=jluvcmt%> &nbsp; <%=jluecmt%> &nbsp; <%=jlusqcmt%> &nbsp; <%=jlunqcmt%> &nbsp; <%=jluncmt%> &nbsp; <%=jluscmt%> &nbsp; <%=jluwcmt%></font><br>
<font style="font-size:14pt;">Compliments (<%=complimentnp%>): </font><font style="color:grey;"> <%=scbhqcmp%> &nbsp; <%=jluvcmp%> &nbsp; <%=jluecmp%> &nbsp; <%=jlusqcmp%> &nbsp; <%=jlunqcmp%> &nbsp; <%=jluncmp%> &nbsp; <%=jluscmp%> &nbsp; <%=jluwcmp%></font>
</div></div>



<div class="panel panel-default">
<div class="panel-heading">
<h1 class="panel-title">
<a href="#collapse5" data-toggle="collapse" data-parent="#accordion" ><%=period%> Submitted Complaints By Target Unit +</a></div>
</h1></div>
<div id="collapse5" class="panel-collapse collapse"> 
<div class="panel-body">
<font style="font-size:14pt;">SCBHQ (<%=scbhqall%>): </font><font style="color:grey;"> <%=scbhqmm%> &nbsp; <%=scbhqwd%> &nbsp; <%=scbhqlp%> &nbsp; <%=scbhqcl%> &nbsp; <%=scbhqnv%> &nbsp; <%=scbhqgd%> &nbsp; <%=scbhqop%></font><br>
<font style="font-size:14pt;">JLUN (<%=jlunall%>): </font><font style="color:grey;"> <%=jlunmm%> &nbsp; <%=jlunwd%> &nbsp; <%=jlunlp%> &nbsp; <%=jluncl%> &nbsp; <%=jlunnv%> &nbsp; <%=jlungd%> &nbsp; <%=jlunop%></font><br>
<font style="font-size:14pt;">JLUS (<%=jlusall%>): </font><font style="color:grey;"> <%=jlusmm%> &nbsp; <%=jluswd%> &nbsp; <%=jluslp%> &nbsp; <%=jluscl%> &nbsp; <%=jlusnv%> &nbsp; <%=jlusgd%> &nbsp; <%=jlusop%></font><br>
<font style="font-size:14pt;">JLUE (<%=jlueall%>): </font><font style="color:grey;"> <%=jluemm%> &nbsp; <%=jluewd%> &nbsp; <%=jluelp%> &nbsp; <%=jluecl%> &nbsp; <%=jluenv%> &nbsp; <%=jluegd%> &nbsp; <%=jlueop%></font><br>
<font style="font-size:14pt;">JLUW (<%=jluwall%>): </font><font style="color:grey;"> <%=jluwmm%> &nbsp; <%=jluwwd%> &nbsp; <%=jluwlp%> &nbsp; <%=jluwcl%> &nbsp; <%=jluwnv%> &nbsp; <%=jluwgd%> &nbsp; <%=jluwop%></font><br>
<font style="font-size:14pt;">JLUNQ (<%=jlunqall%>): </font><font style="color:grey;"> <%=jlunqmm%> &nbsp; <%=jlunqwd%> &nbsp; <%=jlunqlp%> &nbsp; <%=jlunqcl%> &nbsp; <%=jlunqnv%> &nbsp; <%=jlunqgd%> &nbsp; <%=jlunqop%></font><br>
<font style="font-size:14pt;">JLUSQ (<%=jlusqall%>): </font><font style="color:grey;"> <%=jlusqmm%> &nbsp; <%=jlusqwd%> &nbsp; <%=jlusqlp%> &nbsp; <%=jlusqcl%> &nbsp; <%=jlusqnv%> &nbsp; <%=jlusqgd%> &nbsp; <%=jlusqop%></font><br>
<font style="font-size:14pt;">JLUV (<%=jluvall%>): </font><font style="color:grey;"> <%=jluvmm%> &nbsp; <%=jluvwd%> &nbsp; <%=jluvlp%> &nbsp; <%=jluvcl%> &nbsp; <%=jluvnv%> &nbsp; <%=jluvgd%> &nbsp; <%=jluvop%></font>
</div></div>



</div>

<a href="#" class="btn btn-default btn-sm" onclick="execSumPrint(<%=yearmonthsel%>)">Print ALL</a>
<a href="javascript:window.parent.location.reload(true);" class="btn btn-primary btn-sm pull-right">Close</a>
	</div>


</body>
</html>
<!-- #include virtual = "/includes/WebTools_AccessLog.asp" -->
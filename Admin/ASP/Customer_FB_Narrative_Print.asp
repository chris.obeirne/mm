<!-- #include virtual = "/includes/WebTools_Core.asp" -->
<html>
<head>
  <meta name="Shane Kline" content="CFTv2">
<!-- Bootstrap -->  
    <link href="<%= Application("TP-BOOTSTRAP-CSS") %>" rel="stylesheet">
    <link href="<%= Application("TP-BOOTSTRAP-TOGGLE-CSS") %>" rel="stylesheet">
<!-- Jquery -->
	<script src='<%= Application("TP-JQUERY") %>'></script>
	<script src='<%= Application("TP-JQUERYUI") %>'></script>
<!-- Bootstrap -->  	
	<script src='<%= Application("TP-BOOTSTRAP") %>'></script>
	<script src='<%= Application("TP-BOOTSTRAP-TOGGLE") %>'></script>
<!-- Font Awesome -->
	    <link href="<%= Application("TP-FONTAWESOME") %>" rel="stylesheet">
<!-- Modernizr -->
    <script src='<%= Application("TP-MODERNIZR") %>'></script>
	
  <html xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:w='urn:schemas-microsoft-com:office:word'>
  <meta http-equiv="Content-Type" content = "charset=iso-8859-1"/>
  <meta http-equiv="Content-Type" content = "application/vnd.msword;charset=iso-8859-1"/>
<!--[if true]><xml>
 <w:WordDocument>
  <w:View>Print</w:View>
  <w:Zoom>BestFit</w:Zoom>
  <w:SpellingState>Clean</w:SpellingState>
  <w:GrammarState>Clean</w:GrammarState>
  <w:ValidateAgainstSchemas/>
  <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>
  <w:IgnoreMixedContent>false</w:IgnoreMixedContent>
  <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>
  <w:BrowserLevel>MicrosoftInternetExplorer7</w:BrowserLevel>
  <w:DoNotOptimizeForBrowser/>"
 </w:WordDocument>
</xml><![endif]-->

</head>

<%
reportTime = Now()
fbID = request.querystring("fbID")
'fbID = "986"

If fbID <> "" Then
SQLQuery = "SELECT FeedbackID, feedback, comment_DateTime FROM WEB_JLC.dbo.vProd_Customer_FB_full_stuff_Narrative WHERE (FeedbackID = "&fbID&")"
Set SQLData = SQL_WebTools_PRODDB.Execute(SQLQuery)
 Do while not SQLData.EOF 
 fbID		 = SQLData("feedbackID")
 fb  		 = SQLData("feedback")
 cmts   	 = SQLData("comment_DateTime")
SQLData.MoveNext
Loop
If cmts <> "" Then
cmts = replace(cmts,"|","<br>")
cmts = Replace(cmts,"&amp;","&")
cmts = Replace(cmts,"&#039;","'")
cmts = Replace(cmts,"&lt;","<")
cmts = Replace(cmts,"&gt;",">")
cmts = Replace(cmts,"(This is the closing response)","<font style='color:grey;'> (This is the closing response)</font>")
End If
SQLQuery1 = "SELECT feedbackID, targetName, feedbackType, CAST(feedbackText AS nvarchar(4000)) AS feedbackText, creationUserID, feedbackDateTime, NatureParent, NatureShortDesc, targetEmail, SCBHQ_Email, "&_
"deleted, deluser, deldate, changeuser, changedate, dateClosed, closingUserID, cdOriginator, cdUnit, cdPhone, cdEmail, fdRodum, "&_
"fdEquipNo, fdEquipAN, fdMaintReqNo, fdWorkOrderNo, fdReqNo, DATEDIFF(d, feedbackDateTime, dateClosed) AS Closure_Days "&_
"FROM WEB_JLC.dbo.vProd_Customer_FB_full_nocmts WHERE (feedbackID = "&fbID&")"
Set SQLData1 = SQL_WebTools_PRODDB.Execute(SQLQuery1)
 Do while not SQLData1.EOF 
 fb_ID		         = SQLData1("feedbackID")
 feedbackType        = SQLData1("feedbackType")
 fbcreateDteTm		 = SQLData1("feedbackDateTime")
 fbdelDte  		     = SQLData1("deldate")
 fbchangeDteTm   	 = SQLData1("changedate")
 fbcloseDteTm   	 = SQLData1("dateClosed")
 closureTime  	     = SQLData1("Closure_Days")
 cdPhone		     = SQLData1("cdPhone")
 cdEmail		     = SQLData1("cdEmail")
 cdOriginator  	     = SQLData1("cdOriginator")
 targetName    	     = SQLData1("targetName")
 NatureParent  	     = SQLData1("NatureParent")
 NatureShortDesc     = SQLData1("NatureShortDesc")
 fdRodum             = SQLData1("fdRodum")
 fdEquipNo           = SQLData1("fdEquipNo")
 fdEquipAN           = SQLData1("fdEquipAN")
 fdMaintReqNo        = SQLData1("fdMaintReqNo")
 fdWorkOrderNo       = SQLData1("fdWorkOrderNo")
 fdReqNo             = SQLData1("fdReqNo")
SQLData1.MoveNext
Loop
Else
response.write("No Feedback Selected")
End If

If fbID <> "" and IsNull(fbcloseDteTm) = true Then
note = "This Feedback remains Open"
Else If fbID <> "" and IsNull(fbcloseDteTm) = false Then
note = "This Feedback is Closed"
Else
note = ""
End If
End If

details = "<tr><th>Type</th><th>Date</th><th>Originator</th><th>Target SCB Unit</th><th>Nature Parent</th><th>Nature Specific</th></tr>"
details = details&"<tr><td>"&feedbackType&"</td><td>"&fbcreateDteTm&"</td><td>"&cdOriginator&"</td><td>"&targetName&"</td><td>"&NatureParent&"</td><td>"&NatureShortDesc&"</td></tr>"
details1 = "<tr><th>Rodum#</th><th>Equip#</th><th>EquipARN/NSN</th><th>MaintReq#</th><th>WorkOrder#</th><th>Requisition#</th></tr>"
details1 = details1&"<tr><td>"&fdRodum&"</td><td>"&fdEquipNo&"</td><td>"&fdEquipAN&"</td><td>"&fdMaintReqNo&"</td><td>"&fdWorkOrderNo&"</td><td>"&fdReqNo&"</td></tr>"


response.ContentType = "application/msword"
response.AddHeader "content-disposition", "attachment; filename=CFT Feedback Narrative ID"&fbID&"-"&reportTime&".doc"
Print = "style='border-bottom:2px solid #dddddd;'" 

%>

<style type="text/css">

@page Section1 {
size: 21.0cm 29.7cm;
mso-page-orientation: portrait;
margin: 1.5cm 1.5cm 1.0cm 1.5cm;
mso-header-margin: 0.5cm;
mso-footer-margin: 0.5cm;
mso-paper-source:0;
}

div.Section1
	{
	page:Section1;
	}

.FOUO {
	font-family: Times;
	font-size:12pt;
	text-align:center;
	color:red;
	padding-top: 0px;
	padding-bottom: 0px;
}

#heading {
	font-family: Times;
	font-size:24pt;
	text-align:center;
	vertical-align: text-bottom;
	color:blue;
	padding-top: 5px;
	padding-bottom: 0px;
}

table {
    font-family: Traditional arabic;
	border: 1px solid #dddddd;
    border-collapse: collapse;
	font-size:10pt;
    width: 100%;
	text-align: center;
}

td, th {
    border: 1px solid #dddddd;
	border-collapse: collapse;
    padding: 1px;
	text-align: center;
}

p {
	font-size:9pt;
	font-family: Traditional arabic;
	text-align: left;
	vertical-align: text-bottom;
	color: #000000;
}

.container{
	font-family: Traditional arabic;
}

</style>
</head>
<body>
<div class="Section1">
<div class="FOUO">For-Official-Use-Only</div>
<p style="text-align:center;">This page produced by  <%=strMyFormalName%> on <%= reportTime %></p>
<h1 id="heading">CFT Narrative Print for ID: <%=fbID%></h1>
	<br>
	<div class="row" id="fbstats">Feedback ID: <% response.write fbID %><br><% response.write fb %><br><% response.write cmts %><br>
	</div>
	<br>
	<div class="row" id="notes"><% response.write note %><br></div>	
	<br>
	<div class="row" id="notes1"><table style="width:100%"><% response.write details %></table><br><br><br>
	</div>
	<div class="row" id="notes1"><table style="width:100%"><% response.write details1 %></table>
	</div>	
</div>
<br /><br />
<div class="FOUO">For-Official-Use-Only</div>
</body>
</html>
<!-- #include virtual = "/includes/WebTools_AccessLog.asp" -->
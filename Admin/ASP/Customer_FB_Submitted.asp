<!-- #include virtual = "/includes/WebTools_Core.asp" -->
<html>
<head>
  <meta name="Shane Kline" content="CFTv2">
<!-- Bootstrap -->  
    <link href="<%= Application("TP-BOOTSTRAP-CSS") %>" rel="stylesheet">
    <link href="<%= Application("TP-BOOTSTRAP-TOGGLE-CSS") %>" rel="stylesheet">
<!-- Jquery -->
	<script src='<%= Application("TP-JQUERY") %>'></script>
	<script src='<%= Application("TP-JQUERYUI") %>'></script>
<!-- Bootstrap -->  	
	<script src='<%= Application("TP-BOOTSTRAP") %>'></script>
	<script src='<%= Application("TP-BOOTSTRAP-TOGGLE") %>'></script>
<!-- Font Awesome -->
    <link href="<%= Application("TP-FONTAWESOME") %>" rel="stylesheet">
<!-- Modernizr -->
    <script src='<%= Application("TP-MODERNIZR") %>'></script>
</head>
<%
Sub SendMail2(strEmailTitle,strEmailSubject,strHTMLHead,strHTMLBody,strMailToList)
If Application("SERVERURL") <> "http://Win2k8server/" Then
  Dim HTML
  Set myMail=CreateObject("CDO.Message")
  myMail.Subject=strEmailSubject
  myMail.From="JLC-SCBHQ-CustomerFeedback@drn.mil.au"
  myMail.To=strMailToList
  'response.write(strMailToList)
  HTML = "<!DOCTYPE HTML PUBLIC""-//IETF//DTD HTML//EN""><html><head><title>" & strEmailTitle & "</title>" & strHTMLHead & "</head>"
  HTML = HTML & "<body>" & strHTMLBody & "</body></html>"
  myMail.HTMLBody=HTML
  myMail.HTMLBodyPart.ContentTransferEncoding = "quoted-printable"
  myMail.Configuration.Fields.Item _
  ("http://schemas.microsoft.com/cdo/configuration/sendusing")=2
  'Name or IP of remote SMTP server
  myMail.Configuration.Fields.Item _
  ("http://schemas.microsoft.com/cdo/configuration/smtpserver")="relay.services.dpe.protected.mil.au"
  'Server port
  myMail.Configuration.Fields.Item _
  ("http://schemas.microsoft.com/cdo/configuration/smtpserverport")=25 
  myMail.Configuration.Fields.Update
  myMail.Send
  set myMail=nothing
End If
End Sub

targetID          = request.querystring("targetID")
feedbackType      = request.querystring("feedbackType")
feedbackText      = request.querystring("freetext")
PageUSERID    = strDRNLogon
'feedbackDateTime  = Now()   --not needed as using getdate() in insert statement
natureParentID    = request.querystring("nature")
natureID          = request.querystring("natureDesc")
cdOriginator      = request.querystring("originator")
cdUnit            = request.querystring("unit")
cdPhone           = request.querystring("phone")
'cdEmail           = request.querystring("email")     --not needed as running sql on webtools core items to retrieve email address
fdrodum           = request.querystring("rodum")
fdequipt          = request.querystring("equipt")
fdequipan         = request.querystring("equipan")
fdmr              = request.querystring("mr")
fdworkorder       = request.querystring("workorder")
fdreq             = request.querystring("req")

If feedbackType <> "Complaint" Then
SQLIDQuery = "SELECT SUBSTRING(NatureID, 1, 2) AS Expr1, NatureParentID FROM WEB_JLC.dbo.Customer_FB_Nature_PROD GROUP BY SUBSTRING(NatureID, 1, 2), NatureParentID HAVING (NatureParentID = N'"&natureParentID&"')"
Set SQLIDData = SQL_WebTools_PRODDB.Execute(SQLIDQuery)
'Do While not SQLIDData.EOF
  natureIDsub = SQLIDData("Expr1")
  SQLIDData.MoveNext  
'Loop
natureID = natureIDsub
End If

SQLEMailQuery = "SELECT ItemID, ItemImage, ItemHeading, ItemBlock, ItemSBlock, ItemOverview, ItemEmail FROM WEBTOOLS_PROD.dbo.CORE_Items WHERE (ItemCreatedBy = N'"&PageUSERID&"') AND (ItemType = N'PERSON')"
Set SQLEmalData = SQL_WebTools_PRODDB.Execute(SQLEMailQuery)
'Do While not SQLEmalData.EOF
  creationUserEmail = SQLEmalData("ItemEmail")
  SQLEmalData.MoveNext  
'Loop

cdOriginator = Replace(cdOriginator,"&","&amp;")
cdOriginator = Replace(cdOriginator,"'","&#039;") 
cdOriginator = Replace(cdOriginator,"<"," ") 
cdOriginator = Replace(cdOriginator,">"," ")
cdOriginator = Replace(cdOriginator,vbCr," ")
cdOriginator = Replace(cdOriginator,vbLf," ")

cdUnit = Replace(cdUnit,"&","&amp;")
cdUnit = Replace(cdUnit,"'","&#039;")
cdUnit = Replace(cdUnit,"<"," ")
cdUnit = Replace(cdUnit,">"," ")
cdUnit = Replace(cdUnit,vbCr," ")
cdUnit = Replace(cdUnit,vbLf," ")

cdPhone = Replace(cdPhone,"&","&amp;")
cdPhone = Replace(cdPhone,"'","&#039;")
cdPhone = Replace(cdPhone,"<"," ")
cdPhone = Replace(cdPhone,">"," ")
cdPhone = Replace(cdPhone,vbCr," ")
cdPhone = Replace(cdPhone,vbLf," ")

fdrodum = Replace(fdrodum,"&","&amp;")
fdrodum = Replace(fdrodum,"'","&#039;")
fdrodum = Replace(fdrodum,"<"," ")
fdrodum = Replace(fdrodum,">"," ")
fdrodum = Replace(fdrodum,vbCr," ")
fdrodum = Replace(fdrodum,vbLf," ")

fdequipt = Replace(fdequipt,"&","&amp;")
fdequipt = Replace(fdequipt,"'","&#039;")
fdequipt = Replace(fdequipt,"<"," ")
fdequipt = Replace(fdequipt,">"," ")
fdequipt = Replace(fdequipt,vbCr," ")
fdequipt = Replace(fdequipt,vbLf," ")

fdequipan = Replace(fdequipan,"&","&amp;")
fdequipan = Replace(fdequipan,"'","&#039;")
fdequipan = Replace(fdequipan,"<"," ")
fdequipan = Replace(fdequipan,">"," ")
fdequipan = Replace(fdequipan,vbCr," ")
fdequipan = Replace(fdequipan,vbLf," ")

fdmr = Replace(fdmr,"&","&amp;")
fdmr = Replace(fdmr,"'","&#039;")
fdmr = Replace(fdmr,"<"," ")
fdmr = Replace(fdmr,">"," ")
fdmr = Replace(fdmr,vbCr," ")
fdmr = Replace(fdmr,vbLf," ")

fdworkorder = Replace(fdworkorder,"&","&amp;")
fdworkorder = Replace(fdworkorder,"'","&#039;")
fdworkorder = Replace(fdworkorder,"<"," ")
fdworkorder = Replace(fdworkorder,">"," ")
fdworkorder = Replace(fdworkorder,vbCr," ")
fdworkorder = Replace(fdworkorder,vbLf," ")

fdreq = Replace(fdreq,"&","&amp;")
fdreq = Replace(fdreq,"'","&#039;")
fdreq = Replace(fdreq,"<"," ")
fdreq = Replace(fdreq,">"," ")
fdreq = Replace(fdreq,vbCr," ")
fdreq = Replace(fdreq,vbLf," ")

feedbackText = Replace(feedbackText,"&","&amp;")
feedbackText = Replace(feedbackText,"'","&#039;")
feedbackText = Replace(feedbackText,"<"," ")
feedbackText = Replace(feedbackText,">"," ")
feedbackText = Replace(feedbackText,vbCr," ")
feedbackText = Replace(feedbackText,vbLf," ")

SQLQuery = "SELECT  MAX(feedbackID) as LastFeedbackID FROM Web_JLC.dbo.CUSTOMDATA_Feedback_prod"
Set SQLData = SQL_WebTools_PRODDB.Execute(SQLQuery)
'Do While not SQLData.EOF
  LastFeedbackID = SQLData("LastFeedbackID")
  SQLData.MoveNext  
'Loop
'LastFeedbackID = 0
LastFeedbackID = LastFeedbackID +1 

SQLstatement = "INSERT INTO Web_JLC.dbo.CUSTOMDATA_Feedback_prod (feedbackID, targetID, feedbackType, feedbackText, creationUserID, feedbackDateTime, natureParentID, natureID, cdOriginator, cdUnit, cdPhone, cdEmail, fdRodum, fdEquipNo, fdEquipAN, fdMaintReqNo, fdWorkOrderNo, fdReqNo) "
SQLstatement = SQLstatement & "VALUES ("&LastFeedbackID&","&targetID&",'"&feedbackType&"','"&feedbackText&"','"&PageUSERID&"',GetDate(), "&natureParentID&",'"&natureID&"','"&cdOriginator&"','"&cdUnit&"','"&cdPhone&"','"&creationUserEmail&"','"&fdrodum&"','"&fdequipt&"','"&fdequipan&"','"&fdmr&"','"&fdworkorder&"','"&fdreq&"')"
SQL_WebTools_PRODDB.Execute(SQLstatement)

SQLdeetsQuery = "SELECT     feedbackID, targetID, feedbackType, feedbackText, creationUserID, feedbackDateTime, natureParentID, natureID, "&_
"dateClosed, closingUserID, cdOriginator, cdUnit, cdPhone, cdEmail, fdRodum, fdEquipNo, fdEquipAN, fdMaintReqNo, fdWorkOrderNo, fdReqNo, "&_
"NatureParent, NatureShortDesc, SCBHQ_Email, targetName, targetEmail FROM WEB_JLC.dbo.vProd_Customer_FB_byID WHERE (feedbackID = "&LastFeedbackID&")"
Set SQLdeetsData = SQL_WebTools_PRODDB.Execute(SQLdeetsQuery)
'Do While not SQLdeetsData.EOF
  NatureParent    = SQLdeetsData("NatureParent")
  NatureShortDesc = SQLdeetsData("NatureShortDesc")
  targetName      = SQLdeetsData("targetName")
  feedbackText    = SQLdeetsData("feedbackText")
  cdUnit          = SQLdeetsData("cdUnit")
  targetEmail     = SQLdeetsData("targetEmail")
  SCBHQ_Email     = SQLdeetsData("SCBHQ_Email")
  cdPhone         = SQLdeetsData("cdPhone")
  cdEmail         = SQLdeetsData("cdEmail")
  fdRodum         = SQLdeetsData("fdRodum")
  fdEquipNo       = SQLdeetsData("fdEquipNo")
  fdEquipAN       = SQLdeetsData("fdEquipAN")
  fdMaintReqNo    = SQLdeetsData("fdMaintReqNo")
  fdWorkOrderNo   = SQLdeetsData("fdWorkOrderNo")
  fdReqNo         = SQLdeetsData("fdReqNo")
  SQLdeetsData.MoveNext  
'Loop

submitDatetime = now()

If feedbackType = "Complaint" Then
specnature = "<br></b>The specific nature of the "&feedbackType&" was related to: <b>"&NatureShortDesc
emailBodySubmitter1 = "<br><br><table class='table table-condensed' table border='1'><tr><th>Unit Rodum No.</th><th>Equip Type</th><th>Equip ARN/NSN</th><th>MR No.</th><th>WO No.</th><th>Req No.</th></tr>"&_
"<tr><td>"&fdRodum&"</td><td>"&fdEquipNo&"</td><td>"&fdEquipAN&"</td><td>"&fdMaintReqNo&"</td><td>"&fdWorkOrderNo&"</td><td>"&fdReqNo&"</td></tr></table>"&_
"<br><br><br>To view, add comment or reply against your "&feedbackType&", click <a href='http://WEBTOOLS.defence.gov.au/prod/Core/Content/contentLoading.asp?PageURL=http://WEBTOOLS.defence.gov.au/prod/default.asp?PageID=32F7ABE0-1394-4AD6-ADEA-EEF2C19B132A'>here</a> and select the menu option: 'Customer Help' then 'View individual Feedback Narrative' or 'Post a reply on my open Feedback'"
Else
specnature = ""
emailBodySubmitter1 = "<br><br><table class='table table-condensed' table border='1'><tr><th>Unit Rodum No.</th><th>Equip Type</th><th>Equip ARN/NSN</th><th>MR No.</th><th>WO No.</th><th>Req No.</th></tr>"&_
"<tr><td>"&fdRodum&"</td><td>"&fdEquipNo&"</td><td>"&fdEquipAN&"</td><td>"&fdMaintReqNo&"</td><td>"&fdWorkOrderNo&"</td><td>"&fdReqNo&"</td></tr></table>"&_
"<br><br><br></b>To view your "&feedbackType&", click <a href='http://WEBTOOLS.defence.gov.au/prod/Core/Content/contentLoading.asp?PageURL=http://WEBTOOLS.defence.gov.au/prod/default.asp?PageID=32F7ABE0-1394-4AD6-ADEA-EEF2C19B132A'>here</a> and select the menu option: 'Customer Help' then 'View individual Feedback Narrative'"
End If

emailBodyreceiver1 = "<br><br><table class='table table-condensed' table border='1'><tr><th>Unit Rodum No.</th><th>Equip Type</th><th>Equip ARN/NSN</th><th>MR No.</th><th>WO No.</th><th>Req No.</th></tr>"&_
"<tr><td>"&fdRodum&"</td><td>"&fdEquipNo&"</td><td>"&fdEquipAN&"</td><td>"&fdMaintReqNo&"</td><td>"&fdWorkOrderNo&"</td><td>"&fdReqNo&"</td></tr></table>"&_
"<br><br><br></b>To view, reply or close this "&feedbackType&", click <a href='http://WEBTOOLS.defence.gov.au/prod/Core/Content/contentLoading.asp?PageURL=http://WEBTOOLS.defence.gov.au/prod/default.asp?PageID=32F7ABE0-1394-4AD6-ADEA-EEF2C19B132A'>here</a> and select the menu option: 'SCB Tools/Reports' then 'View individual Feedback Narrative' or 'Respond/Close Feedback'"
emailBodyreceiver2 = "<br><br>To view a list of all of your feedback, click <a href='http://WEBTOOLS.defence.gov.au/prod/Core/Content/contentLoading.asp?PageURL=http://WEBTOOLS.defence.gov.au/prod/default.asp?PageID=32F7ABE0-1394-4AD6-ADEA-EEF2C19B132A'>here</a> and select the menu option: 'SCB Tools/Reports' then 'Chart/List (Open/Closed) Feedback"

emailBodySubmitter2 = "<br><br>You will be sent an email if: the feedback is changed, the target Unit replies or the feedback is closed.<br>" &_
"The target Unit has received notification of this feedback by email and is expected to reply within 4 business days"

emailBodyreceiverFinal  = emailBodyreceiver1&emailBodyreceiver2
emailBodySubmitterFinal = emailBodySubmitter1&emailBodySubmitter2

emailBodyreceiverSCBHQFinal = "<br><br><table class='table table-condensed' table border='1'><tr><th>Unit Rodum No.</th><th>Equip Type</th><th>Equip ARN/NSN</th><th>MR No.</th><th>WO No.</th><th>Req No.</th></tr>"&_
"<tr><td>"&fdRodum&"</td><td>"&fdEquipNo&"</td><td>"&fdEquipAN&"</td><td>"&fdMaintReqNo&"</td><td>"&fdWorkOrderNo&"</td><td>"&fdReqNo&"</td></tr></table>"&_
"<br><br>You are receiving this email from the SCB Customer Feedback Portal as the specific Nature of this Feedback Type "&_
"("&NatureParent&"-"&NatureShortDesc&") <br>lists this email address against it.<br>" &_
"To change or remove this email address contact SCBHQ-DSCNO-Customer Liaison cell."

SendMail2 feedbackType & " submitted","You submitted a " & feedbackType & " ("&LastFeedbackID&")","<b>You submitted a "&feedbackType&" at the "&_
"<a href='http://WEBTOOLS.defence.gov.au/prod/Core/Content/contentLoading.asp?PageURL=http://WEBTOOLS.defence.gov.au/prod/default.asp?PageID=32F7ABE0-1394-4AD6-ADEA-EEF2C19B132A'>SCB Customer Feedback Portal</a></b><br> "&_
"<br>Feeedback ID: <b>"&LastFeedbackID&"</b><br>The "&feedbackType&" was directed at:<b> "&NatureParent&"/"&targetName&specnature&"<br></b>"&_
"The "&feedbackType&" was made by <b>" & cdOriginator&"/"&cdUnit& "<br></b>At:<b> "&submitDatetime&" (EST)","</b><br>Comment note:"&_
"<br><b>"&feedbackText&emailBodySubmitterFinal,creationUserEmail



SendMail2 feedbackType & " received","You received a " & feedbackType & " ("&LastFeedbackID&")","<b>A "&feedbackType&" was submitted at the "&_
"<a href='http://WEBTOOLS.defence.gov.au/prod/Core/Content/contentLoading.asp?PageURL=http://WEBTOOLS.defence.gov.au/prod/default.asp?PageID=32F7ABE0-1394-4AD6-ADEA-EEF2C19B132A'>SCB Customer Feedback Portal</a></b><br> "&_
"<br>Feeedback ID: <b>"&LastFeedbackID&"</b><br>The "&feedbackType&" was directed at:<b> "&NatureParent&"/"&targetName&specnature&"<br></b> "&_
"The "&feedbackType&" was made by: <b>" & cdOriginator&"/"&cdUnit& "<br></b>Customer contacts: <b>"&cdPhone&" - "&cdEmail&"</b><br>   At:<b> "&submitDatetime&_
" (EST)","</b><br>Comment note:<br><b>"&feedbackText&emailBodyreceiverFinal,targetEmail
'response.write("the scbhq emal is_"&left(SCBHQ_Email,3))







If SCBHQ_Email <> "" Then
SendMail2 feedbackType & " received",feedbackType & " submitted ("&LastFeedbackID&")","<b>A "&feedbackType&" was submitted at the "&_
"<a href='http://WEBTOOLS.defence.gov.au/prod/Core/Content/contentLoading.asp?PageURL=http://WEBTOOLS.defence.gov.au/prod/default.asp?PageID=32F7ABE0-1394-4AD6-ADEA-EEF2C19B132A'>SCB Customer Feedback Portal</a></b><br> "&_
"<br>Feeedback ID: <b>"&LastFeedbackID&"</b><br>The "&feedbackType&" was directed at:<b> "&NatureParent&"/"&targetName&specnature&"<br></b> "&_
"The "&feedbackType&" was made by: <b>" & cdOriginator&"/"&cdUnit& "<br></b>   At:<b> "&submitDatetime&_
" (EST)","</b><br>Comment note:<br>"&feedbackText&emailBodyreceiverSCBHQFinal,SCBHQ_Email
End If
'response.write "<br><br><br>"&PageUSERID
updateNote = "Feedback submitted successfully"
%>

<body>
<div class="container"><br><br><br><br><br><br><br><br><br>
<h1 align="center" id="msg"><small><%=response.write(updateNote)%></small></h1><br>
<h1 align="center"><br><i class="fa fa-thumbs-up fa-4x" aria-hidden="true"></i><br>
You can follow the progress of your feedback <a href="<%= Application("CONTENTLOAD") %>?PageURL=<%= Application("USERDEVELOPED") %>JLC/DSCNO/Customer_FB_Main.asp|vfbID=<%=LastFeedbackID%>">here</a>
<br>This window will automatically close in 6 seconds<br></h1>
</div>
<script>
setTimeout(function() {
window.parent.location.reload();
}, 6000);
</script>
</body>
</html>
<!-- #include virtual = "/includes/WebTools_AccessLog.asp" -->
<!-- #include virtual = "/includes/WebTools_Core.asp" -->
<html>
<head>
  <meta name="Shane Kline" content="CFTv2">
<!-- Bootstrap -->  
    <link href="<%= Application("TP-BOOTSTRAP-CSS") %>" rel="stylesheet">
    <link href="<%= Application("TP-BOOTSTRAP-TOGGLE-CSS") %>" rel="stylesheet">
<!-- Jquery -->
	<script src='<%= Application("TP-JQUERY") %>'></script>
	<script src='<%= Application("TP-JQUERYUI") %>'></script>
<!-- Bootstrap -->  	
	<script src='<%= Application("TP-BOOTSTRAP") %>'></script>
	<script src='<%= Application("TP-BOOTSTRAP-TOGGLE") %>'></script>
<!-- Font Awesome -->
	    <link href="<%= Application("TP-FONTAWESOME") %>" rel="stylesheet">
<!-- Modernizr -->
    <script src='<%= Application("TP-MODERNIZR") %>'></script>

<style>
#notes {
	font-family: verdana;
    font-size: 16px;
	font-weight: bold;
}

#fbstats {
	font-family: verdana;
    font-size: 16px;
	font-weight: bold;
	border: 4px solid #397ee6;
	border-radius: 5px;
}

table, th, td {
	border: 1px solid black;
	border-collapse: collapse;
}
.hideB {

visibility: hidden;
}
.showB {
visibility: visible;
}
</style>
</head>
<%
fbID = request.querystring("vfbID") 
'fbID = "950"
If fbID <> "" Then
SQLQuery = "SELECT FeedbackID, feedback, comment_DateTime FROM WEB_JLC.dbo.vProd_Customer_FB_full_stuff_Narrative WHERE (FeedbackID = "&fbID&")"
'response.write(SQLQuery & "<br>")
Set SQLData = SQL_WebTools_PRODDB.Execute(SQLQuery)
 Do while not SQLData.EOF 
 fbID		 = SQLData("feedbackID")
 fb  		 = SQLData("feedback")
 cmts   	 = SQLData("comment_DateTime")
SQLData.MoveNext
Loop
If cmts <> "" Then
cmts = replace(cmts,"|","<br>")
cmts = Replace(cmts,"&amp;","&")
cmts = Replace(cmts,"&#039;","'")
cmts = Replace(cmts,"&lt;","<")
cmts = Replace(cmts,"&gt;",">")
cmts = Replace(cmts,"(This is the closing response)","<font style='color:grey;'> (This is the closing response)</font>")
End If
SQLQuery1 = "SELECT feedbackID, targetName, feedbackType, CAST(feedbackText AS nvarchar(4000)) AS feedbackText, creationUserID, feedbackDateTime, NatureParent, NatureShortDesc, targetEmail, SCBHQ_Email, "&_
"deleted, deluser, deldate, changeuser, changedate, dateClosed, closingUserID, cdOriginator, cdUnit, cdPhone, cdEmail, fdRodum, "&_
"fdEquipNo, fdEquipAN, fdMaintReqNo, fdWorkOrderNo, fdReqNo, DATEDIFF(d, feedbackDateTime, dateClosed) AS Closure_Days "&_
"FROM WEB_JLC.dbo.vProd_Customer_FB_full_nocmts WHERE (feedbackID = "&fbID&")"
'response.write(SQLQuery1 & "<br>")
Set SQLData1 = SQL_WebTools_PRODDB.Execute(SQLQuery1)
 Do while not SQLData1.EOF 
 fb_ID		         = SQLData1("feedbackID")
 feedbackType        = SQLData1("feedbackType")
 fbcreateDteTm		 = SQLData1("feedbackDateTime")
 fbdelDte  		     = SQLData1("deldate")
 fbchangeDteTm   	 = SQLData1("changedate")
 fbcloseDteTm   	 = SQLData1("dateClosed")
 closureTime  	     = SQLData1("Closure_Days")
 cdPhone		     = SQLData1("cdPhone")
 cdEmail		     = SQLData1("cdEmail")
 cdOriginator  	     = SQLData1("cdOriginator")
 targetName    	     = SQLData1("targetName")
 NatureParent  	     = SQLData1("NatureParent")
 NatureShortDesc     = SQLData1("NatureShortDesc")
 fdRodum             = SQLData1("fdRodum")
 fdEquipNo           = SQLData1("fdEquipNo")
 fdEquipAN           = SQLData1("fdEquipAN")
 fdMaintReqNo        = SQLData1("fdMaintReqNo")
 fdWorkOrderNo       = SQLData1("fdWorkOrderNo")
 fdReqNo             = SQLData1("fdReqNo")
SQLData1.MoveNext
Loop
End If
classBtn = "hideB"
If fbID <> "" and IsNull(fbcloseDteTm) = true Then
note = "This Feedback remains Open"
classBtn = "hideB"
Else If fbID <> "" and IsNull(fbcloseDteTm) = false Then
note = "This Feedback is Closed"
classBtn = "showB"
Else
note = ""
classBtn = "hideB"
End If
End If

details = "<tr><th>Type</th><th>Date</th><th>Originator</th><th>Target SCB Unit</th><th>Nature Parent</th><th>Nature Specific</th></tr>"
details = details&"<tr><td>"&feedbackType&"</td><td>"&fbcreateDteTm&"</td><td>"&cdOriginator&"</td><td>"&targetName&"</td><td>"&NatureParent&"</td><td>"&NatureShortDesc&"</td></tr>"
details1 = "<tr><th>Rodum#</th><th>Equip#</th><th>EquipARN/NSN</th><th>MaintReq#</th><th>WorkOrder#</th><th>Requisition#</th></tr>"
details1 = details1&"<tr><td>"&fdRodum&"</td><td>"&fdEquipNo&"</td><td>"&fdEquipAN&"</td><td>"&fdMaintReqNo&"</td><td>"&fdWorkOrderNo&"</td><td>"&fdReqNo&"</td></tr>"

%>
<script>

function reloadpage(fbID) {
	  
	  parent.SetupOverlay(parent.document.body.clientWidth-700,parent.document.body.clientHeight-100);
      parent.window.frames["ifrmFrontOverlay"].location ="<%= Application("CONTENTLOAD") %>?PageURL=<%= Application("USERDEVELOPED") %>JLC/DSCNO/Customer_FB_Narrative.asp|vfbID=" + fbID
      parent.openOverlay();
};

function fbNarrative() {
	  var fbIDp = <%=fbID%>
	  parent.closeOverlay();
	  parent.SetupOverlay(parent.document.body.clientWidth-700,parent.document.body.clientHeight-100);
      parent.window.frames["ifrmFrontOverlay"].location ="<%= Application("CONTENTLOAD") %>?PageURL=<%= Application("USERDEVELOPED") %>JLC/DSCNO/Customer_FB_Narrative_Print.asp|fbID=" + fbIDp
};
function fbReopenFBItem() {
	  var fbIDp = <%=fbID%>
	  // parent.closeOverlay();
	  parent.SetupOverlay(parent.document.body.clientWidth-700,parent.document.body.clientHeight-100);
      parent.window.frames["ifrmFrontOverlay"].location ="<%= Application("CONTENTLOAD") %>?PageURL=<%= Application("USERDEVELOPED") %>JLC/DSCNO/Customer_FB_ReopenFB.asp|fbID=" + fbIDp
	  parent.openOverlay();
};


</script>
<body>
<div class="container-fluid">
<div class="row"><br></div>
	<div class="row">
		<div class="col-sm-12">
						<div class="form-group">
					<label for="fbID">1. Feedback ID</label>
					<select class="form-control" id="fbID" name="fbID" onchange="reloadpage(this.value)">
					<option value="">Select your Feedback: &nbsp;&nbsp;&nbsp;&nbsp;(Originator-Date-Target Unit)</option>
                    <% SQLQuery2 = "SELECT     TOP (100) PERCENT WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackID, WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.cdOriginator, WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackDateTime, "&_
"WEB_JLC.dbo.Customer_FB_Target_prod.targetName, CONVERT(nvarchar(50), WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.cdOriginator) + N'-' + replace(CONVERT(nvarchar(9), "&_
"WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackDateTime, 6), ' ','/') + N'-' + WEB_JLC.dbo.Customer_FB_Target_prod.targetName AS FBIDadd "&_
"FROM         WEB_JLC.dbo.CUSTOMDATA_Feedback_prod LEFT OUTER JOIN WEB_JLC.dbo.Customer_FB_Target_prod ON WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.targetID = WEB_JLC.dbo.Customer_FB_Target_prod.targetID "&_
"ORDER BY WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackID DESC"
                    Set SQLData2 = SQL_WebTools_PRODDB.Execute(SQLQuery2)
                    Do While not SQLData2.EOF
                        feedbackID      = SQLData2("feedbackID")
						FBIDadd         = SQLData2("FBIDadd")
                        response.write "<option value='"&feedbackID&"'>"&feedbackID&"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;("&FBIDadd&")</option>"
                        SQLData2.MoveNext 
                    Loop 
                    %>					
					</select>
				</div> 
		<a href="#" class="btn btn-default btn-sm" onclick="fbNarrative()">Print</a><a href="#" class="btn btn-default btn-sm <%=classBtn%>" onclick="fbReopenFBItem()">Reopen Feedback</a>
		<a href="javascript:window.parent.location.reload(true);" class="btn btn-primary btn-sm pull-right">Close this window</a></div><br>		
	</div>
	<div class="row" id="fbstats">Feedback ID: <% response.write fbID %><br><% response.write fb %><br><% response.write cmts %><br>
	</div>
	<br>
	<div class="row" id="notes"><% response.write note %><br>
	</div>	
	<br>
	<div class="row" id="notes1"><table style="width:100%"><% response.write details %></table><br><br><br>
	</div>
	<div class="row" id="notes1"><table style="width:100%"><% response.write details1 %></table>
	</div>	
	
</div>
	
<script type="text/javascript" language="javascript">
$(document).ready(function() {
     $('[data-toggle="tooltip"]').tooltip();
});
</script> 
</body>
</html>
<!-- #include virtual = "/includes/WebTools_AccessLog.asp" -->
<!-- #include virtual = "/includes/WebTools_Core.asp" -->
<html>
<head>
  <meta name="Shane Kline" content="CFTv2">
<!-- Bootstrap -->  
    <link href="<%= Application("TP-BOOTSTRAP-CSS") %>" rel="stylesheet">
    <link href="<%= Application("TP-BOOTSTRAP-TOGGLE-CSS") %>" rel="stylesheet">
<!-- Jquery -->
	<script src='<%= Application("TP-JQUERY") %>'></script>
	<script src='<%= Application("TP-JQUERYUI") %>'></script>
<!-- Bootstrap -->  	
	<script src='<%= Application("TP-BOOTSTRAP") %>'></script>
	<script src='<%= Application("TP-BOOTSTRAP-TOGGLE") %>'></script>
<!-- Font Awesome -->
	    <link href="<%= Application("TP-FONTAWESOME") %>" rel="stylesheet">
<!-- Modernizr -->
    <script src='<%= Application("TP-MODERNIZR") %>'></script>
</head>
<%
Sub SendMail2(strEmailTitle,strEmailSubject,strHTMLHead,strHTMLBody,strMailToList)
If Application("SERVERURL") <> "http://Win2k8server/" Then
  Dim HTML
  Set myMail=CreateObject("CDO.Message")
  myMail.Subject=strEmailSubject
  myMail.From="JLC-SCBHQ-CustomerFeedback@drn.mil.au"
  myMail.To=strMailToList
  'response.write(strMailToList)
  HTML = "<!DOCTYPE HTML PUBLIC""-//IETF//DTD HTML//EN""><html><head><title>" & strEmailTitle & "</title>" & strHTMLHead & "</head>"
  HTML = HTML & "<body>" & strHTMLBody & "</body></html>"
  myMail.HTMLBody=HTML
  myMail.HTMLBodyPart.ContentTransferEncoding = "quoted-printable"
  myMail.Configuration.Fields.Item _
  ("http://schemas.microsoft.com/cdo/configuration/sendusing")=2
  'Name or IP of remote SMTP server
  myMail.Configuration.Fields.Item _
  ("http://schemas.microsoft.com/cdo/configuration/smtpserver")="relay.services.dpe.protected.mil.au"
  'Server port
  myMail.Configuration.Fields.Item _
  ("http://schemas.microsoft.com/cdo/configuration/smtpserverport")=25 
  myMail.Configuration.Fields.Update
  myMail.Send
  set myMail=nothing
End If
End Sub

close            = request.querystring("vclose")
fbID             = request.querystring("fbID")
jlu              = request.querystring("jlu")
userID           = request.querystring("userID")
chDate           = request.querystring("cDate")
fbtext           = request.querystring("fbtexta")
custupdt         = request.querystring("custupdt")

SQLEMailQuery = "SELECT ItemID, ItemImage, ItemHeading, ItemBlock, ItemSBlock, ItemOverview, ItemEmail FROM WEBTOOLS_PROD.dbo.CORE_Items WHERE (ItemCreatedBy = N'"&userID&"') AND (ItemType = N'PERSON')"
Set SQLEmalData = SQL_WebTools_PRODDB.Execute(SQLEMailQuery)
Do While not SQLEmalData.EOF
  UserEmail = SQLEmalData("ItemEmail")
  SQLEmalData.MoveNext  
Loop

If close = "1" Then
fbtext = fbtext&" (This is the closing response)"
End If

If jlu = "" Then
jlu = "NULL"
End If
'response.write("the close ind is_"&close&"<br>the id is_"&fbID&"<br>the jlu is_"&jlu&"<br>the useridf is_"&userID&"<br>the date is_"&chDate&"<br>the text is_"&fbtext)

SQLQuery = "SELECT  MAX(commentID) as LastCommentID FROM Web_JLC.dbo.Customer_FB_Comments_prod"
Set SQLData = SQL_WebTools_PRODDB.Execute(SQLQuery)
Do While not SQLData.EOF
  LastCommentID = SQLData("LastCommentID")
  SQLData.MoveNext  
Loop
'LastCommentID = 0
LastCommentID = LastCommentID +1 

fbtext = Replace(fbtext,"&","&amp;")
fbtext = Replace(fbtext,"'","&#039;")
fbtext = Replace(fbtext,"<"," ")
fbtext = Replace(fbtext,">"," ")
fbtext = Replace(fbtext,vbCr," ")
fbtext = Replace(fbtext,vbLf," ")

'----------this query is for the updatenote and emails, it retrieves the feedback details before the changes are applied--------
SQLEditQuery = "SELECT     TOP (100) PERCENT WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackID, WEB_JLC.dbo.Customer_FB_Target_prod.targetID, "&_
"CAST(WEB_JLC.dbo.Customer_FB_Target_prod.targetName AS nvarchar(100)) AS targetName, WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackType, "&_
"WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.natureParentID, WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.creationUserID, "&_
"WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackDateTime, WEB_JLC.dbo.Customer_FB_Nature_prod.NatureParent, WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.natureID, "&_
"WEB_JLC.dbo.Customer_FB_Nature_prod.NatureShortDesc, CAST(WEB_JLC.dbo.Customer_FB_Target_prod.targetEmail AS nvarchar(100)) AS targetEmail, "&_
"WEB_JLC.dbo.Customer_FB_Nature_prod.SCBHQ_Email, WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.cdOriginator "&_
"FROM WEB_JLC.dbo.CUSTOMDATA_Feedback_prod LEFT OUTER JOIN "&_
"WEB_JLC.dbo.Customer_FB_Target_prod ON WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.targetID = WEB_JLC.dbo.Customer_FB_Target_prod.targetID LEFT OUTER JOIN "&_
"WEB_JLC.dbo.Customer_FB_Nature_prod ON WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.natureID = WEB_JLC.dbo.Customer_FB_Nature_prod.NatureID "&_
"WHERE (WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackID = "&fbID&")"

Set SQLEditData = SQL_WebTools_PRODDB.Execute(SQLEditQuery)
 Do while not SQLEditData.EOF 
 fbID		 = SQLEditData("feedbackID")
 originatorID= SQLEditData("creationUserID")
 originator  = SQLEditData("cdOriginator")
 tgID        = SQLEditData("targetID")
 tgName		 = SQLEditData("targetName")
 fbTY   	 = SQLEditData("feedbackType")
 npID        = SQLEditData("NatureParentID")
 npName      = SQLEditData("NatureParent")
 nsID        = SQLEditData("natureID")
 nsName      = SQLEditData("NatureShortDesc")
 targetEmail = SQLEditData("targetEmail")

SQLEditData.MoveNext
Loop

submitDatetime = dateadd("n",30,now())

'this insert into creates a record in the comments table
SQLstatement1 = 					"INSERT INTO Web_JLC.dbo.Customer_FB_Comments_prod (commentID, feedback_ID, commentText, UserID, commentDateTime, commentOwner) "
SQLstatement1 = SQLstatement1 & 	"VALUES ("&LastCommentID&","&fbID&",'"&fbtext&"','"&userID&"',GetDate(),"&jlu&")"
'response.write(SQLstatement1)
SQL_WebTools_PRODDB.Execute(SQLstatement1)

If close = "1" and custupdt = "" Then
'this update into updates a record in the feedback table
SQLstatement2 = 					"UPDATE Web_JLC.dbo.CUSTOMDATA_Feedback_prod"
SQLstatement2 = SQLstatement2 & 	" SET  dateClosed=GetDate(), closingUserID='"&userID&"'"
SQLstatement2 = SQLstatement2 & 	" WHERE (feedbackID = "&fbID&")"
SQL_WebTools_PRODDB.Execute(SQLstatement2)
'response.write(SQLstatement2&"<br>")
'update and close from unit
updateNote = "Feedback Updated and Closed successfully!<br>A 'Close Email' has been sent to the Feedback Originator ("&originator&")."

emailBodySubmitter1 = "<br><br><br>To view your closed "&fbTY&", click <a href='http://WEBTOOLS.defence.gov.au/prod/Core/Content/contentLoading.asp?PageURL=http://WEBTOOLS.defence.gov.au/prod/default.asp?PageID=32F7ABE0-1394-4AD6-ADEA-EEF2C19B132A'>here</a> and select the menu option: 'Customer Help' then 'View individual Feedback Narrative'"
emailBodySubmitter2 = "<br><br><br><br><b>How did we do?<br>" &_
"Please rate our performance in responding to your feedback.<br> "&_
"Click <a href='http://WEBTOOLS.defence.gov.au/prod/Core/Content/contentLoading.asp?PageURL=http://WEBTOOLS.defence.gov.au/prod/default.asp?PageID=32F7ABE0-1394-4AD6-ADEA-EEF2C19B132A'>here</a> and select the menu option: 'Customer Help' then 'Complete Survey' to take a quick 5 question multichoice survey.  Your Survey invitation will remain open for 7 days</b>"
emailBodySubmitterFinal = emailBodySubmitter1&emailBodySubmitter2


SendMail2 fbTY& "Updated!","Your Feedback ("&fbID&") has been updated and closed","<b><a href='http://WEBTOOLS.defence.gov.au/prod/Core/Content/contentLoading.asp?PageURL=http://WEBTOOLS.defence.gov.au/prod/default.asp?PageID=32F7ABE0-1394-4AD6-ADEA-EEF2C19B132A'>SCB Customer Feedback Portal</a></b><br> "&_
"<br>Feedback number <b>"&fbID&"</b> was closed by <b>"&tgName&"</b> on <b>"&submitDatetime&" (EST).</b>","<br><br> "&_
"The target Unit ("&tgName&") responded with:<br>"&fbtext&emailBodySubmitterFinal,originatorID&"@defence.gov.au"
'response.write(originatorID)
Else If close = "0" and custupdt = "" Then

'update only from unit
emailBodySubmitterFinal = "<br><br><br>To view or respond to your "&fbTY&", click <a href='http://WEBTOOLS.defence.gov.au/prod/Core/Content/contentLoading.asp?PageURL=http://WEBTOOLS.defence.gov.au/prod/default.asp?PageID=32F7ABE0-1394-4AD6-ADEA-EEF2C19B132A'>here</a> and select the menu option: 'Customer Help' then 'View individual Feedback Narrative' or 'Post a reply on my open Feedback'"

SendMail2 fbTY& "Updated!","Your Feedback has been updated","<b><a href='http://WEBTOOLS.defence.gov.au/prod/Core/Content/contentLoading.asp?PageURL=http://WEBTOOLS.defence.gov.au/prod/default.asp?PageID=32F7ABE0-1394-4AD6-ADEA-EEF2C19B132A'>SCB Customer Feedback Portal</a></b><br> "&_
"<br>Feedback number <b>"&fbID&"</b> was updated by <b>"&tgName&"</b> on <b>"&submitDatetime&" (EST).</b>","<br><br> "&_
"The target Unit ("&tgName&") responded with:<br>"&fbtext&emailBodySubmitterFinal,originatorID&"@defence.gov.au"
	
updateNote = "Feedback Updated successfully!<br>An 'Update Email' has been sent to the Feedback Originator ("&originator&")."
'response.write(originatorID)
Else

'update only from customer
emailBodyReceiverFinal = "<br><br><br>To view or respond to this "&fbTY&", click <a href='http://WEBTOOLS.defence.gov.au/prod/Core/Content/contentLoading.asp?PageURL=http://WEBTOOLS.defence.gov.au/prod/default.asp?PageID=32F7ABE0-1394-4AD6-ADEA-EEF2C19B132A'>here</a> and select the menu option: 'SCB Tools/Reports' then 'View individual Feedback Narrative' or 'Respond/Close Feedback'"

SendMail2 fbTY& "Updated! ("&fbID&")","Feedback Id ("&fbID&") has been updated","<b><a href='http://WEBTOOLS.defence.gov.au/prod/Core/Content/contentLoading.asp?PageURL=http://WEBTOOLS.defence.gov.au/prod/default.asp?PageID=32F7ABE0-1394-4AD6-ADEA-EEF2C19B132A'>SCB Customer Feedback Portal</a></b><br> "&_
"<br>Feedback number <b>"&fbID&"</b> was updated by <b>"&originator&"</b> on <b>"&submitDatetime&" (EST).</b>","<br><br> "&_
"The Customer ("&originator&") responded with:<br>"&fbtext&emailBodyReceiverFinal,targetEmail
	
updateNote = "Feedback Updated successfully!<br>An 'Update Email' has been sent to the target Unit ("&tgName&")."

End If
End If
%>
<html>
<body>
<br><br><br>
<div class="container">
<h1 align="center" id="msg"><small><%=response.write(updateNote)%></small><br></h1>
<h1 align="center"><small><br><i class="fa fa-thumbs-up fa-4x" aria-hidden="true"></i><br><br>
This window will automatically close in 6 seconds</small></h1>
</div>
</body>
</html>
<script>
setTimeout(function() {
window.parent.location.reload();
}, 6000);
</script>
<!-- #include virtual = "/includes/WebTools_AccessLog.asp" -->
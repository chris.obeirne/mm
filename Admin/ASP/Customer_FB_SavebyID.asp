<!-- #include virtual = "/includes/WebTools_Core.asp" -->
<html>
<head>
  <meta name="Shane Kline" content="CFTv2">
<!-- Bootstrap -->  
    <link href="<%= Application("TP-BOOTSTRAP-CSS") %>" rel="stylesheet">
    <link href="<%= Application("TP-BOOTSTRAP-TOGGLE-CSS") %>" rel="stylesheet">
<!-- Jquery -->
	<script src='<%= Application("TP-JQUERY") %>'></script>
	<script src='<%= Application("TP-JQUERYUI") %>'></script>
<!-- Bootstrap -->  	
	<script src='<%= Application("TP-BOOTSTRAP") %>'></script>
	<script src='<%= Application("TP-BOOTSTRAP-TOGGLE") %>'></script>
<!-- Font Awesome -->
    <link href="<%= Application("TP-FONTAWESOME") %>" rel="stylesheet">
<!-- Modernizr -->
    <script src='<%= Application("TP-MODERNIZR") %>'></script>
</head>
<%
Sub SendMail2(strEmailTitle,strEmailSubject,strHTMLHead,strHTMLBody,strMailToList)
If Application("SERVERURL") <> "http://Win2k8server/" Then
  Dim HTML
  Set myMail=CreateObject("CDO.Message")
  myMail.Subject=strEmailSubject
  myMail.From="JLC-SCBHQ-CustomerFeedback@drn.mil.au"
  myMail.To=strMailToList
  'response.write(strMailToList)
  HTML = "<!DOCTYPE HTML PUBLIC""-//IETF//DTD HTML//EN""><html><head><title>" & strEmailTitle & "</title>" & strHTMLHead & "</head>"
  HTML = HTML & "<body>" & strHTMLBody & "</body></html>"
  myMail.HTMLBody=HTML
  myMail.HTMLBodyPart.ContentTransferEncoding = "quoted-printable"
  myMail.Configuration.Fields.Item _
  ("http://schemas.microsoft.com/cdo/configuration/sendusing")=2
  'Name or IP of remote SMTP server
  myMail.Configuration.Fields.Item _
  ("http://schemas.microsoft.com/cdo/configuration/smtpserver")="relay.services.dpe.protected.mil.au"
  'Server port
  myMail.Configuration.Fields.Item _
  ("http://schemas.microsoft.com/cdo/configuration/smtpserverport")=25 
  myMail.Configuration.Fields.Update
  myMail.Send
  set myMail=nothing
End If
End Sub

feedbackid        = request.querystring("feedbackid")
fbtype            = request.querystring("fbtype")
targetID          = request.querystring("targetunit")
np_ID             = request.querystring("np_ID")
natureID          = request.querystring("nid")
userID            = request.querystring("userID")
'chDate            = request.querystring("cDate") not used, using getdate() for sql insert/update
changeIndicator   = request.querystring("changeIndicator")
fbDel             = request.querystring("fbDel")

If fbtype <> "Complaint" and np_ID = "2" Then
natureID = "MM"
Else If fbtype <> "Complaint" and np_ID = "3" Then
natureID = "WD"
Else If fbtype <> "Complaint" and np_ID = "4" Then
natureID = "LP"
Else If fbtype <> "Complaint" and np_ID = "5" Then
natureID = "CL"
Else If fbtype <> "Complaint" and np_ID = "6" Then
natureID = "NV"
Else If fbtype <> "Complaint" and np_ID = "7" Then
natureID = "GD"
Else If fbtype <> "Complaint" and np_ID = "8" Then
natureID = "OC"
End If
End If
End If
End If
End If
End If
End If

'response.write("the fbID is_"&feedbackid&"<br>the fb type is_"&fbtype&"<br>the targetid is_"&targetID&"<br>nature parent id is_"&np_ID&"<br>the nature id is_"&natureID&"<br>the user id is_"&userID&"<br>the change ind is_"&changeIndicator&"<br>the del ind is_"&fbDel&"<br>")

'check to see if fbid exists in change table
SQLQuery = "SELECT feedbackID FROM WEB_JLC.dbo.Customer_FB_ChangeHistory_prod WHERE (feedbackID = "&feedbackid&")"
Set SQLData = SQL_WebTools_PRODDB.Execute(SQLQuery)
If SQLData.BOF Then
fbIDchangeexist = "0"
Else
 Do while not SQLData.EOF 
 fbIDchangeexist		 = SQLData("feedbackID")
SQLData.MoveNext
Loop
End If

'----------this query is for the updatenote and emails, it retrieves the feedback details before the changes are applied--------
SQLQuery1 = "SELECT     TOP (100) PERCENT WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackID, CAST(WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackText AS nvarchar(4000)) AS feedbackText, WEB_JLC.dbo.Customer_FB_Target_prod.targetID, "&_
"CAST(WEB_JLC.dbo.Customer_FB_Target_prod.targetName AS nvarchar(100)) AS targetName, WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackType, "&_
"WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.natureParentID, WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.creationUserID, "&_
"WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackDateTime, WEB_JLC.dbo.Customer_FB_Nature_prod.NatureParent, WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.natureID, "&_
"WEB_JLC.dbo.Customer_FB_Nature_prod.NatureShortDesc, CAST(WEB_JLC.dbo.Customer_FB_Target_prod.targetEmail AS nvarchar(100)) AS targetEmail, "&_
"WEB_JLC.dbo.Customer_FB_Nature_prod.SCBHQ_Email, WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.cdOriginator, WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.cdUnit, WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.cdPhone, "&_
"WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.cdEmail, WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.fdRodum, WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.fdEquipNo, WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.fdEquipAN, "&_
"WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.fdMaintReqNo, WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.fdWorkOrderNo, WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.fdReqNo FROM WEB_JLC.dbo.CUSTOMDATA_Feedback_prod LEFT OUTER JOIN "&_
"WEB_JLC.dbo.Customer_FB_Target_prod ON WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.targetID = WEB_JLC.dbo.Customer_FB_Target_prod.targetID LEFT OUTER JOIN "&_
"WEB_JLC.dbo.Customer_FB_Nature_prod ON WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.natureID = WEB_JLC.dbo.Customer_FB_Nature_prod.NatureID "&_
"WHERE (WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackID = "&feedbackid&")"

Set SQLData1 = SQL_WebTools_PRODDB.Execute(SQLQuery1)
 Do while not SQLData1.EOF 
 fbID		 		= SQLData1("feedbackID")
 originatorID		= SQLData1("creationUserID")
 originator  		= SQLData1("cdOriginator")
 tgID        		= SQLData1("targetID")
 tgName		 		= SQLData1("targetName")
 fbTY   	 		= SQLData1("feedbackType")
 npID        		= SQLData1("NatureParentID")
 npName      		= SQLData1("NatureParent")
 nsID        		= SQLData1("natureID")
 nsName      		= SQLData1("NatureShortDesc")
 targetEmail 		= SQLData1("targetEmail")
 cdEmail     		= SQLData1("cdEmail")
 cdPhone     		= SQLData1("cdPhone")
 cdUnit      		= SQLData1("cdUnit")
 feedbackDateTime   = SQLData1("feedbackDateTime")
 feedbackText 		= SQLData1("feedbackText")
 fdRodum 		    = SQLData1("fdRodum")
 fdEquipNo 	     	= SQLData1("fdEquipNo")
 fdEquipAN 	    	= SQLData1("fdEquipAN")
 fdMaintReqNo 		= SQLData1("fdMaintReqNo")
 fdWorkOrderNo 		= SQLData1("fdWorkOrderNo")
 fdReqNo 	    	= SQLData1("fdReqNo")
 SQLData1.MoveNext
Loop




If fbIDchangeexist = "0" and fbDel = "1" Then 'if there is no change history and it is a delete then insert into the change history
SQLstatement1 = "INSERT INTO Web_JLC.dbo.Customer_FB_ChangeHistory_prod (feedbackID, deleted, deluser, deldate) "
SQLstatement1 = SQLstatement1 & "VALUES ("&feedbackid&",0,'"&userID&"',GetDate())"
SQL_WebTools_PRODDB.Execute(SQLstatement1)

Else If fbIDchangeexist = "0" and fbDel = "0" Then 'if there is no change history and it is not a delete then insert into feedback change history table and update the feedback test
SQLstatement2 = "INSERT INTO Web_JLC.dbo.Customer_FB_ChangeHistory_prod (feedbackID, changeuser, changedate) "
SQLstatement2 = SQLstatement2 & "VALUES ("&feedbackid&",'"&userID&"',GetDate())"
SQL_WebTools_PRODDB.Execute(SQLstatement2)

SQLstatement3 = "UPDATE Web_JLC.dbo.CUSTOMDATA_Feedback_prod " 
SQLstatement3 = SQLstatement3 & "SET targetID="&targetID&", feedbackType='"&fbtype&"', natureParentID="&np_ID&", natureID='"&natureID&"' WHERE (feedbackID = "&feedbackid&")"
SQL_WebTools_PRODDB.Execute(SQLstatement3)


Else If fbIDchangeexist <> "0" and fbDel = "1" Then 'if there is a change history and it is a delete then update into feedback change history table
SQLstatement4 = "UPDATE Web_JLC.dbo.Customer_FB_ChangeHistory_prod "
SQLstatement4 = SQLstatement4 & "SET deleted=0, deluser='"&userID&"', deldate=GetDate() WHERE (feedbackID = "&feedbackid&")"
SQL_WebTools_PRODDB.Execute(SQLstatement4)

Else 'if there is a change history and is it not a delete then update the change history and feedback test

SQLstatement5 = "UPDATE Web_JLC.dbo.Customer_FB_ChangeHistory_prod"
SQLstatement5 = SQLstatement5 & " SET changeuser='"&userID&"',changedate=GetDate() WHERE (feedbackID = "&feedbackid&")"
SQL_WebTools_PRODDB.Execute(SQLstatement5)

SQLstatement6 = "UPDATE Web_JLC.dbo.CUSTOMDATA_Feedback_prod " 
SQLstatement6 = SQLstatement6 & "SET targetID="&targetID&", feedbackType='"&fbtype&"', natureParentID="&np_ID&", natureID='"&natureID&"' WHERE (feedbackID = "&feedbackid&")"
SQL_WebTools_PRODDB.Execute(SQLstatement6)

End If
End If
End If

'----------this query is for the emails, it retrieves SOME feedback details after the changes are applied--------(ac)
SQLQueryAC = "SELECT     TOP (100) PERCENT WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackID, WEB_JLC.dbo.Customer_FB_Target_prod.targetID, "&_
"CAST(WEB_JLC.dbo.Customer_FB_Target_prod.targetName AS nvarchar(100)) AS targetName, WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackType, "&_
"WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.natureParentID, WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.creationUserID, "&_
"WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackDateTime, WEB_JLC.dbo.Customer_FB_Nature_prod.NatureParent, WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.natureID, "&_
"WEB_JLC.dbo.Customer_FB_Nature_prod.NatureShortDesc, CAST(WEB_JLC.dbo.Customer_FB_Target_prod.targetEmail AS nvarchar(100)) AS targetEmail, "&_
"WEB_JLC.dbo.Customer_FB_Nature_prod.SCBHQ_Email, WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.cdOriginator, WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.cdUnit, WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.cdPhone, "&_
"WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.cdEmail FROM WEB_JLC.dbo.CUSTOMDATA_Feedback_prod LEFT OUTER JOIN "&_
"WEB_JLC.dbo.Customer_FB_Target_prod ON WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.targetID = WEB_JLC.dbo.Customer_FB_Target_prod.targetID LEFT OUTER JOIN "&_
"WEB_JLC.dbo.Customer_FB_Nature_prod ON WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.natureID = WEB_JLC.dbo.Customer_FB_Nature_prod.NatureID "&_
"WHERE (WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackID = "&feedbackid&")"

Set SQLDataAC = SQL_WebTools_PRODDB.Execute(SQLQueryAC)
 Do while not SQLDataAC.EOF 
 targetEmailAC     = SQLDataAC("targetEmail")
 targetIDAC        = SQLDataAC("targetID")
 targetnameAC      = SQLDataAC("targetName")
 NatureShortDescAC = SQLDataAC("NatureShortDesc")
 NatureParentAC    = SQLDataAC("NatureParent")
 feedbackTypeAC    = SQLDataAC("feedbackType") 
SQLDataAC.MoveNext
Loop

submitDatetime = dateadd("n",30,now())
'targetIDAC = "1"
'response.write("the fbdel is_"&fbDel&"<br>the from targetID is_"&targetID&"<br>the to targetIDAC is_"&targetIDAC&"<br>")
'the next chunks of asp is for emails

If feedbackTypeAC = "Complaint" Then
specnature = "<br></b>The specific nature of the "&feedbackTypeAC&" is related to: <b>"&NatureShortDescAC
emailBodySubmitter1sub = "<br><br><table class='table table-condensed' table border='1'><tr><th>Unit Rodum No.</th><th>Equip Type</th><th>Equip ARN/NSN</th><th>MR No.</th><th>WO No.</th><th>Req No.</th></tr>"&_
"<tr><td>"&fdRodum&"</td><td>"&fdEquipNo&"</td><td>"&fdEquipAN&"</td><td>"&fdMaintReqNo&"</td><td>"&fdWorkOrderNo&"</td><td>"&fdReqNo&"</td></tr></table>"&_
"<br><br><br>To view, add comment or reply against your "&feedbackTypeAC&", click <a href='http://WEBTOOLS.defence.gov.au/prod/Core/Content/contentLoading.asp?PageURL=http://WEBTOOLS.defence.gov.au/prod/default.asp?http://WEBTOOLS.defence.gov.au/prod/default.asp?PageID=32F7ABE0-1394-4AD6-ADEA-EEF2C19B132A'>here</a>"
Else
specnature = ""
emailBodySubmitter1sub = "<br><br><table class='table table-condensed' table border='1'><tr><th>Unit Rodum No.</th><th>Equip Type</th><th>Equip ARN/NSN</th><th>MR No.</th><th>WO No.</th><th>Req No.</th></tr>"&_
"<tr><td>"&fdRodum&"</td><td>"&fdEquipNo&"</td><td>"&fdEquipAN&"</td><td>"&fdMaintReqNo&"</td><td>"&fdWorkOrderNo&"</td><td>"&fdReqNo&"</td></tr></table>"&_
"<br><br><br>To view your "&feedbackTypeAC&", click <a href='http://WEBTOOLS.defence.gov.au/prod/Core/Content/contentLoading.asp?PageURL=http://WEBTOOLS.defence.gov.au/prod/default.asp?http://WEBTOOLS.defence.gov.au/prod/default.asp?PageID=32F7ABE0-1394-4AD6-ADEA-EEF2C19B132A'>here</a>"
End If

emailBodyreceiver2sub = "<br><br><table class='table table-condensed' table border='1'><tr><th>Unit Rodum No.</th><th>Equip Type</th><th>Equip ARN/NSN</th><th>MR No.</th><th>WO No.</th><th>Req No.</th></tr>"&_
"<tr><td>"&fdRodum&"</td><td>"&fdEquipNo&"</td><td>"&fdEquipAN&"</td><td>"&fdMaintReqNo&"</td><td>"&fdWorkOrderNo&"</td><td>"&fdReqNo&"</td></tr></table>"&_
"<br><br><br>To view a list of all of your feedback, click <a href='http://WEBTOOLS.defence.gov.au/prod/Core/Content/contentLoading.asp?PageURL=http://WEBTOOLS.defence.gov.au/prod/default.asp?http://WEBTOOLS.defence.gov.au/prod/default.asp?PageID=32F7ABE0-1394-4AD6-ADEA-EEF2C19B132A'>here</a>"

If fbDel = "1" Then 'we need an email to the customer (submitter) and target scb unit (receiver) when feedback is deleted

emailBodySubmitterFinal = "Your "&fbtype&" was deleted at "&submitDatetime&" (EST) by the SCB-DSCNO-Customer Liaison Team."
emailBodyReceiverFinal = "The "&fbtype&" was deleted at "&submitDatetime&" (EST) by the SCB-DSCNO-Customer Liaison Team."

SendMail2 fbtype & " Deleted","Your feedback ("&feedbackid&") has been deleted","<br><b>"&_
"<a href='http://WEBTOOLS.defence.gov.au/prod/Core/Content/contentLoading.asp?PageURL=http://WEBTOOLS.defence.gov.au/prod/default.asp?http://WEBTOOLS.defence.gov.au/prod/default.asp?PageID=32F7ABE0-1394-4AD6-ADEA-EEF2C19B132A'>SCB Customer Feedback Portal</a></b><br> "&_
"<br>",emailBodySubmitterFinal,cdEmail

SendMail2 fbtype & " Deleted","The feedback ID ("&feedbackid&") has been deleted", "<br><b> "&_
"<a href='http://WEBTOOLS.defence.gov.au/prod/Core/Content/contentLoading.asp?PageURL=http://WEBTOOLS.defence.gov.au/prod/default.asp?http://WEBTOOLS.defence.gov.au/prod/default.asp?PageID=32F7ABE0-1394-4AD6-ADEA-EEF2C19B132A'>SCB Customer Feedback Portal</a></b><br> "&_
"<br>",emailBodyreceiverFinal,targetEmail

updateNote = "Feedback deleted successfully!"








Else If fbDel = "0" and tgID = targetIDAC Then 'we need an email to the customer (submitter) and target scb unit (receiver) when feedback is changed where the target scb unit is the same

emailBodySubmitterFinal = "Your "&fbtype&" was changed at "&submitDatetime&" (EST) by the SCB-DSCNO-Customer Liaison Team."
emailBodyReceiverFinal = "The "&fbtype&" was changed at "&submitDatetime&" (EST) by the SCB-DSCNO-Customer Liaison Team."

SendMail2 fbtype & " Changed","Your feedback ("&feedbackid&") has been changed","<b>"&_
"<a href='http://WEBTOOLS.defence.gov.au/prod/Core/Content/contentLoading.asp?PageURL=http://WEBTOOLS.defence.gov.au/prod/default.asp?http://WEBTOOLS.defence.gov.au/prod/default.asp?PageID=32F7ABE0-1394-4AD6-ADEA-EEF2C19B132A'>SCB Customer Feedback Portal</a></b><br><br>  "&_
"The Feedback Type was: <b>"&fbTY&"</b>, it is now: <b>"&feedbackTypeAC&"</b><br>"&_
"The Area Nature was: <b>"&npName&"</b>, it is now: <b>"&NatureParentAC&"</b><br>"&_
"The Specific Nature was: <b>"&nsName&"</b>, it is now: <b>"&NatureShortDescAC&"</b><br>"&_
"<br>",emailBodySubmitterFinal&emailBodySubmitter1sub,cdEmail

SendMail2 fbtype & " Changed","The feedback ID ("&feedbackid&") has been changed", "<b> "&_
"<a href='http://WEBTOOLS.defence.gov.au/prod/Core/Content/contentLoading.asp?PageURL=http://WEBTOOLS.defence.gov.au/prod/default.asp?http://WEBTOOLS.defence.gov.au/prod/default.asp?PageID=32F7ABE0-1394-4AD6-ADEA-EEF2C19B132A'>SCB Customer Feedback Portal</a></b><br><br>  "&_
"The Feedback Type was: <b>"&fbTY&"</b>, it is now: <b>"&feedbackTypeAC&"</b><br>"&_
"The Area Nature was: <b>"&npName&"</b>, it is now: <b>"&NatureParentAC&"</b><br>"&_
"The Specific Nature was: <b>"&nsName&"</b>, it is now: <b>"&NatureShortDescAC&"</b><br>"&_
"<br>",emailBodyreceiverFinal&emailBodyreceiver2sub,targetEmail

updateNote = "Feedback changed successfully!"


Else 'we need an email to the customer (submitter) and target scb unit (receiver) when feedback is changed and where the target scb unit is the different

emailBodyreceiver1 = "<br><br><table class='table table-condensed' table border='1'><tr><th>Unit Rodum No.</th><th>Equip Type</th><th>Equip ARN/NSN</th><th>MR No.</th><th>WO No.</th><th>Req No.</th></tr>"&_
"<tr><td>"&fdRodum&"</td><td>"&fdEquipNo&"</td><td>"&fdEquipAN&"</td><td>"&fdMaintReqNo&"</td><td>"&fdWorkOrderNo&"</td><td>"&fdReqNo&"</td></tr></table>"&_
"<br><br></b>The "&fbtype&" was changed at "&submitDatetime&" (EST) by the SCB-DSCNO-Customer Liaison Team.<br><br><br>To view, reply or close this "&feedbackType&", click <a href='http://WEBTOOLS.defence.gov.au/prod/Core/Content/contentLoading.asp?PageURL=http://WEBTOOLS.defence.gov.au/prod/default.asp?http://WEBTOOLS.defence.gov.au/prod/default.asp?PageID=32F7ABE0-1394-4AD6-ADEA-EEF2C19B132A'>here</a> and select the menu option: 'SCB Tools/Reports' then 'View individual Feedback Narrative' or 'Respond/Close Feedback'"
emailBodyreceiver2 = "<br><br>To view a list of all of your feedback, click <a href='http://WEBTOOLS.defence.gov.au/prod/Core/Content/contentLoading.asp?PageURL=http://WEBTOOLS.defence.gov.au/prod/default.asp?http://WEBTOOLS.defence.gov.au/prod/default.asp?PageID=32F7ABE0-1394-4AD6-ADEA-EEF2C19B132A'>here</a> and select the menu option: 'SCB Tools/Reports' then 'Chart/List (Open/Closed) Feedback"


emailBodySubmitterFinal = "Your "&fbtype&" was changed at "&submitDatetime&" (EST) by the SCB-DSCNO-Customer Liaison Team."
emailBodyReceiverFinal1 = "The "&fbtype&" was changed at "&submitDatetime&" (EST) by the SCB-DSCNO-Customer Liaison Team."
emailBodyReceiverFinal2 = emailBodyreceiver1&emailBodyreceiver2

SendMail2 fbtype & " Changed","Your feedback ("&feedbackid&") has been changed","<b>"&_
"<a href='http://WEBTOOLS.defence.gov.au/prod/Core/Content/contentLoading.asp?PageURL=http://WEBTOOLS.defence.gov.au/prod/default.asp?http://WEBTOOLS.defence.gov.au/prod/default.asp?PageID=32F7ABE0-1394-4AD6-ADEA-EEF2C19B132A'>SCB Customer Feedback Portal</a></b><br><br> "&_
"This Feedback has been re-directed to another target SCB Unit ("&targetnameAC&")<br>" &_
"<br>",emailBodySubmitterFinal&emailBodySubmitter1sub,cdEmail

SendMail2 fbtype & " Changed","The feedback ID ("&feedbackid&") has been changed", "<b> "&_
"<a href='http://WEBTOOLS.defence.gov.au/prod/Core/Content/contentLoading.asp?PageURL=http://WEBTOOLS.defence.gov.au/prod/default.asp?http://WEBTOOLS.defence.gov.au/prod/default.asp?PageID=32F7ABE0-1394-4AD6-ADEA-EEF2C19B132A'>SCB Customer Feedback Portal</a></b><br><br>  "&_
"This Feedback has been re-directed to another target SCB Unit ("&targetnameAC&")<br>" &_
"<br>",emailBodyreceiverFinal1&emailBodyreceiver2sub,targetEmail

SendMail2 fbtype & " received","You received a " & fbtype & " ("&feedbackid&")","<b>A "&fbtype&" was redirected at the "&_
"<a href='http://WEBTOOLS.defence.gov.au/prod/Core/Content/contentLoading.asp?PageURL=http://WEBTOOLS.defence.gov.au/prod/default.asp?http://WEBTOOLS.defence.gov.au/prod/default.asp?PageID=32F7ABE0-1394-4AD6-ADEA-EEF2C19B132A'>SCB Customer Feedback Portal</a></b><br><br>  "&_
"<br>Feeedback ID: <b>"&feedbackid&"</b><br>The "&fbtype&" is directed at:<b> "&NatureParentAC&"/"&targetnameAC&specnature&"<br></b> "&_
"The "&fbtype&" was made by: <b>" & originator&"/"&cdUnit& "<br></b>Customer contacts: <b>"&cdPhone&" - "&cdEmail&"</b>     At:<b> "&feedbackDateTime&_
" (EST)","</b><br>Comment note:<br><b>"&feedbackText&emailBodyreceiverFinal2,targetEmailAC

updateNote = "Feedback changed successfully!"

End If
End If



%>

<body>
<div class="container"><br><br><br>
<h1 align="center" id="msg"><small><%=response.write(updateNote)%></small></h1><br>
<h1 align="center"><br><i class="fa fa-thumbs-up fa-4x" aria-hidden="true"></i><br>
You can follow the progress of your feedback <a href="<%= Application("CONTENTLOAD") %>?PageURL=<%= Application("USERDEVELOPED") %>JLC/DSCNO/Customer_FB_Main.asp">here</a>
<br>This window will automatically close in 6 seconds<br></h1>
</div>
<script>
setTimeout(function() {
window.parent.location.reload();
}, 6000);
</script>
</body>
</html>
<!-- #include virtual = "/includes/WebTools_AccessLog.asp" -->
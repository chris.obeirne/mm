<!-- #include virtual = "/includes/WebTools_Core.asp" -->
<html>
<head>
  <meta name="Shane Kline" content="CFTv2">
<!-- Bootstrap -->  
    <link href="<%= Application("TP-BOOTSTRAP-CSS") %>" rel="stylesheet">
    <link href="<%= Application("TP-BOOTSTRAP-TOGGLE-CSS") %>" rel="stylesheet">
<!-- Jquery -->
	<script src='<%= Application("TP-JQUERY") %>'></script>
	<script src='<%= Application("TP-JQUERYUI") %>'></script>
<!-- Bootstrap -->  	
	<script src='<%= Application("TP-BOOTSTRAP") %>'></script>
	<script src='<%= Application("TP-BOOTSTRAP-TOGGLE") %>'></script>
<!-- Font Awesome -->
	    <link href="<%= Application("TP-FONTAWESOME") %>" rel="stylesheet">
<!-- Modernizr -->
    <script src='<%= Application("TP-MODERNIZR") %>'></script>
<!-- HighCharts -->

  <script src='<%= Application("THIRDPARTY_CORE") %>HighCharts/highcharts.js'></script>
  <script src='<%= Application("THIRDPARTY_CORE") %>HighCharts/Highcharts/js/highcharts-3d.js'></script>
  <script src='<%= Application("THIRDPARTY_CORE") %>HighCharts/highstock/js/highcharts-more.js'></script>
  <script src='<%= Application("THIRDPARTY_CORE") %>HighCharts/exporting.js'></script>
  <script src='<%= Application("THIRDPARTY_CORE") %>HighCharts/highstock/js/modules/solid-gauge.js'></script>
  <script src='<%= Application("THIRDPARTY_CORE") %>HighCharts/highstock/js/modules/drilldown.js'></script>
  
  
<style>
.highcharts-title {
    fill: #434348;
    font-weight: bold;
    font-size: 20px;
}

.checkbox-inline {
border: 1px solid #6495ED;
border-radius: 20px;
padding: .2cm;
}

.form-control {
border: 1px solid #6495ED;
border-radius: 20px;
}

#Pie {
    z-index: 1
}

#modal_pieSQL {
   position: absolute;
   top: 50px;
   right: 0;
   bottom: 0;
   left: 0;
}
</style>
</head>    
<%
fbType          = request.querystring("vType")

If fbType = "" Then
fbType = "Complaint"
End If


SQLNatQuery = "SELECT     TOP (100) PERCENT WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.natureParentID, COUNT(WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackID) AS Count_FB, "&_
"WEB_JLC.dbo.Customer_FB_Nature_prod.NatureParent FROM         WEB_JLC.dbo.CUSTOMDATA_Feedback_prod LEFT OUTER JOIN "&_
"WEB_JLC.dbo.Customer_FB_Nature_prod ON WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.natureID = WEB_JLC.dbo.Customer_FB_Nature_prod.NatureID LEFT OUTER JOIN "&_
"WEB_JLC.dbo.Customer_FB_ChangeHistory_prod ON WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackID = WEB_JLC.dbo.Customer_FB_ChangeHistory_prod.feedbackID "&_
"WHERE     (WEB_JLC.dbo.Customer_FB_ChangeHistory_prod.deleted IS NULL) AND (WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackType = N'"&fbType&"')" &_
"GROUP BY WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.natureParentID, WEB_JLC.dbo.Customer_FB_Nature_prod.NatureParent "&_
"HAVING      (WEB_JLC.dbo.Customer_FB_Nature_prod.NatureParent IS NOT NULL) ORDER BY WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.natureParentID"
Set SQLNatData = SQL_WebTools_PRODDB.Execute(SQLNatQuery)
Do while not SQLNatData.EOF	
countNat = SQLNatData("Count_FB")
NaturePIDNat = SQLNatData("natureParentID")
NatureParentNat = SQLNatData("NatureParent")



SQLNatQuery1 = "SELECT     TOP (100) PERCENT COUNT(WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackID) AS Count_FB, WEB_JLC.dbo.Customer_FB_Nature_prod.NatureID "&_
", WEB_JLC.dbo.Customer_FB_Nature_prod.NIDColor, WEB_JLC.dbo.Customer_FB_Nature_prod.NatureShortDesc FROM WEB_JLC.dbo.CUSTOMDATA_Feedback_prod LEFT OUTER JOIN "&_
"WEB_JLC.dbo.Customer_FB_Nature_prod ON WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.natureID = WEB_JLC.dbo.Customer_FB_Nature_prod.NatureID LEFT OUTER JOIN "&_
"WEB_JLC.dbo.Customer_FB_ChangeHistory_prod ON WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackID = WEB_JLC.dbo.Customer_FB_ChangeHistory_prod.feedbackID "&_
"WHERE (WEB_JLC.dbo.Customer_FB_ChangeHistory_prod.deleted IS NULL) AND (WEB_JLC.dbo.Customer_FB_Nature_prod.NatureParent IS NOT NULL) "&_
"AND (WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackType = N'"&fbType&"') AND (WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.natureParentID = '"&NaturePIDNat&"') "&_
" AND (WEB_JLC.dbo.Customer_FB_Nature_prod.NatureStatus = 1) GROUP BY WEB_JLC.dbo.Customer_FB_Nature_prod.NatureID, WEB_JLC.dbo.Customer_FB_Nature_prod.NIDColor, "&_
"WEB_JLC.dbo.Customer_FB_Nature_prod.NatureShortDesc ORDER BY WEB_JLC.dbo.Customer_FB_Nature_prod.NatureID"
'response.write(SQLNatQuery1)
Set SQLNatData1 = SQL_Webtools_PRODDB.Execute(SQLNatQuery1)
Do while not SQLNatData1.EOF	
countNat1 = SQLNatData1("Count_FB")
nameNat1 = SQLNatData1("NatureID")
fbColourNat = SQLNatData1("NIDColor")
fbshortdescNat = SQLNatData1("NatureShortDesc")


SQLNatData1.MoveNext
Piesub1     = Piesub1& "{name: '"&nameNat1&"-"&fbshortdescNat&"', y:"&countNat1&", color: '"&fbColourNat&"',dataLabels: {color: '#000000', style: {textShadow: false}}},"
Loop

If Piesub1 <> "" Then
Piesub1    = left(Piesub1,len(Piesub1)-1)
End If

Piesub     = Piesub&"{name: '"&NatureParentNat&"', id: '"&NatureParentNat&"', data: ["&Piesub1&"]},"

SQLNatData.MoveNext

If fbType = "Complaint" Then
drillevent = NatureParentNat
Else 
drillevent = "null"
End If

If NaturePIDNat = "2" Then 
NatureParentColour = "#bfd0ff"
Else If NaturePIDNat = "3" Then 
NatureParentColour = "#9886b3"
Else If NaturePIDNat = "4" Then 
NatureParentColour = "#d0ffbf"
Else If NaturePIDNat = "5" Then
NatureParentColour = "#ffc8bf"
Else If NaturePIDNat = "6" Then
NatureParentColour = "#e63995"
Else If NaturePIDNat = "7" Then
NatureParentColour = "#990052"
Else If NaturePIDNat = "8" Then
NatureParentColour = "#e6acd2"
End If
End If 
End If
End If
End If 
End If
End If

strwedgeNat = strwedgeNat&"{name: '"&NatureParentNat&"', y:"&countNat&", color: '"&NatureParentColour&"', drilldown:'"&drillevent&"', dataLabels: {color: '#000000', style: {textShadow: false}}},"
Piesub1 = ""
Loop
If strwedgeNat <> "" Then
strwedgeNat = left(strwedgeNat,len(strwedgeNat)-1)
nildata = ""
End If
Pie     = "[{name: 'Area Nature', data: ["&strwedgeNat&"]}],"


If Piesub <> "" Then
Piesub    = left(Piesub,len(Piesub)-1)
End If

Piesub = "["&Piesub&"]"

'--------------------------------------SITES---------------------------------------------------------------------------

Dim scb(7)
scb(0)="1"
scb(1)="2"
scb(2)="3"
scb(3)="4"
scb(4)="5"
scb(5)="6" 
scb(6)="7" 
scb(7)="8" 
For Each jlu In scb

SQLSiteQuery = "SELECT     TOP (100) PERCENT WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.natureParentID, COUNT(WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackID) AS Count_FB, "&_
"WEB_JLC.dbo.Customer_FB_Nature_prod.NatureParent, WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.targetID FROM WEB_JLC.dbo.CUSTOMDATA_Feedback_prod LEFT OUTER JOIN "&_
"WEB_JLC.dbo.Customer_FB_Nature_prod ON WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.natureID = WEB_JLC.dbo.Customer_FB_Nature_prod.NatureID LEFT OUTER JOIN "&_
"WEB_JLC.dbo.Customer_FB_ChangeHistory_prod ON WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackID = WEB_JLC.dbo.Customer_FB_ChangeHistory_prod.feedbackID "&_
"WHERE (WEB_JLC.dbo.Customer_FB_ChangeHistory_prod.deleted IS NULL) AND (WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackType = N'"&fbType&"') "&_
"GROUP BY WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.natureParentID, WEB_JLC.dbo.Customer_FB_Nature_prod.NatureParent, WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.targetID "&_
"HAVING (WEB_JLC.dbo.Customer_FB_Nature_prod.NatureParent IS NOT NULL) AND (WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.targetID = "&jlu&") "&_
"ORDER BY WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.natureParentID"
Set SQLSiteData = SQL_WebTools_PRODDB.Execute(SQLSiteQuery)
Do while not SQLSiteData.EOF	
countSite = SQLSiteData("Count_FB")
NaturePIDSite = SQLSiteData("natureParentID")
NatureParentSite = SQLSiteData("NatureParent")
site = jlu




SQLNSiteQuery1 = "SELECT     TOP (100) PERCENT COUNT(WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackID) AS Count_FB, WEB_JLC.dbo.Customer_FB_Nature_prod.NatureID, "&_
"WEB_JLC.dbo.Customer_FB_Nature_prod.NIDColor, WEB_JLC.dbo.Customer_FB_Nature_prod.NatureShortDesc, WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.targetID "&_
"FROM WEB_JLC.dbo.CUSTOMDATA_Feedback_prod LEFT OUTER JOIN "&_
"WEB_JLC.dbo.Customer_FB_Nature_prod ON WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.natureID = WEB_JLC.dbo.Customer_FB_Nature_prod.NatureID LEFT OUTER JOIN "&_
"WEB_JLC.dbo.Customer_FB_ChangeHistory_prod ON WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackID = WEB_JLC.dbo.Customer_FB_ChangeHistory_prod.feedbackID "&_
"WHERE (WEB_JLC.dbo.Customer_FB_ChangeHistory_prod.deleted IS NULL) AND (WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackType = N'"&fbType&"') AND "&_
"(WEB_JLC.dbo.Customer_FB_Nature_prod.NatureParent IS NOT NULL) AND (WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.natureParentID = '"&NaturePIDSite&"') AND "&_
"(WEB_JLC.dbo.Customer_FB_Nature_prod.NatureStatus = 1) GROUP BY WEB_JLC.dbo.Customer_FB_Nature_prod.NatureID, WEB_JLC.dbo.Customer_FB_Nature_prod.NIDColor, "&_
"WEB_JLC.dbo.Customer_FB_Nature_prod.NatureShortDesc, WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.targetID "&_
"HAVING (WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.targetID = "&site&") ORDER BY WEB_JLC.dbo.Customer_FB_Nature_prod.NatureID"
'response.write(SQLNSiteQuery1)
Set SQLSiteData1 = SQL_Webtools_PRODDB.Execute(SQLNSiteQuery1)
Do while not SQLSiteData1.EOF	
countSite1 = SQLSiteData1("Count_FB")
nameSite1 = SQLSiteData1("NatureID")
fbColourSite = SQLSiteData1("NIDColor")
fbshortdescSite = SQLSiteData1("NatureShortDesc")
targetID = SQLSiteData1("targetID")

SQLSiteData1.MoveNext
PiesubSite1     = PiesubSite1& "{name: '"&nameSite1&"-"&fbshortdescSite&"', y:"&countSite1&", color: '"&fbColourSite&"',dataLabels: {color: '#000000', style: {textShadow: false}}},"
Loop

If PiesubSite1 <> "" Then
PiesubSite1    = left(PiesubSite1,len(PiesubSite1)-1)
End If

PiesubSite     = PiesubSite&"{name: '"&NatureParentSite&"', id: '"&NatureParentSite&"', data: ["&PiesubSite1&"]},"

SQLSiteData.MoveNext

If fbType = "Complaint" Then
drilleventsite = NatureParentSite
Else 
drilleventsite = "null"
End If


If NaturePIDSite = "2" Then 'mm
NatureParentColour = "#bfd0ff"
Else If NaturePIDSite = "3" Then 'wd
NatureParentColour = "#9886b3"
Else If NaturePIDSite = "4" Then 'lp
NatureParentColour = "#d0ffbf"
Else If NaturePIDSite = "5" Then   'cl
NatureParentColour = "#ffc8bf"
Else If NaturePIDNat = "6" Then
NatureParentColour = "#e63995"
Else If NaturePIDNat = "7" Then
NatureParentColour = "#990052"
Else If NaturePIDNat = "8" Then
NatureParentColour = "#e6acd2"
End If
End If 
End If
End If
End If 
End If
End If


strwedgeSite = strwedgeSite&"{name: '"&NatureParentSite&"', y:"&countSite&", color: '"&NatureParentColour&"', drilldown:'"&drilleventsite&"', dataLabels: {color: '#000000', style: {textShadow: false}}},"
PiesubSite1 = ""



Loop
If strwedgeSite <> "" Then
strwedgeSite = left(strwedgeSite,len(strwedgeSite)-1)
nildatasite = ""
End If
PieSite     = "[{name: 'Area Nature', data: ["&strwedgeSite&"]}],"
strwedgeSite = ""

If PiesubSite <> "" Then
PiesubSite    = left(PiesubSite,len(PiesubSite)-1)
End If
PiesubSite = "["&PiesubSite&"]"

strall = strall&PieSite&" drilldown: {series: "&PiesubSite&"}|"
PiesubSite = ""

Next

strsplit = Split(strall,"|")
SCBHQ = strsplit(0)
JLUN = strsplit(1)
JLUS = strsplit(2)
JLUE = strsplit(3)
JLUW = strsplit(4)
JLUNQ = strsplit(5)
JLUSQ = strsplit(6)
JLUV = strsplit(7)
%>



<script type="text/javascript">
function mailFn() {  

var m_address = 'mailto:jlc-scb-customermanagement@drn.mil.au?subject=WebTools%20Customer%20Feedback%20Tool%20-%20Feedback%20Pie%20Charts%20and%20Details';
location.href= m_address;
};

function openPieReport(gSite, lWedge, fbpietype) {
//alert('im in the details load');
	  parent.SetupOverlay(parent.document.body.clientWidth-70,parent.document.body.clientHeight-70);
      parent.window.frames["ifrmFrontOverlay"].location ="<%= Application("CONTENTLOAD") %>?PageURL=<%= Application("USERDEVELOPED") %>JLC/DSCNO/Customer_FB_PiesPlain_Detail.asp|vpieLoc=" + gSite + "|vlocWedge=" + lWedge + "|vfbType=" + fbpietype 
      parent.openOverlay();
};

 function pieApply(otype) {
       window.open("<%= Application("CONTENTLOAD") %>?PageURL=<%= Application("USERDEVELOPED") %>JLC/DSCNO/Customer_FB_PiesPlain.asp|vType=" + otype, "_self");
 };  
	
$(function() {
			   var fbtypeprevent = '<%=fbType%>';
$('#Pie').highcharts({
	chart: {
        type: 'pie',
		margin: [35, 0, 90, 0],
		spacingBottom: 0,
		events: {
               drilldown: function(e) {		   
                var xAxis = e.point.series.xAxis,
                  gSite = e.point.name;
				  //alert('possum');
                  setTimeout(function () {
                          }, 10);
               },
               drillup: function() {
                var chart = this,
                  xAxis = chart.xAxis[0];
                  setTimeout(function () {
                          }, 10);
               }
           }
    },
    title: {
        text: 'All SCB Entities Combined'
    },
	subtitle: {
			text: '<%=fbType%>s*',
			align: 'left'
	},
    plotOptions: {
        series: {
            dataLabels: {
                enabled: true,
                format: '{point.y}',
				distance: -30,
				color: '#000000',
				style: {fontSize: '16px'}
            },
               point: {
                    events: {
                        click: function (e) {
                            var lWedge = this.name;
							var lWedge = lWedge.substring(0,4);
							var lWedgeshift = lWedge.substring(0,2);
							if (lWedge == 'Mate') {lWedge = 'MM'}
							if (lWedge == 'Ware') {lWedge = 'WD'}
							if (lWedge == 'Loan') {lWedge = 'LP'}
							if (lWedge == 'Clot') {lWedge = 'CL'}
							if (lWedge == 'Nati') {lWedge = 'NV'}
							if (lWedge == 'Glob') {lWedge = 'GD'}
							if (lWedge == 'Oper') {lWedge = 'OC'}
							var gSite = "National";
							var fbpietype = '<%=fbType%>';
                            if (e.ctrlKey) {
							//alert('im in CTRL click with_'+lWedge+'and site is_'+gSite+'and type is_'+fbpietype);
							openPieReport(lWedge, gSite, fbpietype);
							}
							if (e.shiftKey) {
							//alert('im in shift click with_'+lWedgeshift+'and site is_'+gSite+'and type is_'+fbpietype);
							openPieReport(lWedgeshift, gSite, fbpietype);
							}
                        }
                    }
                }
			},
		pie: {
			showInLegend: true
			}
        
    },
	legend: {
		reversed: true
	},
    tooltip: {
        headerFormat: '<span style="font-size:14px">{series.name}</span><br>',
        pointFormat: '<span style="color:{#FF0000}">{point.name}</span>: <br><b>{point.y}</b>'
    },
	credits: {
			  position: {
			  align: 'left',
				  x: 20,
				  y: 0
				  },
			  text: 'Data as at: <%=Now()%>'
    },
   series: <%=Pie%> drilldown: {series: <%=Piesub%>}
});
$('#PieSCBHQ').highcharts({
	chart: {
        type: 'pie',
		margin: [0, 0, 0, 0],
		spacingBottom: 0,
		events: {
               drilldown: function(e) {		   
                var xAxis = e.point.series.xAxis,
                  gSite = e.point.name;
                  setTimeout(function () {
                          }, 10);
               },
               drillup: function() {
                var chart = this,
                  xAxis = chart.xAxis[0];
                  setTimeout(function () {
                          }, 10);
               }
           }
    },
    title: {
        text: 'SCBHQ'
    },
	subtitle: {
			text: '<%=fbType%>s*',
			align: 'left'
	},
    plotOptions: {
        series: {
            dataLabels: {
                enabled: true,
                format: '{point.y}',
				distance: -30,
				color: '#000000',
				style: {fontSize: '16px'}
            },
               point: {
                    events: {
                        click: function (e) {
                            var lWedge = this.name;
							var lWedge = lWedge.substring(0,4);
							var lWedgeshift = lWedge.substring(0,2);
							if (lWedge == 'Mate') {lWedge = 'MM'}
							if (lWedge == 'Ware') {lWedge = 'WD'}
							if (lWedge == 'Loan') {lWedge = 'LP'}
							if (lWedge == 'Clot') {lWedge = 'CL'}
							if (lWedge == 'Nati') {lWedge = 'NV'}
							if (lWedge == 'Glob') {lWedge = 'GD'}
							if (lWedge == 'Oper') {lWedge = 'OC'}
							var gSite = "SCBHQ";
							var fbpietype = '<%=fbType%>';
                            if (e.ctrlKey) {
							//alert('im in CTRL click with_'+lWedge+'and site is_'+gSite+'and type is_'+fbpietype);
							openPieReport(lWedge, gSite, fbpietype);
							}
							if (e.shiftKey) {
							//alert('im in shift click with_'+lWedgeshift+'and site is_'+gSite+'and type is_'+fbpietype);
							openPieReport(lWedgeshift, gSite, fbpietype);
							}
                        }
                    }
                }
			},
		pie: {
			showInLegend: true
			}
        
    },
	legend: false,
    tooltip: {
        headerFormat: '<span style="font-size:14px">{series.name}</span><br>',
        pointFormat: '<span style="color:{#FF0000}">{point.name}</span>: <br><b>{point.y}</b>'
    },
	credits: false,
    series: <%=SCBHQ%>
});
$('#PieJLUE').highcharts({
	chart: {
        type: 'pie',
		margin: [0, 0, 0, 0],
		spacingBottom: 0,
		events: {
               drilldown: function(e) {		   
                var xAxis = e.point.series.xAxis,
                  gSite = e.point.name;
                  setTimeout(function () {
                          }, 10);
               },
               drillup: function() {
                var chart = this,
                  xAxis = chart.xAxis[0];
                  setTimeout(function () {
                          }, 10);
               }
           }
    },
    title: {
        text: 'JLUE'
    },
	subtitle: {
			text: '<%=fbType%>s*',
			align: 'left'
	},
    plotOptions: {
        series: {
            dataLabels: {
                enabled: true,
                format: '{point.y}',
				distance: -30,
				color: '#000000',
				style: {fontSize: '16px'}
            },
               point: {
                    events: {
                        click: function (e) {
                            var lWedge = this.name;
							var lWedge = lWedge.substring(0,4);
							var lWedgeshift = lWedge.substring(0,2);
							if (lWedge == 'Mate') {lWedge = 'MM'}
							if (lWedge == 'Ware') {lWedge = 'WD'}
							if (lWedge == 'Loan') {lWedge = 'LP'}
							if (lWedge == 'Clot') {lWedge = 'CL'}
							if (lWedge == 'Nati') {lWedge = 'NV'}
							if (lWedge == 'Glob') {lWedge = 'GD'}
							if (lWedge == 'Oper') {lWedge = 'OC'}
							var gSite = "JLUE";
							var fbpietype = '<%=fbType%>';
                            if (e.ctrlKey) {
							//alert('im in CTRL click with_'+lWedge+'and site is_'+gSite+'and type is_'+fbpietype);
							openPieReport(lWedge, gSite, fbpietype);
							}
							if (e.shiftKey) {
							//alert('im in shift click with_'+lWedgeshift+'and site is_'+gSite+'and type is_'+fbpietype);
							openPieReport(lWedgeshift, gSite, fbpietype);
							}
                        }
                    }
                }
			},
		pie: {
			showInLegend: true
			}
        
    },
	legend: false,
    tooltip: {
        headerFormat: '<span style="font-size:14px">{series.name}</span><br>',
        pointFormat: '<span style="color:{#FF0000}">{point.name}</span>: <br><b>{point.y}</b>'
    },
	credits: false,
    series: <%=JLUE%>
});
$('#PieJLUV').highcharts({
	chart: {
        type: 'pie',
		margin: [0, 0, 0, 0],
		spacingBottom: 0,
		events: {
               drilldown: function(e) {		   
                var xAxis = e.point.series.xAxis,
                  gSite = e.point.name;
                  setTimeout(function () {
                          }, 10);
               },
               drillup: function() {
                var chart = this,
                  xAxis = chart.xAxis[0];
                  setTimeout(function () {
                          }, 10);
               }
           }
    },
    title: {
        text: 'JLUV'
    },
	subtitle: {
			text: '<%=fbType%>s*',
			align: 'left'
	},
    plotOptions: {
        series: {
            dataLabels: {
                enabled: true,
                format: '{point.y}',
				distance: -30,
				color: '#000000',
				style: {fontSize: '16px'}
            },
               point: {
                    events: {
                        click: function (e) {
                            var lWedge = this.name;
							var lWedge = lWedge.substring(0,4);
							var lWedgeshift = lWedge.substring(0,2);
							if (lWedge == 'Mate') {lWedge = 'MM'}
							if (lWedge == 'Ware') {lWedge = 'WD'}
							if (lWedge == 'Loan') {lWedge = 'LP'}
							if (lWedge == 'Clot') {lWedge = 'CL'}
							if (lWedge == 'Nati') {lWedge = 'NV'}
							if (lWedge == 'Glob') {lWedge = 'GD'}
							if (lWedge == 'Oper') {lWedge = 'OC'}
							var gSite = "JLUV";
							var fbpietype = '<%=fbType%>';
                            if (e.ctrlKey) {
							//alert('im in CTRL click with_'+lWedge+'and site is_'+gSite+'and type is_'+fbpietype);
							openPieReport(lWedge, gSite, fbpietype);
							}
							if (e.shiftKey) {
							//alert('im in shift click with_'+lWedgeshift+'and site is_'+gSite+'and type is_'+fbpietype);
							openPieReport(lWedgeshift, gSite, fbpietype);
							}
                        }
                    }
                }
			},
		pie: {
			showInLegend: true
			}
        
    },
	legend: false,
    tooltip: {
        headerFormat: '<span style="font-size:14px">{series.name}</span><br>',
        pointFormat: '<span style="color:{#FF0000}">{point.name}</span>: <br><b>{point.y}</b>'
    },
	credits: false,
    series: <%=JLUV%>
});
$('#PieJLUS').highcharts({
	chart: {
        type: 'pie',
		margin: [0, 0, 0, 0],
		spacingBottom: 0,
		events: {
               drilldown: function(e) {		   
                var xAxis = e.point.series.xAxis,
                  gSite = e.point.name;
                  setTimeout(function () {
                          }, 10);
               },
               drillup: function() {
                var chart = this,
                  xAxis = chart.xAxis[0];
                  setTimeout(function () {
                          }, 10);
               }
           }
    },
    title: {
        text: 'JLUS'
    },
	subtitle: {
			text: '<%=fbType%>s*',
			align: 'left'
	},
    plotOptions: {
        series: {
            dataLabels: {
                enabled: true,
                format: '{point.y}',
				distance: -30,
				color: '#000000',
				style: {fontSize: '16px'}
            },
               point: {
                    events: {
                        click: function (e) {
                            var lWedge = this.name;
							var lWedge = lWedge.substring(0,4);
							var lWedgeshift = lWedge.substring(0,2);
							if (lWedge == 'Mate') {lWedge = 'MM'}
							if (lWedge == 'Ware') {lWedge = 'WD'}
							if (lWedge == 'Loan') {lWedge = 'LP'}
							if (lWedge == 'Clot') {lWedge = 'CL'}
							if (lWedge == 'Nati') {lWedge = 'NV'}
							if (lWedge == 'Glob') {lWedge = 'GD'}
							if (lWedge == 'Oper') {lWedge = 'OC'}
							var gSite = "JLUS";
							var fbpietype = '<%=fbType%>';
                            if (e.ctrlKey) {
							//alert('im in CTRL click with_'+lWedge+'and site is_'+gSite+'and type is_'+fbpietype);
							openPieReport(lWedge, gSite, fbpietype);
							}
							if (e.shiftKey) {
							//alert('im in shift click with_'+lWedgeshift+'and site is_'+gSite+'and type is_'+fbpietype);
							openPieReport(lWedgeshift, gSite, fbpietype);
							}
                        }
                    }
                }
			},
		pie: {
			showInLegend: true
			}
        
    },
	legend: false,
    tooltip: {
        headerFormat: '<span style="font-size:14px">{series.name}</span><br>',
        pointFormat: '<span style="color:{#FF0000}">{point.name}</span>: <br><b>{point.y}</b>'
    },
	credits: false,
    series: <%=JLUS%>
});
$('#PieJLUW').highcharts({
	chart: {
        type: 'pie',
		margin: [0, 0, 0, 0],
		spacingBottom: 0,
		events: {
               drilldown: function(e) {		   
                var xAxis = e.point.series.xAxis,
                  gSite = e.point.name;
                  setTimeout(function () {
                          }, 10);
               },
               drillup: function() {
                var chart = this,
                  xAxis = chart.xAxis[0];
                  setTimeout(function () {
                          }, 10);
               }
           }
    },
    title: {
        text: 'JLUW'
    },
	subtitle: {
			text: '<%=fbType%>s*',
			align: 'left'
	},
    plotOptions: {
        series: {
            dataLabels: {
                enabled: true,
                format: '{point.y}',
				distance: -30,
				color: '#000000',
				style: {fontSize: '16px'}
            },
               point: {
                    events: {
                        click: function (e) {
                            var lWedge = this.name;
							var lWedge = lWedge.substring(0,4);
							var lWedgeshift = lWedge.substring(0,2);
							if (lWedge == 'Mate') {lWedge = 'MM'}
							if (lWedge == 'Ware') {lWedge = 'WD'}
							if (lWedge == 'Loan') {lWedge = 'LP'}
							if (lWedge == 'Clot') {lWedge = 'CL'}
							if (lWedge == 'Nati') {lWedge = 'NV'}
							if (lWedge == 'Glob') {lWedge = 'GD'}
							if (lWedge == 'Oper') {lWedge = 'OC'}
							var gSite = "JLUW";
							var fbpietype = '<%=fbType%>';
                            if (e.ctrlKey) {
							//alert('im in CTRL click with_'+lWedge+'and site is_'+gSite+'and type is_'+fbpietype);
							openPieReport(lWedge, gSite, fbpietype);
							}
							if (e.shiftKey) {
							//alert('im in shift click with_'+lWedgeshift+'and site is_'+gSite+'and type is_'+fbpietype);
							openPieReport(lWedgeshift, gSite, fbpietype);
							}
                        }
                    }
                }
			},
		pie: {
			showInLegend: true
			}
        
    },
	legend: false,
    tooltip: {
        headerFormat: '<span style="font-size:14px">{series.name}</span><br>',
        pointFormat: '<span style="color:{#FF0000}">{point.name}</span>: <br><b>{point.y}</b>'
    },
   series: <%=JLUW%>
});
$('#PieJLUN').highcharts({
	chart: {
        type: 'pie',
		margin: [0, 0, 0, 0],
		spacingBottom: 0,
		events: {
               drilldown: function(e) {		   
                var xAxis = e.point.series.xAxis,
                  gSite = e.point.name;
                  setTimeout(function () {
                          }, 10);
               },
               drillup: function() {
                var chart = this,
                  xAxis = chart.xAxis[0];
                  setTimeout(function () {
                          }, 10);
               }
           }
    },
    title: {
        text: 'JLUN'
    },
	subtitle: {
			text: '<%=fbType%>s*',
			align: 'left'
	},
    plotOptions: {
        series: {
            dataLabels: {
                enabled: true,
                format: '{point.y}',
				distance: -30,
				color: '#000000',
				style: {fontSize: '16px'}
            },
               point: {
                    events: {
                        click: function (e) {
                            var lWedge = this.name;
							var lWedge = lWedge.substring(0,4);
							var lWedgeshift = lWedge.substring(0,2);
							if (lWedge == 'Mate') {lWedge = 'MM'}
							if (lWedge == 'Ware') {lWedge = 'WD'}
							if (lWedge == 'Loan') {lWedge = 'LP'}
							if (lWedge == 'Clot') {lWedge = 'CL'}
							if (lWedge == 'Nati') {lWedge = 'NV'}
							if (lWedge == 'Glob') {lWedge = 'GD'}
							if (lWedge == 'Oper') {lWedge = 'OC'}
							var gSite = "JLUN";
							var fbpietype = '<%=fbType%>';
                            if (e.ctrlKey) {
							//alert('im in CTRL click with_'+lWedge+'and site is_'+gSite+'and type is_'+fbpietype);
							openPieReport(lWedge, gSite, fbpietype);
							}
							if (e.shiftKey) {
							//alert('im in shift click with_'+lWedgeshift+'and site is_'+gSite+'and type is_'+fbpietype);
							openPieReport(lWedgeshift, gSite, fbpietype);
							}
                        }
                    }
                }
			},
		pie: {
			showInLegend: true
			}
        
    },
	legend: false,
    tooltip: {
        headerFormat: '<span style="font-size:14px">{series.name}</span><br>',
        pointFormat: '<span style="color:{#FF0000}">{point.name}</span>: <br><b>{point.y}</b>'
    },
	credits: false,
    series: <%=JLUN%>
});
$('#PieJLUNQ').highcharts({
	chart: {
        type: 'pie',
		margin: [0, 0, 0, 0],
		spacingBottom: 0,
		events: {
               drilldown: function(e) {		   
                var xAxis = e.point.series.xAxis,
                  gSite = e.point.name;
                  setTimeout(function () {
                          }, 10);
               },
               drillup: function() {
                var chart = this,
                  xAxis = chart.xAxis[0];
                  setTimeout(function () {
                          }, 10);
               }
           }
    },
    title: {
        text: 'JLUNQ'
    },
	subtitle: {
			text: '<%=fbType%>s*',
			align: 'left'
	},
    plotOptions: {
        series: {
            dataLabels: {
                enabled: true,
                format: '{point.y}',
				distance: -30,
				color: '#000000',
				style: {fontSize: '16px'}
            },
               point: {
                    events: {
                        click: function (e) {
                            var lWedge = this.name;
							var lWedge = lWedge.substring(0,4);
							var lWedgeshift = lWedge.substring(0,2);
							if (lWedge == 'Mate') {lWedge = 'MM'}
							if (lWedge == 'Ware') {lWedge = 'WD'}
							if (lWedge == 'Loan') {lWedge = 'LP'}
							if (lWedge == 'Clot') {lWedge = 'CL'}
							if (lWedge == 'Nati') {lWedge = 'NV'}
							if (lWedge == 'Glob') {lWedge = 'GD'}
							if (lWedge == 'Oper') {lWedge = 'OC'}
							var gSite = "JLUNQ";
							var fbpietype = '<%=fbType%>';
                            if (e.ctrlKey) {
							//alert('im in CTRL click with_'+lWedge+'and site is_'+gSite+'and type is_'+fbpietype);
							openPieReport(lWedge, gSite, fbpietype);
							}
							if (e.shiftKey) {
							//alert('im in shift click with_'+lWedgeshift+'and site is_'+gSite+'and type is_'+fbpietype);
							openPieReport(lWedgeshift, gSite, fbpietype);
							}
                        }
                    }
                }
			},
		pie: {
			showInLegend: true
			}
        
    },
	legend: false,
    tooltip: {
        headerFormat: '<span style="font-size:14px">{series.name}</span><br>',
        pointFormat: '<span style="color:{#FF0000}">{point.name}</span>: <br><b>{point.y}</b>'
    },
	credits: false,
    series: <%=JLUNQ%>
});
$('#PieJLUSQ').highcharts({
	chart: {
        type: 'pie',
		margin: [0, 0, 0, 0],
		spacingBottom: 0,
		events: {
               drilldown: function(e) {		   
                var xAxis = e.point.series.xAxis,
                  gSite = e.point.name;
                  setTimeout(function () {
                          }, 10);
               },
               drillup: function() {
                var chart = this,
                  xAxis = chart.xAxis[0];
                  setTimeout(function () {
                          }, 10);
               }
           }
    },
    title: {
        text: 'JLUSQ'
    },
	subtitle: {
			text: '<%=fbType%>s*',
			align: 'left'
	},
    plotOptions: {
        series: {
            dataLabels: {
                enabled: true,
                format: '{point.y}',
				distance: -30,
				color: '#000000',
				style: {fontSize: '16px'}
            },
               point: {
                    events: {
                        click: function (e) {
                            var lWedge = this.name;
							var lWedge = lWedge.substring(0,4);
							var lWedgeshift = lWedge.substring(0,2);
							if (lWedge == 'Mate') {lWedge = 'MM'}
							if (lWedge == 'Ware') {lWedge = 'WD'}
							if (lWedge == 'Loan') {lWedge = 'LP'}
							if (lWedge == 'Clot') {lWedge = 'CL'}
							if (lWedge == 'Nati') {lWedge = 'NV'}
							if (lWedge == 'Glob') {lWedge = 'GD'}
							if (lWedge == 'Oper') {lWedge = 'OC'}
							var gSite = "JLUSQ";
							var fbpietype = '<%=fbType%>';
                            if (e.ctrlKey) {
							//alert('im in CTRL click with_'+lWedge+'and site is_'+gSite+'and type is_'+fbpietype);
							openPieReport(lWedge, gSite, fbpietype);
							}
							if (e.shiftKey) {
							//alert('im in shift click with_'+lWedgeshift+'and site is_'+gSite+'and type is_'+fbpietype);
							openPieReport(lWedgeshift, gSite, fbpietype);
							}
                        }
                    }
                }
			},
		pie: {
			showInLegend: true
			}
        
    },
	legend: false,
    tooltip: {
        headerFormat: '<span style="font-size:14px">{series.name}</span><br>',
        pointFormat: '<span style="color:{#FF0000}">{point.name}</span>: <br><b>{point.y}</b>'
    },
	credits: false,
    series: <%=JLUSQ%>
});
});
</script>
<body>
<div class="container-fluid">
<div class="row">

<!--<p><%response.write(pie&piesub&"<br>"&SCBHQ&"<br>"&JLUN&"<br>"&JLUS&"<br>"&JLUE&"<br>"&JLUW&"<br>"&JLUNQ&"<br>"&JLUSQ&"<br>"&JLUV)%></p>-->
<!--<p><%response.write("the sql is_"&SQLNatQuery1&"<br>the 1st level is_"&SitePie&"<br> the second level is_"&ReasonPie)%></p>-->
<div class="col-md-4" style="height: 175px;">

<b>Filter Charts by Type:</b>
    <select id="filter2" class="form-control" name="vType" onchange="pieApply(this.value)"> 
        <option value="<%=fbType%>">Currently:<%=fbType%></option>
		<%SQLQuery = "SELECT     TOP (100) PERCENT feedbackType FROM WEB_JLC.dbo.CUSTOMDATA_Feedback_prod GROUP BY feedbackType"
  Set SQLData = SQL_WebTools_PRODDB.Execute(SQLQuery)
  Do while not SQLData.EOF %>

  <option value="<%= SQLData("feedbackType") %>" <%If filterType = SQLData("feedbackType") Then %>Selected<%End If %>><%= SQLData("feedbackType") %></option>
  
  <%SQLData.MoveNext
  Loop
%>
</select><br>
CTRL Click to see All Comments or All Compliments.<br>After drilling into a Specific Complaint CTRL Click for detail OR, Shift Click to see all Complaints.<br>
<a href="<%= Application("CONTENTLOAD") %>?PageURL=<%= Application("USERDEVELOPED") %>JLC/DSCNO/Customer_FB_Main.asp" class="btn btn-info pull-right">Back to main</a>
<a href="<%= Application("CONTENTLOAD") %>?PageURL=<%= Application("USERDEVELOPED") %>JLC/DSCNO/Customer_FB_Pies.asp" class="btn btn-warning pull-left">Go/Back to Open/Closed</a>

<div class="col-md-12" style="height: 550px;" id="Pie"></div><br>*Note-Compliments and Comments do not have subcategories</div>
<div class="col-md-2" style="height: 350px;" id="PieSCBHQ"></div>
<div class="col-md-2"  style="height: 350px;" id="PieJLUV"></div>
<div class="col-md-2"  style="height: 350px;" id="PieJLUE"></div>
<div class="col-md-2"  style="height: 350px;" id="PieJLUSQ"></div>

<div class="col-md-4"  style="height: 350px;"></div>

<div class="col-md-2"  style="height: 350px;" id="PieJLUNQ"></div>
<div class="col-md-2"  style="height: 350px;" id="PieJLUN"></div>
<div class="col-md-2"  style="height: 350px;" id="PieJLUS"></div>
<div class="col-md-2"  style="height: 350px;" id="PieJLUW"></div>

</div>
</div>

</body>
</html>
<!-- #include virtual = "/includes/WebTools_AccessLog.asp" -->
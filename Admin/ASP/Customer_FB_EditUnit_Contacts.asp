<!-- #include virtual = "/includes/WebTools_Core.asp" -->
<html>
<head>
  <meta name="Shane Kline" content="CFTv2">
<!-- Bootstrap core CSS -->
    <link href="<%= Application("TP-BOOTSTRAP-CSS") %>" rel="stylesheet">
<!-- Jquery -->	
	<link href="<%= Application("TP-JQUERYUICSS") %>" rel="stylesheet">
<!-- Jquery -->
	<script src='<%= Application("TP-JQUERY") %>'></script>
	<script src='<%= Application("TP-JQUERYUI") %>'></script>
<!-- Bootstrap -->  	
	<script src='<%= Application("TP-BOOTSTRAP") %>'></script>
	<script src='<%= Application("TP-BOOTSTRAP-TOGGLE") %>'></script>
	<script src='<%= Application("THIRDPARTY_CORE") %>bootstrap-multiselect/js/bootstrap-multiselect.js'></script>
	<link href="<%= Application("THIRDPARTY_CORE") %>bootstrap-multiselect/css/bootstrap-multiselect.css" rel="stylesheet">
<!-- Font Awesome -->
	    <link href="<%= Application("TP-FONTAWESOME") %>" rel="stylesheet">
<!-- Modernizr -->
    <script src='<%= Application("TP-MODERNIZR") %>'></script>
</head>


<style>
tr, td {
	padding-bottom: 0px;
	padding-top: 0px;
	font-size: 16px;
}

#shortdesc {
	font-size:16px;
	padding-bottom: 0px;
	padding-top: 0px;
	width: 250px;
	height: 25px;
	}

#longdesc {
	font-size:16px;
	padding-bottom: 0px;
	padding-top: 0px;
	width: 500px;
	height: 25px;
	}

#stat {
	font-size:16px;
	padding-bottom: 0px;
	padding-top: 0px;
	width: 110px;
	height: 25px;

	}
	
#parent {
	width: 225px;
} 
</style>
<%

intro = "<font style='color:red;'>The email address you input here will receive an email for every Feedback submitted to the specified Unit.  Email addresses can not be left 'blank'.</font>"


tableformat = "<th>Target Name</th><th>Currently...</th><th>Change to...</th>"

SQLEditQuery = "SELECT     targetID, targetName, targetEmail FROM WEB_JLC.dbo.Customer_FB_Target_prod"

varTableRows = ""

Set SQLEditData = SQL_WebTools_PRODDB.Execute(SQLEditQuery)
 Do while not SQLEditData.EOF 
 targetID		     = SQLEditData("targetID")
 targetName		     = SQLEditData("targetName")
 targetEmail		 = SQLEditData("targetEmail")

	varTableRows = varTableRows&"<form name='input' id='emailedit' method='get' action='Customer_FB_UnitContacts_Save.asp'>"
	varTableRows = varTableRows&"<tr><td>"&targetName&"</td><td>"&targetEmail&"</td>"
	varTableRows = varTableRows&"<td><input type='email' class='form-control' name='unitemail' id='unitemail' value='"&targetEmail&"' title='full email address'></td>"
	varTableRows = varTableRows&"<input hidden name='pmk' id='pmk' value='"&targetID&"'></tr>"
	varsubbutton = "</form><button type='button' class='btn btn-primary btn-sm pull-right' id='formSubmitemail'>Upload</button>"
	SQLEditData.MoveNext
Loop

%>
<script>
$(document).ready(function(){
	
    $('#formSubmitemail').click(function(){

      var valid = true;
	  $('.form-control').each(function () {
            if (this.value == "" || /^(([^<>))[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(this.value) != true) {
			valid = false;
			alert("Hmm... that didn't work, one or more email addresses are invalid. Please try again!");
			$(this).addClass("btn-warning");
			} else {
			$(this).removeClass("btn-warning");
        } 
		})
        if (valid) {
			$('#emailedit').submit();
        }
			return valid;
	   });
         
});  

$(document).keypress(
	function(event){
		if (event.which == '13') {
			event.preventDefault();
			}
});
</script>
<body>
<div class="container"> 
	<div class="row">
		<div class="col-md-5"><% response.write(intro)%></div><div class="col-md-1"></div>&nbsp;&nbsp;&nbsp;&nbsp;
		<div class="col-md-1"><a href="javascript:window.parent.location.reload(true);" class="btn btn-primary btn-sm">Back</a></div>		
	</div>
		
	<div class="row">
		<div class="col-md-12">
		<table class="table table-condensed">
			<tr>
<% response.write(tableformat)%>
			</tr>
			<% response.write(varTableRows)%>
		</table><% response.write(varsubbutton)%>
		</div>
	</div>
</div>
</body>
</html>
<!-- #include virtual = "/includes/WebTools_AccessLog.asp" -->
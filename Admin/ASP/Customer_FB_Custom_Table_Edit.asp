<!-- #include virtual = "/includes/WebTools_Core.asp" -->
<html>
<head>
  <meta name="Shane Kline" content="CFTv2">
<!-- Bootstrap -->
    <link href="<%= Application("TP-BOOTSTRAP-CSS") %>" rel="stylesheet">
<!-- Jquery -->	
	<link href="<%= Application("TP-JQUERYUICSS") %>" rel="stylesheet">
<!-- Jquery -->
	<script src='<%= Application("TP-JQUERY") %>'></script>
	<script src='<%= Application("TP-JQUERYUI") %>'></script>
<!-- Bootstrap -->  	
	<script src='<%= Application("TP-BOOTSTRAP") %>'></script>
	<script src='<%= Application("TP-BOOTSTRAP-TOGGLE") %>'></script>
	<script src='<%= Application("THIRDPARTY_CORE") %>bootstrap-multiselect/js/bootstrap-multiselect.js'></script>
	<link href="<%= Application("THIRDPARTY_CORE") %>bootstrap-multiselect/css/bootstrap-multiselect.css" rel="stylesheet">
<!-- Font Awesome -->
	    <link href="<%= Application("TP-FONTAWESOME") %>" rel="stylesheet">
<!-- Modernizr -->
    <script src='<%= Application("TP-MODERNIZR") %>'></script>
</head>

<style>
tr, td {
	padding-bottom: 0px;
	padding-top: 0px;
	font-size: 16px;
}

#shortdesc {
	font-size:16px;
	padding-bottom: 0px;
	padding-top: 0px;
	width: 250px;
	height: 25px;
	}

#longdesc {
	font-size:16px;
	padding-bottom: 0px;
	padding-top: 0px;
	width: 500px;
	height: 25px;
	}

#stat {
	font-size:16px;
	padding-bottom: 0px;
	padding-top: 0px;
	width: 110px;
	height: 25px;

	}
	
#parent {
	width: 225px;
} 
</style>
<%
tablefield = request.querystring("vTable")

'response.write(tablefield)

If left(tablefield,1) = "e" Then
intro = "<font style='color:red;'>The email contact you input here will receive a Cc email for every Feedback with this Nature Type.  To remove an email click in the email and then click on the 'cross' that appears to the right, then Scroll down for the upload button</font>"
Else
intro = "<font style='color:red;'>When adding a new Nature specific don't forget to set its Status to 'Active'<br>When a Nature Specific is no longer required simply set the Status to 'Suspended'</font>"
End If
If left(tablefield,1) = "e" Then

tableformat = "<th>Nature ID</th><th>Nature Parent</th><th>Short Description</th><th>Status</th><th>SCBHQ Cc: Email</th>"

SQLEditQuery = "SELECT TOP (100) PERCENT NatureID, NatureParent, NatureShortDesc, NatureLongDesc, NatureStatus, NatureParentID, SCBHQ_Email "&_
"FROM web_jlc.dbo.Customer_FB_Nature_prod WHERE (NatureStatus = 1) AND (NatureParentID = "&right(tablefield,1)&") ORDER BY NatureParentID, NatureID"

varTableRows = ""

Set SQLEditData = SQL_WebTools_PRODDB.Execute(SQLEditQuery)
 Do while not SQLEditData.EOF 
 NatureID		     = SQLEditData("NatureID")
 NatureParent		 = SQLEditData("NatureParent")
 NatureShortDesc   	 = SQLEditData("NatureShortDesc")
 NatureStatus      	 = SQLEditData("NatureStatus")
 SCBHQ_Email         = SQLEditData("SCBHQ_Email")
 If NatureStatus = "True" Then
 NatureStatus = "Active"
 Else
 NatureStatus = "Suspended"
 End If

	varTableRows = varTableRows&"<form name='input' id='emailedit' method='get' action='Customer_FB_Custom_Table_Save.asp'>"
	varTableRows = varTableRows&"<tr><td>"&NatureID&"</td><td id='parent'>"&NatureParent&"</td><td id='shortdesc'>"&NatureShortDesc&"</td><td id='stat'>"&NatureStatus&"</td>"
	varTableRows = varTableRows&"<td><input type='email' class='form-control' name='scbhqemail' id='scbhqemail' value='"&SCBHQ_Email&"' title='full email address'></td>"
	varTableRows = varTableRows&"<input hidden name='pmk' id='pmk' value='"&NatureID&"'></tr>"
	varsubbutton = "</form><button type='button' class='btn btn-primary btn-sm pull-right' id='formSubmitemail'>Upload</button>"
	
SQLEditData.MoveNext

Loop


Else
tableformat = "<th>Nature ID</th><th>Nature Parent</th><th>Short Description</th><th>Long Description</th><th>Status</th>"

SQLEditQuery = "SELECT     TOP (100) PERCENT NatureID, NatureParent, NatureShortDesc, NatureLongDesc, NatureStatus, NatureParentID "&_
"FROM WEB_JLC.dbo.Customer_FB_Nature_prod WHERE (NatureParentID = "&right(tablefield,1)&") AND (CASE WHEN len(NatureID) = 2 THEN 'Exclude' ELSE 'Include' END = 'Include') "&_
"ORDER BY NatureParentID, NatureID"

varTableRows = ""

Set SQLEditData = SQL_WebTools_PRODDB.Execute(SQLEditQuery)
 Do while not SQLEditData.EOF 
 NatureID		     = SQLEditData("NatureID")
 NatureParent		 = SQLEditData("NatureParent")
 NatureShortDesc   	 = SQLEditData("NatureShortDesc")
 NatureLongDesc      = SQLEditData("NatureLongDesc")
 NatureStatus      	 = SQLEditData("NatureStatus")
 If NatureStatus = "True" Then
 NatureStatus = "Active"
 Else
 NatureStatus = "Suspended"
 End If

	varTableRows = varTableRows&"<form name='input' id='natureedit' method='get' action='Customer_FB_Custom_Table_Save.asp'>"
	
	varTableRows = varTableRows&"<tr><td>"&NatureID&"</td><td id='parent'>"&NatureParent&"</td>"
	varTableRows = varTableRows&"<td><input type='text' class='form-control' name='shortdesc' id='shortdesc' value='"&NatureShortDesc&"' title='No punctuation'></td>"
	varTableRows = varTableRows&"<td><input type='text' class='form-control' name='longdesc' id='longdesc' value='"&NatureLongDesc&"' title='No punctuation'></td>"
	varTableRows = varTableRows&"<td><input type='text' class='form-control' name='stat' id='stat' value='"&NatureStatus&"'></td>"
	varTableRows = varTableRows&"<input hidden name='pmk' id='pmk' value='"&NatureID&"'></tr>"
	varsubbutton = "</form><button type='button' class='btn btn-primary btn-sm pull-right' id='formSubmitnatureid'>Upload</button>"
SQLEditData.MoveNext
Loop


End If
%>
<script>
$(document).ready(function(){
	
    $('#formSubmitemail').click(function(){

      var valid = true;
	  $('.form-control').each(function () {
            if (this.value != "" && /^(([^<>))[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(this.value) != true) {
			valid = false;
			alert("Hmm... that didn't work, one or more email addresses are invalid. Please try again!");
			$(this).addClass("btn-warning");
			} else {
			$(this).removeClass("btn-warning");
        } 
		})
        if (valid) {
			$('#emailedit').submit();
        }
			return valid;
	   });
       
	$('#formSubmitnatureid').click(function(){

      var valid = true;
	  $('.form-control').each(function () {
            if (this.value == "" || /[a-zA-Z]/g.test(this.value) != true) {
			valid = false;
			alert("Hmm... that didn't work, one or more fields contain an invalid character or have been left blank. Please try again!");
			$(this).addClass("btn-warning");
			} else {
			$(this).removeClass("btn-warning");
        } 
		})
		$('#stat').each(function () {
            if (this.value == "" || this.value != "Active" && this.value != "Suspended") {
			valid = false;
			alert("Hmm... that didn't work, your speeling is off or you have a blank field. Please try again!");
			$(this).addClass("btn-warning");
			} else {
			$(this).removeClass("btn-warning");
        } 
		})
        if (valid) {
			$('#natureedit').submit();
        }
			return valid;
	   });  
});  

$(document).keypress(
	function(event){
		if (event.which == '13') {
			event.preventDefault();
			}
});
</script>
<body>
<div class="container"> 
	<div class="row">
		<div class="col-md-5"><% response.write(intro)%></div><div class="col-md-1"></div>&nbsp;&nbsp;&nbsp;&nbsp;
		<div class="col-md-1"><a href="javascript:window.parent.location.reload(true);" class="btn btn-primary btn-sm">Back to main</a></div>		
	</div>
		
	<div class="row">
		<div class="col-md-12">
		<table class="table table-condensed">
			<tr>
<% response.write(tableformat)%>
			</tr>
			<% response.write(varTableRows)%>
		</table><% response.write(varsubbutton)%>
		</div>
	</div>
</div>
</body>
</html>
<!-- #include virtual = "/includes/WebTools_AccessLog.asp" -->
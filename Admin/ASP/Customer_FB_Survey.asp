<!-- #include virtual = "/includes/WebTools_Core.asp" -->
<html>
<head>
  <meta name="Shane Kline" content="CFTv2">
<!-- Bootstrap -->  
    <link href="<%= Application("TP-BOOTSTRAP-CSS") %>" rel="stylesheet">
    <link href="<%= Application("TP-BOOTSTRAP-TOGGLE-CSS") %>" rel="stylesheet">
<!-- Jquery -->
	<script src='<%= Application("TP-JQUERY") %>'></script>
	<script src='<%= Application("TP-JQUERYUI") %>'></script>
<!-- Bootstrap -->  	
	<script src='<%= Application("TP-BOOTSTRAP") %>'></script>
	<script src='<%= Application("TP-BOOTSTRAP-TOGGLE") %>'></script>
<!-- Font Awesome -->
	    <link href="<%= Application("TP-FONTAWESOME") %>" rel="stylesheet">
<!-- Modernizr -->
    <script src='<%= Application("TP-MODERNIZR") %>'></script>

<style>
#surveyupdate {
	background-color: #e6e2ac;
	font-size:16px;
	border: 2px solid #ffd940;
	border-radius: 2px;	
}

#fbstats {
	font-family: verdana;
    font-size: 16px;
	font-weight: bold;
	border: 4px solid #397ee6;
	border-radius: 5px;
}
</style>
</head>
<%
fbID = request.querystring("vfbID")
'response.write("the passed id is_"&fbID)
'fbID = "950"
If fbID <> "" Then
SQLQuery = "SELECT FeedbackID, feedback, comment_DateTime FROM WEB_JLC.dbo.vProd_Customer_FB_full_stuff WHERE (FeedbackID = "&fbID&")"
Set SQLData = SQL_WebTools_PRODDB.Execute(SQLQuery)
 Do while not SQLData.EOF 
 fbID		 = SQLData("feedbackID")
 fb  		 = SQLData("feedback")
 cmts   	 = SQLData("comment_DateTime")
SQLData.MoveNext
Loop
If cmts <> "" Then
cmts = replace(cmts,"|","<br>")
cmts = Replace(cmts,"&amp;","&")
cmts = Replace(cmts,"&#039;","'")
cmts = Replace(cmts,"&lt;","<")
cmts = Replace(cmts,"&gt;",">")
cmts = Replace(cmts,"(This is the closing response)","<font style='color:grey;'> (This is the closing response)</font>")
End If

SQLQuery1 = "SELECT feedbackID, targetName, feedbackType, CAST(feedbackText AS nvarchar(4000)) AS feedbackText, creationUserID, feedbackDateTime, NatureParent, NatureShortDesc, targetEmail, SCBHQ_Email, "&_
"deleted, deluser, deldate, changeuser, changedate, dateClosed, closingUserID, cdOriginator, cdUnit, cdPhone, cdEmail, fdRodum, "&_
"fdEquipNo, fdEquipAN, fdMaintReqNo, fdWorkOrderNo, fdReqNo, DATEDIFF(d, feedbackDateTime, dateClosed) AS Closure_Days "&_
"FROM WEB_JLC.dbo.vProd_Customer_FB_full_nocmts WHERE (feedbackID = "&fbID&")"
Set SQLData1 = SQL_WebTools_PRODDB.Execute(SQLQuery1)
 Do while not SQLData1.EOF 
 fb_ID		         = SQLData1("feedbackID")
 fbcreateDteTm		 = SQLData1("feedbackDateTime")
 fbdelDte  		     = SQLData1("deldate")
 fbchangeDteTm   	 = SQLData1("changedate")
 fbcloseDteTm   	 = SQLData1("dateClosed")
 closureTime  	     = SQLData1("Closure_Days")
 cdPhone		     = SQLData1("cdPhone")
 cdEmail		     = SQLData1("cdEmail")
SQLData1.MoveNext
Loop

SQLQuery2 = "SELECT     TOP (100) PERCENT WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackID, WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackDateTime, "&_
"MIN(WEB_JLC.dbo.Customer_FB_Comments_prod.commentDateTime) AS Init_Response, MIN(DATEDIFF(d, WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackDateTime, "&_
"WEB_JLC.dbo.Customer_FB_Comments_prod.commentDateTime)) AS IR_Days FROM WEB_JLC.dbo.Customer_FB_Target_prod AS Customer_FB_Target_prod_1 RIGHT OUTER JOIN "&_
"WEB_JLC.dbo.Customer_FB_Comments_prod ON Customer_FB_Target_prod_1.targetID = WEB_JLC.dbo.Customer_FB_Comments_prod.commentOwner RIGHT OUTER JOIN "&_
"WEB_JLC.dbo.CUSTOMDATA_Feedback_prod ON WEB_JLC.dbo.Customer_FB_Comments_prod.feedback_ID = WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackID "&_
"WHERE (Customer_FB_Target_prod_1.targetName IS NOT NULL) AND (WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.dateClosed IS NOT NULL) "&_
"GROUP BY WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackID, WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackDateTime "&_
"HAVING (WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackID = "&fbID&")"
Set SQLData2 = SQL_WebTools_PRODDB.Execute(SQLQuery2)
 Do while not SQLData2.EOF 
 fbIRTime  		     = SQLData2("IR_Days")
SQLData2.MoveNext
Loop
If fbIRTime = "" Then
fbIRTime = "0"
End If

End If

%>
<script>

$(document).ready(function(){
		var validation = 0;		
		var validation1 = 0;
		var validation2 = 0;
		var validation3 = 0;
		var validation4 = 0;
		
		var fb_ID = '<%=fb_ID%>'
		
		if(fb_ID=="")            {
            validation = validation+1
            $("#fbID").addClass("btn-warning");
        }
		
		
		$("#q1y").click(function() {
		validation1 = validation1+1;
		});
		$("#q1n").click(function() {
		validation1 = validation1+1;
		});
		
		$("#q2y").click(function() {
		validation2 = validation2+1
		});
		$("#q2n").click(function() {
		validation2 = validation2+1
		});		
		
		$("#q3y").click(function() {
		validation3 = validation3+1
		});
		$("#q3n").click(function() {
		validation3 = validation3+1
		});

		$("#q4n").click(function() {
		validation4 = validation4+1
		});		
		$("#q4y1").click(function() {
		validation4 = validation4+1
		});
		$("#q4y2").click(function() {
		validation4 = validation4+1
		});	
		
	$("#updatesurvey").click(function(){
	
		if (validation == 1 || validation1 == 0 || validation2 == 0 || validation3 == 0 || validation4 == 0) {
		alert('You have missed a question!!');
		} else {
		$( "#survey_update" ).submit();
		};
});

});

$(function() {
    $("#fbID").change(function() {
        $("#fbID").removeClass("btn-warning");
    });
});

$(document).keypress(
	function(event){
		if (event.which == '13') {
			event.preventDefault();
			}
});

function reloadpage(fbID) {
	  parent.SetupOverlay(parent.document.body.clientWidth-700,parent.document.body.clientHeight-100);
      parent.window.frames["ifrmFrontOverlay"].location ="<%= Application("CONTENTLOAD") %>?PageURL=<%= Application("USERDEVELOPED") %>JLC/DSCNO/Customer_FB_Survey.asp|vfbID=" + fbID
      parent.openOverlay();
	  //document.getElementById("fbstats").setAttribute('visible');
};





</script>
<body>
<div class="container-fluid">
<div class="row"><br></div>
	<div class="row">
		<div class="col-sm-12">
						<div class="form-group">
					<label for="fbID">1. Feedback ID</label>
					<select class="form-control" id="fbID" name="fbID" onchange="reloadpage(this.value)">
					<option value="">Select your Feedback: &nbsp;&nbsp;&nbsp;&nbsp;(Originator-Date-Target Unit)</option>
                    <% SQLQuery = "SELECT TOP (100) PERCENT WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackID, CONVERT(nvarchar(50), WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.cdOriginator) + N'-' + REPLACE(CONVERT(nvarchar(9), "&_
"WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackDateTime, 6), ' ', '/') + N'-' + WEB_JLC.dbo.Customer_FB_Target_prod.targetName AS FBIDadd FROM WEB_JLC.dbo.CUSTOMDATA_Feedback_prod LEFT OUTER JOIN "&_
"WEB_JLC.dbo.Customer_FB_Target_prod ON WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.targetID = WEB_JLC.dbo.Customer_FB_Target_prod.targetID LEFT OUTER JOIN "&_
"WEB_JLC.dbo.Customer_FB_Survey_prod ON WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackID = WEB_JLC.dbo.Customer_FB_Survey_prod.fbID "&_
"WHERE     (WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.dateClosed > DATEADD(dd, - 7, GETDATE())) AND (WEB_JLC.dbo.Customer_FB_Survey_prod.surveyID IS NULL) ORDER BY WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackID DESC"
                    Set SQLDataRefresh = SQL_WebTools_PRODDB.Execute(SQLQuery)
					If SQLDataRefresh.BOF Then
					response.write "<option value=''>The Survey window (7 Days) has elapsed</option>"
					Else
                    Do While not SQLDataRefresh.EOF
                        feedbackID      = SQLDataRefresh("feedbackID")
						FBIDadd         = SQLDataRefresh("FBIDadd")
                        response.write "<option value='"&feedbackID&"'>"&feedbackID&"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;("&FBIDadd&")</option>"
                        SQLDataRefresh.MoveNext 
                    Loop
					End If
                    %>					
					</select>
				</div> 
		<a href="javascript:window.parent.location.reload(true);" class="btn btn-primary btn-sm pull-right">Close</a></div><br>		
	</div>
	<div class="row" id="fbstats">Feedback ID: <% response.write fb_ID %><br><% response.write fb %><br><% response.write cmts %><br><br>
	The target SCB Unit initially responded in: <% response.write fbIRTime %> Days<br>
	The target SCB Unit followed up and closed the Feedback in: <% response.write closureTime %> Days<br>
	
	
	
	</div>
	<br>
<form name="input" id="survey_update" method="get" action="Customer_FB_Survey_Submitted.asp">		
	<div class="row" id="surveyupdate">
			<div class="col-sm-12">
				<div class="form-group">
					<label for="q1">Did the target SCB Unit initially respond with a reasonable response?</label><br>
					<input type="radio" id="q1y" name="q1" value="1"> Yes</br>
					<input type="radio" id="q1n" name="q1" value="0"> No
				</div> 
			</div>
		</div>
	<div class="row" id="surveyupdate">		
        <div class="col-sm-12">
			<div class="form-group">
				<label for="q2">Did the target SCB Unit close the feedback with a reasonable response?</label><br>
					<input type="radio" id="q2y" name="q2" value="1"> Yes</br>
					<input type="radio" id="q2n" name="q2" value="0"> No
			</div>
		</div>
				</div>
	<div class="row" id="surveyupdate">	
		<div class="col-sm-12">
			<div class="form-group">
				<label for="q3">Overall are you satisfied with the Customer Feedback Process?</label><br>
					<input type="radio" id="q3y" name="q3" value="1"> Yes</br>
					<input type="radio" id="q3n" name="q3" value="0"> No
			</div>
		</div>
				</div>
	<div class="row" id="surveyupdate">	
		<div class="col-sm-12">
			<div class="form-group">
				<label for="q4">If you answered 'No' to any question, would you like to be contacted?</label><br>
					<input type="radio" id="q4n" name="q4" value="0"> No</br>
					<input type="radio" id="q4y1" name="q4" value="1"> Yes (email-<% response.write cdEmail %>)</br>
					<input type="radio" id="q4y2" name="q4" value="2"> Yes (phone-<% response.write cdPhone %>)
			</div>
		</div>
	</div>
	<div class="row" id="surveyupdate" hidden>	
			<div class="col-sm-12">
			<div class="form-group">
				<label for="q5">Q5</label><br>
					<input type="radio" id="q5" name="q5" value="NULL" checked>option1</br>
					<input type="radio" id="q5" name="q5" value="NULL">option2</br>
					<input type="radio" id="q5" name="q5" value="NULL">option3
			</div>
		</div>
	</div>
		<div class="row" id="surveyupdate" hidden>	
			<div class="col-sm-12">
			<div class="form-group">
				<label for="q6">Q6</label><br>
					<input type="radio" id="q6" name="q6" value="NULL" checked>option1</br>
					<input type="radio" id="q6" name="q6" value="NULL">option2</br>
					<input type="radio" id="q6" name="q6" value="NULL">option3
			</div>
		</div>
	</div>
		<div class="row" id="surveyupdate" hidden>	
			<div class="col-sm-12">
			<div class="form-group">
				<label for="keydata">keydata</label><br>
					<input type="radio" id="keydata1" name="kd1" value="<% response.write fbID %>" checked>
					<input type="radio" id="keydata2" name="kd2" value="<% response.write fbIRTime %>" checked>
					<input type="radio" id="keydata3" name="kd3" value="<% response.write closureTime %>" checked>
			</div>
		</div>
	</div>
</form>
	<div class="row">
	<div class="col-sm-12"><button type="button" class="btn btn-success pull-right" id="updatesurvey">Submit</button></div>
</div>
	
</div>
	
</body>
<script type="text/javascript" language="javascript">
$(document).ready(function() {
     $('[data-toggle="tooltip"]').tooltip();
});
</script> 
<!-- #include virtual = "/includes/WebTools_AccessLog.asp" -->
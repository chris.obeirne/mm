<!-- #include virtual = "/includes/WebTools_Core.asp" -->
<html>
<head>
  <meta name="Chris O'Beirne" content="CFTv2">
<!-- Bootstrap -->  
    <link href="<%= Application("TP-BOOTSTRAP-CSS") %>" rel="stylesheet">
    <link href="<%= Application("TP-BOOTSTRAP-TOGGLE-CSS") %>" rel="stylesheet">
<!-- Jquery -->
	<script src='<%= Application("TP-JQUERY") %>'></script>
	<script src='<%= Application("TP-JQUERYUI") %>'></script>
<!-- Bootstrap -->  	
	<script src='<%= Application("TP-BOOTSTRAP") %>'></script>
	<script src='<%= Application("TP-BOOTSTRAP-TOGGLE") %>'></script>
<!-- Font Awesome -->
	    <link href="<%= Application("TP-FONTAWESOME") %>" rel="stylesheet">
<!-- Modernizr -->
    <script src='<%= Application("TP-MODERNIZR") %>'></script>
</head>
<%

fbid = request.querystring("fbID")
'fbid= 2341
fbtext = "Reopened Feedback"
'strDRNLogon
'This functionality was added in to allow people to reopen FB items

If fbid <> "" Then
		SQLstatement = 					"UPDATE Web_JLC.dbo.CUSTOMDATA_Feedback_prod "
		SQLstatement = SQLstatement & 	" SET  dateClosed = NULL, closingUserID = NULL "
		SQLstatement = SQLstatement & 	" WHERE (feedbackID = "&fbid&")"
		SQL_WebTools_PRODDB.Execute(SQLstatement)

		SQLQuery = "SELECT  MAX(commentID) as LastCommentID FROM Web_JLC.dbo.Customer_FB_Comments_prod"
Set SQLData = SQL_WebTools_PRODDB.Execute(SQLQuery)
Do While not SQLData.EOF
  LastCommentID = SQLData("LastCommentID")
  SQLData.MoveNext  
Loop
'LastCommentID = 0
LastCommentID = LastCommentID +1 

'this insert into creates a record in the comments table
SQLstatement1 = 					"INSERT INTO Web_JLC.dbo.Customer_FB_Comments_prod (commentID, feedback_ID, commentText, UserID, commentDateTime) "
SQLstatement1 = SQLstatement1 & 	"VALUES ("&LastCommentID&","&fbid&",'"&fbtext&"','"&strDRNLogon&"',GetDate())"
'response.write(SQLstatement1)
SQL_WebTools_PRODDB.Execute(SQLstatement1)

End If
%>
<br><br><br>
<div class="container">
<h1 align="center"><small><br><br><br>
Feedback reopened <br>
</small></h1>
</div>
<script>
setTimeout(function() {
window.parent.location.reload();
}, 200);
</script>
<!-- #include virtual = "/includes/WebTools_AccessLog.asp" -->
<!-- #include virtual = "/includes/WebTools_Core.asp" -->
<html>
<head>
  <meta name="Shane Kline" content="cftV2">
<!-- Bootstrap -->  
    <link href="<%= Application("TP-BOOTSTRAP-CSS") %>" rel="stylesheet">
    <link href="<%= Application("TP-BOOTSTRAP-TOGGLE-CSS") %>" rel="stylesheet">
<!-- Jquery -->
	<script src='<%= Application("TP-JQUERY") %>'></script>
	<script src='<%= Application("TP-JQUERYUI") %>'></script>
<!-- Bootstrap -->  	
	<script src='<%= Application("TP-BOOTSTRAP") %>'></script>
	<script src='<%= Application("TP-BOOTSTRAP-TOGGLE") %>'></script>
<!-- Font Awesome -->
	    <link href="<%= Application("TP-FONTAWESOME") %>" rel="stylesheet">
<!-- Modernizr -->
    <script src='<%= Application("TP-MODERNIZR") %>'></script>
<!-- HighCharts -->
	<script src='<%=Application("TP-HIGHCHARTS-SOLID-GAUGE") %>'></script>
	<script src='<%=Application("TP-HIGHCHARTS") %>'></script>
	<script src='<%=Application("TP-HIGHCHARTS-MORE") %>'></script>
	<script src='<%=Application("TP-HIGHCHARTS-EXPORTING") %>'></script>
    <script src='<%= Application("TP-HIGHCHARTS-EXPORT-CLIENT-JS") %>'></script>
</head>

<style>
.highcharts-title {
    fill: #434348;
    font-weight: bold;
    font-size: 20px;
}

.checkbox-inline {
border: 1px solid #6495ED;
border-radius: 20px;
padding: .2cm;
}

.col-md-2 {
border: 1px solid #6495ED;
border-radius: 20px;
}

#Pie {
    z-index: 1
}

#modal_pieSQL {
   position: absolute;
   top: 50px;
   right: 0;
   bottom: 0;
   left: 0;
}
</style>
</head>    
<%
pageview = request.querystring("page")

If pageview = "column" Then
chtype = "'column'"
chtypeNat = "'column'"
buttoncolvis = "class='btn btn-info hidden'"
buttonlinvis = "class='btn btn-warning visible'" 
SQLNatQuery = "SELECT     COUNT(CASE WHEN q1response = 1 THEN 1 ELSE NULL END) AS q1Yes, COUNT(CASE WHEN q1response = 0 THEN 1 ELSE NULL END) AS q1No, "&_
"COUNT(CASE WHEN q2response = 1 THEN 1 ELSE NULL END) AS q2Yes, COUNT(CASE WHEN q2response = 0 THEN 1 ELSE NULL END) AS q2No, "&_
"COUNT(CASE WHEN q3response = 1 THEN 1 ELSE NULL END) AS q3Yes, COUNT(CASE WHEN q3response = 0 THEN 1 ELSE NULL END) AS q3No, "&_
"COUNT(CASE WHEN q4response <> 0 THEN 1 ELSE NULL END) AS q4Yes, COUNT(CASE WHEN q4response = 0 THEN 1 ELSE NULL END) AS q4No, "&_
"AVG(unitReply_days) AS Av_Reply_Days, AVG(unitClose_days) AS Av_Close_Days FROM WEB_JLC.dbo.Customer_FB_Survey_prod"
Set SQLNatData = SQL_WebTools_PRODDB.Execute(SQLNatQuery)
Do while not SQLNatData.EOF	
Av_Reply_DaysNat = SQLNatData("Av_Reply_Days")
Av_Close_DaysNat = SQLNatData("Av_Close_Days")
q1YesNat         = SQLNatData("q1Yes")
q1NoNat          = SQLNatData("q1No")
q2YesNat         = SQLNatData("q2Yes")
q2NoNat          = SQLNatData("q2No")
q3YesNat         = SQLNatData("q3Yes")
q3NoNat          = SQLNatData("q3No")
q4YesNat         = SQLNatData("q4Yes")
q4NoNat          = SQLNatData("q4No")

SQLNatData.MoveNext
Loop

surveyNat = "[{name: 'Not Satisfied', data: ["&q1NoNat&", "&q2NoNat&", "&q3NoNat&"], color: '#807160'}, "&_
"{name: 'Satisfied', data: ["&q1YesNat&", "&q2YesNat&", "&q3YesNat&"], color: '#5ccc33'}]"


Dim scb(7)
scb(0)="1"
scb(1)="2"
scb(2)="3"
scb(3)="4"
scb(4)="5"
scb(5)="6" 
scb(6)="7" 
scb(7)="8" 

For Each jlu In scb



SQLSiteQuery = "SELECT     TOP (100) PERCENT COUNT(CASE WHEN q1response = 1 THEN 1 ELSE NULL END) AS q1Yes, COUNT(CASE WHEN q1response = 0 THEN 1 ELSE NULL "&_
"END) AS q1No, COUNT(CASE WHEN q2response = 1 THEN 1 ELSE NULL END) AS q2Yes, COUNT(CASE WHEN q2response = 0 THEN 1 ELSE NULL "&_
"END) AS q2No, COUNT(CASE WHEN q3response = 1 THEN 1 ELSE NULL END) AS q3Yes, COUNT(CASE WHEN q3response = 0 THEN 1 ELSE NULL "&_
"END) AS q3No, COUNT(CASE WHEN q4response <> 0 THEN 1 ELSE NULL END) AS q4Yes, COUNT(CASE WHEN q4response = 0 THEN 1 ELSE NULL "&_
"END) AS q4No, AVG(WEB_JLC.dbo.Customer_FB_Survey_prod.unitReply_days) AS Av_Reply_Days, AVG(WEB_JLC.dbo.Customer_FB_Survey_prod.unitClose_days) "&_
"AS Av_Close_Days, WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.targetID FROM         WEB_JLC.dbo.Customer_FB_Survey_prod INNER JOIN "&_
"WEB_JLC.dbo.CUSTOMDATA_Feedback_prod ON WEB_JLC.dbo.Customer_FB_Survey_prod.fbID = WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackID "&_
"GROUP BY WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.targetID HAVING (WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.targetID = "&jlu&") ORDER BY WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.targetID"
Set SQLSiteData = SQL_WebTools_PRODDB.Execute(SQLSiteQuery)
If SQLSiteData.BOF Then
strsitewedge = "[{name: 'Not Satisfied', data:[0,0,0]},{name: 'Satisfied', data:[0,0,0]}]|"
strsiteavDays = "0-0|"
Else
Do while not SQLSiteData.EOF	
Av_Reply_Days = SQLSiteData("Av_Reply_Days")
Av_Close_Days = SQLSiteData("Av_Close_Days")
q1Yes         = SQLSiteData("q1Yes")
q1No          = SQLSiteData("q1No")
q2Yes         = SQLSiteData("q2Yes")
q2No          = SQLSiteData("q2No")
q3Yes         = SQLSiteData("q3Yes")
q3No          = SQLSiteData("q3No")
q4Yes         = SQLSiteData("q4Yes")
q4No          = SQLSiteData("q4No")
targetID      = SQLSiteData("targetID")

strsitewedge = "[{name: 'Not Satisfied', data: ["&q1No&", "&q2No&", "&q3No&"], color: '#807160'}, "&_
"{name: 'Satisfied', data: ["&q1Yes&", "&q2Yes&", "&q3Yes&"], color: '#5ccc33'}]|"
strsiteavDays = Av_Reply_Days&"-"&Av_Close_Days&"|"

SQLSiteData.MoveNext


Loop

End If
strall = strall&strsitewedge
strall1 = strall1&strsiteavDays
Next

strsplit = Split(strall,"|")
SCBHQ = strsplit(0)
JLUN = strsplit(1)
JLUS = strsplit(2)
JLUE = strsplit(3)
JLUW = strsplit(4)
JLUNQ = strsplit(5)
JLUSQ = strsplit(6)
JLUV = strsplit(7)

strsplit1 = Split(strall1,"|")
SCBHQ1 = strsplit1(0)
JLUN1 = strsplit1(1)
JLUS1 = strsplit1(2)
JLUE1 = strsplit1(3)
JLUW1 = strsplit1(4)
JLUNQ1 = strsplit1(5)
JLUSQ1 = strsplit1(6)
JLUV1 = strsplit1(7)

SCBHQ2 = Split(SCBHQ1,"-")
SCBHQIR = SCBHQ2(0)
SCBHQCR = SCBHQ2(1)
JLUN2 = Split(JLUN1,"-")
JLUNIR = JLUN2(0)
JLUNCR = JLUN2(1)
JLUS2 = Split(JLUS1,"-")
JLUSIR = JLUS2(0)
JLUSCR = JLUS2(1)
JLUE2 = Split(JLUE1,"-")
JLUEIR = JLUE2(0)
JLUECR = JLUE2(1)
JLUW2 = Split(JLUW1,"-")
JLUWIR = JLUW2(0)
JLUWCR = JLUW2(1)
JLUNQ2 = Split(JLUNQ1,"-")
JLUNQIR = JLUNQ2(0)
JLUNQCR = JLUNQ2(1)
JLUSQ2 = Split(JLUSQ1,"-")
JLUSQIR = JLUSQ2(0)
JLUSQCR = JLUSQ2(1)
JLUV2 = Split(JLUV1,"-")
JLUVIR = JLUV2(0)
JLUVCR = JLUV2(1)



natcat = "'<span style=""""color:black"""">(IR)</span><br>(Av "&Av_Reply_DaysNat&" Days)', '<span style=""""color:black"""">(CR)</span><br>(Av "&Av_Close_DaysNat&" Days)', '<span style=""""color:black"""">(FO)</span>'"
scbhqcat = "'<span style=""""color:black"""">(IR)</span><br>(Av "&scbhqIR&" Days)', '<span style=""""color:black"""">(CR)</span><br>(Av "&scbhqCR&" Days)', '<span style=""""color:black"""">(FO)</span>'"
ecat = "'<span style=""""color:black"""">(IR)</span><br>(Av "&JLUeIR&" Days)', '<span style=""""color:black"""">(CR)</span><br>(Av "&JLUeCR&" Days)', '<span style=""""color:black"""">(FO)</span>'"
vcat = "'<span style=""""color:black"""">(IR)</span><br>(Av "&JLUvIR&" Days)', '<span style=""""color:black"""">(CR)</span><br>(Av "&JLUvCR&" Days)', '<span style=""""color:black"""">(FO)</span>'"
scat = "'<span style=""""color:black"""">(IR)</span><br>(Av "&JLUsIR&" Days)', '<span style=""""color:black"""">(CR)</span><br>(Av "&JLUsCR&" Days)', '<span style=""""color:black"""">(FO)</span>'"
wcat = "'<span style=""""color:black"""">(IR)</span><br>(Av "&JLUwIR&" Days)', '<span style=""""color:black"""">(CR)</span><br>(Av "&JLUwCR&" Days)', '<span style=""""color:black"""">(FO)</span>'"
ncat = "'<span style=""""color:black"""">(IR)</span><br>(Av "&JLUnIR&" Days)', '<span style=""""color:black"""">(CR)</span><br>(Av "&JLUnCR&" Days)', '<span style=""""color:black"""">(FO)</span>'"
nqcat = "'<span style=""""color:black"""">(IR)</span><br>(Av "&JLUnqIR&" Days)', '<span style=""""color:black"""">(CR)</span><br>(Av "&JLUnqCR&" Days)', '<span style=""""color:black"""">(FO)</span>'"
sqcat = "'<span style=""""color:black"""">(IR)</span><br>(Av "&JLUSQIR&" Days)', '<span style=""""color:black"""">(CR)</span><br>(Av "&JLUSQCR&" Days)', '<span style=""""color:black"""">(FO)</span>'"
'response.write("the nat is_"&surveyNat&"<br>SCBHQ is_"&SCBHQ&"<br>JLUN is_"&JLUN&"<br>JLUS is_"&JLUS&"<br>JLUE is_"&JLUE&"<br>JLUW is_"&JLUW&"<br>JLUV is_"&JLUV&"<br>JLUNQ is_"&JLUNQ&"<br>JLUSQ is_"&JLUSQ)
days = "6"
Else









SQLxAxisCat = " SELECT     TOP (6) cMonth FROM WEB_JLC.dbo.vProd_Customer_FB_Last12Mths GROUP BY cMonth, Yr, Mth ORDER BY Yr DESC, Mth DESC"
Set SQLDataxAxisCat = SQL_WebTools_PRODDB.Execute(SQLxAxisCat)
 Do while not SQLDataxAxisCat.EOF	
 mth = SQLDataxAxisCat("cMonth")
 SQLDataxAxisCat.MoveNext
 Mths = "'"&Mth&"',"&Mths
 Loop
cat = left(Mths,len(Mths)-1)

natcat = cat
scbhqcat = cat
ecat = cat
vcat = cat
scat = cat
wcat = cat
ncat = cat
nqcat = cat
sqcat = cat


chtype = "'line', height: 300"
chtypeNat = "'line', height: 400"
buttoncolvis = "class='btn btn-warning visible'"
buttonlinvis = "class='btn btn-info hidden'" 
max = "max: 100,"

date1y = DatePart("yyyy",dateadd("m", -5, Now()))
If len(DatePart("m",dateadd("m", -5, Now()))) = 1 Then
date1m = "0"&DatePart("m",dateadd("m", -5, Now()))
Else
date1m = DatePart("m",dateadd("m", -5, Now()))
End If

date2y = DatePart("yyyy",dateadd("m", -4, Now()))
If len(DatePart("m",dateadd("m", -4, Now()))) = 1 Then
date2m = "0"&DatePart("m",dateadd("m", -4, Now()))
Else
date2m = DatePart("m",dateadd("m", -4, Now()))
End If

date3y = DatePart("yyyy",dateadd("m", -3, Now()))
If len(DatePart("m",dateadd("m", -3, Now()))) = 1 Then
date3m = "0"&DatePart("m",dateadd("m", -3, Now()))
Else
date3m = DatePart("m",dateadd("m", -3, Now()))
End If

date4y = DatePart("yyyy",dateadd("m", -2, Now()))
If len(DatePart("m",dateadd("m", -2, Now()))) = 1 Then
date4m = "0"&DatePart("m",dateadd("m", -2, Now()))
Else
date4m = DatePart("m",dateadd("m", -2, Now()))
End If

date5y = DatePart("yyyy",dateadd("m", -1, Now()))
If len(DatePart("m",dateadd("m", -1, Now()))) = 1 Then
date5m = "0"&DatePart("m",dateadd("m", -1, Now()))
Else
date5m = DatePart("m",dateadd("m", -1, Now()))
End If

date6y = DatePart("yyyy",Now())
If len(DatePart("m",Now())) = 1 Then
date6m = "0"&DatePart("m",Now())
Else
date6m = DatePart("m",Now())
End If

'------line national
Dim scblinenat(5)
scblinenat(0) = date1y&"-"&date1m
scblinenat(1) = date2y&"-"&date2m
scblinenat(2) = date3y&"-"&date3m
scblinenat(3) = date4y&"-"&date4m
scblinenat(4) = date5y&"-"&date5m
scblinenat(5) = date6y&"-"&date6m

For Each mthline In scblinenat

SQLNatQuery = "SELECT     TOP (100) PERCENT CONVERT(decimal(2, 1), q1Yes) / (CONVERT(decimal(2, 1), q1Yes) + CONVERT(decimal(2, 1), q1No)) * 100 AS percentq1, CONVERT(decimal(2, 1), "&_
"q2Yes) / (CONVERT(decimal(2, 1), q2Yes) + CONVERT(decimal(2, 1), q2No)) * 100 AS percentq2, CONVERT(decimal(2, 1), q3Yes) / (CONVERT(decimal(2, 1), q3Yes) "&_
" + CONVERT(decimal(2, 1), q3No)) * 100 AS percentq3, month FROM WEB_JLC.dbo.vProd_Customer_FB_Survey_NatLine WHERE (month = N'"&mthline&"')"
Set SQLNatData = SQL_WebTools_PRODDB.Execute(SQLNatQuery)
 If SQLNatData.BOF Then
 dataq1z = "'NULL',"
 dataq2z = "'NULL',"
 dataq3z = "'NULL',"
  Else
Do while not SQLNatData.EOF	
percentq1         = SQLNatData("percentq1")
percentq2         = SQLNatData("percentq2")
percentq3         = SQLNatData("percentq3")
SQLNatData.MoveNext
dataq1 = dataq1 & percentq1& ","
dataq2 = dataq2 & percentq2& ","
dataq3 = dataq3 & percentq3& ","
Loop

End If
dataq1 = dataq1 & dataq1z
dataq2 = dataq2 & dataq2z
dataq3 = dataq3 & dataq3z

surveyNat = "[{name: 'Initial Response %', data:["&dataq1&"]}, {name: 'Closing Response %', data:["&dataq2&"]}, {name: 'Overall Response %', data:["&dataq3&"]}]"
 percentq1 = ""
 percentq2 = ""
 percentq3 = ""
 dataq1z = ""
 dataq2z = ""
 dataq3z = ""
Next


SQLSiteQuery = "SELECT TOP (100) PERCENT targetID, targetName FROM WEB_JLC.dbo.Customer_FB_Target_prod"
Set SQLSiteData = SQL_WebTools_PRODDB.Execute(SQLSiteQuery)
Do while not SQLSiteData.EOF
tID = SQLSiteData("targetID")
targetName = SQLSiteData("targetName")

SQLdateQuery = "SELECT     targetID, mth FROM WEB_JLC.dbo.vDate_Last6Mths unpivot (mth for mths  in (curmth, [mth-1], [mth-2], [mth-3], [mth-4], [mth-5])) unpiv where targetID = '"&tID&"' order by mth"
Set SQLdateData = SQL_WebTools_PRODDB.Execute(SQLdateQuery)
Do while not SQLdateData.EOF
mth = SQLdateData("mth")
					  
SQLtotSiteQuery = "SELECT     TOP (100) PERCENT CONVERT(decimal(2, 1), q1Yes) / (CONVERT(decimal(2, 1), q1Yes) + CONVERT(decimal(2, 1), q1No)) * 100 AS percentq1, CONVERT(decimal(2, 1), "&_
"q2Yes) / (CONVERT(decimal(2, 1), q2Yes) + CONVERT(decimal(2, 1), q2No)) * 100 AS percentq2, CONVERT(decimal(2, 1), q3Yes) / (CONVERT(decimal(2, 1), q3Yes) "&_
" + CONVERT(decimal(2, 1), q3No)) * 100 AS percentq3, month, targetID FROM WEB_JLC.dbo.vProd_Customer_FB_Survey_UnitLine WHERE (month = N'"&mth&"') AND (targetID = "&tID&")"
Set SQLtotSiteData = SQL_WebTools_PRODDB.Execute(SQLtotSiteQuery)
 If SQLtotSiteData.BOF Then
 dataq1nodata = "'NULL'"
 dataq2nodata = "'NULL'"
 dataq3nodata = "'NULL'"
  Else
Do while not SQLtotSiteData.EOF	
 percentq1u = SQLtotSiteData("percentq1")
 percentq2u = SQLtotSiteData("percentq2")
 percentq3u = SQLtotSiteData("percentq3")

 SQLtotSiteData.MoveNext
Loop

End IF
dataq1u = percentq1u&dataq1nodata&","
dataq2u = percentq2u&dataq2nodata&","
dataq3u = percentq3u&dataq3nodata&","

 dataq1nodata = ""
 dataq2nodata = ""
 dataq3nodata = ""
 percentq1u = ""
 percentq2u = ""
 percentq3u = ""

SQLdateData.MoveNext

dataq1uz = dataq1uz & dataq1u
dataq2uz = dataq2uz & dataq2u
dataq3uz = dataq3uz & dataq3u

Loop

strall = strall & "[{name: '"&tID&"-"&targetName&"Initial Response %', data:["&dataq1uz&"]}, {name: 'Closing Response %', data:["&dataq2uz&"]}, {name: 'Overall Response %', data:["&dataq3uz&"]}]|"

SQLSiteData.MoveNext
 
dataq1uz = ""
dataq2uz = ""
dataq3uz = ""
Loop
strallz = strallz & strall

strsplitline = Split(strallz,"|")
SCBHQ = strsplitline(0)
JLUN = strsplitline(1)
JLUS = strsplitline(2)
JLUE = strsplitline(3)
JLUW = strsplitline(4)
JLUNQ = strsplitline(5)
JLUSQ = strsplitline(6)
JLUV = strsplitline(7)
End IF
%>

<script type="text/javascript">
var fbType = '<%=fbType%>';
function mailFn() {  

var m_address = 'mailto:jlc-scb-customermanagement@drn.mil.au?subject=WebTools%20Customer%20Feedback%20Tool%20-%20Survey%20Results%20and%20Charts';
location.href= m_address;
};


$(function() {
$('#Pie').highcharts({
	chart: {
        type: <%=chtypeNat%>
    },
    title: {
        text: 'All SCB Entities Combined'
    },
    xAxis: {
        categories: [<%=natcat%>]
    },
    yAxis: {
        min: 0,
		<%=max%>
        title: {
            text: '<span style="color:black">Satisfaction %</span>'
        }
    },
    tooltip: {
        pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
        shared: true
    },
    plotOptions: {
        column: {
            stacking: 'percent'
        }
    },
	credits: false,
	legend: {
		itemStyle: {
			fontSize: '12pt'
		}
	},
    series: <%=surveyNat%>
});
$('#PieSCBHQ').highcharts({
	chart: {
        type: <%=chtype%>
    },
    title: {
        text: 'SCBHQ'
    },
    xAxis: {
        categories: [<%=scbhqcat%>]
    },
    yAxis: {
        min: 0,
		<%=max%>
        title: {
            text: '<span style="color:black">Satisfaction %</span>'
        }
    },
    tooltip: {
        pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
        shared: true
    },
    plotOptions: {
        column: {
            stacking: 'percent'
        }
    },
	credits: false,
	legend: false,
    series: <%=SCBHQ%>
});
$('#PieJLUE').highcharts({
	chart: {
        type: <%=chtype%>
    },
    title: {
        text: 'JLUE'
    },
    xAxis: {
        categories: [<%=ecat%>]
    },
    yAxis: {
        min: 0,
		<%=max%>
        title: {
            text: '<span style="color:black">Satisfaction %</span>'
        }
    },
    tooltip: {
        pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
        shared: true
    },
    plotOptions: {
        column: {
            stacking: 'percent'
        }
    },
	credits: false,
	legend: false,
    series: <%=JLUE%>
});
$('#PieJLUV').highcharts({
	chart: {
        type: <%=chtype%>
    },
    title: {
        text: 'JLUV'
    },
    xAxis: {
        categories: [<%=vcat%>]
    },
    yAxis: {
        min: 0,
		<%=max%>
        title: {
            text: '<span style="color:black">Satisfaction %</span>'
        }
    },
    tooltip: {
        pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
        shared: true
    },
    plotOptions: {
        column: {
            stacking: 'percent'
        }
    },
	credits: false,
	legend: false,
    series: <%=JLUV%>
});
$('#PieJLUS').highcharts({
	chart: {
        type: <%=chtype%>
    },
    title: {
        text: 'JLUS'
    },
    xAxis: {
        categories: [<%=scat%>]
    },
    yAxis: {
        min: 0,
		<%=max%>
        title: {
            text: '<span style="color:black">Satisfaction %</span>'
        }
    },
    tooltip: {
        pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
        shared: true
    },
    plotOptions: {
        column: {
            stacking: 'percent'
        }
    },
	credits: false,
	legend: false,
    series: <%=JLUS%>
});
$('#PieJLUW').highcharts({
	chart: {
        type: <%=chtype%>
    },
    title: {
        text: 'JLUW'
    },
    xAxis: {
        categories: [<%=wcat%>]
    },
    yAxis: {
        min: 0,
		<%=max%>
        title: {
            text: '<span style="color:black">Satisfaction %</span>'
        }
    },
    tooltip: {
        pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
        shared: true
    },
    plotOptions: {
        column: {
            stacking: 'percent'
        }
    },
	credits: false,
	legend: false,
    series: <%=JLUW%>
});
$('#PieJLUN').highcharts({
	chart: {
        type: <%=chtype%>
    },
    title: {
        text: 'JLUN'
    },
    xAxis: {
        categories: [<%=ncat%>]
    },
    yAxis: {
        min: 0,
		<%=max%>
        title: {
            text: '<span style="color:black">Satisfaction %</span>'
        }
    },
    tooltip: {
        pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
        shared: true
    },
    plotOptions: {
        column: {
            stacking: 'percent'
        }
    },
	credits: false,
	legend: false,
    series: <%=JLUN%>
});
$('#PieJLUNQ').highcharts({
	chart: {
        type: <%=chtype%>
    },
    title: {
        text: 'JLUNQ'
    },
    xAxis: {
        categories: [<%=nqcat%>]
    },
    yAxis: {
        min: 0,
		<%=max%>
        title: {
            text: '<span style="color:black">Satisfaction %</span>'
        }
    },
    tooltip: {
        pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
        shared: true
    },
    plotOptions: {
        column: {
            stacking: 'percent'
        }
    },
	credits: false,
	legend: false,
    series: <%=JLUNQ%>
});
$('#PieJLUSQ').highcharts({
	chart: {
        type: <%=chtype%>
    },
    title: {
        text: 'JLUSQ'
    },
    xAxis: {
        categories: [<%=sqcat%>]
    },
    yAxis: {
        min: 0,
		<%=max%>
        title: {
            text: '<span style="color:black">Satisfaction %</span>'
        }
    },
    tooltip: {
        pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
        shared: true
    },
    plotOptions: {
        column: {
            stacking: 'percent'
        }
    },
	credits: false,
	legend: false,
    series: <%=JLUSQ%>
});
});
</script>
<body>
<div class="container-fluid">
<div class="row">
<div class="col-md-4" style="height: 175px;"><h1 style="text-align: center;">Customer Feedback Survey Results</h1>
<a href="<%= Application("CONTENTLOAD") %>?PageURL=<%= Application("USERDEVELOPED") %>JLC/DSCNO/Customer_FB_Main.asp" class="btn btn-primary">Back</a>
<a href="<%= Application("CONTENTLOAD") %>?PageURL=<%= Application("USERDEVELOPED") %>JLC/DSCNO/Customer_FB_Survey_Results.asp|page=tot" <%=buttonlinvis%>>Trend Over Time</a>
<a href="<%= Application("CONTENTLOAD") %>?PageURL=<%= Application("USERDEVELOPED") %>JLC/DSCNO/Customer_FB_Survey_Results.asp|page=column" <%=buttoncolvis%>>Back to column chart</a>

<a href="" class="btn btn-info pull-right" data-toggle="modal" data-target="#modal_about">More Info</a>
<br><br>
<div class="col-md-12" style="height: 525px;" id="Pie"></div></div>
<div class="col-md-2" style="height: 350px;" id="PieSCBHQ"></div>
<div class="col-md-2"  style="height: 350px;" id="PieJLUV"></div>
<div class="col-md-2"  style="height: 350px;" id="PieJLUE"></div>
<div class="col-md-2"  style="height: 350px;" id="PieJLUSQ"></div>

<div class="col-md-4"  style="height: 350px;"></div>

<div class="col-md-2"  style="height: 350px;" id="PieJLUNQ"></div>
<div class="col-md-2"  style="height: 350px;" id="PieJLUN"></div>
<div class="col-md-2"  style="height: 350px;" id="PieJLUS"></div>
<div class="col-md-2"  style="height: 350px;" id="PieJLUW"></div>

</div>
</div>
<div class="modal fade" id="modal_about" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="myModalLabel"><br>About this Chart</h4>
</div>
<div class="modal-body">
When a customer completes a survey these charts will update.  Customers are asked to provide feedback on Initial Response (IR), Closing Response (CR) and Feedback Overall (FO) and are expressed 
here as a percentage of overall surveys completed.  By clicking on the "Trend Over Time" button you can get a sense of customer satisfaction with regards to the CFT and how the feedback was handled.</div>
<div class="modal-footer">
<button type="button" class="btn btn-info" data-dismiss="modal" role="button" onclick="mailFn()">I would like to know more...</button>
     <button type="button" class="btn btn-success" data-dismiss="modal">Ok, Got it!</button>
      </div>
    </div>
</div>
</div>

</body>
</html>
<!-- #include virtual = "/includes/WebTools_AccessLog.asp" -->

<!-- #include virtual = "/includes/WebTools_Core.asp" -->
<html>
<head>
  <meta name="Shane Kline" content="CFTv2">
<!-- Bootstrap -->  
    <link href="<%= Application("TP-BOOTSTRAP-CSS") %>" rel="stylesheet">
    <link href="<%= Application("TP-BOOTSTRAP-TOGGLE-CSS") %>" rel="stylesheet">
<!-- Jquery -->
	<script src='<%= Application("TP-JQUERY") %>'></script>
	<script src='<%= Application("TP-JQUERYUI") %>'></script>
<!-- Bootstrap -->  	
	<script src='<%= Application("TP-BOOTSTRAP") %>'></script>
	<script src='<%= Application("TP-BOOTSTRAP-TOGGLE") %>'></script>
<!-- Font Awesome -->
	    <link href="<%= Application("TP-FONTAWESOME") %>" rel="stylesheet">
<!-- Modernizr -->
    <script src='<%= Application("TP-MODERNIZR") %>'></script>
</head>
<style>
tr, td {
	padding-bottom: 0px;
	padding-top: 0px;
	font-size: 16px;
}

#shortdesc {
	font-size:16px;
	padding-bottom: 0px;
	padding-top: 0px;
	width: 250px;
	height: 25px;
	}

#longdesc {
	font-size:16px;
	padding-bottom: 0px;
	padding-top: 0px;
	width: 500px;
	height: 25px;
	}

#stat {
	font-size:16px;
	padding-bottom: 0px;
	padding-top: 0px;
	width: 110px;
	height: 25px;

	}
	
#parent {
	width: 225px;
}

#formupdate {
	background-color: #e6e2ac;
}
</style>
<%
feedbackID = request.querystring("vfbID")
'authuser = request.querystring("authuser")
authuser = "1"
'response.write(feedbackID)
If authuser = "1" Then
intro = "<font style='color:red;'><strong>You are in Customer Response Mode.<br>You may add your response here </strong></font>"
Else 
response.Redirect "http://WEBTOOLS.defence.gov.au/prod/Core/showPage.asp?PageURL=http://WEBTOOLS.defence.gov.au/prod/UserDeveloped/JLC/DSCNO/responseredirect.asp"
End If

%>
<script>

$(document).ready(function(){
$("#updatefb").click(function(){
	$( "#feedbackID_update" ).append("<input type='hidden' name='custupdt' value='1' />");
	$( "#feedbackID_update" ).submit();
});
});

$(document).keypress(
	function(event){
		if (event.which == '13') {
			event.preventDefault();
			}
});	
	function showfb() {  
      //alert("im in the pulldown function");
	  //
	var fbid = document.getElementById("fbID").value;
			//alert('the thing is_'+jlu);

            var selectSeed = document.getElementById("fbID");
            //alert("select seed is_"+selectSeed);
            
				document.getElementById("labelunittext").removeAttribute('hidden');
				
				var optSeed = document.createElement("textarea");
				optSeed.value = "";
				
				selectSeed.appendChild(optSeed);
                
            $.ajax({
                           //alert('im in the ajax');
						   type: "Get",
                           cache: false, 
                           url: "Customer_FB_AJAX1.asp?vfbid=" + fbid,
                           datatype: "text/html",
                           success: function(response) {

						   var select = document.getElementById("#fbtext1");
						   
                           var option = response;
                           $("#fbtext1").append(option);
                           }
                     })
					 $('#fbtext1').empty();
	};
	
</script>
<body>
<div class="container-fluid">
<div class="row"><br></div>
	<div class="row">
		<div class="col-sm-12"><% response.write intro %>
		<a href="javascript:window.parent.location.reload(true);" class="btn btn-primary btn-sm pull-right">Close</a></div><br>		
	</div>
<form name="input" id="feedbackID_update" method="get" action="Customer_FB_ReplyUploadbyID.asp">		
	<div class="row" id="formupdate">
			<div class="col-sm-3">
				<div class="form-group">
					<label for="fbID">1. Feedback ID</label>
					<select class="form-control" id="fbID" name="fbID" onchange="showfb()">
					<option value="">Select your Feedback ID: &nbsp;&nbsp;&nbsp;&nbsp;(Originator-Date-Target Unit)</option>
                    <% SQLQuery = "SELECT     TOP (100) PERCENT WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackID, WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.cdOriginator, WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackDateTime, "&_
"WEB_JLC.dbo.Customer_FB_Target_prod.targetName, CONVERT(nvarchar(50), WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.cdOriginator) + N'-' + replace(CONVERT(nvarchar(9), "&_
"WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackDateTime, 6), ' ','/') + N'-' + WEB_JLC.dbo.Customer_FB_Target_prod.targetName AS FBIDadd "&_
"FROM         WEB_JLC.dbo.CUSTOMDATA_Feedback_prod LEFT OUTER JOIN WEB_JLC.dbo.Customer_FB_Target_prod ON WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.targetID = WEB_JLC.dbo.Customer_FB_Target_prod.targetID "&_
"WHERE     (WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.dateClosed IS NULL) ORDER BY WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackID DESC"
                    Set SQLDataRefresh = SQL_WebTools_PRODDB.Execute(SQLQuery)
                    Do While not SQLDataRefresh.EOF
                        feedbackID      = SQLDataRefresh("feedbackID")
						FBIDadd      = SQLDataRefresh("FBIDadd")
                        response.write "<option value='"&feedbackID&"'>"&feedbackID&"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;("&FBIDadd&")</option>"
                        SQLDataRefresh.MoveNext 
                    Loop 
                    %>					
					</select>
				</div> 
			</div>

        <div class="col-sm-3">
			<div class="form-group">
				<label for="userID">Response UserID</label>
				<select class="form-control" id="userID" name="userID">
				<%response.write "<option value='"&strDRNLogon&"'>"&strDRNLogon&"</option>"%>
				</select>
			</div>
		</div>
		
		<div class="col-sm-3">
			<div class="form-group">
				<label for="cDate">Response Date</label>
				<select class="form-control" id="cDate" name="cDate">
				<%response.write "<option value='"&Now()&"'>"&Now()&"</option>"%>
				</select>
			</div>
		</div>

</div>
	<div class="row" id="formupdate">
	<div class="col-sm-1"></div>
			<label for="fbtext">2. You may write your response here:</label>
				<div class="col-sm-11" id="fbtext">
					<div class="form-group">
						<textarea rows="10" cols="120" id="fbtexta" name="fbtexta"></textarea>
				</div>
		</div>
	</div>
	
</form>
	<div class="row">
	<div class="col-sm-12"><button type="button" class="btn btn-success pull-right" id="updatefb" >Update</button></div>
</div>
	
<div class="row">
	<div class="col-sm-1"></div>
			<label for="fbtext1" hidden id="labelunittext">The Unit recently wrote (editing not permitted):</label>
				<div class="col-sm-11" id="fbtext1">
		</div>
	</div>
</div>
	

<script type="text/javascript" language="javascript">
$(document).ready(function() {
     $('[data-toggle="tooltip"]').tooltip();
});
</script> 
</body>
</html>
<!-- #include virtual = "/includes/WebTools_AccessLog.asp" -->
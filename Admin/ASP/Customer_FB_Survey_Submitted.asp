<!-- #include virtual = "/includes/WebTools_Core.asp" -->
<html>
<head>
  <meta name="Shane Kline" content="CFTv2">
<!-- Bootstrap -->  
    <link href="<%= Application("TP-BOOTSTRAP-CSS") %>" rel="stylesheet">
    <link href="<%= Application("TP-BOOTSTRAP-TOGGLE-CSS") %>" rel="stylesheet">
<!-- Jquery -->
	<script src='<%= Application("TP-JQUERY") %>'></script>
	<script src='<%= Application("TP-JQUERYUI") %>'></script>
<!-- Bootstrap -->  	
	<script src='<%= Application("TP-BOOTSTRAP") %>'></script>
	<script src='<%= Application("TP-BOOTSTRAP-TOGGLE") %>'></script>
<!-- Font Awesome -->
    <link href="<%= Application("TP-FONTAWESOME") %>" rel="stylesheet">
<!-- Modernizr -->
    <script src='<%= Application("TP-MODERNIZR") %>'></script>
</head>
<%
Sub SendMail2(strEmailTitle,strEmailSubject,strHTMLHead,strHTMLBody,strMailToList)
If Application("SERVERURL") <> "http://Win2k8server/" Then
  Dim HTML
  Set myMail=CreateObject("CDO.Message")
  myMail.Subject=strEmailSubject
  myMail.From="JLC-SCBHQ-CustomerFeedback@drn.mil.au"
  myMail.To=strMailToList
  'response.write(strMailToList)
  HTML = "<!DOCTYPE HTML PUBLIC""-//IETF//DTD HTML//EN""><html><head><title>" & strEmailTitle & "</title>" & strHTMLHead & "</head>"
  HTML = HTML & "<body>" & strHTMLBody & "</body></html>"
  myMail.HTMLBody=HTML
  myMail.HTMLBodyPart.ContentTransferEncoding = "quoted-printable"
  myMail.Configuration.Fields.Item _
  ("http://schemas.microsoft.com/cdo/configuration/sendusing")=2
  'Name or IP of remote SMTP server
  myMail.Configuration.Fields.Item _
  ("http://schemas.microsoft.com/cdo/configuration/smtpserver")="relay.services.dpe.protected.mil.au"
  'Server port
  myMail.Configuration.Fields.Item _
  ("http://schemas.microsoft.com/cdo/configuration/smtpserverport")=25 
  myMail.Configuration.Fields.Update
  myMail.Send
  set myMail=nothing
End If
End Sub

q1            = request.querystring("q1")
q2            = request.querystring("q2")
q3            = request.querystring("q3")
q4            = request.querystring("q4")
q5            = request.querystring("q5")
q6            = request.querystring("q6")
kd1           = request.querystring("kd1")
kd2           = request.querystring("kd2")
kd3           = request.querystring("kd3")

SQLQuery1 = "SELECT  MAX(surveyID) as surveyID FROM Web_JLC.dbo.Customer_FB_Survey_prod"
Set SQLData1 = SQL_WebTools_PRODDB.Execute(SQLQuery1)
Do While not SQLData1.EOF
  lastsurveyID = SQLData1("surveyID")
  SQLData1.MoveNext  
Loop
'lastsurveyID = 0
surveyID = lastsurveyID +1 



'this insert into creates a record in the comments table
SQLstatement1 = 					"INSERT INTO Web_JLC.dbo.Customer_FB_Survey_prod (surveyID, q1response, q2response, q3response, q4response, q5response, q6response, fbID, unitReply_days, unitClose_days, surveyDate) "
SQLstatement1 = SQLstatement1 & 	"VALUES ("&surveyID&","&q1&","&q2&","&q3&","&q4&","&q5&","&q6&","&kd1&","&kd2&","&kd3&", getdate())"
'response.write(SQLstatement1)
SQL_WebTools_PRODDB.Execute(SQLstatement1)



SQLQuery2 = "SELECT WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.cdOriginator, WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.cdUnit, WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.cdPhone, "&_
"WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.cdEmail, WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackID, WEB_JLC.dbo.Customer_FB_Survey_prod.q1response, "&_
"WEB_JLC.dbo.Customer_FB_Survey_prod.q2response, WEB_JLC.dbo.Customer_FB_Survey_prod.q3response, WEB_JLC.dbo.Customer_FB_Survey_prod.q4response, "&_
"WEB_JLC.dbo.Customer_FB_Survey_prod.unitReply_days, WEB_JLC.dbo.Customer_FB_Survey_prod.unitClose_days "&_
"FROM WEB_JLC.dbo.CUSTOMDATA_Feedback_prod INNER JOIN WEB_JLC.dbo.Customer_FB_Survey_prod ON WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackID = WEB_JLC.dbo.Customer_FB_Survey_prod.fbID "&_
"WHERE     (WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackID = "&kd1&")"					  
Set SQLData2 = SQL_WebTools_PRODDB.Execute(SQLQuery2)
Do While not SQLData2.EOF
  cdOriginator = SQLData2("cdOriginator")
  cdUnit = SQLData2("cdUnit")
  cdPhone = SQLData2("cdPhone")
  cdEmail = SQLData2("cdEmail")
  unitReply_days = SQLData2("unitReply_days")
  unitClose_days = SQLData2("unitClose_days")
  SQLData2.MoveNext  
Loop

If q4 = "2" Then
contact = "by Phone on "&cdPhone
Else If q4 = "1" Then
contact = "by Email on "&cdEmail
End If
End If

If q1 = "0" Then
q1text = "unreasonable"
Else
q1text = "reasonable"
End If

If q2 = "0" Then
q2text = "unreasonable"
Else
q2text = "reasonable"
End If

If q3 = "1" Then
q3text = "satisfied"
Else
q3text = "not satisfied"
End If

updatenote = "Survey response submitted"

If q4 <> "0" Then
SendMail2 "Survey Response Received","You received a Survey Response from the SCB CFT ","<b><a href='http://WEBTOOLS.defence.gov.au/prod/Core/Content/contentLoading.asp?PageURL=http://WEBTOOLS.defence.gov.au/prod/default.asp?PageID=32F7ABE0-1394-4AD6-ADEA-EEF2C19B132A'>SCB Customer Feedback Portal</a></b><br> "&_
"<br>The Customer ("&cdOriginator&") has selected (on the Survey) to be contacted "&contact&"<br> "&_
"The Customer felt that the Units ("&cdUnit&") initial response was "&q1text&" and that the closing response was "&q2text&"<br> "&_
"The Unit initially replied in "&unitReply_days&" Days and closed the feedback in "&unitClose_days&" Days from the Feedback submission date "&_
"<br>A transcript of the Feedback can be found <a href='http://WEBTOOLS.defence.gov.au/prod/Core/Content/contentLoading.asp?PageURL=http://WEBTOOLS.defence.gov.au/prod/default.asp?PageID=32F7ABE0-1394-4AD6-ADEA-EEF2C19B132A'>here</a>","The Customer is "&q3text&".","jlc-scb-customermanagement@drn.mil.au"

End If



%>
<html>
<body>
<br><br><br>
<div class="container">
<h1 align="center" id="msg"><small><%=response.write(updateNote)%></small><br></h1>
<h1 align="center"><small><br><i class="fa fa-thumbs-up fa-4x" aria-hidden="true"></i><br><br>
This window will automatically close in 3 seconds</small></h1>
</div>
</body>
</html>
<script>
setTimeout(function() {
window.parent.location.reload();
}, 3000);
</script>
<!-- #include virtual = "/includes/WebTools_AccessLog.asp" -->
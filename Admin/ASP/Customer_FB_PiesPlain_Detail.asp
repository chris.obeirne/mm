<!-- #include virtual = "/includes/WebTools_Core.asp" -->
<% Response.Buffer=true %>
<html>
<head>
  <meta name="Shane Kline" content="CFTv2">
<!-- Bootstrap -->  
    <link href="<%= Application("TP-BOOTSTRAP-CSS") %>" rel="stylesheet">
    <link href="<%= Application("TP-BOOTSTRAP-TOGGLE-CSS") %>" rel="stylesheet">
<!-- Jquery -->
	<script src='<%= Application("TP-JQUERY") %>'></script>
	<script src='<%= Application("TP-JQUERYUI") %>'></script>
<!-- Bootstrap -->  	
	<script src='<%= Application("TP-BOOTSTRAP") %>'></script>
	<script src='<%= Application("TP-BOOTSTRAP-TOGGLE") %>'></script>
<!-- Font Awesome -->
	    <link href="<%= Application("TP-FONTAWESOME") %>" rel="stylesheet">
<!-- Modernizr -->
    <script src='<%= Application("TP-MODERNIZR") %>'></script>
<!-- DataTables -->
    <link href="<%=Application("TP-DTABLES-ALL-CSS") %>" rel="stylesheet">
    <script src='<%= Application("TP-DTABLES-ALL-JS") %>'></script>
	<script src='<%= Application("THIRDPARTY_CORE") %>DataTables/datatables.min.js'></script>

  <title>CFT Details</title>

  <style>
    body th, body td
    {
      font-size: 12px;
    }
    body .spin-bg {
      opacity:    1;
      background: #fff;
      width:      100%;
      height:     100%;
      z-index:    10;
      top:        0;
      left:       0;
      position:   fixed;
      text-align: center;
    }
    
	.text-wrap {
		white-space:normal;
	}
	.width-500{
		width:500px;
	}
	
  </style>
</head>
<body>

<%
clickLocation = Request("vpieLoc") 
locationWedge = Request("vlocWedge")
'fbNat = Request("vfbNat")
fbType = Request("vfbType")
'strViewName = "vProd_Customer_FB_Detail"
strViewName = "vCustomer_FB_Detail_with_stuffNarative"

If len(clickLocation) = "2" Then
strWhereadd = "AND (left(natureID, 2) = '"&clickLocation&"') "
Else
strWhereadd = "AND (natureID = '"&clickLocation&"') "
End If


If locationWedge = "National" Then
strWhere = " WHERE (deleted IS NULL) AND (feedbackType = '"&fbType&"') "&strWhereadd
Else
strWhere = " WHERE (deleted IS NULL) AND (feedbackType = '"&fbType&"') AND (targetName = N'"&locationWedge&"') "&strWhereadd
End If



excludeCols = "and COLUMN_NAME <> 'deleted' and COLUMN_NAME <> 'Days Open' "
strFilterText = "<b>You selected: " & locationWedge & " for all " & fbType & "s with a Nature Specific of "&clickLocation&"</b>"
	  
'response.write(strWhere)
Response.Flush

SQLTableQuery = "SELECT     TABLE_NAME, COLUMN_NAME, ORDINAL_POSITION " &_
                "FROM         Web_JLC.INFORMATION_SCHEMA.COLUMNS " &_
                "WHERE     (TABLE_NAME = N'" & strViewName & "') "&excludeCols&_
                "ORDER BY ORDINAL_POSITION"
'response.write(SQLTableQuery)
Set SQLTableData = SQL_WebTools_PRODDB.Execute(SQLTableQuery)
%>

    <div class="container-fluid"><h4><%= strFilterText%></h4> <%=colouringnote%></div>
    <div class="container-fluid"><!-- Required to stop the datatable from leaving the edge of the page -->
        <table class="display nowrap table table-striped table-bordered" id="Details" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <%Do while not SQLTableData.eof %>
                    <th><%= replace(SQLTableData("COLUMN_NAME"),"_"," ") %></th>
                    <%strColumnNames = strColumnNames + "[" + SQLTableData("COLUMN_NAME") + "]," %>
                    <%SQLTableData.MoveNext
                    Loop %>
                    <%SQLTableData.MoveFirst %>
                </tr>
            </thead>

            <%
            SQLQuery = "SELECT " & left(strColumnNames,len(strColumnNames)-1)  &_
                       " FROM Web_JLC.dbo." & strViewName & strWhere
'response.write(SQLQuery)
            Set SQLData = SQL_WebTools_PRODDB.Execute(SQLQuery)

            Do While not SQLData.EOF

            strDataArray = strDataArray & " ["
                Do While not SQLTableData.EOF
                    strColName = SQLTableData("COLUMN_NAME")
                    strColData = SQLData("" & strColName & "")
                    If strColData & "" <> "" Then
                        strColData = Replace(strColData, """", "\""")
                    End If
					If strColData & "" <> "" Then strColData = Replace(strColData, """", " ") End If
					If strColData & "" <> "" Then strColData = Replace(strColData, "'", " ") End If
					If strColData & "" <> "" Then strColData = Replace(strColData, vbCr, " ") End If
					If strColData & "" <> "" Then strColData = Replace(strColData, vbLf, " ") End If
					If strColData & "" <> "" Then strColData = Replace(strColData, "'", " ") End If
					If strColData & "" <> "" Then strColData = Replace(strColData, "&", " ") End If	
					If strColData & "" <> "" Then strColData = Replace(strColData, "<", " ") End If
					If strColData & "" <> "" Then strColData = Replace(strColData, ">", " ") End If	
					If strColData & "" <> "" Then strColData = Replace(strColData, "/", " ") End If
					If strColName = "FeedbackText" Then strColData = Replace(strColData, strColData, "<div class='text-wrap width-500'>"&strColData&"</div>") End If
					If strColName = "comment_DateTime" and strColData <> "" Then strColData = Replace(strColData, strColData, "<div class='text-wrap width-500'>"&strColData&"</div>") End If					
                    strSubAray = strSubAray & """" & strColData & """, "
                    SQLTableData.MoveNext
                Loop
                if strDataArray <> "" then
                strDataArray = strDataArray & left(strSubAray,len(strSubAray)-2)
                strSubAray = ""
                end if

            SQLTableData.MoveFirst
            strDataArray = strDataArray & "],"

            SQLData.MoveNext
            Loop
            if strDataArray <> "" then
                strDataArray = left(strDataArray,len(strDataArray)-1)
            end if
            %>


       </table>
    </div>
       <script>
	  
     function IEvCh() {
        var myNav = navigator.userAgent.toLowerCase();
        return (myNav.indexOf('chrome') != -1) ? 'Chrome' : 'IE';
      }
      if (IEvCh() == 'Chrome') {
        var Hght = '65vh';
      } else {
        var Hght = screen.height;
        var Hght = Hght * .55;
        var Hght = Hght + 'px';
      };
	  var locw = '<%=locationWedge%>';

        $(document).ready(function() {
          var MyTaskTable = $('#Details').dataTable({
            scrollY: Hght,
            scrollX: 'initial',
            pagingType: 'full_numbers',
            colReorder: true,
			destroy: true,	
            colReorder: {
              realtime: false
            },
            language: {
              search: ""
            },
            initComplete: function(settings, json) {
              $('.dataTables_filter label input').attr('placeholder','Type here to search');
            },
            sDom: "<'row'<'col-sm-6'B><'col-sm-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-2'l><'col-sm-4'i><'col-sm-6'p>>",
            buttons: ['copyHtml5', 'excelHtml5', 'csvHtml5', {
              extend: 'pdfHtml5',
              orientation: 'landscape',
              pageSize: 'A1'
            }, 'print', 'colvis'],
            deferRender: true,
            iDisplayLength: 50,
			aaSorting: [0, 'dsc'],
	data: [<%= strDataArray %>]
        });
      });
      $('Details').ready(function() {
        el.spin(false).remove();
      })
    </script>
</body>
</html>
<!-- #include virtual = "/includes/WebTools_AccessLog.asp" -->
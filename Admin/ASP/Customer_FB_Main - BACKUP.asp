<!-- #include virtual = "/includes/WebTools_Core.asp" -->
<html>
<head>
  <meta name="Shane Kline" content="CFTv2" title="SCB Customer Feedback">
	<link href="<%= Application("CSS") %>" rel="stylesheet">
  <!-- Bootstrap -->  
    <link href="<%= Application("TP-BOOTSTRAP-CSS") %>" rel="stylesheet">
    <link href="<%= Application("TP-BOOTSTRAP-TOGGLE-CSS") %>" rel="stylesheet">
<!-- Jquery -->
	<script src='<%= Application("TP-JQUERY") %>'></script>
	<script src='<%= Application("TP-JQUERYUI") %>'></script>
<!-- Bootstrap -->  	
	<script src='<%= Application("TP-BOOTSTRAP") %>'></script>
	<script src='<%= Application("TP-BOOTSTRAP-TOGGLE") %>'></script>
<!-- Font Awesome -->
	<script src='<%= Application("TP-FONTAWESOME") %>'></script>
<!-- Modernizr -->
    <script src='<%= Application("TP-MODERNIZR") %>'></script>

<style>
#formsection {
    font-family: verdana;
    font-size: 12px;
	border: 6px solid #007780;
	border-radius: 5px;
	padding: 8px;
	background-color: #e6e2ac;
}

#menusection {
	font-family: verdana;
    font-size: 16px;
}

#header {
	font-family: verdana;
    font-size: 20px;
    color: white;
    text-align: center;
	background-color: #007780;
}

h4 {
	font-family: verdana;
    font-size: 18px;
    text-align: center;
}

#cdeets {
	margin-left: 10px;
	border: 2px solid #4d3939;
	border-radius: 2px;
}

#fdeets {
	margin-left: 10px;
	border: 2px solid #4d3939;
	border-radius: 2px;
}

#ftext {
	margin-left: 10px;
	border: 2px solid #4d3939;
	border-radius: 2px;
}

#vacant1 {
	margin-left: 10px;
	border: 2px solid #ff0000;
	border-radius: 2px;
}

body {
    font-family: verdana;
    font-size: 20px;
	color: black;
	
	background-color: #007780;
}

#fs {
	font-family: verdana;
    font-size: 24px;
}

#naturech1 {
	font-size: 12px;
}

#naturech2 {
	font-size: 12px;
}

</style>
</head>
<%
page = request.querystring("page")
vara = request.querystring("vara")
varb = request.querystring("varb")
varc = request.querystring("varc")
If page = "" or page = "0" Then
loading = ""
else if page = "1" Then
loading = "onload='loadCustSurvey()'"
else if page = "2" Then
loading = "onload='loadNarrative()'"
else if page = "3" Then
loading = "onload='loadJLUresponse()'"
else if page = "4" Then
loading = "onload='loadCustresponse()'"
else if page = "5" Then
loading = ""
Response.Redirect "http://WEBTOOLS.defence.gov.au/prod/Core/Content/contentLoading.asp?PageURL=http://WEBTOOLS.defence.gov.au/prod/UserDeveloped/JLC/DSCNO/Customer_FB_Pies.asp"
End If
End If
End If
End If
End If
End If

authorisedUser = 1    '0
'if strDRNLogon = "shane.kline" then authorisedUser = 1 end if
'if strDRNLogon = "derek.anscombe1" then authorisedUser = 1 end if
'if strDRNLogon = "adam.hennessy1" then authorisedUser = 1 end if
'if strDRNLogon = "clancy.horman" then authorisedUser = 1 end if
'if authorisedUser = 0 then 

'	erVisE = "class='btn btn-warning disabled'"
'	erVisN = "class='btn btn-warning disabled'"
'	erVisUC = "disabled"
'	erVisCT = "disabled"
'	erVisDT = "disabled"
	
'    Else
	erVisE = "class='btn btn-primary' value='emailcs' onclick='openEditTable(this.value)'"
	erVisN = "onchange='openEditTable(this.value)'"
	erVisUC = "onclick='openEditUnitContacts()'"
	
	erVisCT = "data-toggle='modal' data-target='#modal_fid'"
	erVisDE = "data-toggle='modal' data-target='#modal_ct'"
	
'end if

authorisedUsershow = "no user restrictions" '"derek.anscombe1, adam.hennessy1"

%>

<script type="text/javascript">

function mailFn() {  
var m_address = 'mailto:jlc-scb-customermanagement@drn.mil.au?subject=WebTools%20Customer%20Feedback%20Tool%20-%20List%20or%20Report%20Request';
location.href= m_address;
};

function mailFn1() { 
var ts_address = 'mailto:jlc-scbhq-customerfeedback@drn.mil.au?subject=WebTools%20Customer%20Feedback%20Tool%20-%20Technical%20Support';
location.href= ts_address;
};


function openEditTable(table) {
	  parent.SetupOverlay(parent.document.body.clientWidth-400,parent.document.body.clientHeight-100);
      parent.window.frames["ifrmFrontOverlay"].location ="<%= Application("CONTENTLOAD") %>?PageURL=<%= Application("USERDEVELOPED") %>JLC/DSCNO/Customer_FB_Custom_Table_Edit.asp|vTable=" + table
      parent.openOverlay();
};

function openEditUnitContacts(table) {
	  parent.SetupOverlay(parent.document.body.clientWidth-400,parent.document.body.clientHeight-100);
      parent.window.frames["ifrmFrontOverlay"].location ="<%= Application("CONTENTLOAD") %>?PageURL=<%= Application("USERDEVELOPED") %>JLC/DSCNO/Customer_FB_EditUnit_Contacts.asp"
      parent.openOverlay();
};

function loadEditView(feedbackID) {
	  parent.SetupOverlay(parent.document.body.clientWidth-700,parent.document.body.clientHeight-100);
      parent.window.frames["ifrmFrontOverlay"].location ="<%= Application("CONTENTLOAD") %>?PageURL=<%= Application("USERDEVELOPED") %>JLC/DSCNO/Customer_FB_EditbyID.asp|vfbID=" + feedbackID + "|authuser=" + <%=authorisedUser%>
      parent.openOverlay();
};

function loadJLUresponse() {
	  parent.SetupOverlay(parent.document.body.clientWidth-700,parent.document.body.clientHeight-100);
      parent.window.frames["ifrmFrontOverlay"].location ="<%= Application("CONTENTLOAD") %>?PageURL=<%= Application("USERDEVELOPED") %>JLC/DSCNO/Customer_FB_JLU_ReplybyID.asp"
      parent.openOverlay();
};

function loadCustresponse() {
	  parent.SetupOverlay(parent.document.body.clientWidth-700,parent.document.body.clientHeight-100);
      parent.window.frames["ifrmFrontOverlay"].location ="<%= Application("CONTENTLOAD") %>?PageURL=<%= Application("USERDEVELOPED") %>JLC/DSCNO/Customer_FB_Cust_ReplybyID.asp"
      parent.openOverlay();
};

function loadCustSurvey() {
	  parent.SetupOverlay(parent.document.body.clientWidth-700,parent.document.body.clientHeight-100);
	  parent.window.frames["ifrmFrontOverlay"].location ="<%= Application("CONTENTLOAD") %>?PageURL=<%= Application("USERDEVELOPED") %>JLC/DSCNO/Customer_FB_Survey.asp" 
	  parent.openOverlay();
};

function loadNarrative() {
	  parent.SetupOverlay(parent.document.body.clientWidth-700,parent.document.body.clientHeight-100);
      parent.window.frames["ifrmFrontOverlay"].location ="<%= Application("CONTENTLOAD") %>?PageURL=<%= Application("USERDEVELOPED") %>JLC/DSCNO/Customer_FB_Narrative.asp"
      parent.openOverlay();
};
   
function execSummary(mth) {
	  parent.SetupOverlay(parent.document.body.clientWidth-200,parent.document.body.clientHeight-100);
      parent.window.frames["ifrmFrontOverlay"].location ="<%= Application("CONTENTLOAD") %>?PageURL=<%= Application("USERDEVELOPED") %>JLC/DSCNO/Customer_FB_Exec_Summary.asp|mthSelect=" + mth
      parent.openOverlay();
};

$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();
	
    $("#formsubmitButton").click(function(){
    var validation = 0;
	
	var feedbackType = $("#feedbackType").val()
	var targetID = $("#targetID").val()
	var nature = $("#nature").val()
	var natureDesc = $("#natureDesc").val()
	var originator = $("#originator").val()
	var unit = $("#unit").val()
	var phone = $("#phone").val()
	var email = $("#email").val()
	
    var freetext = $("#freetext").val()
	
		if(feedbackType=="")        {
		validation = validation+1
		$("#feedbackType").addClass("btn-warning");
        }
        if(targetID=="")            {
            validation = validation+1
            $("#targetID").addClass("btn-warning");
        }
        if(nature=="")          {
            validation = validation+1
            $("#nature").addClass("btn-warning");
        }
        if(natureDesc=="" && feedbackType == "Complaint") {
			validation = validation+1
			$("#natureDesc").addClass("btn-warning");
		} else if (natureDesc=="" && feedbackType != "Complaint") {
			validation = validation+0
		} else
		{validation = validation+0;
        }
        if(originator=="")        {
            validation = validation+1
            $("#originator").addClass("btn-warning");
        }
        if(unit=="")        {
            validation = validation+1
            $("#unit").addClass("btn-warning");
        }
        if(phone=="")        {
            validation = validation+1
            $("#phone").addClass("btn-warning");
        }
        if(email=="")        {
            validation = validation+1
            $("#email").addClass("btn-warning");
        }	
	
		if(freetext=="")             {
		validation = validation+1
		$("#freetext").addClass("btn-warning");
		}

		if(freetext.length > 1800)   {
		validation = validation+1
		$("#freetext").addClass("btn-warning");
		$("#freetextErrorMsg").html("<div class='alert alert-danger' role='alert'>You have exceeded the maximum number of characters</div>");
		}
		
        if (validation == 0 ){
            $( "#feedbackform" ).submit();
        }
        else {
             alert("You have missed a mandatory field and/or exceeded the maximum free text limit.  Please select any highlighted field or reduce feedback text, formatting, styling & unnecessary characters/punctuation");
        };
		
});
});
$(function() {
    $("#feedbackType").change(function() {
        $("#feedbackType").removeClass("btn-warning");
    });
});
$(function() {
    $("#targetID").change(function() {
        $("#targetID").removeClass("btn-warning");
    });
});
$(function() {
    $("#nature").change(function() {
        $("#nature").removeClass("btn-warning");
    });
});
$(function() {
    $("#natureDesc").change(function() {
        $("#natureDesc").removeClass("btn-warning");
    });
});
$(function() {
    $("#originator").change(function() {
        $("#originator").removeClass("btn-warning");
    });
});
$(function() {
    $("#unit").change(function() {
        $("#unit").removeClass("btn-warning");
    });
});
$(function() {
    $("#phone").change(function() {
        $("#phone").removeClass("btn-warning");
    });
});
$(function() {
   $("#email").change(function() {
      $("#email").removeClass("btn-warning");
 });
});
$(function() {
   $("#freetext").change(function() {
      $("#freetext").removeClass("btn-warning");
 });
});		

function actionPulldown() {  

	var type = document.getElementById('feedbackType').value;
	if (type == "Complaint") {
	document.getElementById("natureDesc").setAttribute('enabled');
	
	
	var natureP = document.getElementById('nature').value;

	var selectSeed = document.getElementById("natureDesc");
	
		var optSeed = document.createElement("option");
		optSeed.innerHTML = "Select one:";
		optSeed.value = "";
		
		selectSeed.appendChild(optSeed);
	
	
	$.ajax({

				   type: "Get",
				   cache: false, 
				   url: "Customer_FB_AJAX.asp?vnatureP=" + natureP,
				   datatype: "text/html",
				   success: function(response) {

				   var select = document.getElementById("#natureDesc");

				   var option = response;
				   $("#natureDesc").append(option);
				   }
			 })
	  $('#natureDesc').empty();
	  
} else {document.getElementById("naturelab").innerHTML = "4.not required, if you have changed Feedback Type to Complaint, please reset the form";
		document.getElementById("natureDesc").setAttribute('disabled');
};
};
function resetform() {
location.reload();
	
};

function opennaturechoice() {
		document.getElementById("naturech1").removeAttribute('hidden');
		document.getElementById("naturech2").removeAttribute('hidden');
};


</script>
<body <%=loading%>>
<div class="container-fluid">
<!--<div class="row">the page is_<%=page%> the vara is_<%=vara%></div>-->
<div class="col-md-12 col-sm-12" id="menusection">
<nav class="navbar navbar-inverse">
    <ul class="nav navbar-nav">
      <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Customer Help<span class="caret"></span></a>
        <ul class="dropdown-menu">
		  <li onClick="loadCustresponse()"><a href="#"><font style="color:green;">Post a reply on my open Feedback</font></a></li>
          <li onClick="loadCustSurvey()"><a href="#"><font style="color:green;">Complete Survey</font></a></li>
		  <li onClick="loadNarrative()"><a href="#"><font style="color:green;">View individual Feedback Narrative</font></a></li>
		  <li class="divider"></li>
          <li><a href="<%= Application("CONTENTLOAD") %>?PageURL=<%= Application("USERDEVELOPED") %>JLC/DSCNO/CFTv2.pdf" target="_blank"><font style="color:green;">How does this work?</font></a></li>
        </ul>
      </li>
      <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">SCB Tools/Reports<span class="caret"></span></a>
	    <ul class="dropdown-menu">
		  <li onClick="loadJLUresponse()"><a href="#"><font style="color:green;">Respond/Close Feedback</font></a></li>
		  <li><a href="<%= Application("CONTENTLOAD") %>?PageURL=<%= Application("USERDEVELOPED") %>JLC/DSCNO/Customer_FB_Pies.asp"><font style="color:green">Chart/List (Open/Closed) Feedback</font></a></li>
		  <li onClick="loadNarrative()"><a href="#"><font style="color:green;">View individual Feedback Narrative</font></a></li>
          <li><a href="<%= Application("CONTENTLOAD") %>?PageURL=<%= Application("USERDEVELOPED") %>JLC/DSCNO/Customer_FB_Survey_Results.asp|page=column"><font style="color:green;">Survey Results</font></a></li>

		  <li><a href="#" onclick="mailFn()"><font style="color:green">Request a List or Chart</font></a></li>
		  <li><a href="#" <%=erVisUC%> title="<%=authorisedUsershow%>"><font style="color:green">Edit Unit Contacts</font></a></li>
		  <li class="divider"></li>
		  <li><a href="<%= Application("CONTENTLOAD") %>?PageURL=<%= Application("USERDEVELOPED") %>JLC/DSCNO/CFTv2.pdf" target="_blank"><font style="color:green;">How does this work?</font></a></li>
		  <li><a href="<%= Application("CONTENTLOAD") %>?PageURL=<%= Application("USERDEVELOPED") %>JLC/DSCNO/SCB_CFT.pdf" target="_blank"><font style="color:green;">I don't know what I should be doing...</font></a></li>
        </ul>
      <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">DSCNO Administration<span class="caret"></span></a>
	    <ul class="dropdown-menu">
		  <li><a href="#" data-toggle='modal' data-target='#modal_execsum'><font style="color:green">Executive Summary</font></a></li>
		  <li><a href="<%= Application("CONTENTLOAD") %>?PageURL=<%= Application("USERDEVELOPED") %>JLC/DSCNO/Customer_FB_PiesPlain.asp"><font style="color:green">Chart/List (Area/Specific Nature) Feedback</font></a></li>
          <li><a href="#" <%=erVisDE%> title="<%=authorisedUsershow%>"><font style="color:green">Edit Custom Tables</font></a></li>
		  <li><a href="#" <%=erVisCT%> title="<%=authorisedUsershow%>"><font style="color:green">Delete/Edit Feedback by ID</font></a></li>
		  <li class="divider"></li>
		  <li><a href="<%= Application("CONTENTLOAD") %>?PageURL=<%= Application("USERDEVELOPED") %>JLC/DSCNO/CFTv2.pdf" target="_blank"><font style="color:green;">How does this work?</font></a></li>
		  
		  <li><a href="<%= Application("CONTENTLOAD") %>?PageURL=<%= Application("USERDEVELOPED") %>JLC/DSCNO/SCB_CFT.pdf" target="_blank"><font style="color:green;">I don't know what I should be doing...</font></a></li>
		</ul>
		<li><a href="#" onclick="mailFn1()">Report technical fault</a></li>
		<li><a href="#"><font style="color:red;"><b>Please use MS Internet Explorer to view this site (NOT MS Edge or Chrome!)</b></font></a></li>
    </ul>
</nav></div>

<div class="row" id="header"><h1>Supply Chain Branch Customer Feedback</h1>
</div>
<div class="row"><h3>Submit Your Feedback here...</h3>
<div class="col-md-12" id="formsection">
<form name="input" id="feedbackform" method="get" action="Customer_FB_Submitted.asp">

<div class="row">

    <div class="col-sm-2">
        <div class="form-group"><label for="feedbackType">1. Select feedback type</label>
            <select class="form-control" id="feedbackType" name="feedbackType" data-toggle="tooltip" title="mandatory field">
                <option value="">&nbsp;</option>
				<option>Complaint</option>
                <option>Compliment</option>
                <option>Comment</option>
            </select>   
        </div>
    </div>

    <div class="col-sm-3">
        <div class="form-group"><label for="targetID">2. Which SCB Unit does your Feedback relate to?</label>   
            <select class="form-control" id="targetID" name="targetID" data-toggle="tooltip" title="mandatory field">
				<option value="">&nbsp;</option>
                    <% SQLQuery = "SELECT targetID, targetName, targetEmail, parentTargetID, districtLink FROM Web_JLC.dbo.Customer_FB_Target_prod WHERE (parentTargetID = 1)"
                    Set SQLDataRefresh = SQL_WebTools_PRODDB.Execute(SQLQuery)
                    Do While not SQLDataRefresh.EOF
                        targetName    = SQLDataRefresh("targetName")
                        targetID      = SQLDataRefresh("targetID")
                        response.write "<option value='"&targetID&"'>"&targetName&"</option>"

                        SQLDataRefresh.MoveNext 
                    Loop 
                    %>
            </select>
        </div>
    </div>


    <div class="col-sm-3">
	<div class="form-group"><label for="nature">3. Select the area nature of your feedback</label>
	   
            <select class="form-control" id="nature" name="nature" onchange="actionPulldown()" data-toggle="tooltip" title="mandatory field">
            <option value="">&nbsp;</option>
            <%SQLQuery = "SELECT TOP (100) PERCENT NatureParent, NatureParentID FROM WEB_JLC.dbo.Customer_FB_Nature_prod GROUP BY NatureParent, NatureParentID ORDER BY NatureParent"
            Set SQLDataRefresh = SQL_WebTools_PRODDB.Execute(SQLQuery)
            Do While not SQLDataRefresh.EOF
                natureName = SQLDataRefresh("NatureParent")
				natureNameID = SQLDataRefresh("NatureParentID")
                response.write "<option value='"&natureNameID&"'>"&natureName&"</option>"
                SQLDataRefresh.MoveNext 
            Loop 
            %>
            </select>
        </div>
	</div>
	<div class="col-sm-3">
			<div class="form-group" id="natureDescDiv"><label id="naturelab" for="natureDesc">4. Select the specific nature of your feedback*</label>  
            
			<select class="form-control" id="natureDesc" name="natureDesc" data-toggle="tooltip" title="This field is for Complaints only, if it is not populating a list ensure you are using MS Internet Explorer or click on the 'Reset Form' button">
			
			<option value="">&nbsp;</option>
			</select>
		</div>
	</div>	
	
</div>

<div class="row">
    <div class="col-sm-3" id="cdeets"><label>5. Contact Details:</label><legend></legend>
        <div class="form-group"><label for="originator">Originator Name & Rank</label>
            <textarea class="form-control" style="height:34px;" name="originator" id="originator" href="#" data-toggle="tooltip" title="mandatory field"></textarea>
        </div>
        <div class="form-group"><label for="unit">Unit</label>
            <textarea class="form-control" style="height:34px;" name="unit" id="unit" href="#" data-toggle="tooltip" title="mandatory field"></textarea>
        </div>
        <div class="form-group"><label for="phone">Telephone</label>
            <textarea class="form-control" style="height:34px;" name="phone" id="phone" href="#" data-toggle="tooltip" title="mandatory field"></textarea>
        </div>    
        <div class="form-group"><label for="email">Email</label>
            <select class="form-control" name="email" id="email" data-toggle="tooltip" title="mandatory field">
			<%response.write "<option value='"&strDRNLogon&"@defence.gov.au'>"&strDRNLogon&"@defence.gov.au</option>"%></select>
        </div>
</div>
    <div class="col-sm-3" id="fdeets"><label>6. Feedback Details:</label><legend></legend>
        <div class="form-group"><label for="rodum">Unit RODUM No.</label>
            <textarea class="form-control" style="height:34px;" name="rodum" id="rodum" href="#" data-toggle="tooltip" title="optional field"></textarea>
        </div>
        <div class="form-group"><label for="equipt">Equipment Type</label>
            <textarea class="form-control" style="height:34px;" name="equipt" id="equipt" href="#" data-toggle="tooltip" title="optional field"></textarea>
        </div>
        <div class="form-group"><label for="equipan">Equipment ARN/NSN</label>
            <textarea class="form-control" style="height:34px;" name="equipan" id="equipan" href="#" data-toggle="tooltip" title="optional field"></textarea>
        </div>    
        <div class="form-group"><label for="mr">Maint Request No.</label>
            <textarea class="form-control" style="height:34px;" name="mr" id="mr" href="#" data-toggle="tooltip" title="optional field"></textarea>
        </div>
		<div class="form-group"><label for="workorder">WorkOrder No.</label>
            <textarea class="form-control" style="height:34px;" name="workorder" id="workorder" href="#" data-toggle="tooltip" title="optional field"></textarea>
        </div>    
        <div class="form-group"><label for="req">Requisition No.</label>
            <textarea class="form-control" style="height:34px;" name="req" id="req" href="#" data-toggle="tooltip" title="optional field"></textarea>
        </div>
</div>

    <div class="col-sm-5" id="ftext"><div class="form-group"><label for="freetext">7. Type your feedback here:</label><legend></legend>
            <textarea class="form-control" rows="20" name="freetext" id="freetext" data-toggle="tooltip" title="mandatory field - Do not repeat Contact or Feedback Details here.
			To reduce risk of error (if pasting from another source) remove all formatting, styling & unnecessary characters/punctuation"></textarea>
            <p align="right"><span id="remainingCharacters"></span></p> 
            <p align="right"><span id="commentErrorMsg"> </span></p>
        </div>
</div>
</div>

</form>
<input type="button" value="*Reset Form" onclick="resetform()" class="btn btn-danger pull-left" title="Click here if you are prevented from selecting a Specific Nature type for a Complaint"></button>
<button class='btn btn-success pull-right' id="formsubmitButton">Submit</button>
</div>
</div>



<div class="modal fade" id="modal_customer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
	  <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Heading</h4>
      </div>
      <div class="modal-body">
type stuff here that will help the customer understand what happens when feedback is submitted
      </div>
      <div class="modal-footer">
    <button type="button" class="btn btn-info" data-dismiss="modal" role="button" onclick="mailFn()">I would like to know more...</button>
	<button type="button" class="btn btn-success" data-dismiss="modal">Got it!</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_scb" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
	  <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Heading</h4>
      </div>
      <div class="modal-body">
type stuff here that will help SCBHQ and JLUs how to use Tools and Reports
      </div>
      <div class="modal-footer">
    <button type="button" class="btn btn-info" data-dismiss="modal" role="button" onclick="mailFn()">I would like to know more...</button>
	<button type="button" class="btn btn-success" data-dismiss="modal">Got it!</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_dscno" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
	  <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Heading</h4>
      </div>
      <div class="modal-body">
type stuff here that will help DSCNO understand how to use Admin functions
      </div>
      <div class="modal-footer">
    <button type="button" class="btn btn-info" data-dismiss="modal" role="button" onclick="mailFn()">I would like to know more...</button>
	<button type="button" class="btn btn-success" data-dismiss="modal">Got it!</button>
      </div>
    </div>
  </div>
</div>

</div>

<div class="modal fade" id="modal_ct" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="myModalLabel"></h4>
</div>
<div class="modal-body">
<div class="row"><h4>&nbsp;&nbsp;&nbsp;1. What Custom Table are you editing?</h4>
&nbsp;&nbsp;&nbsp;<button type="button" <%=erVisE%> title="Restricted to DSCNO <%=authorisedUsershow%>">Edit Cc: Email Contacts</button>
&nbsp;&nbsp;&nbsp;<button type="button" class="btn btn-primary" title="Restricted to DSCNO <%=authorisedUsershow%>" onclick="opennaturechoice()">Edit Nature Specific Categories</button>
       <label hidden for="naturech" id="naturech1">Which area?</label>
	   <select hidden ="form-control" id="naturech2" name="naturech" style="width:175px;" <%=erVisN%> title="Restricted to DSCNO <%=authorisedUsershow%>">
            <option value=""></option>
			<option value="2">Materiel Maintenance</option>
			<option value="3">Warehouse and Distribution</option>
			<option value="4">Loan Pool</option>
			<option value="5">Clothing</option>
			<option value="6">National Vehicle Recovery</option>
			<option value="7">Global Distribution</option>
			<option value="8">Operational Contracts</option>
            </select>

			</div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-info" data-dismiss="modal" role="button" onclick="mailFn()">I would like to know more...</button>
     <button type="button" class="btn btn-danger" data-dismiss="modal">I shouldn't be here!</button>
      </div>
    </div>
</div>
</div>

<div class="modal fade" id="modal_fid" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="myModalLabel"></h4>
</div>
<div class="modal-body">
<h4>1. Select the Feedback ID to delete/edit</h4>
      <div id="tableEdit" class="form-group" style="width:200px;">
      <select id="fbID" class="form-control" onchange="loadEditView(this.value)">
        <option value="">Select Feedback:</option>
<%SQLfbIDQuery = "SELECT     TOP (100) PERCENT WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackID FROM WEB_JLC.dbo.CUSTOMDATA_Feedback_prod LEFT OUTER JOIN "&_
"WEB_JLC.dbo.Customer_FB_ChangeHistory_prod ON WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackID = WEB_JLC.dbo.Customer_FB_ChangeHistory_prod.feedbackID "&_
"WHERE     (WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.dateClosed IS NULL) AND (WEB_JLC.dbo.Customer_FB_ChangeHistory_prod.deleted IS NULL) "&_
"GROUP BY WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackID ORDER BY WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackID DESC"
        Set SQLfbIDData = SQL_WebTools_PRODDB.Execute(SQLfbIDQuery)
        Do while not SQLfbIDData.EOF %>
        <option value="<%= SQLfbIDData("feedbackID") %>" <%If selected = SQLfbIDData("feedbackID") Then %>Selected<%End If %>><%= SQLfbIDData("feedbackID") %></option>
        <%SQLfbIDData.MoveNext
        Loop %>
    </select>

</div><p style="font-size: 10px;">(Only open Feedback will be available to edit)</p>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-info" data-dismiss="modal" role="button" onclick="mailFn()">I would like to know more...</button>
     <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="resetform()">I shouldn't be here!</button>
      </div>
    </div>
</div>
</div>

<div class="modal fade" id="modal_execsum" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="myModalLabel"></h4>
</div>
<div class="modal-body">
      <div id="tableEdit" class="form-group" style="width:200px;">
      <select id="exsmth" class="form-control" onchange="execSummary(this.value)">
        <option value="">Select Month</option>
<%SQLexsmthQuery = "SELECT     TOP (100) PERCENT CAST({ fn MONTHNAME(feedbackDateTime) } AS varchar) + ' ' + CAST(YEAR(feedbackDateTime) AS varchar) AS CreatedMonth, COUNT(feedbackDateTime) AS CountFB, "&_ 
"YEAR(feedbackDateTime) AS Yr, MONTH(feedbackDateTime) AS Mth FROM WEB_JLC.dbo.CUSTOMDATA_Feedback_prod GROUP BY CAST({ fn MONTHNAME(feedbackDateTime) } AS varchar) + ' ' + CAST(YEAR(feedbackDateTime) AS varchar), YEAR(feedbackDateTime), MONTH(feedbackDateTime) "&_
"ORDER BY YEAR(feedbackDateTime), MONTH(feedbackDateTime)"
        Set SQLexsmthData = SQL_WebTools_PRODDB.Execute(SQLexsmthQuery)
        Do while not SQLexsmthData.EOF %>
        <option value="<%= SQLexsmthData("Mth")&","&SQLexsmthData("Yr") %>" <%If selected = SQLexsmthData("CreatedMonth") Then %>Selected<%End If %>><%= SQLexsmthData("CreatedMonth") %></option>
        <%SQLexsmthData.MoveNext
        Loop %>
    </select>

</div><p style="font-size: 10px;">(Only Months where Feedback has been submitted will show for selection)</p>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-info" data-dismiss="modal" role="button" onclick="mailFn()">I would like to know more...</button>
<button type="button" class="btn btn-success" data-dismiss="modal" role="button" onclick="execSummary()">Take me straight to Year To Date Summary</button>
     <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="resetform()">I shouldn't be here!</button>
      </div>
    </div>
</div>
</div>

<script type="text/javascript" language="javascript">
$(document).ready(function() {
    var characterLimit = 1700;  
    $('#remainingCharacters').html(characterLimit + ' characters remaining');

    $('#freetext').keyup(function() {  
        var charactersUsed = $('#freetext').val().length;
        var charactersRemaining = characterLimit - charactersUsed;

        $('#remainingCharacters').html(charactersRemaining + ' characters remaining');  
    });  
}); 
</script> 
</body>
</html>
<!-- #include virtual = "/includes/WebTools_AccessLog.asp" -->

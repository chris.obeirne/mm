<!-- #include virtual = "/includes/WebTools_Core.asp" -->
<html>
<head>
  <meta name="Shane Kline" content="CFTv2">
<!-- Bootstrap core CSS -->
    <link href="<%= Application("TP-BOOTSTRAP-CSS") %>" rel="stylesheet">
<!-- Jquery -->	
	<link href="<%= Application("TP-JQUERYUICSS") %>" rel="stylesheet">
<!-- Jquery -->
	<script src='<%= Application("TP-JQUERY") %>'></script>
	<script src='<%= Application("TP-JQUERYUI") %>'></script>
<!-- Bootstrap -->  	
	<script src='<%= Application("TP-BOOTSTRAP") %>'></script>
	<script src='<%= Application("TP-BOOTSTRAP-TOGGLE") %>'></script>
	<script src='<%= Application("THIRDPARTY_CORE") %>bootstrap-multiselect/js/bootstrap-multiselect.js'></script>
	<link href="<%= Application("THIRDPARTY_CORE") %>bootstrap-multiselect/css/bootstrap-multiselect.css" rel="stylesheet">
<!-- Font Awesome -->
    <link href="<%= Application("TP-FONTAWESOME") %>" rel="stylesheet">
<!-- Modernizr -->
    <script src='<%= Application("TP-MODERNIZR") %>'></script>
</head>

<%
emailvalue     = request.querystring("scbhqemail")
shortdesc      = request.querystring("shortdesc")
longdesc       = request.querystring("longdesc")
stat           = request.querystring("stat")
pmk            = request.querystring("pmk")

pmk = replace(pmk," ","")

If shortdesc = "" Then
valuestring = replace(emailvalue," ","")
note = "Emails"


Dim pmkArrayem, emailvalueArray
Dim item1, maxcountem

pmkArrayem=split(pmk,",")
emailvalueArray=split(valuestring,",")

If IsArray(pmkArrayem) Then
	maxcountem = UBound(pmkArrayem)
	For item1 = 0 to maxcountem
		If emailvalueArray(item1) <> "" Then
		SQLstatement = 					"UPDATE Web_JLC.dbo.Customer_FB_Nature_prod"
		SQLstatement = SQLstatement & 	" SET  SCBHQ_Email='"&emailvalueArray(item1)&"'"
		SQLstatement = SQLstatement & 	" WHERE (NatureID = N'"&pmkArrayem(item1)&"')"
		SQL_WebTools_PRODDB.Execute(SQLstatement)

		Else
		SQLstatement = 					"UPDATE Web_JLC.dbo.Customer_FB_Nature_prod"
		SQLstatement = SQLstatement & 	" SET  SCBHQ_Email=NULL"
		SQLstatement = SQLstatement & 	" WHERE (NatureID = N'"&pmkArrayem(item1)&"')"
		SQL_WebTools_PRODDB.Execute(SQLstatement)

		End If
	Next
Else
errornote = "Yikes!!!! that was not meant to happen... please contact technical support"
End If
'-------------------
Else
valuestring1 = replace(shortdesc,", ",",")
valuestring2 = replace(longdesc,", ",",")
valuestring3 = replace(stat," ","")
note = "Nature Descriptions/Status"
valuestring3 = replace(valuestring3,"Active","1") 
valuestring3 = replace(valuestring3,"Suspended","0") 

Dim pmkArrayni, shortdesc, longdesc, stat
Dim item2, maxcountni

pmkArrayni=split(pmk,",")
shortdesc=split(valuestring1,",")
longdesc=split(valuestring2,",")
stat=split(valuestring3,",")

If IsArray(pmkArrayni) Then
	maxcountni = UBound(pmkArrayni)
	For item2 = 0 to maxcountni
		SQLstatement = 					"UPDATE Web_JLC.dbo.Customer_FB_Nature_prod"
		SQLstatement = SQLstatement & 	" SET  NatureShortDesc='"&shortdesc(item2)&"', NatureLongDesc='"&longdesc(item2)&"', NatureStatus='"&stat(item2)&"'"
		SQLstatement = SQLstatement & 	" WHERE (NatureID = N'"&pmkArrayni(item2)&"')"
		SQL_WebTools_PRODDB.Execute(SQLstatement)
		'response.write(SQLstatement&"<br>")
	Next
Else
errornote = "Yikes!!!! that was not meant to happen... please contact technical support"
End If
'----------------------
End If

If IsArray(pmkArrayem) or  IsArray(pmkArrayni) Then		
updateNote = note&" uploaded successfully"
Else
updateNote = errornote
End If
%>
<body>
<br><br><br>
<div class="container">
<h1 align="center" id="msg"><small><%=response.write(updateNote)%></small></h1><br>
<h1 align="center"><small><br><i class="fa fa-thumbs-up fa-4x" aria-hidden="true"></i><br><br>
This window will automatically close in 39 seconds <br>
</small></h1>
</div>
<script>
setTimeout(function() {

window.parent.location.reload();
}, 3000);
</script>
</body>
</html>
<!-- #include virtual = "/includes/WebTools_AccessLog.asp" -->
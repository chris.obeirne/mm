<!-- #include virtual = "/includes/WebTools_Core.asp" -->
<html>
<head>
  <meta name="Shane Kline" content="CFTv2">
<!-- Bootstrap core CSS -->
    <link href="<%= Application("TP-BOOTSTRAP-CSS") %>" rel="stylesheet">
<!-- Jquery -->	
	<link href="<%= Application("TP-JQUERYUICSS") %>" rel="stylesheet">
<!-- Jquery -->
	<script src='<%= Application("TP-JQUERY") %>'></script>
	<script src='<%= Application("TP-JQUERYUI") %>'></script>
<!-- Bootstrap -->  	
	<script src='<%= Application("TP-BOOTSTRAP") %>'></script>
	<script src='<%= Application("TP-BOOTSTRAP-TOGGLE") %>'></script>
	<script src='<%= Application("THIRDPARTY_CORE") %>bootstrap-multiselect/js/bootstrap-multiselect.js'></script>
	<link href="<%= Application("THIRDPARTY_CORE") %>bootstrap-multiselect/css/bootstrap-multiselect.css" rel="stylesheet">
<!-- Font Awesome -->
	    <link href="<%= Application("TP-FONTAWESOME") %>" rel="stylesheet">
<!-- Modernizr -->
    <script src='<%= Application("TP-MODERNIZR") %>'></script>
</head>

<style>
tr, td {
	padding-bottom: 0px;
	padding-top: 0px;
	font-size: 16px;
}

#shortdesc {
	font-size:16px;
	padding-bottom: 0px;
	padding-top: 0px;
	width: 250px;
	height: 25px;
	}

#longdesc {
	font-size:16px;
	padding-bottom: 0px;
	padding-top: 0px;
	width: 500px;
	height: 25px;
	}

#stat {
	font-size:16px;
	padding-bottom: 0px;
	padding-top: 0px;
	width: 110px;
	height: 25px;

	}
	
#parent {
	width: 225px;
}

#formchange {
	background-color: #e6e2ac;
}
</style>
<%
feedbackID = request.querystring("vfbID")
authuser = request.querystring("authuser")

If authuser = "1" Then
intro = "<font style='color:red;'><strong>You are in Administration Edit Mode.<br>You may choose to Delete....<br>OR,<br>You may change 'Feedback Type', 'Target SCB Unit', "&_
"'Area Nature' and 'Specific Nature', (To change only the Specific Nature, first select a different Area Nature, then return the selection to the original, this enables changes to the Specific Nature to occur)</strong></font>"
Else 
response.Redirect "http://WEBTOOLS.defence.gov.au/prod/Core/Content/contentLoading.asp?PageURL=http://WEBTOOLS.defence.gov.au/prod/UserDeveloped/JLC/DSCNO/responseredirect.asp"
End If



SQLEditQuery = "SELECT     TOP (100) PERCENT WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackID, WEB_JLC.dbo.CUSTOMer_fb_Target_prod.targetID, "&_
"CAST(WEB_JLC.dbo.CUSTOMer_fb_Target_prod.targetName AS nvarchar(100)) AS targetName, WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackType, "&_
"WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.natureParentID, WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.creationUserID, "&_
"WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackDateTime, WEB_JLC.dbo.Customer_FB_Nature_prod.NatureParent, WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.natureID, "&_
"WEB_JLC.dbo.Customer_FB_Nature_prod.NatureShortDesc, CAST(WEB_JLC.dbo.CUSTOMer_fb_Target_prod.targetEmail AS nvarchar(100)) AS targetEmail, "&_
"WEB_JLC.dbo.Customer_FB_Nature_prod.SCBHQ_Email, WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.cdOriginator "&_
"FROM WEB_JLC.dbo.CUSTOMDATA_Feedback_prod LEFT OUTER JOIN "&_
"WEB_JLC.dbo.CUSTOMer_fb_Target_prod ON WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.targetID = WEB_JLC.dbo.CUSTOMer_fb_Target_prod.targetID LEFT OUTER JOIN "&_
"WEB_JLC.dbo.Customer_FB_Nature_prod ON WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.natureID = WEB_JLC.dbo.Customer_FB_Nature_prod.NatureID "&_
"WHERE (WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackID = "&feedbackid&")"

'response.write("The qry - " & SQLEditQuery)
Set SQLEditData = SQL_WebTools_PRODDB.Execute(SQLEditQuery)
 Do while not SQLEditData.EOF 
 fbID		 = SQLEditData("feedbackID") 'no change
 tgID        = SQLEditData("targetID") 'hidden (yes change)
 tgName		 = SQLEditData("targetName") 'yes change
 fbTY   	 = SQLEditData("feedbackType") 'yes change
 npID        = SQLEditData("NatureParentID") 'hidden (yes change)
 npName      = SQLEditData("NatureParent") 'yes change
 nsID        = SQLEditData("natureID") 'hidden (yes change)
 nsName      = SQLEditData("NatureShortDesc") 'yes change

SQLEditData.MoveNext
Loop



%>
<script>

$(document).ready(function(){
$("#applyfbChanges").click(function(){
	$( "#feedbackID_cha" ).submit();
});
});

$(document).keypress(
	function(event){
		if (event.which == '13') {
			event.preventDefault();
			}
});

function actionPulldown(arenature) {  
      //alert("im in the pulldown function");
	  var fbty = document.getElementById("currfbtype").innerHTML;
			//alert('the thing is_'+fbty+'_');
			if (fbty != "currently: Complaint") {

						$("#optradio3").val(0);
						$("#applyfbChanges").removeClass("btn-danger");
						$("#applyfbChanges").addClass("btn-success");
						$("#applyfbChanges").text("Apply");
						//alert("select seed is_"+selectSeed);

			} else {

            var selectSeed = document.getElementById("nid");
            //alert("select seed is_"+selectSeed);
            
				var optSeed = document.createElement("option");
				optSeed.innerHTML = "Select one:";
				optSeed.value = "";
				
				selectSeed.appendChild(optSeed);
                
            $.ajax({
                           type: "Get",
                           cache: false, 
                           url: "Customer_FB_AJAX.asp?varenat=" + arenature,
                           datatype: "text/html",
                           success: function(response) {

                           var areaspec = response;
                           //alert("the returned value is_"+reasonAJ);
                           var select = document.getElementById("#nid");

                           var option = areaspec;
                           $("#nid").append(option);
                           }
                     })
              $('#nid').empty();   
						$("#optradio3").val(0);
						$("#applyfbChanges").removeClass("btn-danger");
						$("#applyfbChanges").addClass("btn-success");
						$("#applyfbChanges").text("Apply");			  
			};
	
	};

function actionremovedelete() {
		$("#optradio3").val(0);
		$("#applyfbChanges").removeClass("btn-danger");
		$("#applyfbChanges").addClass("btn-success");
		$("#applyfbChanges").text("Apply");
};

function disableSpecnat() {
	var fbtyz = document.getElementById("currfbtype").innerHTML;
			if (fbtyz == "currently: Complaint") {
				document.getElementById("nid").setAttribute('disabled');
				document.getElementById("nid").innerHTML='';
			} {
				document.getElementById("nid").setAttribute('enabled');
			}

};


</script>
<body>
<div class="container-fluid">
<div class="row"><br><br></div>
	<div class="row">
		<div class="col-sm-12"><% response.write intro %>
		<a href="javascript:window.parent.location.reload(true);" class="btn btn-primary btn-sm pull-right">Back</a></div><br>		
	</div>
		
	<div class="row" id="formchange">
		<div class="col-sm-12">
			<form name="input" id="feedbackID_cha" method="get" action="Customer_FB_SavebyID.asp">
				<div class="col-sm-2">
					<div class="form-group">
						<label for="feedbackid">Feedback ID</label>
						<select id="feedbackid" class="form-control" name ="feedbackid">
						<%response.write "<option value='"&fbID&"'>"&fbID&"</option>"%>
						</select>   
					</div>
				</div>
				
				<div class="col-sm-3">
					<div class="form-group">
						<label for="fbtype">Feedback Type</label>
						<select id="fbtype" class="form-control" name="fbtype" onchange="actionremovedelete(); disableSpecnat()">
						<option value="<%=fbTY%>" id="currfbtype">currently: <%=fbTY%></option>
							<option value="Complaint">Complaint</option>
							<option value="Comment">Comment</option>
							<option value="Compliment">Compliment</option>
						</select>
					</div>
				</div>
				
				<div class="col-sm-3">
					<div class="form-group">
						<label for="targetunit">Target SCB Unit</label>
						<select id="targetunit" class="form-control" name="targetunit" onchange="actionremovedelete()">
						<option value="<%=tgID%>">currently: <%=tgName%></option>
							 <% SQLQuery1 = "SELECT targetID, targetName FROM WEB_JLC.dbo.Customer_FB_Target_prod WHERE "&_
												"(parentTargetID = 1) AND (targetID <> "&tgID&")"
							Set SQLData1 = SQL_WebTools_PRODDB.Execute(SQLQuery1)
							Do While not SQLData1.EOF
								targetID    = SQLData1("targetID")
								targetName  = SQLData1("targetName")
								response.write "<option value='"&targetID&"'>"&targetName&"</option>"
								SQLData1.MoveNext 
							Loop 
							%>
						</select>
					</div>
				</div>
				
				<div class="col-sm-4">
					<div class="form-group">
						<label for="np_ID">Area Nature</label>
						<select class="form-control" id="np_ID" name="np_ID" onchange="actionPulldown(this.value)">
						<option value="<%=npID%>">currently: <%=npName%></option>
							 <% SQLQuery2 = "SELECT NatureParentID, NatureParent FROM WEB_JLC.dbo.Customer_FB_Nature_prod GROUP BY NatureParent, "&_
											"NatureParentID HAVING (NatureParentID <> "&npID&")"
							Set SQLData2 = SQL_WebTools_PRODDB.Execute(SQLQuery2)
							Do While not SQLData2.EOF
								NatureParentID    = SQLData2("NatureParentID")
								NatureParent      = SQLData2("NatureParent")
								response.write "<option value='"&NatureParentID&"'>"&NatureParent&"</option>"
								SQLData2.MoveNext 
							Loop 
							%>
						</select>
					</div>
				</div>
           </div>
	</div>
<div class="row" id="formchange">
		<div class="col-sm-4">
			<div class="form-group ">
				<label for="nid">Specific Nature</label>
				<select class="form-control" id="nid" name="nid" onchange="actionremovedelete()">
				<option value="<%=nsID%>">currently: <%=nsName%></option>
				</select>
			</div> 
		</div>

        <div class="col-sm-2">
			<div class="form-group">
				<label for="userID">Change UserID</label>
				<select class="form-control" id="userID" name="userID">
				<%response.write "<option value='"&strDRNLogon&"'>"&strDRNLogon&"</option>"%>
				</select>
			</div>
		</div>
		
		<div class="col-sm-3">
			<div class="form-group">
				<label for="cDate">Change Date</label>
				<select class="form-control" id="cDate" name="cDate">
				<%response.write "<option value='"&Now()&"'>"&Now()&"</option>"%>
				</select>
			</div>
		</div>
		
           <div class="col-sm-1">
         <div class="form-group">
            <input type="radio" id="changeIndicator" name="changeIndicator" value="1" class="hidden" checked>
         </div>
		</div>

		<div class="col-sm-2"><br>
			<div class="form-group">
				<input type="radio" id="optradio3" name="fbDel" value="1" checked hidden>
			</div>
		</div>
</div>
</form>
	<div class="row">
	<div class="col-sm-12"><button type="button" class="btn btn-danger pull-right" id="applyfbChanges" >Delete</button></div>
</div>
<br><br>
<div class="row">
	<div class="col-sm-1"></div>
			<label for="fbtext">The Customer wrote (editing not permitted):</label>
				<div class="col-sm-11" id="fbtext">
				 <% SQLQuery3 = "SELECT     TOP (100) PERCENT feedbackID, feedbackText FROM WEB_JLC.dbo.CUSTOMDATA_Feedback_prod "&_
								"WHERE     (feedbackID = "&fbID&")"
				Set SQLData3 = SQL_WebTools_PRODDB.Execute(SQLQuery3)
				Do While not SQLData3.EOF
					feedbackText    = SQLData3("feedbackText")
					SQLData3.MoveNext 
				Loop 
				response.write("<textarea rows='12' cols='150' disabled>"&feedbackText&"</textarea>")
				%>

		</div>
	</div>

</div>
	
</div>

<script type="text/javascript" language="javascript">
$(document).ready(function() {
     $('[data-toggle="tooltip"]').tooltip();

});

</script> 
</body>
</html>
<!-- #include virtual = "/includes/WebTools_AccessLog.asp" -->
<!-- #include virtual = "/includes/WebTools_Core.asp" -->
<html>
<head>
  <meta name="Shane Kline" content="CFTv2">
<!-- Bootstrap -->  
    <link href="<%= Application("TP-BOOTSTRAP-CSS") %>" rel="stylesheet">
    <link href="<%= Application("TP-BOOTSTRAP-TOGGLE-CSS") %>" rel="stylesheet">
<!-- Jquery -->
	<script src='<%= Application("TP-JQUERY") %>'></script>
	<script src='<%= Application("TP-JQUERYUI") %>'></script>
<!-- Bootstrap -->  	
	<script src='<%= Application("TP-BOOTSTRAP") %>'></script>
	<script src='<%= Application("TP-BOOTSTRAP-TOGGLE") %>'></script>
<!-- Font Awesome -->
    <link href="<%= Application("TP-FONTAWESOME") %>" rel="stylesheet">
<!-- Modernizr -->
    <script src='<%= Application("TP-MODERNIZR") %>'></script>
</head>
<style>
tr, td {
	padding-bottom: 0px;
	padding-top: 0px;
	font-size: 16px;
}

#shortdesc {
	font-size:16px;
	padding-bottom: 0px;
	padding-top: 0px;
	width: 250px;
	height: 25px;
	}

#longdesc {
	font-size:16px;
	padding-bottom: 0px;
	padding-top: 0px;
	width: 500px;
	height: 25px;
	}

#stat {
	font-size:16px;
	padding-bottom: 0px;
	padding-top: 0px;
	width: 110px;
	height: 25px;

	}
	
#parent {
	width: 225px;
}

#formupdate {
	background-color: #e6e2ac;
}
</style>
<%
feedbackID = request.querystring("vfbID")
targetID = request.querystring("vUnit")


'authuser = request.querystring("authuser")
authuser = "1"
'response.write(feedbackID)
If authuser = "1" Then
intro = "<font style='color:red;'><strong>You are in JLU Response/Close Mode.<br>You may add a reply and/or close Feedback "&_
"from the selection list below</strong></font>"
Else 
response.Redirect "http://WEBTOOLS.defence.gov.au/prod/Core/Content/contentLoading.asp?PageURL=http://WEBTOOLS.defence.gov.au/prod/UserDeveloped/JLC/DSCNO/responseredirect.asp"
End If


If feedbackID = "" Then
showcurrent = "Select One:"
fbID = feedbackID

Else
showcurrent = "currently: "&feedbackID
End If





%>
<script>

$(document).ready(function(){
$("#updatefb").click(function(){
	$( "#feedbackID_update" ).append("<input type='hidden' name='vclose' value='0' />");
	$( "#feedbackID_update" ).submit();
});

$("#updateclosefb").click(function(){
	$( "#feedbackID_update" ).append("<input type='hidden' name='vclose' value='1' />");
	$( "#feedbackID_update" ).submit();
});
});

$(document).keypress(
	function(event){
		if (event.which == '13') {
			event.preventDefault();
			}
});

	var fb_id = [<% response.write feedbackID%>];
	//alert('the id is_'+fb_id);//
	//if (fb_id != "") {
	//showfb
	//};
	
	function pulldownfbID() {  

			var jlu = document.getElementById("jlu").value;

            var selectSeed = document.getElementById("fbID");
                
            $.ajax({
						   type: "Get",
                           cache: false, 
                           url: "Customer_FB_AJAX.asp?vtargetID=" + jlu,
                           datatype: "text/html",
                           success: function(response) {

						   var select = document.getElementById("#fbID");
						   
                           var option = response;
						   
						   var optSeed = document.createElement("option");
						   optSeed.innerHTML = "Select one:";
						   optSeed.value = "";
						   selectSeed.appendChild(optSeed);
						   
                           $("#fbID").append(option);
                           }
                     })
					 $('#fbID').empty();
	};
	
	function showfb() {  
	var fbid = document.getElementById("fbID").value;

            var selectSeed = document.getElementById("fbID");

				document.getElementById("labelcusttext").removeAttribute('hidden');
				
				var optSeed = document.createElement("textarea");
				optSeed.value = "";
				
				selectSeed.appendChild(optSeed);
                
            $.ajax({
						   type: "Get",
                           cache: false, 
                           url: "Customer_FB_AJAX.asp?vfbid=" + fbid,
                           datatype: "text/html",
                           success: function(response) {

						   var select = document.getElementById("#fbtext1");
						   
                           var option = response;
                           $("#fbtext1").append(option);
                           }
                     })
					 $('#fbtext1').empty();
	};
</script>
<body>
<div class="container-fluid">
<div class="row"><br></div>
	<div class="row">
		<div class="col-sm-12"><% response.write intro %>
		<a href="javascript:window.parent.location.reload(true);" class="btn btn-primary btn-sm pull-right">Back</a></div><br>		
	</div>
<form name="input" id="feedbackID_update" method="get" action="Customer_FB_ReplyUploadbyID.asp">		
	<div class="row" id="formupdate">
				<div class="col-sm-3">
					<div class="form-group">
						<label for="jlu">1. Target Feedback Unit</label>
						<select id="jlu" class="form-control" name ="jlu" onchange="pulldownfbID()">
						<option value="">Select One:</option>
						<option value="1">SCBHQ</option>
						<option value="8">JLUV</option>
						<option value="4">JLUE</option>
						<option value="7">JLUSQ</option>
						<option value="6">JLUNQ</option>
						<option value="2">JLUN</option>
						<option value="5">JLUW</option>
						<option value="3">JLUS</option>
						</select>   
					</div>
				</div>
			<div class="col-sm-3">
				<div class="form-group">
					<label for="fbID">2. Feedback ID</label>
					<select class="form-control" id="fbID" name="fbID" onchange="showfb()">
					<option value="">Select One:</option>
					</select>
				</div> 
			</div>
	</div>
<div class="row" id="formupdate">
        <div class="col-sm-3">
			<div class="form-group">
				<label for="userID">Response UserID</label>
				<select class="form-control" id="userID" name="userID">
				<%response.write "<option value='"&strDRNLogon&"'>"&strDRNLogon&"</option>"%>
				</select>
			</div>
		</div>
		
		<div class="col-sm-3">
			<div class="form-group">
				<label for="cDate">Response Date</label>
				<select class="form-control" id="cDate" name="cDate">
				<%response.write "<option value='"&Now()&"'>"&Now()&"</option>"%>
				</select>
			</div>
		</div>

</div>
	<div class="row" id="formupdate">
	<div class="col-sm-1"></div>
			<label for="fbtext">3. You may write your response here:</label>
				<div class="col-sm-11" id="fbtext">
					<div class="form-group">
						<textarea rows="10" cols="120" id="fbtexta" name="fbtexta"></textarea>
				</div>
		</div>
	</div>
	
</form>
	<div class="row">
	<div class="col-sm-12"><button type="button" class="btn btn-warning pull-right" id="updateclosefb" >Submit Final Response and Close Feedback</button>
	<button type="button" class="btn btn-success pull-left" id="updatefb" >Update and leave Feedback Open</button></div>
</div>
	
<div class="row">
	<div class="col-sm-1"></div>
			<label for="fbtext1" hidden id="labelcusttext">The Narrative so far is: (editing not permitted):</label>
				<div class="col-sm-11" id="fbtext1">
		</div>
	<!--<div class="col-sm-1"></div>
	<div class="row" id="notes1"><table style="width:50%"><% response.write details %></table><br><br><br>
	</div>
	<div class="col-sm-1"></div>
	<div class="row" id="notes1"><table style="width:50%"><% response.write details1 %></table>
	</div>-->
	</div>
</div>

<script type="text/javascript" language="javascript">
$(document).ready(function() {
     $('[data-toggle="tooltip"]').tooltip();
});
</script> 
</body>
</html>
<!-- #include virtual = "/includes/WebTools_AccessLog.asp" -->
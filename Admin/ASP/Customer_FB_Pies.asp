<!-- #include virtual = "/includes/WebTools_Core.asp" -->
<html>
<head>
  <meta name="Shane Kline" content="CFTv2">
<!-- Bootstrap -->  
    <link href="<%= Application("TP-BOOTSTRAP-CSS") %>" rel="stylesheet">
    <link href="<%= Application("TP-BOOTSTRAP-TOGGLE-CSS") %>" rel="stylesheet">
<!-- Jquery -->
	<script src='<%= Application("TP-JQUERY") %>'></script>
	<script src='<%= Application("TP-JQUERYUI") %>'></script>
<!-- Bootstrap -->  	
	<script src='<%= Application("TP-BOOTSTRAP") %>'></script>
	<script src='<%= Application("TP-BOOTSTRAP-TOGGLE") %>'></script>
<!-- Font Awesome -->
	 <link href="<%= Application("TP-FONTAWESOME") %>" rel="stylesheet">
<!-- Modernizr -->
    <script src='<%= Application("TP-MODERNIZR") %>'></script>
<!-- HighCharts -->
	<script src='<%=Application("TP-HIGHCHARTS-SOLID-GAUGE") %>'></script>
	<script src='<%=Application("TP-HIGHCHARTS") %>'></script>
	<script src='<%=Application("TP-HIGHCHARTS-MORE") %>'></script>
	<script src='<%=Application("TP-HIGHCHARTS-EXPORTING") %>'></script>
    <script src='<%= Application("TP-HIGHCHARTS-EXPORT-CLIENT-JS") %>'></script>
	


<style>
.highcharts-title {
    fill: #434348;
    font-weight: bold;
    font-size: 20px;
}

.checkbox-inline {
border: 1px solid #6495ED;
border-radius: 20px;
padding: .2cm;
}

.form-control {
border: 1px solid #6495ED;
border-radius: 20px;
}

#Pie {
    z-index: 1
}

#modal_pieSQL {
   position: absolute;
   top: 50px;
   right: 0;
   bottom: 0;
   left: 0;
}
</style>
</head>    
<%
status          = request.querystring("status")
fbNat           = request.querystring("vNat")
fbType          = request.querystring("vType")
fbYr            = request.querystring("vYr")



If left(fbNat, 1) = "2" Then
fbNatshow = "Materiel Maintenance"
Else If left(fbNat, 1) = "3" Then
fbNatshow = "Warehouse Distribution"
Else If left(fbNat, 1) = "4" Then
fbNatshow = "Loan Pool"
Else If left(fbNat, 1) = "5" Then
fbNatshow = "Clothing"
Else If left(fbNat, 1) = "6" Then
fbNatshow = "National Vehicle Recovery"
Else If left(fbNat, 1) = "7" Then
fbNatshow = "Global Distribution"
Else If left(fbNat, 1) = "8" Then
fbNatshow = "Operational Contracts"
Else
fbNatshow = "All Areas"
End If
End If
End If
End If
End If
End If
End If


If len(fbYr) = 4 then 


fbYearshow = fbYr

If fbNat <> "" and fbType = "" Then
fbNatTypeshow = right(fbNat, len(fbNat)-1)&" All Types"
fbTypeshow = "All Areas"
fbNat = left(fbNat, 1)
strWhereadd = " AND (WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.natureParentID = "&fbNat&") AND (YEAR(WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackDateTime) = "&fbYr&") "
strWhereaddsites = " WHERE (WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.natureParentID = "&fbNat&") AND (YEAR(WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackDateTime) = "&fbYr&") "

Else If fbNat <> "" and fbType <> "" Then
fbNat = left(fbNat, 1)
strWhereadd = " AND (WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.natureParentID = "&fbNat&") AND (WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackType = N'"&fbType&"') AND (YEAR(WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackDateTime) = "&fbYr&") "
strWhereaddsites = " WHERE (WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.natureParentID = "&fbNat&") AND (WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackType = N'"&fbType&"') AND (YEAR(WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackDateTime) = "&fbYr&") "
fbNatTypeshow = right(fbNat, len(fbNat)-1)&" - "&fbType
fbTypeshow = fbType

Else If fbNat = "" and fbType <> "" Then
strWhereadd = " AND (WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackType = N'"&fbType&"') AND (YEAR(WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackDateTime) = "&fbYr&") "
strWhereaddsites = " WHERE (WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackType = N'"&fbType&"') AND (YEAR(WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackDateTime) = "&fbYr&") "
fbTypeshow = fbType
fbNatTypeshow = "All Areas "&fbType

Else
strWhereadd = " AND (YEAR(WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackDateTime) = "&fbYr&") "
strWhereaddsites = " AND (YEAR(WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackDateTime) = "&fbYr&") "
fbNatTypeshow = "All Areas All Types"
fbTypeshow = "All Types"
End If
End If
End If

ELSE


fbYearshow = "All Years" 

If fbNat <> "" and fbType = "" Then
fbNatTypeshow = right(fbNat, len(fbNat)-1)&" All Types"
fbTypeshow = "All Areas"
fbNat = left(fbNat, 1)
strWhereadd = " AND (WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.natureParentID = "&fbNat&") "
strWhereaddsites = " WHERE (WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.natureParentID = "&fbNat&") "

Else If fbNat <> "" and fbType <> "" Then
fbNat = left(fbNat, 1)
strWhereadd = " AND (WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.natureParentID = "&fbNat&") AND (WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackType = N'"&fbType&"') "
strWhereaddsites = " WHERE (WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.natureParentID = "&fbNat&") AND (WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackType = N'"&fbType&"') "
fbNatTypeshow = right(fbNat, len(fbNat)-1)&" - "&fbType
fbTypeshow = fbType

Else If fbNat = "" and fbType <> "" Then
strWhereadd = " AND (WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackType = N'"&fbType&"') "
strWhereaddsites = " WHERE (WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackType = N'"&fbType&"') "
fbTypeshow = fbType
fbNatTypeshow = "All Areas "&fbType

Else
strWhereadd = ""
strWhereaddsites = ""
fbNatTypeshow = "All Areas All Types"
fbTypeshow = "All Types"
End If
End If
End If




end if


SQLNatQuery = "SELECT     COUNT(WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackID) AS Count_FB, CASE WHEN dateClosed IS NULL THEN 'Open' ELSE 'Closed' END AS Status_FB, "&_
"CASE WHEN dateClosed IS NULL THEN '#807160' ELSE '#5ccc33' END AS Status_FB_colour "&_
"FROM WEB_JLC.dbo.CUSTOMDATA_Feedback_prod LEFT OUTER JOIN "&_
"WEB_JLC.dbo.Customer_FB_ChangeHistory_prod ON WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackID = WEB_JLC.dbo.Customer_FB_ChangeHistory_prod.feedbackID LEFT OUTER JOIN "&_
"WEB_JLC.dbo.Customer_FB_Target_prod ON WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.targetID = WEB_JLC.dbo.Customer_FB_Target_prod.targetID "&_
"WHERE (WEB_JLC.dbo.Customer_FB_ChangeHistory_prod.deleted IS NULL) "&strWhereadd &_
"GROUP BY CASE WHEN dateClosed IS NULL THEN 'Open' ELSE 'Closed' END, CASE WHEN dateClosed IS NULL THEN '#807160' ELSE '#5ccc33' END"
Set SQLNatData = SQL_WebTools_PRODDB.Execute(SQLNatQuery)
Do while not SQLNatData.EOF	
sitecountNat = SQLNatData("Count_FB")
fbStatusNat = SQLNatData("Status_FB")
fbColourNat = SQLNatData("Status_FB_colour")
strsitewedgeNat = strsitewedgeNat&"{name: '"&fbStatusNat&"', y:"&sitecountNat&", color: '"&fbColourNat&"'},"
SQLNatData.MoveNext
Loop
strsiteDataNat = left(strsitewedgeNat,len(strsitewedgeNat)-1)
strsiteDataNat = "[{name: 'All SCB Entities', data: ["&strsiteDataNat&"]"

'response.write(SQLNatQuery)
Dim scb(7)
scb(0)="SCBHQ"
scb(1)="JLUN"
scb(2)="JLUS"
scb(3)="JLUE"
scb(4)="JLUW"
scb(5)="JLUNQ" 
scb(6)="JLUSQ" 
scb(7)="JLUV" 

For Each jlu In scb



SQLSiteQuery = "SELECT     COUNT(WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackID) AS Count_FB, WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.targetID, "&_
"WEB_JLC.dbo.Customer_FB_Target_prod.targetName, CASE WHEN dateClosed IS NULL THEN 'Open' ELSE 'Closed' END AS Status_FB, "&_
"CASE WHEN dateClosed IS NULL THEN '#807160' ELSE '#5ccc33' END AS Status_FB_colour, WEB_JLC.dbo.Customer_FB_ChangeHistory_prod.deleted "&_
"FROM WEB_JLC.dbo.CUSTOMDATA_Feedback_prod LEFT OUTER JOIN WEB_JLC.dbo.Customer_FB_ChangeHistory_prod ON "&_
"WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.feedbackID = WEB_JLC.dbo.Customer_FB_ChangeHistory_prod.feedbackID LEFT OUTER JOIN "&_
"WEB_JLC.dbo.Customer_FB_Target_prod ON WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.targetID = WEB_JLC.dbo.Customer_FB_Target_prod.targetID "&_
strWhereaddsites &_
"GROUP BY WEB_JLC.dbo.Customer_FB_Target_prod.targetName, WEB_JLC.dbo.CUSTOMDATA_Feedback_prod.targetID, CASE WHEN dateClosed IS NULL "&_
"THEN 'Open' ELSE 'Closed' END, CASE WHEN dateClosed IS NULL THEN '#807160' ELSE '#5ccc33' END, "&_
"WEB_JLC.dbo.Customer_FB_ChangeHistory_prod.deleted "&_
"HAVING      (WEB_JLC.dbo.Customer_FB_ChangeHistory_prod.deleted IS NULL) AND (WEB_JLC.dbo.Customer_FB_Target_prod.targetName = N'"&jlu&"')"
Set SQLSiteData = SQL_WebTools_PRODDB.Execute(SQLSiteQuery)
If SQLSiteData.BOF Then
strsiteData = "[{name: '"&jlu&"', data:[]|"
Else
Do while not SQLSiteData.EOF	

sitecount = SQLSiteData("Count_FB")
sitename = SQLSiteData("targetName")
fbStatus = SQLSiteData("Status_FB")
fbColour = SQLSiteData("Status_FB_colour")
strsitewedge = strsitewedge&"{name: '"&fbStatus&"', y:"&sitecount&", color: '"&fbColour&"'},"

SQLSiteData.MoveNext

strsiteData = left(strsitewedge,len(strsitewedge)-1)
strsiteData = "[{name: '"&sitename&"', data: ["&strsiteData&"]|"

Loop
End If
strall = strall&strsiteData
strsitewedge = ""

Next

strsplit = Split(strall,"|")
SCBHQ = strsplit(0)
JLUN = strsplit(1)
JLUS = strsplit(2)
JLUE = strsplit(3)
JLUW = strsplit(4)
JLUNQ = strsplit(5)
JLUSQ = strsplit(6)
JLUV = strsplit(7)
%>

<script type="text/javascript">
var fbNat = '<%=fbNat%>';
var fbType = '<%=fbType%>';
var fbYr = '<%=fbYr%>';

if (fbNat == '2' && fbType == '') {
fbNatTypeshow = 'Materiel Maintenance All Types'
} else if (fbNat == '3' && fbType == '') {
fbNatTypeshow = 'Warehouse Distribution All Types'
} else if (fbNat == '4' && fbType == '') {
fbNatTypeshow = 'Loan Pool All Types'
} else if (fbNat == '5' && fbType == '') {
fbNatTypeshow = 'Clothing All Types'
} else if (fbNat == '6' && fbType == '') {
fbNatTypeshow = 'National Vehicle Recovery All Types'
} else if (fbNat == '7' && fbType == '') {
fbNatTypeshow = 'Global Distribution All Types'
} else if (fbNat == '8' && fbType == '') {
fbNatTypeshow = 'Operational Contracts All Types'
} else if (fbNat == '2' && fbType != '') {
fbNatTypeshow = 'Materiel Maintenance '+fbType
} else if (fbNat == '3' && fbType != '') {
fbNatTypeshow = 'Warehouse Distribution '+fbType
} else if (fbNat == '4' && fbType != '') {
fbNatTypeshow = 'Loan Pool '+fbType
} else if (fbNat == '5' && fbType != '') {
fbNatTypeshow = 'Clothing '+fbType
} else if (fbNat == '6' && fbType != '') {
fbNatTypeshow = 'National Vehicle Recovery '+fbType
} else if (fbNat == '7' && fbType != '') {
fbNatTypeshow = 'Global Distribution '+fbType
} else if (fbNat == '8' && fbType != '') {
fbNatTypeshow = 'Operational Contracts  '+fbType
} else if (fbNat == '' && fbType != '') {
fbNatTypeshow = 'All Areas '+fbType
} else {
fbNatTypeshow = ' All Areas All Types'
}

function mailFn() {  

var m_address = 'mailto:jlc-scb-customermanagement@drn.mil.au?subject=WebTools%20Customer%20Feedback%20Tool%20-%20Feedback%20Pie%20Charts%20and%20Details';
location.href= m_address;
};

function openPieReport(gSite, lWedge) {
	  parent.SetupOverlay(parent.document.body.clientWidth-70,parent.document.body.clientHeight-70);
      parent.window.frames["ifrmFrontOverlay"].location ="<%= Application("CONTENTLOAD") %>?PageURL=<%= Application("USERDEVELOPED") %>JLC/DSCNO/Customer_FB_Pies_Detail.asp|vpieLoc=" + gSite + "|vlocWedge=" + lWedge + "|vfbNat=" + fbNat + "|vfbType=" + fbType + "|vfbYear=" + fbYr 
      parent.openOverlay();
};

$(document).ready(function() {
    $("#applyselections").click(function(){
	$( "#pieselect" ).submit();
});
});
	
$(function() {
$('#Pie').highcharts({
	chart: {
        type: 'pie',
		margin: [35, 0, 35, 0]
    },
    title: {
        text: 'All SCB Entities Combined'
    },
	subtitle: {
			text: fbNatTypeshow,
			align: 'right'
	},
    plotOptions: {
        series: {
            dataLabels: {
                enabled: true,
                format: '{point.y}',
				distance: -30,
				color: '#000000',
				style: {fontSize: '20px'}
            },
               point: {
                    events: {
                        click: function (e) {
                            var lWedge = this.name;
							var gSite = "National";
                            if (e.ctrlKey) {
							var lWedge = "All";
							openPieReport(gSite, lWedge);
							} else {
							openPieReport(gSite, lWedge);
							}
                        }
                    }
                }
			},
		pie: {
			showInLegend: true
			}
        
    },
	legend: {
		reversed: true
	},
    tooltip: {
        //headerFormat: '<span style="font-size:15px">Number of Site TradeTypes in the </span><br>',
        pointFormat: '<span style="color:{#FF0000}">Feeback</span>: <br><b>{point.y}</b>'
    },
	credits: {
			  position: {
			  align: 'left',
				  x: 20,
				  y: -7
				  },
			  text: 'Data as at: <%=Now()%>'
    },
    series: <%=strsiteDataNat%>,dataLabels: {color: '#000000', style: {textShadow: false}}
    }]
});
$('#PieSCBHQ').highcharts({
	chart: {
        type: 'pie',
		margin: [0, 0, 0, 0]
    },
    title: {
        text: 'SCBHQ'
    },
	subtitle: {
			text: fbNatTypeshow,
			align: 'right'
	},	
    plotOptions: {
        series: {
            dataLabels: {
                enabled: true,
                format: '{point.y}',
				distance: -30,
				style: {fontSize: '15px'}
				},
               point: {
                    events: {
                        click: function (e) {
                            var lWedge = this.name;
							var gSite = "SCBHQ";
                            if (e.ctrlKey) {
							var lWedge = "All";
							openPieReport(gSite, lWedge);
							} else {
							openPieReport(gSite, lWedge);
							}
                        }
                    }
                }
			},
		pie: {
			showInLegend: true
			}
        
    },
	legend: {
		reversed: true
	},
    tooltip: {
        //headerFormat: '<span style="font-size:15px">Number of Site TradeTypes in the </span><br>',
        pointFormat: '<span style="color:{#FF0000}">Feeback</span>: <br><b>{point.y}</b>'
    },
	credits: false,
    series: <%=SCBHQ%>,dataLabels: {color: '#000000', style: {textShadow: false}}
    }]
});
$('#PieJLUE').highcharts({
	chart: {
        type: 'pie',
		margin: [0, 0, 0, 0]
    },
    title: {
        text: 'JLUE'
    },
	subtitle: {
			text: fbNatTypeshow,
			align: 'right'
	},	
    plotOptions: {
        series: {
            dataLabels: {
                enabled: true,
                format: '{point.y}',
				distance: -30,
				style: {fontSize: '15px'}
				},
               point: {
                    events: {
                        click: function (e) {
                            var lWedge = this.name;
							var gSite = "JLUE";
                            if (e.ctrlKey) {
							var lWedge = "All";
							openPieReport(gSite, lWedge);
							} else {
							openPieReport(gSite, lWedge);
							}
                        }
                    }
                }
			},
		pie: {
			showInLegend: true
			}
        
    },
	legend: {
		reversed: true
	},
    tooltip: {
        //headerFormat: '<span style="font-size:15px">Number of Site TradeTypes in the </span><br>',
        pointFormat: '<span style="color:{#FF0000}">Feeback</span>: <br><b>{point.y}</b>'
    },
	credits: false,
    series: <%=JLUE%>,dataLabels: {color: '#000000', style: {textShadow: false}}
    }]
});
$('#PieJLUV').highcharts({
	chart: {
        type: 'pie',
		margin: [0, 0, 0, 0]
    },
    title: {
        text: 'JLUV'
    },
	subtitle: {
			text: fbNatTypeshow,
			align: 'right'
	},
    plotOptions: {
        series: {
            dataLabels: {
                enabled: true,
                format: '{point.y}',
				distance: -30,
				style: {fontSize: '15px'}
            },
               point: {
                    events: {
                        click: function (e) {
                            var lWedge = this.name;
							var gSite = "JLUV";
                            if (e.ctrlKey) {
							var lWedge = "All";
							openPieReport(gSite, lWedge);
							} else {
							openPieReport(gSite, lWedge);
							}
                        }
                    }
                }
			
			},
		pie: {
			showInLegend: true
			}
        
    },
	legend: {
		reversed: true
	},
    tooltip: {
        //headerFormat: '<span style="font-size:15px">Number of Site TradeTypes in the </span><br>',
        pointFormat: '<span style="color:{#FF0000}">Feeback</span>: <br><b>{point.y}</b>'
    },
	credits: false,
    series: <%=JLUV%>,dataLabels: {color: '#000000', style: {textShadow: false}}
    }]
});
$('#PieJLUS').highcharts({
	chart: {
        type: 'pie',
		margin: [0, 0, 0, 0]
    },
    title: {
        text: 'JLUS'
    },
	subtitle: {
			text: fbNatTypeshow,
			align: 'right'
	},
    plotOptions: {
        series: {
            dataLabels: {
                enabled: true,
                format: '{point.y}',
				distance: -30,
				style: {fontSize: '15px'}
            },
               point: {
                    events: {
                        click: function (e) {
                            var lWedge = this.name;
							var gSite = "JLUS";
                            if (e.ctrlKey) {
							var lWedge = "All";
							openPieReport(gSite, lWedge);
							} else {
							openPieReport(gSite, lWedge);
							}
                        }
                    }
                }
			},
		pie: {
			showInLegend: true
			}
        
    },
	legend: {
		reversed: true
	},
    tooltip: {
        //headerFormat: '<span style="font-size:15px">Number of Site TradeTypes in the </span><br>',
        pointFormat: '<span style="color:{#FF0000}">Feeback</span>: <br><b>{point.y}</b>'
    },
	credits: false,
    series: <%=JLUS%>,dataLabels: {color: '#000000', style: {textShadow: false}}
    }]
});
$('#PieJLUW').highcharts({
	chart: {
        type: 'pie',
		margin: [0, 0, 0, 0]
    },
    title: {
        text: 'JLUW'
    },
	subtitle: {
			text: fbNatTypeshow,
			align: 'right'
	},
    plotOptions: {
        series: {
            dataLabels: {
                enabled: true,
                format: '{point.y}',
				distance: -30,
				style: {fontSize: '15px'}
            },
               point: {
                    events: {
                        click: function (e) {
                            var lWedge = this.name;
							var gSite = "JLUW";
                            if (e.ctrlKey) {
							var lWedge = "All";
							openPieReport(gSite, lWedge);
							} else {
							openPieReport(gSite, lWedge);
							}
                        }
                    }
                }
			},
		pie: {
			showInLegend: true
			}
        
    },
	legend: {
		reversed: true
	},
    tooltip: {
        //headerFormat: '<span style="font-size:15px">Number of Site TradeTypes in the </span><br>',
        pointFormat: '<span style="color:{#FF0000}">Feeback</span>: <br><b>{point.y}</b>'
    },
	credits: false,
    series: <%=JLUW%>,dataLabels: {color: '#000000', style: {textShadow: false}}
    }]
});
$('#PieJLUN').highcharts({
	chart: {
        type: 'pie',
		margin: [0, 0, 0, 0]
    },
    title: {
        text: 'JLUN'
    },
	subtitle: {
			text: fbNatTypeshow,
			align: 'right'
	},
    plotOptions: {
        series: {
            dataLabels: {
                enabled: true,
                format: '{point.y}',
				distance: -30,
				style: {fontSize: '15px'}
            },
               point: {
                    events: {
                        click: function (e) {
                            var lWedge = this.name;
							var gSite = "JLUN";
                            if (e.ctrlKey) {
							var lWedge = "All";
							openPieReport(gSite, lWedge);
							} else {
							openPieReport(gSite, lWedge);
							}
                        }
                    }
                }
			},
		pie: {
			showInLegend: true
			}
        
    },
	legend: {
		reversed: true
	},
    tooltip: {
        //headerFormat: '<span style="font-size:15px">Number of Site TradeTypes in the </span><br>',
        pointFormat: '<span style="color:{#FF0000}">Feeback</span>: <br><b>{point.y}</b>'
    },
	credits: false,
    series: <%=JLUN%>,dataLabels: {color: '#000000', style: {textShadow: false}}
    }]
});
$('#PieJLUNQ').highcharts({
	chart: {
        type: 'pie',
		margin: [0, 0, 0, 0]
    },
    title: {
        text: 'JLUNQ'
    },
	subtitle: {
			text: fbNatTypeshow,
			align: 'right'
	},
    plotOptions: {
        series: {
            dataLabels: {
                enabled: true,
                format: '{point.y}',
				distance: -30,
				style: {fontSize: '15px'}
            },
               point: {
                    events: {
                        click: function (e) {
                            var lWedge = this.name;
							var gSite = "JLUNQ";
                            if (e.ctrlKey) {
							var lWedge = "All";
							openPieReport(gSite, lWedge);
							} else {
							openPieReport(gSite, lWedge);
							}
                        }
                    }
                }
			},
		pie: {
			showInLegend: true
			}
        
    },
	legend: {
		reversed: true
	},
    tooltip: {
        //headerFormat: '<span style="font-size:15px">Number of Site TradeTypes in the </span><br>',
        pointFormat: '<span style="color:{#FF0000}">Feeback</span>: <br><b>{point.y}</b>'
    },
	credits: false,
    series: <%=JLUNQ%>,dataLabels: {color: '#000000', style: {textShadow: false}}
    }]
});
$('#PieJLUSQ').highcharts({
	chart: {
        type: 'pie',
		margin: [0, 0, 0, 0]
    },
    title: {
        text: 'JLUSQ'
    },
	subtitle: {
			text: fbNatTypeshow,
			align: 'right'
	},
    plotOptions: {
        series: {
            dataLabels: {
                enabled: true,
                format: '{point.y}',
				distance: -30,
				style: {fontSize: '15px'}
            },
               point: {
                    events: {
                        click: function (e) {
                            var lWedge = this.name;
							var gSite = "JLUSQ";
							if (e.ctrlKey) {
							var lWedge = "All";
							openPieReport(gSite, lWedge);
							} else {
							openPieReport(gSite, lWedge);
							}
						}
                    }
                }
			},
		pie: {
			showInLegend: true
			}
        
    },
	legend: {
		reversed: true
	},
    tooltip: {
        //headerFormat: '<span style="font-size:15px">Number of Site TradeTypes in the </span><br>',
        pointFormat: '<span style="color:{#FF0000}">Feeback</span>: <br><b>{point.y}</b>'
    },
	credits: false,
    series: <%=JLUSQ%>,dataLabels: {color: '#000000', style: {textShadow: false}}
    }]
});
});
</script>
<body>
<div class="container-fluid">
<div class="row">
<form name="input" id="pieselect" method="get" action="Customer_FB_Pies.asp">
<div class="col-md-4" style="height: 175px;"><b>Filter Charts by Area:</b>
    <select id="filter" class="form-control" name="vNat">
        <option value="<%=fbNat%>">Currently:<%=fbNatshow%></option>
		<option value="">All</option>
            <%SQLQuery = "SELECT NatureParentID, NatureParent FROM WEB_JLC.dbo.Customer_FB_Nature_prod GROUP BY NatureParentID, NatureParent"
            Set SQLDataRefresh = SQL_WebTools_PRODDB.Execute(SQLQuery)
            Do While not SQLDataRefresh.EOF%>
				<option value="<%= SQLDataRefresh("NatureParentID")&SQLDataRefresh("NatureParent") %>" <%If fbNat = SQLDataRefresh("NatureParentID") Then %>Selected<%End If %>><%= SQLDataRefresh("NatureParent") %></option>
                <%SQLDataRefresh.MoveNext 
            Loop 
            %>
</select><br>
<b>Filter Charts by Type:</b>
    <select id="filter2" class="form-control" name="vType"> 
        <option value="<%=fbType%>">Currently:<%=fbTypeshow%></option>
		<option value="">All</option>
		<option value="Complaint">Complaint</option>
		<option value="Comment">Comment</option>
		<option value="Compliment">Compliment</option>		
</select><br>
<b>Filter Charts by Raised Year:</b>
    <select id="filter" class="form-control" name="vYr">
        <option value="<%=fbYr%>">Currently:<%=fbYearshow%></option>
		<option value="">All</option>
            <%SQLQuery1 = "SELECT     TOP (100) PERCENT YEAR(feedbackDateTime) AS fbYear FROM WEB_JLC.dbo.CUSTOMDATA_Feedback_prod GROUP BY YEAR(feedbackDateTime) ORDER BY fbYear DESC"
            Set SQLDataRefresh1 = SQL_WebTools_PRODDB.Execute(SQLQuery1)
            Do While not SQLDataRefresh1.EOF%>
				<option value="<%= SQLDataRefresh1("fbYear")%>" <%If fbYr = SQLDataRefresh1("fbYear") Then %>Selected<%End If %>><%= SQLDataRefresh1("fbYear") %></option>
                <%SQLDataRefresh1.MoveNext 
            Loop 
            %>
</select><br>

<button class="btn btn-success" id="applyselections">Apply</button>CTRL Click to open detail for ALL Open and Closed feedback
<a href="<%= Application("CONTENTLOAD") %>?PageURL=<%= Application("USERDEVELOPED") %>JLC/DSCNO/Customer_FB_Main.asp" class="btn btn-info">Back to main</a>
<div class="col-sm-6">
<a href="<%= Application("CONTENTLOAD") %>?PageURL=<%= Application("USERDEVELOPED") %>JLC/DSCNO/Customer_FB_PiesPlain.asp" class="btn btn-warning btn-block" role="button">View by Feedback Area Nature</a>
</div><div class="col-sm-6">
<a href="<%= Application("CONTENTLOAD") %>?PageURL=<%= Application("USERDEVELOPED") %>JLC/DSCNO/Customer_FB_Pies_compare.asp" class="btn btn-warning btn-block" role="button">View by Feedback Type</a>
</div>
<div class="col-md-12" style="height: 450px;" id="Pie"></div></div>
<div class="col-md-2" style="height: 350px;" id="PieSCBHQ"></div>
<div class="col-md-2"  style="height: 350px;" id="PieJLUV"></div>
<div class="col-md-2"  style="height: 350px;" id="PieJLUE"></div>
<div class="col-md-2"  style="height: 350px;" id="PieJLUSQ"></div>

<div class="col-md-4"  style="height: 350px;"></div>

<div class="col-md-2"  style="height: 350px;" id="PieJLUNQ"></div>
<div class="col-md-2"  style="height: 350px;" id="PieJLUN"></div>
<div class="col-md-2"  style="height: 350px;" id="PieJLUS"></div>
<div class="col-md-2"  style="height: 350px;" id="PieJLUW"></div>

</div>
</div>
</form>
</body>
</html>
<!-- #include virtual = "/includes/WebTools_AccessLog.asp" -->
const branch = require('git-branch');

module.exports = {
  /** Fixes obscure eslint bug. */
  // lintOnSave: false,
  lintOnSave: true,
  devServer: {
    overlay: {
      warnings: true,
      errors: true,
    },
  },
  publicPath: './',
  transpileDependencies: [
    'vuetify',
  ],
  chainWebpack: (config) => {
    config.plugin('define').tap((definitions) => {
      /* eslint-disable no-param-reassign, global-require */
      definitions[0]['process.env'].PACKAGE_VERSION = JSON.stringify(require('./package.json').version);
      definitions[0]['process.env'].BRANCH_NAME = JSON.stringify(branch.sync());
      /* eslint-enable no-param-reassign, global-require */
      return definitions;
    });
  },
};

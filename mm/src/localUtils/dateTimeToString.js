import dateToString from './dateToString';

export default (d, opts = { }) => {
  const {
    timezone,
    timeOnly,
    showAMPM,
    delim,
    militaryTime,
  } = opts;

  if (!d) return '';
  const date = new Date(d);
  const h = date.getHours();
  const m = date.getMinutes();
  const minutes = m < 10 ? `0${m}` : m;
  const hours = h < 10 ? `0${h}` : h;
  const ampm = h > 11 ? 'pm' : 'am';

  // let output = '';
  let timeOut = '';
  let dateOut = '';

  if (militaryTime) {
    timeOut += `${hours}${minutes}`;
  } else if (showAMPM) {
    timeOut += `${hours}:${minutes}${showAMPM ? ampm : ''}`;
  } else {
    timeOut += `${hours}:${minutes}`;
  }

  if (!timeOnly) {
    dateOut += `${dateToString(d, delim)}, `;
  }

  if (timezone) {
    timeOut += `${timezone ? ` (${timezone})` : ''}`;
  }

  return `${dateOut}${timeOut}`;
  // eslint-disable-next-line
  // return timeOnly ? `${hours}:${minutes}${showAMPM ? ampm : ''}${timezone ? ` (${timezone})` : ''}` : `${formatDate(d, delim)}, ${hours}:${minutes}${showAMPM ? ampm : ''}${timezone ? ` (${timezone})` : ''}`;
};

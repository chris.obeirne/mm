import { isDate, isNaN } from 'lodash';

const monthNames = [
  'Jan', 'Feb', 'Mar',
  'Apr', 'May', 'Jun', 'Jul',
  'Aug', 'Sep', 'Oct',
  'Nov', 'Dec',
];

// function addZero(i) {
//   if (i < 10) {
//     // eslint-disable-next-line
//     i = `0${i}`;
//   }
//   return i;
// }

// function adjHr(i) {
//   let adj = i;
//   if (adj > 12) {
//     // eslint-disable-next-line
//     adj = adj - 12;
//   }
//   if (adj < 10) {
//     // eslint-disable-next-line
//     adj = `0${adj}`;
//   }
//   return adj;
// }

// function amPm(i) {
//   const ap = (i > 12) ? 'pm' : 'am';
//   return ap;
// }
// const test = new Date('2020-09-15T00:23:22.000Z');
const dateShortFromLong = (val) => {
  // let dte = (val === '' || val === null) ?
  // new Date().toISOString().replace('T', ' ').slice(0, 19) : val;
  let r = null;
  if (val !== '' && val !== null) {
    const dte = new Date(val);
    const isValidDate = isDate(dte) && !isNaN(dte.valueOf());
    if (isValidDate) {
      // const d = new Date();
    // let dte = new Date(val);
      // const z = dte.getTimezoneOffset() * 60 * 1000;
      // dte -= z;
      // dte = new Date(dte);
      const day = dte.getDate();
      const monthIndex = dte.getMonth();
      const year = dte.getFullYear();

      r = `${day} ${monthNames[monthIndex]} ${year}`;
    } else {
      r = '';
    }
  }
  return r;
};

export default dateShortFromLong;

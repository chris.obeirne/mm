import { isDate, isNaN } from 'lodash';

const monthNames = [
  'Jan', 'Feb', 'Mar',
  'Apr', 'May', 'Jun', 'Jul',
  'Aug', 'Sep', 'Oct',
  'Nov', 'Dec',
];

const dateOnly = (val) => {
  // let dte = (val === '' || val === null) ?
  // new Date().toISOString().replace('T', ' ').slice(0, 19) : val;
  let r = null;
  if (val !== '' && val !== null) {
    let dte = new Date(val);
    const isValidDate = isDate(dte) && !isNaN(dte.valueOf());
    if (isValidDate) {
      // const d = new Date();
    // let dte = new Date(val);
      const z = dte.getTimezoneOffset() * 60 * 1000;
      dte -= z;
      dte = new Date(dte);
      const day = dte.getDate();
      const monthIndex = dte.getMonth();
      const year = dte.getFullYear();

      r = `${day} ${monthNames[monthIndex]} ${year}`;
    } else {
      r = '';
    }
  }
  return r;
};

export default dateOnly;

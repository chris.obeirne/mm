import {
  isDate,
  isNaN,
} from 'lodash';

const dateISOShort = (val) => {
  // let dte = (val === '' || val === null) ?
  // new Date().toISOString().replace('T', ' ').slice(0, 19) : val;
  let dte = null;
  if (val !== '' && val !== null) {
    dte = new Date(val);
    const isValidDate = isDate(dte) && !isNaN(dte.valueOf());
    // let r;
    if (isValidDate) {
    // const d = new Date();
    // let dte = new Date(val);
      const z = dte.getTimezoneOffset() * 60 * 1000;
      dte -= z;
      dte = new Date(dte).toISOString().slice(0, 10);
      // const day = dte.getDate();
      // const monthIndex = dte.getMonth();
      // const year = dte.getFullYear();

    // r = `${day} ${monthNames[monthIndex]} ${year}`;
    } else {
      dte = null;
    }
  }
  return dte;
};

export default dateISOShort;

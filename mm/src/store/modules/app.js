/* eslint-disable */
import {
  isString, isPlainObject, isArray,
} from 'lodash';
import { get, patch } from 'kyd-utils';

export default {
  state: {
    favouritesId: 'D3DD8CF4-9B6D-4C1B-A081-BBC79186BA9F',
    spotlightId: '00494032-9BD4-4099-820D-7CBAE533A928',
    globalSearchFields: ['name', 'description', 'keywords'],
    user: {
      id: '',
      pmKeys: 'Not Available',
      name: 'Not Available',
      initials: 'NA',
      username: 'Not Available',
      email: 'Not Avaliable',
      defaultTilesIsCollapsed: false,
    },

    themePrimaryColors: [
      // The text value is the value given to the server to store in the database
      // We'll use it to resolve to a unique colour depending on whether the app
      // is in light mode or dark mode
      // The text value is also used in the settings page to populate the dropdown
      // TODO: Colours for dark vs light theme will be slightly different
      { dark: '#3F51B5', light: '#3F51B5', text: 'indigo' },
      { dark: '#F44336', light: '#F44336', text: 'red' },
      { dark: '#E91E63', light: '#E91E63', text: 'pink' },
      { dark: '#9C27B0', light: '#9C27B0', text: 'purple' },
      { dark: '#2196F3', light: '#2196F3', text: 'blue' },
      { dark: '#00BCD4', light: '#00BCD4', text: 'cyan' },
      { dark: '#009688', light: '#009688', text: 'teal' },
      { dark: '#4CAF50', light: '#4CAF50', text: 'green' },
      { dark: '#CDDC39', light: '#CDDC39', text: 'lime' },
      { dark: '#FFEB3B', light: '#FFEB3B', text: 'yellow' },
      { dark: '#FFC107', light: '#FFC107', text: 'amber' },
      { dark: '#FF9800', light: '#FF9800', text: 'orange' },
      { dark: '#795548', light: '#795548', text: 'brown' },
      { dark: '#9E9E9E', light: '#9E9E9E', text: 'grey' },
      { dark: '#999999', light: '#000000', text: 'inverted' },
    ],
    refreshSecondsSelection: [10, 20, 30, 40, 50, 60],
    vWH: {},
    drawer: false,
    // emailNotifications: false,
    error: {},
    phasings: [],
    phasingsMeta: {
      totalItems: 0,
      itemsPerPage: 10,
      sort: [{ asc: true, key: 'dsa' }],
      filter: [{ key: 'jluId', equals: null }, { key: 'dsa', equals: null }, { key: 'fy', equals: null }],
      page: 1,
    },
    log: [],
    logMeta: {
      totalItems: 0,
      itemsPerPage: 10,
      sort: [{ asc: false, key: 'entryDate' }],
      filter: [{ key: 'jluId', equals: null }, { key: 'dsa', equals: null }, { key: 'fy', equals: null }],
      page: 1,
    },
    jlus: [],
    jlusMeta: {
      totalItems: 0,
      itemsPerPage: 100,
      sort: [{ asc: true, key: 'romanCode' }],
      filter: [],
      page: 1,
    },
  },

  getters: {
    authenticated: (state) => !!state.user.id,
    user: (state) => state.user, // const [first, last] = state.user.username
    //   .toUpperCase()
    //   .split(/\s|\./);
    // return isString(first) ? {
    //   ...state.user,
    //   initials: `${first.charAt()}${isString(last)
    //     ? last.charAt()
    //     : ''}`,
    // } : state.user;
    getPhasings: (state) => state.phasings,
    getPhasingsMeta: (state) => state.phasingsMeta,
    getLog: (state) => state.log,
    getLogMeta: (state) => state.logMeta,
    getJlus: (state) => state.jlus,
    getJlusMeta: (state) => state.jlusMeta,
    settings: ({ settings }) => settings,
    themePrimaryColors: ({ themePrimaryColors }) => themePrimaryColors,
    getvWH: (state) => state.vWH,
    drawer: (state) => state.drawer,
  },

  mutations: {

    SET_USER(state, data) {
      if (isPlainObject(data)) {
        state.user = data;
      }
    },
    SET_PHASINGS(state, arr) {
      if (Array.isArray(arr)) {
        state.phasings = arr;
      }
    },
    SET_PHASINGS_FILTER_META(state, payload) {
      const i = payload.idx;
      const v = payload.val;
      state.phasingsMeta.filter[i].equals = v;
    },
    // SET_PHASINGS_DSA_FILTER_META(state, payload) {
    //   const i = payload.idx;
    //   const d = payload.dsa;
    //   state.phasingsMeta.filter[i].equals =d;
    // },
    SET_PHASINGS_META(state, payload) {
      // console.log('the payload in meta', payload);
      const tm = state.phasingsMeta;
      // console.log('the opening meta in meta', tm);
      state.phasingsMeta = {
        ...tm,
        ...payload,
      };
      // console.log('the meta in app module', state.phasingsMeta);
    },
    SET_LOG(state, arr) {
      if (Array.isArray(arr)) {
        state.log = arr;
      }
    },
    SET_LOG_META(state, payload) {
      // console.log('the payload in meta', payload);
      const tm = state.logMeta;
      // console.log('the opening meta in meta', tm);
      state.logMeta = {
        ...tm,
        ...payload,
      };
      // console.log('the meta in app module', state.phasingsMeta);
    },
    SET_JLUS(state, arr) {
      if (Array.isArray(arr)) {
        state.jlus = arr;
        // console.log(state.tilesCategories.sort((firstItem, secondItem) => firstItem.categoryRowOrder - secondItem.categoryRowOrder));
      }
    },
    SET_JLUS_META(state, payload) {
      // console.log('the payload in meta', payload);
      const jm = state.jlusMeta;
      // console.log('the opening meta in meta', tm);
      state.jlusMeta = {
        ...jm,
        ...payload,
      };
      // console.log('the meta in app module', state.phasingsMeta);
    },
    SET_VWH(state, payload) {
      state.vWH = payload;
    },
    SET_DRAWER(state, payload) {
      state.drawer = payload;
    },
    SET_ERROR(state, payload) {
      if (isObject(payload)) {
        state.error = payload;
      } else {
        state.error = {};
      }
    },
  },

  actions: {
    setViewPort: ({
      commit,
    }, payload) => {
      // eslint-disable-next-line
      // console.log(payload);
      commit('SET_VWH', payload);
    },
    setDrawer: ({
      commit,
    }, payload) => {
      // eslint-disable-next-line
      // console.log(payload);
      commit('SET_DRAWER', payload);
    },

    getUser: ({ commit }) => get('/user')
      .then(({ data }) => {
        // console.log('the user', data)
        commit('SET_USER', data);
        return data;
      }),
    grabTable: ({ commit }, payload) => {
      // console.log('in grabTable the payload before get', payload)
      get('/phasings', { params: payload })
        .then((data) => {
          // console.log('getTilesCategories', data);
          // console.log('in the return of grabTable', data)
          commit('SET_PHASINGS', data.data);
          commit('SET_PHASINGS_META', data.meta);
        // return data;
        });
    },
    // grabPhasingsSummed: ({ commit }, payload) => {
    //   // console.log('in grabTable the payload before get', payload)
    //   get('/phasingsSummed', { params: payload })
    //     .then((data) => {
    //       // console.log('getTilesCategories', data);
    //       // console.log('in the return of grabTable', data)
    //       commit('SET_PHASINGS_SUMMED', data.data);
    //     // return data;
    //     });
    // },
    grabChangeLog: ({ commit }, payload) => {
      // console.log('in grabTable the payload before get', payload)
      get('/change-log', { params: payload })
        .then((data) => {
          // console.log('getTilesCategories', data);
          // console.log('in the return of grabTable', data)
          commit('SET_LOG', data.data);
          commit('SET_LOG_META', data.meta);
        // return data;
        });
    },
    // updateTable: ({commit}, payload) => {
    //   // console.log('in grabTable the payload before get', payload)
    //   patch('/phasings', { params: payload })
    //     .then((data) => {
    //       // console.log('getTilesCategories', data);
    //       // console.log('in the return of grabTable', data)
    //       commit('SET_PHASINGS', data.data);
    //       commit('SET_PHASINGS_META', data.meta);
    //     // return data;
    //     });
    // },
  },
};
